import datetime
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from helpers.utils import *


class Question(models.Model):
    ct = get_content_type
    question_text = models.CharField(
            max_length=200
    )
    group = models.ForeignKey(
            "Group",
            blank=False,
            null=False,
            default=None,
            related_name="group"
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Question Sort Order."),
            default=0
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    pub_date = models.DateTimeField(
            'date published'
    )

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = _('Published recently?')

    class Meta:
        ordering = ('sort_order',)
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')


class Choice(models.Model):
    ct = get_content_type
    question = models.ForeignKey(
            Question,
            on_delete=models.CASCADE
    )
    choice_text = models.CharField(
            max_length=200
    )
    votes = models.IntegerField(
            default=0
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Choice Sort Order."),
            default=0
    )

    def __str__(self):
        return self.choice_text

    class Meta:
        ordering = ('sort_order',)
        verbose_name = _('Choice')
        verbose_name_plural = _('Choices')


def was_published_recently(self):
    now = timezone.now()
    return now - datetime.timedelta(days=1) <= self.pub_date <= now


class Group(models.Model):
    ct = get_content_type
    title = models.CharField(
            max_length=200
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_('Slug'),
            help_text=_('Automatically generated from title if left empty.')
    )
    questions = models.ManyToManyField(
            Question,
            blank=True,
            related_name="questions"
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Group Sort Order."),
            default=0
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    pub_date = models.DateTimeField(
            'date published'
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="group_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="group_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('sort_order',)
        verbose_name = _('Question Group')
        verbose_name_plural = _('Question Groups')