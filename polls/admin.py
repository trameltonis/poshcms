from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import export_as_json, make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate
import nested_admin
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from modeltranslation.admin import TranslationStackedInline, TranslationTabularInline
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationAdmin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django.contrib.contenttypes.admin import GenericTabularInline
from .models import Question, Group, Choice


class ChoiceInline(SortableInlineAdminMixin, admin.TabularInline):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    model = Choice
    extra = 1


@admin.register(Choice)
class ChoiceAdmin(SortableAdminMixin, admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = [
        (None,               {'fields': ['question', 'choice_text', 'votes', ]}),
    ]

    list_display = ('question', 'choice_text', 'votes', 'sort_order')
    list_filter = ['votes']
    search_fields = ['choice_text']


@admin.register(Question)
class QuestionAdmin(SortableAdminMixin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = [
        (None,               {'fields': ['question_text', 'group', 'pub_date']}),
        # ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]

    inlines = [ChoiceInline]
    list_display = ('question_text', 'group', 'sort_order', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class QuestionInline(SortableInlineAdminMixin, admin.TabularInline):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    model = Question
    extra = 1


@admin.register(Group)
class GroupAdmin(SortableAdminMixin, admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = [
        (None,               {'fields': ['title', 'slug', 'questions', 'pub_date']}),
        # ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    # raw_id_fields = ("questions",)
    filter_horizontal =  ["questions"]
    # inlines = [QuestionInline]
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title',  'pub_date', 'created_on', 'updated')
    list_filter = ['pub_date']
    search_fields = ['title']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))

