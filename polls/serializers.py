from django.contrib.auth.models import User, Group
from rest_framework import serializers
from polls.models import Question, Choice, Group


class PollQuestionSerializer(serializers.ModelSerializer):
    # group = PollGroupSerializer(many=False, read_only=True)

    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class PollQuestionCreateSerializer(serializers.ModelSerializer):
    # group = PollGroupSerializer(many=False, read_only=True)

    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class PollChoiceSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Choice
        fields = '__all__'
        depth = 3


class PollChoiceCreateSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Choice
        fields = '__all__'
        depth = 3


class PollGroupSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Group
        fields = '__all__'
        depth = 3


class PollGroupCreateSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Group
        fields = '__all__'
        depth = 3