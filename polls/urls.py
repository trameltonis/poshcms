from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

# urlpatterns = [
#     # url(r'^locale/$', LocaleListView.as_view(), name='locale_list'),
#     # url(r'^locale/create/$', LocaleCreateView.as_view(), name='locale_create'),
#     # url(r'^locale/edit/(?P<slug>[\w-]+)/$', LocaleUpdateView.as_view(), name='locale_update'),
#     # url(r'^locale/(?P<slug>[\w-]+)/$', LocaleDetailView.as_view(), name='locale_detail'),
#     # url(r'^format/$', FormatListView.as_view(), name='format_list'),
#
#     # ex: /polls/
#     url(r'^$', views.index, name='index'),
#     # ex: /polls/5/
#     url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
#     url(r'^specifics/(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
#     # ex: /polls/5/results/
#     url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
#     # ex: /polls/5/vote/
#     url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
# ]
#
# urlpatterns = format_suffix_patterns(urlpatterns)


app_name = 'polls'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]