from modeltranslation.translator import translator, register, TranslationOptions
from polls.models import Group, Choice, Question


@register(Group)
class GroupTranslationOptions(TranslationOptions):
    fields = [
        'title',
        ]

@register(Question)
class IconTranslationOptions(TranslationOptions):
    fields = [
        'question_text',
        ]


@register(Choice)
class IconTranslationOptions(TranslationOptions):
    fields = [
        'choice_text',
        ]
        

