from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import export_as_json, make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate
import nested_admin
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from modeltranslation.admin import TranslationStackedInline
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from .models import *
from modeltranslation.admin import TranslationAdmin


@admin.register(Collection)
class CollectionAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    filter_horizontal = ('ctas', 'video',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status', 'section', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'active']
    # date_hierarchy = 'created_on'
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Category)
class CategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Cta)
class CtaAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'language_selectors', 'status', 'section', 'active', 'sort_order', 'updated')
    search_fields = ['title', 'slug', 'hero_text', 'sub_text', 'label','link', 'link_label', ]
    list_filter = ['active']
    fields = [
        'section',
        'title',
        'slug',
        'url',
        'label',
        'collection',
        'hero_text',
        'hero_text_size',
        'hero_text_color',
        'hero_icon',
        'sub_text',
        'sub_text_size',
        'sub_text_color',
        'svg',
        'shape',
        'shape_color',
        'width',
        'height',
        'padding',
        'position',
        'float',
        'margin_top',
        'right',
        'left',
        'link',
        'link_label',
        'link_color',
        'is_pws',
        'pws_link',
        'pws_link_label',
        'pws_link_color',
        'xtralink_label',
        'xtralink',
        'xtralink_color',
        'img',
        'cf1',
        'cf2',
        'cf3',
        'cf4',
        'status',
        'sort_order',
        'active',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
    # date_hierarchy = 'created_on'
    # radio_fields = {'float': admin.HORIZONTAL, 'position':admin.HORIZONTAL }
    # list_editable = ['status', 'sort_order']


@admin.register(Icon)
class IconAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'language_selectors', 'status', 'section', 'active', 'updated')
    search_fields = ['title', 'slug']
    list_filter = ['active']
    fields = [
        'section',
        'title',
        'slug',
        'url',
        'icon_class',
        'icon_color',
        'icon_bg_color',
        'size',
        'placement',
        'img',
        'sort_order',
        'active',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class ImageInline(nested_admin.NestedStackedInline, TranslationStackedInline):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'caption',
                    'img',
                    'set_primary',
                    'sort_order',
                )
            }
        ),
    )

    model = ImageInline
    extra = 0


@admin.register(Banner)
class BannerAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    # inlines = [ImageInline, ]
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'language_selectors', 'category', 'section', 'status', 'active', 'progress_status', 'sort_order', 'created_on', 'updated')
    search_fields = ['title', 'caption']
    # list_filter = (
    #     ('created_by', admin.RelatedOnlyFieldListFilter),
    # )
    list_filter = ['status', 'active']
    filter_horizontal = (
        'cta',
        'social',
        'collectn',
        'timelines',
        'dashboards',
        'specials',
        'incentives',
        'quotes',
        'testimonials',
        'snippets',
        'videos',
    )
    fields = [
        'section',
        'title',
        'slug',
        'url',
        'cta',
        'collectn',
        'timelines',
        'quotes',
        'testimonials',
        'snippets',
        'dashboards',
        'specials',
        'incentives',
        'videos',
        'placement',
        'icon',
        'icon_color_override',
        'layout',
        # 'custom_layouts',
        'category',
        'margin_top',
        'margin_bottom',
        'caption',
        'social',
        'status',
        'img',
        'sort_order',
        'publish_start',
        'publish_end',
        'lock_locale',
        'visibility',
        'active',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
    # list_editable = ['sort_order']


class CarouselImageInline(nested_admin.NestedStackedInline, TranslationStackedInline):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'caption',
                    'img',
                    'sort_order',
                )
            }
        ),
    )

    model = CarouselImageInline
    extra = 0


@admin.register(Carousel)
class CarouselAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    inlines = [CarouselImageInline, ]
    filter_horizontal = ('social', )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'id', 'status', 'section', 'visibility', 'active', 'progress_status', 'updated')
    search_fields = ['title', 'caption']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(CustomLayout)
class CustomLayoutAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'visibility', 'active', 'status', 'updated')
    search_fields = ['name', 'slug', 'description', 'code']
    list_filter = ['active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
