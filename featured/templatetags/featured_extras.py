from django import template
import re
import markdown
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse, NoReverseMatch
from collections import Iterable
from django.core.urlresolvers import reverse, NoReverseMatch

from featured.models import Banner
from cmf.models import Page, Resource, Email, Dashboard, Specials, Incentives

register = template.Library()


@register.simple_tag()
def newest_banners():
    '''Gets the most recent banners '''
    return Banner.objects.latest('created_on')


@register.inclusion_tag('cmf/menu/page_nav.html')
def page_menu_list():
    '''Returns dictionary of pages to display on page menu across pages on the application '''
    pages = Page.objects.all().filter(active=True, status='p').order_by('-created_on')
    return {'pages': pages}


@register.filter('time_estimate')
def time_estimate(word_count):
    '''Estimates the number of minutes it will take to complete a step based on the passed-in wordcount.'''
    minutes = round(word_count/20)
    return minutes


@register.filter('markdown_to_html')
def markdown_to_html(markdown_text):
    '''Converts markdown text to html'''
    html_body = markdown.markdown(markdown_text)
    return mark_safe(html_body)


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname
    path = context['request'].path
    if re.search(pattern, path):
        return 'active'
    return ''



