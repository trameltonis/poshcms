from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns = [
    url(r'^banners/$', BannerListView.as_view(), name='banner_list'),
    url(r'^$', HomeBannerListView.as_view(), name='home_banner_list'),
    url(r'^about/$', AboutBannerListView.as_view(), name='about_banner_list'),
    url(r'^shop/$', ShopBannerListView.as_view(), name='shop_banner_list'),
    url(r'^host-a-party/$', HostBannerListView.as_view(), name='host_banner_list'),
    url(r'^join/$', JoinBannerListView.as_view(), name='join_banner_list'),
    url(r'^our-purpose/$', OurPurposeBannerListView.as_view(), name='our_purpose_banner_list'),
    url(r'^global-impact/$', GlobalImpactBannerListView.as_view(), name='global_impact_banner_list'),
    url(r'^charitable-partners/$', CharitablePartnersBannerListView.as_view(), name='charitable_partners_banner_list'),
    url(r'^product-mission/$', ProductMissionBannerListView.as_view(), name='product_mission_banner_list'),
    url(r'^specials-and-sales/$', SpecialsAndSalesBannerListView.as_view(), name='specials_sales_banner_list'),
    url(r'^microfiber/$', MicrofiberBannerListView.as_view(), name='microfiber_banner_list'),
    url(r'^safe-haven-house/$', SafeHavenHouseBannerListView.as_view(), name='safe_haven_house_banner_list'),
    url(r'^chemicals-of-concern-quiz/$', ChemicalsOfConcernQuizBannerListView.as_view(), name='chemicals_concern_quiz_banner_list'),
    url(r'^banners/create/', BannerCreateView.as_view(), name='banner_create'),
    url(r'^banners/edit/(?P<slug>[\w-]+)/$', BannerUpdateView.as_view(), name='banner_update'),
    # url(r'^banners/delete/(?P<pk>\d+)/', BannerDeleteView.as_view(), name='banner_delete'),
    url(r'^banners/(?P<category>[\w-]+)/(?P<slug>[\w-]+)/$', BannerDetailView.as_view(), name='banner_detail'),

    url(r'^ctas/$', CtaListView.as_view(), name='cta_list'),
    url(r'^ctas/create/', CtaCreateView.as_view(), name='cta_create'),
    url(r'^ctas/edit/(?P<slug>[\w-]+)/$', CtaUpdateView.as_view(), name='cta_update'),
    # url(r'^ctas/delete/(?P<pk>\d+)/', CtaDeleteView.as_view(), name='cta_delete'),
    url(r'^ctas/(?P<slug>[\w-]+)/$', CtaDetailView.as_view(), name='cta_detail'),

    # PWS Pages
    # url(r'^customer/$', PWSHomeBannerListView.as_view(), name='pws_home_banner_list'),
    # url(r'^customer/about/$', PWSAboutBannerListView.as_view(), name='pws_about_banner_list'),
    # url(r'^customer/shop/$',PWSShopBannerListView.as_view(), name='pws_shop_banner_list'),
    # url(r'^customer/host-a-party/$', PWSHostBannerListView.as_view(), name='pws_host_banner_list'),
    # url(r'^customer/join/$', PWSJoinBannerListView.as_view(), name='pws_join_banner_list'),
    # url(r'^customer/our-purpose/$', PWSOurPurposeBannerListView.as_view(), name='pws_our_purpose_banner_list'),
    # url(r'^customer/global-impact/$', PWSGlobalImpactBannerListView.as_view(), name='pws_global_impact_banner_list'),
    # url(r'^customer/charitable-partners/$', PWSCharitablePartnersBannerListView.as_view(), name='pws_charitable_partners_banner_list'),
    # url(r'^customer/product-mission/$', PWSProductMissionBannerListView.as_view(), name='pws_product_mission_banner_list'),
    # url(r'^customer/specials-and-sales/$', PWSSpecialsAndSalesBannerListView.as_view(), name='pws_specials_sales_banner_list'),
    # url(r'^customer/microfiber/$', PWSMicrofiberBannerListView.as_view(), name='pws_microfiber_banner_list'),
    # url(r'^customer/safe-haven-house/$', PWSSafeHavenHouseBannerListView.as_view(), name='pws_safe_haven_house_banner_list'),
    # url(r'^customer/chemicals-of-concern-quiz/$', PWSChemicalsOfConcernQuizBannerListView.as_view(), name='pws_chemicals_concern_quiz_banner_list'),
]
urlpatterns = format_suffix_patterns(urlpatterns)