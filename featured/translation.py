from modeltranslation.translator import translator, register, TranslationOptions
from featured.models import *


@register(Cta)
class CtaTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'label',
        'hero_text',
        'sub_text',
        'link_label',
        'link',
        'pws_link_label',
        'pws_link',
        'xtralink_label',
        'xtralink',
        'cf1',
        'cf2',
        'cf3',
        'cf4',
        'img',
        ]

@register(Icon)
class IconTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'img',
        ]


@register(Banner)
class BannerTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'caption',
        'img',
        ]


@register(ImageInline)
class ImageInlineTranslationOptions(TranslationOptions):
    fields = [
        'caption',
        'img',
        ]


@register(Carousel)
class CarouselTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'caption',
        ]


@register(CarouselImageInline)
class CarouselImageInlineTranslationOptions(TranslationOptions):
    fields = [
        'caption',
        'img',
        ]

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]

@register(CustomLayout)
class CustomLayoutTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'description',
        ]


@register(Collection)
class CollectionTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]
