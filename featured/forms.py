from django import forms
from django.utils.translation import ugettext_lazy as _
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, Row, Field, Hidden, ButtonHolder
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import *
from menu_manager.models import *
from menu_manager.forms import *


class BannerCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Banner
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'section',
            'cta',
            'collectn',
            'placement',
            'icon',
            'layout',
            # 'custom_layouts',
            'category',
            'margin_top',
            'margin_bottom',
            'caption',
            'status',
            'img',
            'sort_order',
            'publish_start',
            'publish_end',
            'lock_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class BannerEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Banner
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'section',
            'cta',
            'collectn',
            'placement',
            'icon',
            'layout',
            # 'custom_layouts',
            'category',
            'margin_top',
            'margin_bottom',
            'caption',
            'status',
            'img',
            'sort_order',
            'publish_start',
            'publish_end',
            'lock_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class CtaCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Cta
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'label',
            'section',
            'cta',
            'placement',
            'icon',
            'layout',
            # 'custom_layouts',
            'category',
            'margin_top',
            'margin_bottom',
            'caption',
            'status',
            'img',
            'link_label',
            'link',
            'link_color',
            'pws_link_label',
            'pws_link',
            'pws_link_color',
            'xtralink_label',
            'xtralink',
            'xtralink_color',
            'cf1',
            'cf2',
            'cf3',
            'cf4',
            'svg',
            'sort_order',
            'publish_start',
            'publish_end',
            'lock_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class CtaEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Cta
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'label',
            'section',
            'cta',
            'placement',
            'icon',
            'layout',
            # 'custom_layouts',
            'category',
            'margin_top',
            'margin_bottom',
            'caption',
            'status',
            'img',
            'link_label',
            'link',
            'link_color',
            'is_pws',
            'pws_link_label',
            'pws_link',
            'pws_link_color',
            'xtralink_label',
            'xtralink',
            'xtralink_color',
            'cf1',
            'cf2',
            'cf3',
            'cf4',
            'svg',
            'sort_order',
            'publish_start',
            'publish_end',
            'lock_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )
