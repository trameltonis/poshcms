# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-05 03:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('featured', '0013_auto_20160804_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='collection',
            name='ctas',
            field=models.ManyToManyField(blank=True, default=None, help_text='Add CTA to Collection', related_name='collection_cta', to='featured.Cta', verbose_name='CTA'),
        ),
        migrations.AlterField(
            model_name='collection',
            name='video',
            field=models.ManyToManyField(blank=True, default=None, help_text='Add videos to CTA Collection', related_name='collection_video', to='media_manager.Video', verbose_name='Videos'),
        ),
    ]
