# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-31 23:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('featured', '0008_auto_20160731_1812'),
    ]

    operations = [
        migrations.AddField(
            model_name='cta',
            name='xlabel',
            field=models.CharField(blank=True, default=None, help_text='Extra CTA Label', max_length=255, null=True, verbose_name='Extra Label'),
        ),
    ]
