# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-05 05:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0005_auto_20160624_0202'),
        ('featured', '0014_auto_20160804_2259'),
    ]

    operations = [
        migrations.AddField(
            model_name='collection',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Add image to Collection', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='collection_image', to='filer.Image', verbose_name='Collection Image'),
        ),
        migrations.AlterField(
            model_name='collection',
            name='ctas',
            field=models.ManyToManyField(blank=True, default=None, help_text='Add CTA to Collection', related_name='collection_cta', to='featured.Cta', verbose_name='CTAs'),
        ),
        migrations.AlterField(
            model_name='collection',
            name='video',
            field=models.ManyToManyField(blank=True, default=None, help_text='Add videos to Collection', related_name='collection_video', to='media_manager.Video', verbose_name='Videos'),
        ),
    ]
