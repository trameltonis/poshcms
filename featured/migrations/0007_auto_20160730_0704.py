# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-30 12:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('featured', '0006_auto_20160723_2338'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cta',
            options={'ordering': ['sort_order'], 'verbose_name': 'Call To Action (CTA)', 'verbose_name_plural': 'Call To Action (CTAs)'},
        ),
        migrations.AddField(
            model_name='carousel',
            name='slides',
            field=models.ManyToManyField(blank=True, default=None, related_name='carousel_slides', to='featured.CarouselImageInline', verbose_name='Slides'),
        ),
    ]
