# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-24 00:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('featured', '0003_auto_20160723_1429'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='layout',
            field=models.ForeignKey(default=None, help_text='Add layout for the banner', max_length=255, on_delete=django.db.models.deletion.CASCADE, related_name='ly_layout', to='featured.CustomLayout'),
        ),
    ]
