from rest_framework import generics
from django.views import generic as cbv
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend

from ..models import *
from .serializers import *



# API Filters
class BannerFilter(filters.FilterSet):
    class Meta:
        model = Banner
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class CollectionFilter(filters.FilterSet):
    class Meta:
        model = Collection
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class CarouselFilter(filters.FilterSet):
    class Meta:
        model = Carousel
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class CtaFilter(filters.FilterSet):
    class Meta:
        model = Cta
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class IconFilter(filters.FilterSet):
    class Meta:
        model = Icon
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


# API ViewSets
class BannerViewSet(viewsets.ModelViewSet):
    queryset = Banner.objects.all().order_by('-created_on')
    serializer_class = BannerSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CtaViewSet(viewsets.ModelViewSet):
    queryset = Cta.objects.all().order_by('-created_on')
    serializer_class = CtaSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CustomLayoutViewSet(viewsets.ModelViewSet):
    queryset = CustomLayout.objects.all().order_by('-created_on')
    serializer_class = CustomLayoutSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CollectionViewSet(viewsets.ModelViewSet):
    queryset = Collection.objects.all().order_by('-created_on')
    serializer_class = CollectionSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CarouselViewSet(viewsets.ModelViewSet):
    queryset = Carousel.objects.all().order_by('-created_on')
    serializer_class = CarouselSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class IconViewSet(viewsets.ModelViewSet):
    queryset = Icon.objects.all().order_by('-created_on')
    serializer_class = IconSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']

class ImageInlineViewSet(viewsets.ModelViewSet):
    queryset = ImageInline.objects.all().order_by('-created_on')
    serializer_class = ImageInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CarouselImageInlineViewSet(viewsets.ModelViewSet):
    queryset = CarouselImageInline.objects.all().order_by('-created_on')
    serializer_class = CarouselImageInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']
















