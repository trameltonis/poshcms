from rest_framework import serializers
from theme.models import *
from ..models import *
from theme.api.serializers import *


class CtaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cta
        fields = '__all__'
        depth = 6


class CtaCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cta
        fields = '__all__'
        depth = 6


class CustomLayoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomLayout
        fields = '__all__'
        depth = 3


class CustomLayoutCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomLayout
        fields = '__all__'
        depth = 3


class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = '__all__'
        depth = 3


class CollectionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = '__all__'
        depth = 3


class IconSerializer(serializers.ModelSerializer):
    class Meta:
        model = Icon
        fields = '__all__'
        depth = 3


class IconCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Icon
        fields = '__all__'
        depth = 3


class BannerSerializer(serializers.ModelSerializer):
    # cta = CtaSerializer(many=True, read_only=True)
    # icon = IconSerializer(many=False, read_only=True)
    # layout = CustomLayoutSerializer(many=False, read_only=True)
    collection = CollectionSerializer(many=True, read_only=True)

    class Meta:
        model = Banner
        verbose_name = "Landing Page"
        fields = '__all__'
        depth = 3


class BannerCreateSerializer(serializers.ModelSerializer):
    # cta = CtaSerializer(many=True, read_only=True)
    # icon = IconSerializer(many=False, read_only=True)
    # layout = CustomLayoutSerializer(many=False, read_only=True)
    collection = CollectionSerializer(many=True, read_only=True)

    class Meta:
        model = Banner
        verbose_name = "Landing Page"
        fields = '__all__'
        depth = 3


class ImageInlineSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = ImageInline
        fields = '__all__'
        depth = 3


class ImageInlineCreateSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = ImageInline
        fields = '__all__'
        depth = 3


class CarouselSerializer(serializers.ModelSerializer):
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Carousel
        fields = '__all__'
        depth = 3


class CarouselCreateSerializer(serializers.ModelSerializer):
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Carousel
        fields = '__all__'
        depth = 3


class CarouselImageInlineSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = CarouselImageInline
        fields = '__all__'
        depth = 3


class CarouselImageInlineCreateSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = CarouselImageInline
        fields = '__all__'
        depth = 3

