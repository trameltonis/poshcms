from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class FeaturedConfig(AppConfig):
    name = 'featured'
    verbose_name = _('Featured Contents & Landing Pages')
