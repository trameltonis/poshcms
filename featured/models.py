from django.conf import settings
from cmf.models import *
from filer.models.abstract import BaseImage
from filer.fields.image import FilerImageField
from filebrowser.fields import FileBrowseField
from filer.fields.file import FilerFileField
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group, User
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
import base64
import uuid
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from core.models import *
from theme.models import Layout
from filer.models import File, Image
from colorfield.fields import ColorField
from media_manager.models import *
from helpers.utils import *



class CustomLayout(models.Model):
    name = models.CharField(
            max_length=255,
            default=None,
            blank=False,
            null=False,
            verbose_name=_("Layout Name"),
            help_text=_("Layout name eg. Video Gallery Layout, Document Listing Layout, etc")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_("Slug")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
            blank=True,
            null=True,
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Layout Status.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="blyt_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="blyt_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Custom Layout')
        verbose_name_plural = _('Custom Layouts')

    @property
    def template(self):
        return 'featured/banner-layouts/{}.html'.format(self.slug)


class Category(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the category."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="featured_category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="featured_category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("featured_category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Collection(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    ctas = models.ManyToManyField(
        'Cta',
        blank=True,
        # null=True,
        default=None,
        verbose_name=_("CTAs"),
        help_text=_("Add CTA to Collection"),
        related_name="%(class)s_cta"
    )
    video = models.ManyToManyField(
        Video,
        blank=True,
        # null=True,
        default=None,
        verbose_name=_("Videos"),
        help_text=_("Add videos to Collection"),
        related_name="%(class)s_video"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("Collection Image"),
        help_text=_("Add image to Collection"),
        related_name="%(class)s_image"
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from collection title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the collection."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Collection Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="%(class)s_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="%(class)s_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("collection_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Collection')
        verbose_name_plural = _('Collections')


class Cta(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name=_('Name'),
        help_text=_("CTA Name"),
        blank=False,
        null=False
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    collection = models.ForeignKey(
        Collection,
        blank=True,
        null=True,
        default=None,
        related_name="cta_collection"
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Slug"),
        help_text=_("Keyword or slug will be auto-generated from name field if left empty")
    )
    label = models.CharField(
        max_length=255,
        verbose_name=_('Label'),
        help_text=_("CTA Label"),
        blank=True,
        null=True,
        default=None
    )
    hero_text = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Hero Text"),
        help_text=_("Text that usually shows below main CTA title.")
    )
    hero_text_size = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Hero Text Size"),
        help_text=_("Font Size of Hero Text. Default is 30px."),
        default='30px'
    )
    hero_text_color = ColorField(
        default='#02AAB5',
        verbose_name=_("Hero Text Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    hero_icon = models.ForeignKey(
        "Icon",
        blank=True,
        null=True,
        default=None,
        verbose_name=_("Hero Icon"),
        help_text=_("Icon that separates each CTA.")
    )
    sub_text = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Sub Text"),
        help_text=_("Smaller Sub Text that usually shows below Hero Text.")
    )
    sub_text_size = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Sub Text Size"),
        help_text=_("Font Size of Sub Text. Default is 18px."),
        default='16px'
    )
    sub_text_color = ColorField(
        default='#02AAB5',
        verbose_name=_("Sub Text Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    svg = models.CharField(
        max_length=255,
        verbose_name=_('SVG Code'),
        help_text=_("SVG Code for template styling"),
        blank=True,
        null=True,
        default=None
    )
    shape = models.CharField(
        max_length=100,
        default="circle",
        choices=shape,
        blank=True,
        null=True,
    )
    shape_color = ColorField(
        default='#02AAB5',
        verbose_name=_("Shape Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    width = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Shape Width"),
        help_text=_("Use to set width of cta shape"),
        default='250px'
    )
    height = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Shape Height"),
        help_text=_("Use to set height of cta shape"),
        default='250px'
    )
    padding = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Shape Padding"),
        help_text=_("Use to set padding of cta shape"),
        default='20px'
    )
    position = models.CharField(
        max_length=100,
        default="absolute",
        null=True,
        blank=True,
        choices=POSITION,
    )
    float = models.CharField(
        max_length=100,
        default=None,
        null=True,
        blank=True,
        choices=FLOAT,
    )
    margin_top = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Margin Top"),
        help_text=_("Use to set top margin of cta shape. Example, '10%' or '10px'"),
        default='10%'
    )
    right = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Right Margin"),
        help_text=_("Use to set right margin of cta shape. Example, '10%' or '10px'"),
        default='10%'
    )
    left = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Left Margin"),
        help_text=_("Use to set left margin of cta shape. Example, '10%' or '10px'"),
        default='10%'
    )

    link = models.CharField(
        max_length=120,
        verbose_name=_('Link'),
        help_text=_("Link to CTA resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default="#",
        blank=True,
        null=True
    )
    link_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=_("Submit"),
        verbose_name=_("Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    link_color = ColorField(
        default='#71c059',
        verbose_name=_("Link Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    is_pws = models.BooleanField(
        default=True,
        verbose_name=_("Add PWS Link")
    )
    pws_link = models.CharField(
        max_length=120,
        verbose_name=_('PWS Link'),
        help_text=_("Link to CTA resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default="#",
        blank=True,
        null=True
    )
    pws_link_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=_("Show PWS Link Here"),
        verbose_name=_("PWS Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    pws_link_color = ColorField(
        default='#71c059',
        verbose_name=_("PWS Link Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    xtralink = models.CharField(
        max_length=120,
        verbose_name=_('Extra Link'),
        help_text=_("Link to CTA resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    xtralink_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Extra Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    xtralink_color = ColorField(
        default='#71c059',
        verbose_name=_("Extra Link Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="cta_image"
    )
    cf1 = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Custom Field"),
        help_text=_("Use to add custom values to template"),
        unique=False
    )
    cf2 = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Custom Field"),
        help_text=_("Use to add custom values to template"),
        unique=False
    )
    cf3 = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Custom Field"),
        help_text=_("Use to add custom values to template"),
        unique=False
    )
    cf4 = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Custom Field"),
        help_text=_("Use to add custom values to template"),
        unique=False
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="cta_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="cta_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.title)

    # def save(self,  *args, **kwargs):
    #
    #     if not self.slug:
    #         self.slug = slugify(self.name)

    def get_absolute_url(self):
        return reverse("cta_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        ordering= ['sort_order']
        verbose_name = _('Call To Action (CTA)')
        verbose_name_plural = _('Call To Action (CTAs)')


class Icon(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        verbose_name=_('Name'),
        help_text=_("Icon Name"),
        blank=False,
        null=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Slug"),
        help_text=_("Keyword or slug will be auto-generated from name field if left empty")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    icon_class = models.CharField(
        max_length=120,
        verbose_name=_('Icon Classes'),
        help_text=_("Icon font classes"),
        blank=True,
        null=True
    )
    icon_color = ColorField(
        default='#00b4c0',
        verbose_name=_("Icon Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    icon_bg_color = ColorField(
        default='#ffffff',
        verbose_name=_("Icon Background Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    size = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Icon Size"),
        help_text=_("Size of Icon. Default is 30."),
        default='30px'
    )
    placement = models.CharField(
        max_length=100,
        default="center",
        choices=placement,
        blank=True,
        null=True
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="icon_inline_image"
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="icon_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="icon_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return reverse("icon_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Icon')
        verbose_name_plural = _('Icons')


class Banner(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Landing Page"),
        help_text=_("Name of landing page"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from  title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    cta = models.ManyToManyField(
        Cta,
        blank=True,
        # null=True,
        verbose_name=_("CTA"),
        help_text=_("Select Call To Action (CTA)"),
        default=None,
        related_name="banner_cta"
    )
    collectn = models.ManyToManyField(
        Collection,
        blank=True,
        verbose_name=_("Collection"),
        help_text=_("Select Collection"),
        default=None,
        related_name="banner_collection"
    )
    timelines = models.ManyToManyField(
        Timeline,
        blank=True,
        verbose_name=_("Timelines"),
        help_text=_("Select timelines"),
        default=None,
        related_name="banner_timelines"
    )
    quotes = models.ManyToManyField(
        Quote,
        blank=True,
        verbose_name=_("Quotes"),
        help_text=_("Select quotes"),
        default=None,
        related_name="banner_quotes"
    )
    testimonials = models.ManyToManyField(
        Testimonial,
        blank=True,
        verbose_name=_("Testimonials"),
        help_text=_("Select testimonials"),
        default=None,
        related_name="banner_testimonials"
    )
    snippets = models.ManyToManyField(
        Snippet,
        blank=True,
        verbose_name=_("Snippets"),
        help_text=_("Select snippet"),
        default=None,
        related_name="banner_snippets"
    )
    dashboards = models.ManyToManyField(
        Dashboard,
        blank=True,
        verbose_name=_("Dashboards"),
        help_text=_("Select dashboard"),
        default=None,
        related_name="banner_dashboard"
    )
    specials = models.ManyToManyField(
        Specials,
        blank=True,
        verbose_name=_("Specials"),
        help_text=_("Select specials"),
        default=None,
        related_name="banner_specials"
    )
    incentives = models.ManyToManyField(
        Incentives,
        blank=True,
        verbose_name=_("Incentives"),
        help_text=_("Select incentives"),
        default=None,
        related_name="banner_incentives"
    )
    videos = models.ManyToManyField(
        Video,
        blank=True,
        verbose_name=_("Videos"),
        help_text=_("Select videos"),
        default=None,
        related_name="banner_videos"
    )
    placement = models.CharField(
        max_length=100,
        default="center",
        choices=placement,
        blank=True,
        null=True
    )
    icon = models.ForeignKey(
        Icon,
        blank=True,
        null=True,
        verbose_name=_("Icon"),
        help_text=_("Set icon image or specify an icon font class)"),
        default=None,
        related_name="banner_icon"
    )
    icon_color_override = ColorField(
        default='#00b4c0',
        verbose_name=_("Icon Color Override"),
        help_text=_("Used to set new color for icons inherited from other banners."),
        blank=True,
        null=True,
    )
    caption = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Caption"),
        help_text=_("Caption."),
        default=None
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="featured_banner_category"
    )
    layout = models.ForeignKey(
        CustomLayout,
        max_length=255,
        blank=False,
        null=False,
        help_text=_("Add layout for the banner"),
        default=None,
        related_name="ly_layout"
    )
    # custom_layouts = models.ManyToManyField(
    #     CustomLayout,
    #     max_length=255,
    #     blank=True,
    #     help_text=_("Add custom layout for the banner"),
    #     default=None,
    #     related_name="cl_layout"
    # )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="banner_bg_image"
    )
    margin_top = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Margin Bottom"),
        help_text=_("Use to set spacing above banner"),
        default='10px'
    )
    margin_bottom = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_("Margin Top"),
        help_text=_("Use to set spacing below banner"),
        default='10px'
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Banner Status.")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="banner_social"
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="banner_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="banner_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Landing Page')
        verbose_name_plural = _('Landing Pages')


class ImageInline(models.Model):
    #ct = get_content_type
    banner = models.ForeignKey(
        Banner,
        blank=False,
        verbose_name=_("Banner"),
        help_text=_("Select a content related to this image"),
        default=None,
        related_name="banner_gallery"
    )
    caption = models.CharField(
        max_length=255,
        verbose_name=_('Caption'),
        help_text=_("Banner caption")
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="banner_inline_image"
    )
    set_primary = models.BooleanField(
        default=False,
        verbose_name=_("Set as primary"),
        help_text=_("Use to set primary image for banner.")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )

    def __str__(self):
        return str(self.caption)

    class Meta:
        verbose_name = _('Image Inline')
        verbose_name_plural = _('Image Inline')


class Carousel(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Carousel Name"),
        help_text=_("Name of carousel"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from  title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    caption = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Caption"),
        help_text=_("Caption."),
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Carousel Layout"),
        help_text=_("Add layout for the carousel"),
        default=None
    )
    slides = models.ManyToManyField(
        "CarouselImageInline",
        blank=True,
        default=None,
        verbose_name=_("Slides"),
        related_name="carousel_slides"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Carousel Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="carousel_social"
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="carousel_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="carousel_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Carousel')
        verbose_name_plural = _('Carousels')


class CarouselImageInline(models.Model):
    #ct = get_content_type
    banner = models.ForeignKey(
        Carousel,
        blank=False,
        verbose_name=_("Carousel"),
        help_text=_("Select a content related to this image"),
        default=None,
        related_name="banner_gallery"
    )
    caption = models.CharField(
        max_length=255,
        verbose_name=_('Caption'),
        help_text=_("Carousel caption")
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="carousel_inline_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )

    def __str__(self):
        return str(self.caption)

    class Meta:
        verbose_name = _('Carousel Image Inline')
        verbose_name_plural = _('Carousel Image Inline')

