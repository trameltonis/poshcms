from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from helpers import pagination, sorting
from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import View
from django.utils import timezone
from django.shortcuts import render_to_response, render
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from .forms import *
from .models import *


class BannerCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = BannerCreateForm
    page_title = _("Banner Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(BannerCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BannerCreateView, self).dispatch(*args, **kwargs)


class BannerUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = BannerEditForm
    model = Banner
    page_title = _("Banner Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(BannerUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BannerUpdateView, self).dispatch(*args, **kwargs)


class BannerDetailView(DetailView):
    model = Banner
    template_name = "featured/_banner_detail.html"


class BannerListView(ListView):
    model = Banner
    page_title = _("Banner List"),
    template_name = "featured/banner_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(BannerListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(title__contains=query) |
                Q(caption__contains=query) |
                Q(category__title__contains=query ) |
                Q(category__title=query )
            )
            try:
                qs2 = self.model.objects.filter(
                Q(slug=query)

            )
                qs = (qs | qs2).distinct()
            except:
                pass
            return qs


class HomeBannerListView(ListView):
    model = Banner
    page_title = _("Welcome to Norwex"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(HomeBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="home", status='p', active=True)
        return qs


class AboutBannerListView(ListView):
    model = Banner
    page_title = _("Our Story"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(AboutBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="about", status='p', active=True)
        return qs


class ShopBannerListView(ListView):
    model = Banner
    page_title = _("Shop Online"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ShopBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="shop", status='p', active=True)
        return qs


class HostBannerListView(ListView):
    model = Banner
    page_title = _("Host A Party"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(HostBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="host-party", status='p', active=True)
        return qs


class JoinBannerListView(ListView):
    model = Banner
    page_title = _("Host A Party"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(JoinBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="join", status='p', active=True)
        return qs


class OurPurposeBannerListView(ListView):
    model = Banner
    page_title = _("Our Purpose"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(OurPurposeBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="our-purpose", status='p', active=True)
        return qs


class GlobalImpactBannerListView(ListView):
    model = Banner
    page_title = _("Global Impact"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(GlobalImpactBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="global-impact", status='p', active=True)
        return qs


class CharitablePartnersBannerListView(ListView):
    model = Banner
    page_title = _("Charitable Partners"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CharitablePartnersBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="charitable-partners", status='p', active=True)
        return qs

class ProductMissionBannerListView(ListView):
    model = Banner
    page_title = _("Product Mission"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ProductMissionBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="product-mission", status='p', active=True)
        return qs

class SpecialsAndSalesBannerListView(ListView):
    model = Banner
    page_title = _("Specials And Sales"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(SpecialsAndSalesBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="specials-and-sales", status='p', active=True)
        return qs


class MicrofiberBannerListView(ListView):
    model = Banner
    page_title = _("Norwex Microbiber"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(MicrofiberBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="microfiber", status='p', active=True)
        return qs


class SafeHavenHouseBannerListView(ListView):
    model = Banner
    page_title = _("Safe Haven House"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(SafeHavenHouseBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="safe-haven-house", status='p', active=True)
        return qs


class ChemicalsOfConcernQuizBannerListView(ListView):
    model = Banner
    page_title = _("Chemicals Of Concern Quiz"),
    template_name = "featured/_banner_layouts.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ChemicalsOfConcernQuizBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="chemicals-concern-quiz", status='p', active=True)
        return qs


# PWS Routes
#
# class PWSHomeBannerListView(ListView):
#     model = Banner
#     page_title = _("Welcome to Norwex"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSHomeBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="home", status='p', active=True)
#         return qs
#
#
# class PWSAboutBannerListView(ListView):
#     model = Banner
#     page_title = _("Our Story"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSAboutBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="about", status='p', active=True)
#         return qs
#
#
# class PWSShopBannerListView(ListView):
#     model = Banner
#     page_title = _("Shop Online"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSShopBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="shop", status='p', active=True)
#         return qs
#
#
# class PWSHostBannerListView(ListView):
#     model = Banner
#     page_title = _("Host A Party"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSHostBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="host-party", status='p', active=True)
#         return qs
#
#
# class PWSJoinBannerListView(ListView):
#     model = Banner
#     page_title = _("Host A Party"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSJoinBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="join", status='p', active=True)
#         return qs
#
#
# class PWSOurPurposeBannerListView(ListView):
#     model = Banner
#     page_title = _("Our Purpose"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSOurPurposeBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="our-purpose", status='p', active=True)
#         return qs
#
#
# class PWSGlobalImpactBannerListView(ListView):
#     model = Banner
#     page_title = _("Global Impact"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSGlobalImpactBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="global-impact", status='p', active=True)
#         return qs
#
#
# class PWSCharitablePartnersBannerListView(ListView):
#     model = Banner
#     page_title = _("Charitable Partners"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSCharitablePartnersBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="charitable-partners", status='p', active=True)
#         return qs
#
# class PWSProductMissionBannerListView(ListView):
#     model = Banner
#     page_title = _("Product Mission"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSProductMissionBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="product-mission", status='p', active=True)
#         return qs
#
# class PWSSpecialsAndSalesBannerListView(ListView):
#     model = Banner
#     page_title = _("Specials And Sales"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSSpecialsAndSalesBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="specials-and-sales", status='p', active=True)
#         return qs
#
#
# class PWSMicrofiberBannerListView(ListView):
#     model = Banner
#     page_title = _("Norwex Microbiber"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSMicrofiberBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="microfiber", status='p', active=True)
#         return qs
#
#
# class PWSSafeHavenHouseBannerListView(ListView):
#     model = Banner
#     page_title = _("Safe Haven House"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSSafeHavenHouseBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="safe-haven-house", status='p', active=True)
#         return qs
#
#
# class PWSChemicalsOfConcernQuizBannerListView(ListView):
#     model = Banner
#     page_title = _("Chemicals Of Concern Quiz"),
#     template_name = "featured/_banner_layouts.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSChemicalsOfConcernQuizBannerListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="chemicals-concern-quiz", status='p', active=True)
#         return qs


class CtaCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = CtaCreateForm
    page_title = _("Cta Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CtaCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CtaCreateView, self).dispatch(*args, **kwargs)


class CtaUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = CtaEditForm
    model = Cta
    page_title = _("Cta Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(CtaUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CtaUpdateView, self).dispatch(*args, **kwargs)


class CtaDetailView(DetailView):
    model = Cta
    template_name = "featured/_cta_detail.html"


class CtaListView(ListView):
    model = Cta
    page_title = _("Cta List"),
    template_name = "featured/cta_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CtaListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(title__contains=query) |
                Q(caption__contains=query) |
                Q(category__title__contains=query ) |
                Q(category__title=query )
            )
            try:
                qs2 = self.model.objects.filter(
                Q(slug=query)

            )
                qs = (qs | qs2).distinct()
            except:
                pass
            return qs

