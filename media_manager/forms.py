from django import forms
from dal import autocomplete
from django.utils.translation import ugettext_lazy as _
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, Row, Field, Hidden, ButtonHolder
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import DocumentGallery, VideoGallery, Gallery, Video, GalleryImageInline, GalleryFileInline
from menu_manager.models import Menu, MenuCategory
from menu_manager.forms import MenuCategoryCreateForm, MenuCategoryEditForm, MenuCreateForm, MenuEditForm


class DocumentGalleryCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = DocumentGallery
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'cta',
            'placement',
            'icon',
            'layout',
            'category',
            'margin_top',
            'margin_bottom',
            'caption',
            'status',
            'img',
            'sort_order',
            'publish_start',
            'publish_end',
            'lock_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class DocumentGalleryEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = DocumentGallery
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'layout',
            'fileinline',
            'placement',
            'margin_top',
            'margin_bottom',
            'caption',
            'status',
            'img',
            'sort_order',
            'publish_start',
            'publish_end',
            'lock_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class VideoCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Video
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'gallery',
            'title',
            'provider',
            'video_code',
            'menu_link',
            'category',
            'layout',
            'related',
            'img',
            # 'preview',
            'caption',
            'featured',
            'sort_order',
            'visibility',
            'progress_status',
            'author',
            'copyright',
            'tags',
            'slug',
            'publish_start',
            'publish_end',
            'active',
            'status',
            'lock_locale',
            'start',
            'width',
            'height',
            'border',
            'html5_player',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class VideoEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Video
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'gallery',
            'title',
            'provider',
            'video_code',
            'menu_link',
            'category',
            'layout',
            'related',
            'img',
            # 'preview',
            'caption',
            'featured',
            'sort_order',
            'visibility',
            'progress_status',
            'author',
            'copyright',
            'tags',
            'slug',
            'publish_start',
            'publish_end',
            'active',
            'status',
            'lock_locale',
            'start',
            'width',
            'height',
            'border',
            'html5_player',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class VideoGalleryForm(forms.ModelForm):
    class Meta:
        model = VideoGallery
        fields = ('__all__')
        widgets = {
            'videos': autocomplete.ModelSelect2(url='video_autocomplete')
        }