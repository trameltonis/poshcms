from modeltranslation.translator import translator, register, TranslationOptions
from .models import DocumentGallery, GalleryFileInline, Gallery, GalleryImageInline, MediaTag, MediaType, File, Image, Video, VideoGallery, Category


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(DocumentGallery)
class DocumentGalleryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(GalleryFileInline)
class GalleryFileInlineTranslationOptions(TranslationOptions):
    fields = [
        'caption',
        'doc_img',
        'doc_file',
        ]

@register(Gallery)
class GalleryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(GalleryImageInline)
class GalleryImageInlineTranslationOptions(TranslationOptions):
    fields = [
        'caption',
        'img',
        ]


@register(MediaTag)
class MediaTagTranslationOptions(TranslationOptions):
    fields = [
        'title',
        ]

@register(MediaType)
class MediaTypeTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        'img',
        ]


@register(File)
class FileTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        'copyright',
        ]


@register(Image)
class ImageTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        'img',
        'copyright',
        ]


@register(VideoGallery)
class VideoGalleryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]

@register(Video)
class VideoTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'video_code',
        'caption',
        'img',
        'copyright',
        ]

