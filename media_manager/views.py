from django.db.models import Q
from dal import autocomplete
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from helpers import pagination, sorting
from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import View
from django.utils import timezone
from django.shortcuts import render_to_response, render
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from .forms import VideoCreateForm, VideoEditForm
from .models import DocumentGallery, VideoGallery, Gallery, Video, GalleryImageInline, GalleryFileInline, Category


class VideoCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = VideoCreateForm
    page_title = _("Video Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(VideoCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(VideoCreateView, self).dispatch(*args, **kwargs)


class VideoUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = VideoEditForm
    model = Video
    page_title = _("Video Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(VideoUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(VideoUpdateView, self).dispatch(*args, **kwargs)


class VideoDetailView(DetailView):
    model = Video
    template_name = "media_manager/_video_detail.html"


class VideoListView(ListView):
    model = Video
    page_title = _("Video List"),
    template_name = "media_manager/video_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(VideoListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(title__contains=query) |
                Q(caption__contains=query) |
                Q(category__title__contains=query ) |
                Q(category__title=query )
            )
            try:
                qs2 = self.model.objects.filter(
                Q(slug=query)

            )
                qs = (qs | qs2).distinct()
            except:
                pass
            return qs


class VideoAutocomplete(autocomplete.Select2QuerySetView):
    model = Video
    page_title = _("Video List"),
    template_name = "media_manager/video_list.html"

    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Video.objects.none()

        qs = Video.objects.all()

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs
