from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MediaManagerConfig(AppConfig):
    name = 'media_manager'
    verbose_name = _('Document Library & Gallery Manager')
