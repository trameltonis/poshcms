import os
from django.db.models import Q
from django.conf import settings
from filer.models.abstract import BaseImage
from filer.fields.image import FilerImageField
from filebrowser.fields import FileBrowseField
from filer.fields.file import FilerFileField
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from core.models import *
from theme.models import *
from menu_manager.models import *
# from cmf.models import Incentives, Specials
from helpers.utils import *
from filer import settings as filer_settings
from filer.models import File, Image


class Category(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the category."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="media_category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="media_category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Gallery(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Name"),
        help_text=_("Name of gallery"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from  title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="gallery_menu_link"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="gallery_theme"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="ga_category"
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("Description."),
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Gallery Layout"),
        help_text=_("Add layout for the gallery"),
        default=None
    )
    # media_layout = models.CharField(
    #     max_length=100,
    #     default="grid-3",
    #     verbose_name=_("Media Layout"),
    #     choices=MEDIA_LAYOUT,
    # )
    imageinline = models.ManyToManyField(
        "GalleryImageInline",
        blank=True,
        # null=True,
        verbose_name=_("Gallery Image"),
        help_text=_("Gallery Image"),
        default=None,
        related_name="gallery_img"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Gallery Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="gallery_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="gallery_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("media_gallery_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Media Gallery')
        verbose_name_plural = _('Media Galleries')


class GalleryImageInline(models.Model):
    ct = get_content_type
    gallery = models.ForeignKey(
        Gallery,
        blank=False,
        verbose_name=_("Gallery"),
        help_text=_("Select a content related to this image"),
        default=None,
        related_name="mmg_gallery"
    )
    caption = models.CharField(
        max_length=255,
        verbose_name=_('Caption'),
        help_text=_("Image caption")
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="gallery_inline_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="giline_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="giline_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("gallery_image_detail", kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Gallery Image')
        verbose_name_plural = _('Gallery Images')


class DocumentGallery(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Name"),
        help_text=_("Name of document gallery"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from  title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="dg_menu_link"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="dg_theme"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="dg_category"
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("Description."),
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Gallery Layout"),
        help_text=_("Add layout for the gallery"),
        default=None
    )
    # media_layout = models.CharField(
    #     max_length=100,
    #     default="grid-3",
    #     verbose_name=_("Media Layout"),
    #     choices=MEDIA_LAYOUT,
    # )
    fileinline = models.ManyToManyField(
        "GalleryFileInline",
        blank=True,
        # null=True,
        verbose_name=_("Gallery File"),
        help_text=_("Gallery File"),
        default=None,
        related_name="gallery_img"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Gallery Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="doc_gallery_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="doc_gallery_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("document_gallery_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Document Gallery')
        verbose_name_plural = _('Document Galleries')


class GalleryFileInline(models.Model):
    ct = get_content_type
    document = models.ForeignKey(
        DocumentGallery,
        blank=False,
        verbose_name=_("Document Gallery"),
        help_text=_("Select a content related to this image"),
        default=None,
        related_name="doc_gallery"
    )
    caption = models.CharField(
        max_length=255,
        verbose_name=_('Caption'),
        help_text=_("Document caption")
    )
    doc_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_('Document Image'),
        related_name="doc_img_inline_image"
    )
    doc_file = FilerFileField(
        null=True,
        blank=True,
        verbose_name=_('Document File'),
        related_name="doc_file_inline_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="gfiline_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="gfiline_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("gallery_file_detail", kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Gallery File')
        verbose_name_plural = _('Gallery Files')


class MediaTag(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=100,
        verbose_name=_('Name')
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="media_tag_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="media_tag_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On'),
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated'),
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("media_tag_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Media Tag')
        verbose_name_plural = _('Media Tags')


class MediaType(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Keyword'),
        help_text=_('Automatically generated from media type title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the media type."),
        default=None
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="media_type"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Media Type Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="media_type_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="media_type_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("media_type_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Media Type')
        verbose_name_plural = _('Media Types')


class Image(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from media type title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    author = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Author"),
        help_text=_("Content Author"),
        unique=False
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="img_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="img_category"
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("Description."),
        default=None
    )
    flush = models.BooleanField(
        default=False,
        verbose_name=_("Flush CDN"),
        help_text = _("Use to flush CDN.")
    )
    copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("Content Copyright"),
        unique=False
    )
    tags = models.ManyToManyField(
        MediaTag,
        blank=True,
        verbose_name=_("Tag"),
        help_text=_("Tags"),
    )
    optional_url = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("Optional Url"),
        help_text=_("Optional Url"),
        unique=False
    )
    related_url = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("Related Url"),
        help_text=_("Related Url"),
        unique=False
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="img"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Image Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="image_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="image_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("image_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Image')
        verbose_name_plural = _('Images')


class File(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from media type title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    author = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Author"),
        help_text=_("Content Author"),
        unique=False
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="fl_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="fl_category"
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("Description."),
        default=None
    )
    flush = models.BooleanField(
        default=False,
        verbose_name=_("Flush CDN"),
        help_text = _("Use to flush CDN.")
    )
    copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("Content Copyright"),
        unique=False
    )
    tags = models.ManyToManyField(
        MediaTag,
        blank=True,
        verbose_name=_("Tag"),
        help_text=_("Tags"),
    )
    optional_url = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("Optional Url"),
        help_text=_("Optional Url"),
        unique=False
    )
    related_url = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("Related Url"),
        help_text=_("Related Url"),
        unique=False
    )
    file_upload = FilerFileField(
        null=True,
        blank=True,
        related_name="file_upload",
        verbose_name=_("File/Reference")
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="file_img",
        verbose_name=_("File Thumbnail")
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Image Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="file_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="file_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("file_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('File')
        verbose_name_plural = _('Files')


class VideoGallery(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Gallery Name"),
        help_text=_("Name of gallery"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from  title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="vg_menu_link"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="vg_theme"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="vid_category"
    )
    videos = models.ManyToManyField(
        "Video",
        blank=True,
        # null=True,
        verbose_name=_("Videos"),
        help_text=_("Select videos to be displayed on this video gallery"),
        default=None,
        related_name="gallery_videos",
        # limit_choices_to=Q(active=True, video__gallery=(isinstance("VideoGallery", class_or_type_or_tuple="gallery")))
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("Description."),
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Gallery Layout"),
        help_text=_("Add layout for the gallery"),
        default=None
    )
    # media_layout = models.CharField(
    #     max_length=100,
    #     default="grid-3",
    #     verbose_name=_("Media Layout"),
    #     choices=MEDIA_LAYOUT,
    # )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Video Gallery Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="vid_gallery_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="vid_gallery_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("video_gallery_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Video Gallery')
        verbose_name_plural = _('Video Galleries')


class Video(models.Model):
    ct = get_content_type
    gallery = models.ForeignKey(
        VideoGallery,
        blank=True,
        null=True,
        verbose_name=_("Video Gallery"),
        help_text=_("Select a video gallery related to this video"),
        default=None,
        related_name="vid_gallery"
    )
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from media type title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    provider = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=VIDEO_PROVIDER,
        verbose_name=_("Video Provider"),
        default="youtube",
        help_text=_("Select a video provider")
    )
    video_code = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        default=None,
        verbose_name=_("Video Url Code"),
        help_text=_("Enter video code at the end of the url. , For example, if video url is https://www.youtube.com/watch?v=2QigyB50yGw, just enter '2QigyB50yGw' "),
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="video_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="video_category"
    )
    caption = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Caption"),
        help_text=_("Video caption."),
        default=None
    )
    author = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Author"),
        help_text=_("Content Author"),
        unique=False
    )
    copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("Content Copyright"),
        unique=False
    )
    tags = models.ManyToManyField(
        MediaTag,
        blank=True,
        verbose_name=_("Tag"),
        help_text=_("Tags"),
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="yt_img",
        verbose_name=_("Video Still Shot")
    )
    layout = models.ForeignKey(
        Layout,
        blank=True,
        null=True,
        verbose_name=_("Gallery Layout"),
        help_text=_("Add layout for the gallery"),
        default=None
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables video to be featured.")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    start = models.PositiveIntegerField(
        null=True,
        blank=True,
        default=0,
        verbose_name=_("Play Start Position"),
        help_text=_("This sets the play start position in seconds. If you want video to start playing 1 mins into the video, you can enter 60 in this field."),
        unique=False
    )
    width = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        default="560px",
        verbose_name=_("Frame width"),
        help_text=_("Video frame width"),
        unique=False
    )
    height = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        default="315px",
        verbose_name=_("Frame height"),
        help_text=_("Video frame height"),
        unique=False
    )
    border = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        default=0,
        verbose_name=_("Frame border"),
        help_text=_("Video frame border. Default is 0"),
        unique=False
    )
    html5_player = models.BooleanField(
        default=True,
        verbose_name=_("Enable HTML5 Player"),
        help_text = _("Enable HTML5 support for this video.")
    )
    related = models.ManyToManyField(
        "Video",
        blank=True,
        # null=True,
        default=None,
        related_name="related_videos",
        verbose_name=_("Related Videos"),
        help_text=_("Other videos related to this video."),
        # limit_choices_to=Q(active=True, )
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Video Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="yt_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="yt_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("video_detail", kwargs={'slug': self.slug, 'category': self.category.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Video')
        verbose_name_plural = _('Videos')


class VideoUpload(File):
    ct = get_content_type
    _icon = "video"
    pass

    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        # the extensions we'll recognise for this file type
        filename_extensions = ['.dv', '.mov', '.mp4', '.avi', '.wmv',]
        ext = os.path.splitext(iname)[1].lower()
        return ext in filename_extensions










