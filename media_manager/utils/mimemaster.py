from __future__ import absolute_import, unicode_literals

import os
from django.conf import settings

from django.utils.timezone import now

from .files import get_valid_filename

try:
    from django.utils.encoding import force_text
except ImportError:
    # Django < 1.5
    from django.utils.encoding import force_unicode as force_text

def get_valid_extensions():
    extensions = settings.FILEBROWSER_EXTENSIONS
    return extensions

def mimemaster(instance, filename):
    import uuid
    uuid_str = str(uuid.uuid4())
    return os.path.join(uuid_str[0:2], uuid_str[2:4], uuid_str,
            get_valid_filename(filename))

