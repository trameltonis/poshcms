from __future__ import absolute_import, unicode_literals
import random
import string
from django.conf import settings
import os
from django.http.multipartparser import (
    ChunkIter,
    SkipFile,
    StopFutureHandlers,
    StopUpload,
    exhaust,
)
from django.template.defaultfilters import slugify as slugify_django
from django.utils.encoding import force_text
from django.utils.text import get_valid_filename as get_valid_filename_django
from unidecode import unidecode



from django.utils.timezone import now

# from .files import get_valid_filename

try:
    from django.utils.encoding import force_text
except ImportError:
    # Django < 1.5
    from django.utils.encoding import force_unicode as force_text


def handle_request_files_upload(request):
    """
    Handle request.FILES if len(request.FILES) == 1.
    Returns tuple(upload, filename, is_raw) where upload is file itself.
    """
    # FILES is a dictionary in Django but Ajax Upload gives the uploaded file
    # an ID based on a random number, so it cannot be guessed here in the code.
    # Rather than editing Ajax Upload to pass the ID in the querystring,
    # note that each upload is a separate request so FILES should only
    # have one entry.
    # Thus, we can just grab the first (and only) value in the dict.
    is_raw = False
    upload = list(request.FILES.values())[0]
    filename = upload.name
    return upload, filename, is_raw


def slugify(string):
    return slugify_django(unidecode(force_text(string)))


def get_valid_filename(s):
    """
    like the regular get_valid_filename, but also slugifies away
    umlauts and stuff.
    """
    s = get_valid_filename_django(s)
    filename, ext = os.path.splitext(s)
    filename = slugify(filename)
    ext = slugify(ext)
    if ext:
        return "%s.%s" % (filename, ext)
    else:
        return "%s" % (filename,)


def by_date(instance, filename):
    datepart = force_text(now().strftime("%Y/%m/%d"))
    return os.path.join(datepart, get_valid_filename(filename))


def randomized(instance, filename):
    import uuid
    uuid_str = str(uuid.uuid4())
    return os.path.join(uuid_str[0:2], uuid_str[2:4], uuid_str,
            get_valid_filename(filename))


def maxilize(instance, filename):
    import random
    import string
    from django.conf import settings
    import os
    folder = random.choice(['vision', 'integrity', 'founders'])
    return os.path.join(folder,
            get_valid_filename(filename))

#
# def maxified(instance, filename):
#     f = get_valid_filename_django(f)
#     filename, ext = os.path.splitext(f)
#     filename = slugify(filename)
#     ext = slugify(ext)
#     av_ext = settings.FILER_EXTENSIONS
#     # folder = random.shuffle(['vision', 'integrity', 'founders'])
#     if ext == '.jpg':
#         folder = av_ext[0]
#     elif ext == '.mp3':
#         folder = av_ext[1]
#     elif ext == '.mov':
#         folder = av_ext[2]
#     elif ext == '.png':
#         folder = av_ext[4]
#
#     return os.path.join(folder, get_valid_filename(filename))