# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import random
import string
from django.conf import settings
from django.template.defaultfilters import slugify as slugify_django
from django.utils.text import get_valid_filename as get_valid_filename_django
from unidecode import unidecode


import os
from django.template.defaultfilters import slugify as slugify_django
from django.utils.encoding import force_text
from django.utils.text import get_valid_filename as get_valid_filename_django
from unidecode import unidecode
from media_manager.utils.maxified import get_valid_filename


import os

from django.utils.timezone import now

from .files import get_valid_filename

try:
    from django.utils.encoding import force_text
except ImportError:
    # Django < 1.5
    from django.utils.encoding import force_unicode as force_text

def slugify(string):
    return slugify_django(unidecode(force_text(string)))


def by_date(instance, filename):
    datepart = force_text(now().strftime("%Y/%m/%d"))
    return os.path.join(datepart, get_valid_filename(filename))


def randomized(instance, filename):
    import uuid
    uuid_str = str(uuid.uuid4())
    return os.path.join(uuid_str[0:2], uuid_str[2:4], uuid_str,
                        get_valid_filename(filename))


def get_valid_filename(f):
    """
    like the regular get_valid_filename, but also slugifies away
    umlauts and stuff.
    """
    f = get_valid_filename_django(f)
    filename, ext = os.path.splitext(f)
    filename = slugify(filename)
    ext = slugify(ext)
    if ext:
        return "%s.%s" % (filename, ext)
    else:
        return "%s" % (filename,)


def maxilize(instance, filename):
    filename = get_valid_filename(filename)
    if filename.endswith(('.jpg','.jpeg','.gif','.png','.tif','.tiff', '.bmp', '.eps', '.psd', '.psb')):
        folder = 'norwex-images'
    elif filename.endswith(('.pdf','.doc','.rtf','.txt', '.json', '.xlsx', '.xls','.csv', '.docx', '.epub', '.pages', '.ppt', '.pptx', '.pub', '.pubx', '.p65', '.ods', '.ppsx', '.zip', '.tar')):
        folder = 'norwex-documents'
    elif filename.endswith(('.mov','.wmv','.mpeg','.mpg','.avi','.rm')):
        folder = 'norwex-videos'
    elif filename.endswith(('.mp3','.mp4','.wav','.aiff','.midi','.m4p')):
        folder = 'norwex-audios'
    else:
        folder = random.choice(['norwex-others'])
        # folder = random.choice(['vision', 'integrity', 'founders'])
    return os.path.join(folder, get_valid_filename(filename))


class prefixed_factory(object):
    def __init__(self, upload_to, prefix):
        self.upload_to = upload_to
        self.prefix = prefix

    def __call__(self, instance, filename):
        if callable(self.upload_to):
            upload_to_str = self.upload_to(instance, filename)
        else:
            upload_to_str = self.upload_to
        if not self.prefix:
            return upload_to_str
        return os.path.join(self.prefix, upload_to_str)



# def maxilize(instance, filename):
#     filename, ext = os.path.splitext(filename)
#     ext = slugify(ext)
#     if ext.endswith(('.jpg','.jpeg','.gif','.png','.tif','.tiff')):
#         folder = 'norwex-images'
#     elif ext.endswith(('.pdf','.doc','.rtf','.txt','.xls','.csv')):
#         folder = 'norwex-documents'
#     elif ext.endswith(('.mov','.wmv','.mpeg','.mpg','.avi','.rm')):
#         folder = 'norwex-video'
#     elif ext.endswith(('.mp3','.mp4','.wav','.aiff','.midi','.m4p')):
#         folder = 'norwex-audio'
#     else:
#         folder = random.choice(['all-media'])
#         # folder = random.choice(['vision', 'integrity', 'founders'])
#     return os.path.join(folder, get_valid_filename(filename))