from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from ..models import MediaTag, Video, MediaType, Gallery, GalleryFileInline, GalleryImageInline, DocumentGallery, VideoUpload, VideoGallery
from filer.models import File, Image
from cmf.api.serializers import *



class MediaTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTag
        fields = '__all__'
        depth = 3


class MediaTagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTag
        fields = '__all__'
        depth = 3


class MediaTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaType
        fields = '__all__'
        depth = 3


class MediaTypeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaType
        fields = '__all__'
        depth = 3


class ImageSerializer(serializers.ModelSerializer):
    # tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Image
        fields = '__all__'
        depth = 3


class ImageCreateSerializer(serializers.ModelSerializer):
    # tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Image
        fields = '__all__'
        depth = 3


class FileSerializer(serializers.ModelSerializer):
    # tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = File
        fields = '__all__'
        depth = 3


class FileCreateSerializer(serializers.ModelSerializer):
    # tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = File
        fields = '__all__'
        depth = 3


class GallerySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Gallery
        fields = '__all__'
        depth = 3


class GalleryCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Gallery
        fields = '__all__'
        depth = 3


class GalleryImageInlineSerializer(serializers.ModelSerializer):
    # tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryImageInline
        fields = '__all__'
        depth = 3


class GalleryImageInlineCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryImageInline
        fields = '__all__'
        depth = 3


class GalleryFileInlineSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryFileInline
        fields = '__all__'
        depth = 3


class GalleryFileInlineCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryFileInline
        fields = '__all__'
        depth = 3


class DocumentGallerySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentGallery
        fields = '__all__'
        depth = 3


class DocumentGalleryCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentGallery
        fields = '__all__'
        depth = 3


class VideoSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3


class VideoCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3


class VideoGallerySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3


class VideoGalleryCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3

#
# class ImageSerializer(serializers.ModelSerializer):
#     # tags = TagSerializer(many=True, read_only=True)
#
#     class Meta:
#         model = Image
#         fields = '__all__'
#         depth = 3
#
# class ImageCreateSerializer(serializers.ModelSerializer):
#     # tags = TagSerializer(many=True, read_only=True)
#
#     class Meta:
#         model = Image
#         fields = '__all__'
#         depth = 3
#
# class FileSerializer(serializers.ModelSerializer):
#     # tags = TagSerializer(many=True, read_only=True)
#
#     class Meta:
#         model = File
#         fields = '__all__'
#         depth = 3
#
# class FileCreateSerializer(serializers.ModelSerializer):
#     # tags = TagSerializer(many=True, read_only=True)
#
#     class Meta:
#         model = File
#         fields = '__all__'
#         depth = 3