from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import User, Group
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponse
from datetime import datetime, timedelta
# from braces.views import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend

from media_manager.api.serializers import *
from ..models import MediaTag, Video, MediaType, Gallery, GalleryFileInline, GalleryImageInline, DocumentGallery, VideoUpload, VideoGallery
from filer.models import File, Image


class GalleryFilter(filters.FilterSet):
    class Meta:
        model = Gallery
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class MediaTypeFilter(filters.FilterSet):
    class Meta:
        model = MediaType
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class MediaTagFilter(filters.FilterSet):
    class Meta:
        model = MediaTag
        fields = ['slug', 'id']


class FileFilter(filters.FilterSet):
    class Meta:
        model = File
        fields = ['id']


class ImageFilter(filters.FilterSet):
    class Meta:
        model = Image
        fields = ['id']


class FileViewSet(viewsets.ModelViewSet):
    queryset = File.objects.all().order_by('-uploaded_at')
    serializer_class = FileSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['id']


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all().order_by('-uploaded_at')
    serializer_class = ImageSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['id']


class MediaTypeViewSet(viewsets.ModelViewSet):
    queryset = MediaType.objects.all().order_by('-created_on')
    serializer_class = MediaTypeSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryViewSet(viewsets.ModelViewSet):
    queryset = Gallery.objects.all().order_by('-created_on')
    serializer_class = GallerySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MediaTagViewSet(viewsets.ModelViewSet):
    queryset = MediaTag.objects.all().order_by('-created_on')
    serializer_class = MediaTagSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all().order_by('-created_on')
    serializer_class = VideoSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class VideoGalleryViewSet(viewsets.ModelViewSet):
    queryset = VideoGallery.objects.all().order_by('-created_on')
    serializer_class = VideoGallerySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class DocumentGalleryViewSet(viewsets.ModelViewSet):
    queryset = DocumentGallery.objects.all().order_by('-created_on')
    serializer_class = DocumentGallerySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryImageInlineViewSet(viewsets.ModelViewSet):
    queryset = GalleryImageInline.objects.all().order_by('-created_on')
    serializer_class = GalleryImageInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryFileInlineViewSet(viewsets.ModelViewSet):
    queryset = GalleryFileInline.objects.all().order_by('-created_on')
    serializer_class = GalleryFileInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class FileList(generics.ListCreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = FileFilter


class FileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = FileFilter


class FileCreate(generics.CreateAPIView):
    serializer_class = FileCreateSerializer


class ImageList(generics.ListCreateAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImageFilter


class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImageFilter


class ImageCreate(generics.CreateAPIView):
    serializer_class = ImageCreateSerializer











