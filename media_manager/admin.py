from django.db import models
from dal import autocomplete
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import *
from django import forms
from ckeditor.widgets import CKEditorWidget
import nested_admin
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from modeltranslation.admin import TranslationStackedInline, TranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from .models import Video, VideoGallery, MediaTag, MediaType, Image, File, Gallery, GalleryImageInline, DocumentGallery, GalleryFileInline, Category
from .forms import VideoGalleryForm


@admin.register(Category)
class CategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))



@admin.register(MediaTag)
class MediaTagAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'language_selectors', 'progress_status',)
    search_fields = ['title', 'slug']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(MediaType)
class MediaTypeAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'language_selectors', 'status', 'active', 'progress_status', 'created_on', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


# @admin.register(Image)
class ImageAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'language_selectors', 'id', 'status', 'progress_status', 'active', 'created_on', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


# @admin.register(File)
class FileAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    filter_horizontal = ('tags',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'language_selectors', 'id', 'status', 'active', 'progress_status', 'created_on', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(GalleryImageInline)
class GalleryImageInline(ExtendedTranslationAdmin, ImportExportModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'caption',
                    'img',
                )
            }
        ),
    )

    model = GalleryImageInline
    extra = 1


@admin.register(Gallery)
class GalleryAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    # inlines = [GalleryImageInline, ]
    filter_horizontal = ('imageinline',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'id', 'status', 'active', 'progress_status', 'created_on', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(GalleryFileInline)
class GalleryFileInline(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'caption',
                    'doc_file',
                    'doc_img',
                )
            }
        ),
    )

    model = GalleryFileInline
    extra = 1
    # prepopulated_fields = {'slug': ('caption',)}
    list_display = ('caption', 'created_on', 'updated')
    search_fields = ['caption']
    list_filter = ['created_on']


@admin.register(DocumentGallery)
class DocumentGalleryAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'description',
                'menu_link',
                'category',
                'theme',
                'layout',
                'fileinline',
                'status',
                'visibility',
                'active',
                'progress_status',
                'sort_order',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )

    model = DocumentGallery
    # fields = ['__all__']
    extra = 0
# class DocumentGalleryAdmin(TranslationAdmin):


    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    # inlines = [GalleryFileInline, ]
    filter_horizontal = ('fileinline',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'id', 'status',  'visibility', 'active', 'progress_status', 'created_on', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Video)
class Video(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'gallery',
                'title',
                'url',
                'provider',
                'video_code',
                'menu_link',
                'category',
                'layout',
                'related',
                'img',
                # 'preview',
                'caption',
                'featured',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'author',
                'copyright',
                'tags',
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
        (_('Video Player Settings'), {
            'classes': ('collapse',),
            'fields': (
                'start',
                'width',
                'height',
                'border',
                'html5_player',
            ),
        }),
    )

    model = Video
    extra = 0
    # def preview(self, obj):
    #     return "<div>" + obj.url + "#t=" + obj.start + "</div>"
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    filter_horizontal = ('tags', 'related',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'provider', 'visibility', 'active', 'featured', 'progress_status', 'created_on', 'updated')
    search_fields = ['title', 'caption']
    limit_choices = 1
    list_filter = ['provider', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(VideoGallery)
class VideoGalleryAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }

    form = VideoGalleryForm
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    # inlines = [Video, ]
    filter_horizontal = ('videos',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'id', 'status', 'visibility', 'active', 'progress_status', 'created_on', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['status', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
