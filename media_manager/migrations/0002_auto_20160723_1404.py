# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-23 19:04
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.file
import filer.fields.image


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('menu_manager', '0001_initial'),
        ('theme', '0001_initial'),
        ('filer', '0005_auto_20160624_0202'),
        ('media_manager', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='videogallery',
            name='layout',
            field=models.ForeignKey(default=None, help_text='Add layout for the gallery', on_delete=django.db.models.deletion.CASCADE, to='theme.Layout', verbose_name='Gallery Layout'),
        ),
        migrations.AddField(
            model_name='videogallery',
            name='menu_link',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vg_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link'),
        ),
        migrations.AddField(
            model_name='videogallery',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vg_theme', to='theme.BaseTheme', verbose_name='Theme'),
        ),
        migrations.AddField(
            model_name='videogallery',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vid_gallery_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='videogallery',
            name='videos',
            field=models.ManyToManyField(blank=True, default=None, help_text='Select videos to be displayed on this video gallery', null=True, related_name='gallery_videos', to='media_manager.Video', verbose_name='Videos'),
        ),
        migrations.AddField(
            model_name='video',
            name='category',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='video_category', to='media_manager.Category'),
        ),
        migrations.AddField(
            model_name='video',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='video',
            name='gallery',
            field=models.ForeignKey(blank=True, default=None, help_text='Select a video gallery related to this video', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vid_gallery', to='media_manager.VideoGallery', verbose_name='Video Gallery'),
        ),
        migrations.AddField(
            model_name='video',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_img', to='filer.Image', verbose_name='Video Still Shot'),
        ),
        migrations.AddField(
            model_name='video',
            name='img_en_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_img', to='filer.Image', verbose_name='Video Still Shot'),
        ),
        migrations.AddField(
            model_name='video',
            name='img_en_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_img', to='filer.Image', verbose_name='Video Still Shot'),
        ),
        migrations.AddField(
            model_name='video',
            name='img_es_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_img', to='filer.Image', verbose_name='Video Still Shot'),
        ),
        migrations.AddField(
            model_name='video',
            name='img_fr_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_img', to='filer.Image', verbose_name='Video Still Shot'),
        ),
        migrations.AddField(
            model_name='video',
            name='layout',
            field=models.ForeignKey(blank=True, default=None, help_text='Add layout for the gallery', null=True, on_delete=django.db.models.deletion.CASCADE, to='theme.Layout', verbose_name='Gallery Layout'),
        ),
        migrations.AddField(
            model_name='video',
            name='menu_link',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='video_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link'),
        ),
        migrations.AddField(
            model_name='video',
            name='tags',
            field=models.ManyToManyField(blank=True, help_text='Tags', to='media_manager.MediaTag', verbose_name='Tag'),
        ),
        migrations.AddField(
            model_name='video',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='yt_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='img_en_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='img_en_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='img_es_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='img_fr_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_type_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mediatag',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_tag_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mediatag',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_tag_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='image',
            name='category',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='img_category', to='media_manager.Category'),
        ),
        migrations.AddField(
            model_name='image',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='image_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='image',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='img', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='image',
            name='img_en_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='img', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='image',
            name='img_en_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='img', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='image',
            name='img_es_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='img', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='image',
            name='img_fr_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='img', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='image',
            name='menu_link',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='img_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link'),
        ),
        migrations.AddField(
            model_name='image',
            name='tags',
            field=models.ManyToManyField(blank=True, help_text='Tags', to='media_manager.MediaTag', verbose_name='Tag'),
        ),
        migrations.AddField(
            model_name='image',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='image_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='giline_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='gallery',
            field=models.ForeignKey(default=None, help_text='Select a content related to this image', on_delete=django.db.models.deletion.CASCADE, related_name='mmg_gallery', to='media_manager.Gallery', verbose_name='Gallery'),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_inline_image', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='img_en_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_inline_image', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='img_en_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_inline_image', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='img_es_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_inline_image', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='img_fr_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_inline_image', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='galleryimageinline',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='giline_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gfiline_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_file',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_file_inline_image', to='filer.File', verbose_name='Document File'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_file_en_ca',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_file_inline_image', to='filer.File', verbose_name='Document File'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_file_en_us',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_file_inline_image', to='filer.File', verbose_name='Document File'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_file_es_us',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_file_inline_image', to='filer.File', verbose_name='Document File'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_file_fr_ca',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_file_inline_image', to='filer.File', verbose_name='Document File'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_img_inline_image', to='filer.Image', verbose_name='Document Image'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_img_en_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_img_inline_image', to='filer.Image', verbose_name='Document Image'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_img_en_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_img_inline_image', to='filer.Image', verbose_name='Document Image'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_img_es_us',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_img_inline_image', to='filer.Image', verbose_name='Document Image'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='doc_img_fr_ca',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_img_inline_image', to='filer.Image', verbose_name='Document Image'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='document',
            field=models.ForeignKey(default=None, help_text='Select a content related to this image', on_delete=django.db.models.deletion.CASCADE, related_name='doc_gallery', to='media_manager.DocumentGallery', verbose_name='Document Gallery'),
        ),
        migrations.AddField(
            model_name='galleryfileinline',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gfiline_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='gallery',
            name='category',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ga_category', to='media_manager.Category'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='gallery',
            name='imageinline',
            field=models.ManyToManyField(blank=True, default=None, help_text='Gallery Image', related_name='gallery_img', to='media_manager.GalleryImageInline', verbose_name='Gallery Image'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='layout',
            field=models.ForeignKey(default=None, help_text='Add layout for the gallery', on_delete=django.db.models.deletion.CASCADE, to='theme.Layout', verbose_name='Gallery Layout'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='menu_link',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_theme', to='theme.BaseTheme', verbose_name='Theme'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='file',
            name='category',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='fl_category', to='media_manager.Category'),
        ),
        migrations.AddField(
            model_name='file',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='file_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='file',
            name='file_upload',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='file_upload', to='filer.File', verbose_name='File/Reference'),
        ),
        migrations.AddField(
            model_name='file',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='file_img', to='filer.Image', verbose_name='File Thumbnail'),
        ),
        migrations.AddField(
            model_name='file',
            name='menu_link',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fl_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link'),
        ),
        migrations.AddField(
            model_name='file',
            name='tags',
            field=models.ManyToManyField(blank=True, help_text='Tags', to='media_manager.MediaTag', verbose_name='Tag'),
        ),
        migrations.AddField(
            model_name='file',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='file_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='category',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='dg_category', to='media_manager.Category'),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_gallery_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='fileinline',
            field=models.ManyToManyField(blank=True, default=None, help_text='Gallery File', related_name='gallery_img', to='media_manager.GalleryFileInline', verbose_name='Gallery File'),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='layout',
            field=models.ForeignKey(default=None, help_text='Add layout for the gallery', on_delete=django.db.models.deletion.CASCADE, to='theme.Layout', verbose_name='Gallery Layout'),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='menu_link',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dg_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link'),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dg_theme', to='theme.BaseTheme', verbose_name='Theme'),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='doc_gallery_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='category',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_category_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='category',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_category_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='videogallery',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='video',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='mediatype',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='mediatag',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='image',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='gallery',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='file',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='documentgallery',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='category',
            unique_together=set([('slug', 'title')]),
        ),
    ]
