# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-30 16:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media_manager', '0007_video_featured'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='documentgallery',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='file',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='image',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='mediatag',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='mediatype',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='video',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='videogallery',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
    ]
