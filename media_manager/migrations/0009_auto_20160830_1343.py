# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-30 18:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media_manager', '0008_auto_20160830_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentgallery',
            name='section',
            field=models.CharField(choices=[('nco', 'NCO'), ('neo', 'NEO'), ('pws', 'PWS'), ('corporate', 'Corporate')], default='corporate', max_length=100, verbose_name='Section'),
        ),
        migrations.AddField(
            model_name='file',
            name='section',
            field=models.CharField(choices=[('nco', 'NCO'), ('neo', 'NEO'), ('pws', 'PWS'), ('corporate', 'Corporate')], default='corporate', max_length=100, verbose_name='Section'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='section',
            field=models.CharField(choices=[('nco', 'NCO'), ('neo', 'NEO'), ('pws', 'PWS'), ('corporate', 'Corporate')], default='corporate', max_length=100, verbose_name='Section'),
        ),
        migrations.AddField(
            model_name='image',
            name='section',
            field=models.CharField(choices=[('nco', 'NCO'), ('neo', 'NEO'), ('pws', 'PWS'), ('corporate', 'Corporate')], default='corporate', max_length=100, verbose_name='Section'),
        ),
        migrations.AddField(
            model_name='video',
            name='section',
            field=models.CharField(choices=[('nco', 'NCO'), ('neo', 'NEO'), ('pws', 'PWS'), ('corporate', 'Corporate')], default='corporate', max_length=100, verbose_name='Section'),
        ),
        migrations.AddField(
            model_name='videogallery',
            name='section',
            field=models.CharField(choices=[('nco', 'NCO'), ('neo', 'NEO'), ('pws', 'PWS'), ('corporate', 'Corporate')], default='corporate', max_length=100, verbose_name='Section'),
        ),
    ]
