from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import VideoListView, VideoCreateView, VideoDetailView, VideoUpdateView, VideoAutocomplete

urlpatterns = [
    url(r'^videos/$', VideoListView.as_view(), name='video_list'),
    url(r'^video-autocomplete/$', VideoAutocomplete.as_view(create_field='title'), name='video_autocomplete'),
    url(r'^videos/create/', VideoCreateView.as_view(), name='video_create'),
    url(r'^videos/edit/(?P<slug>[\w-]+)/$', VideoUpdateView.as_view(), name='video_update'),
    # url(r'^videos/delete/(?P<pk>\d+)/', VideoDeleteView.as_view(), name='video_delete'),
    url(r'^videos/(?P<category>[\w-]+)/(?P<slug>[\w-]+)/$', VideoDetailView.as_view(), name='video_detail'),
]
urlpatterns = format_suffix_patterns(urlpatterns)