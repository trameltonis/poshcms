from django import forms
from ckeditor.widgets import CKEditorWidget
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Field, Hidden
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import Post, News, NewsType, Tags, EventTags, Event, Calendar


class EventCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Event
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'content',
        'start_date_time',
        'end_date_time',
        'calendar',
        'menu_link',
        'category',
        'theme',
        'tags',
        'img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class EventEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Event
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'content',
        'start_date_time',
        'end_date_time',
        'calendar',
        'menu_link',
        'category',
        'theme',
        'tags',
        'img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class CalendarCreateForm(forms.ModelForm):
    class Meta:
        model = Calendar
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'active',
        'progress_status',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class CalendarEditForm(forms.ModelForm):
    class Meta:
        model = Calendar
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'active',
        'progress_status',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class NewsCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = News
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'content',
        'menu_link',
        'category',
        'theme',
        'img',
        'sort_order',
        'status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class NewsEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = News
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'content',
        'menu_link',
        'category',
        'theme',
        'img',
        'sort_order',
        'status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class EventTagCreateForm(forms.ModelForm):
    class Meta:
        model = EventTags
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'progress_status',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class EventTagEditForm(forms.ModelForm):
    class Meta:
        model = EventTags
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'progress_status',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class PostCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Post
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'author',
        'title',
        'slug',
        'content',
        'status',
        'menu_link',
        'category',
        'theme',
        'tags',
        'img',
        'sort_order',
        'draft',
        'publish',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class PostEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Post
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'author',
        'title',
        'slug',
        'content',
        'status',
        'menu_link',
        'category',
        'theme',
        'tags',
        'img',
        'sort_order',
        'draft',
        'publish',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )




