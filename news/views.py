from django.contrib.messages.views import SuccessMessageMixin
import urllib
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.contrib.auth.models import User, Group
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponse
from datetime import datetime, timedelta
# from braces.views import LoginRequiredMixin
from django.shortcuts import get_object_or_404, render
from django.http import Http404
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from rest_framework import status
from rest_framework import mixins
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets
from django.views.generic import View
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from news.models import Post, Tags, News, Event, EventTags, Calendar, NewsType
from .serializers import CalendarSerializer, CalendarCreateSerializer, NewsSerializer, NewsCreateSerializer, PostSerializer, PostCreateSerializer, TagSerializer, TagCreateSerializer, EventSerializer, EventCreateSerializer, EventTagSerializer, EventTagCreateSerializer, NewsTypeSerializer, NewsTypeCreateSerializer
from .forms import EventTagCreateForm, EventTagEditForm, EventCreateForm, EventEditForm, NewsCreateForm, NewsEditForm, PostCreateForm, PostEditForm, CalendarCreateForm, CalendarEditForm


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all().order_by('-created_on')
    serializer_class = PostSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tags.objects.all().order_by('-created_on')
    serializer_class = TagSerializer


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by('-created_on')
    serializer_class = NewsSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class AllPublishedNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-created_on")
    serializer_class = NewsSerializer


class ConsultantNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(visibility="private", status='p', active=True)
    serializer_class = NewsSerializer


class GeneralNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="general-news", status='p', active=True)
    serializer_class = NewsSerializer


class CompanyNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="company-news", status='p', active=True)
    serializer_class = NewsSerializer


class ProductNoticeNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="product-notice", status='p', active=True)
    serializer_class = NewsSerializer


class UrgentNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="urgent-news", status='p', active=True)
    serializer_class = NewsSerializer


class NorwexMovementNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="norwex-movement", status='p', active=True)
    serializer_class = NewsSerializer


class EventsNewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="event-news", status='p', active=True)
    serializer_class = NewsSerializer


class NewsArchiveViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="corporate", status='a', active=True)
    serializer_class = NewsSerializer


class NewsTypeViewSet(viewsets.ModelViewSet):
    queryset = NewsType.objects.all().order_by('-created_on')
    serializer_class = NewsTypeSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all().order_by('-created_on')
    serializer_class = EventSerializer


class EventTagViewSet(viewsets.ModelViewSet):
    queryset = EventTags.objects.all().order_by('-created_on')
    serializer_class = EventTagSerializer


class CalendarViewSet(viewsets.ModelViewSet):
    queryset = Calendar.objects.all().order_by('-created_on')
    serializer_class = CalendarSerializer


# News Api endpoint
class NewsList(generics.ListCreateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = [permissions.IsAuthenticated]


class NewsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = [permissions.IsAuthenticated]


class NewsCreate(generics.CreateAPIView):
    serializer_class = NewsCreateSerializer


# Post Api endpoint
class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticated]


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticated]


class PostCreate(generics.CreateAPIView):
    serializer_class = PostCreateSerializer



# Event Tag Api endpoint
class EventTagList(generics.ListCreateAPIView):
    queryset = EventTags.objects.all()
    serializer_class = EventTagSerializer
    permission_classes = [permissions.IsAuthenticated]


class EventTagDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = EventTags.objects.all()
    serializer_class = EventTagSerializer
    permission_classes = [permissions.IsAuthenticated]


class EventTagCreate(generics.CreateAPIView):
    serializer_class = EventTagCreateSerializer


# Event Api endpoint
class EventList(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [permissions.IsAuthenticated]


class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class EventCreate(generics.CreateAPIView):
    serializer_class = EventCreateSerializer


class EventCreateView(CreateView):
    template_name = "news/create_form.html"
    form_class = EventCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(EventCreateView, self).form_valid(form)


class EventUpdateView(UpdateView):
    template_name = "news/edit.html"
    form_class = EventEditForm
    model = Event

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(EventUpdateView, self).form_valid(form)


class EventDetailView(DetailView):
    model = Event
    template_name = "news/_event_detail.html"


class EventListView(ListView):
    model = Event
    template_name = "news/units/_event_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(EventListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class CorporateEventListView(ListView):
    model = Event
    template_name = "news/units/_event_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CorporateEventListView, self).get_queryset(*args, **kwargs).order_by("-created_on").filter(category__slug="corporate", status='p', active=True)
        return qs


class ConsultantEventListView(ListView):
    model = Event
    template_name = "news/units/_event_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ConsultantEventListView, self).get_queryset(*args, **kwargs).order_by("-created_on").filter(category__slug="private", status='p', active=True)
        return qs


class EventTagCreateView(CreateView):
    template_name = "news/create_form.html"
    form_class = EventTagCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(EventTagCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EventTagCreateView, self).dispatch(*args, **kwargs)


class EventTagUpdateView(UpdateView):
    template_name = "news/edit.html"
    form_class = EventTagEditForm
    model = EventTags

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(EventTagUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EventTagUpdateView, self).dispatch(*args, **kwargs)


class EventTagDetailView(DetailView):
    model = EventTags
    template_name = "news/_event_tag_detail.html"


class EventTagListView(ListView):
    model = EventTags
    template_name = "news/units/_event_tag_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(EventTagListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


# Calendar Api endpoint
class CalendarList(generics.ListCreateAPIView):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
    permission_classes = [permissions.IsAuthenticated]


class CalendarDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer


class CalendarCreate(generics.CreateAPIView):
    serializer_class = CalendarCreateSerializer


class NewsCreateView(CreateView):
    template_name = "news/create_form.html"
    form_class = NewsCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(NewsCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NewsCreateView, self).dispatch(*args, **kwargs)


class NewsUpdateView(UpdateView):
    template_name = "news/edit.html"
    form_class = NewsEditForm
    model = News

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(NewsUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(NewsUpdateView, self).dispatch(*args, **kwargs)


class NewsDetailView(DetailView):
    model = News
    template_name = "news/_news_detail.html"


class NewsListView(ListView):
    model = News
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(NewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start")
        return qs


class AllNewsListView(ListView):
    model = News
    list_title = "News From Norwex"
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(AllNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(publish_start__lte=timezone.now(), status='p', active=True, visibility='public')
        return qs

    def get_context_data(self, **kwargs):
        context = super(AllNewsListView, self).get_context_data(**kwargs)
        all_list = super(AllNewsListView, self).get_queryset(**kwargs).order_by("-publish_start").filter(status='p', active=True)
        paginator = Paginator(all_list, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            all_list = paginator.page(page)
        except PageNotAnInteger:
            all_list = paginator.page(1)
        except EmptyPage:
            all_list = paginator.page(paginator.num_pages)

        context['all_list'] = all_list
        return context


class CompanyNewsListView(ListView):
    model = News
    page_title = _('Company News')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(CompanyNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="company-news", status='p', active=True, visibility='public')
        return qs


class GeneralNewsListView(ListView):
    model = News
    page_title = _('General News')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(GeneralNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="general-news", status='p', active=True, visibility='public')
        return qs


class UrgentNewsListView(ListView):
    model = News
    page_title = _('Urgent News')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(UrgentNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="urgent-news", status='p', active=True, visibility='public')
        return qs


class ProductNoticeNewsListView(ListView):
    model = News
    page_title = _('Product Notice News')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(ProductNoticeNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="product-notice", status='p', active=True, visibility='public')
        return qs


class EventsNewsListView(ListView):
    model = News
    page_title = _('Events News')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(EventsNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="event-news", status='p', active=True, visibility='public')
        return qs


class NorwexMovementNewsListView(ListView):
    model = News
    page_title = _('Norwex Movement News')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(NorwexMovementNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="norwex-movement", status='p', active=True, visibility='public')
        return qs


class ConsultantNewsListView(ListView):
    model = News
    page_title = _('News from Norwex')
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(ConsultantNewsListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(visibility="private", status='p', active=True)
        return qs

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ConsultantNewsListView, self).dispatch(*args, **kwargs)


class CompanyNewsArchiveListView(ListView):
    model = News
    page_title = _('News Archive')
    template_name = "news/units/_news_archive_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(CompanyNewsArchiveListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(status='a', active=True, visibility='public')
        return qs


class ConsultantNewsArchiveListView(ListView):
    model = News
    template_name = "news/units/_news_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(ConsultantNewsArchiveListView, self).get_queryset(*args, **kwargs).order_by("-publish_start").filter(news_type__slug="corporate", status='a', active=True, visibility='private')
        return qs

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ConsultantNewsArchiveListView, self).dispatch(*args, **kwargs)


class PostCreateView(CreateView):
    template_name = "news/create_form.html"
    form_class = PostCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(PostCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PostCreateView, self).dispatch(*args, **kwargs)


class PostUpdateView(UpdateView):
    template_name = "news/edit.html"
    form_class = PostEditForm
    model = Post

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(PostUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PostUpdateView, self).dispatch(*args, **kwargs)


class PostDetailView(DetailView):
    model = Post
    template_name = "news/_post_detail.html"


class PostListView(ListView):
    model = Post
    template_name = "news/units/_post_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(PostListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class CalendarCreateView(CreateView):
    template_name = "news/create_form.html"
    form_class = CalendarCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CalendarCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CalendarCreateView, self).dispatch(*args, **kwargs)


class CalendarUpdateView(UpdateView):
    template_name = "news/edit.html"
    form_class = CalendarEditForm
    model = Calendar

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(CalendarUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CalendarUpdateView, self).dispatch(*args, **kwargs)


class CalendarDetailView(DetailView):
    model = Calendar
    template_name = "news/_calendar_detail.html"


class CalendarListView(ListView):
    model = Calendar
    template_name = "news/units/_calendar_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CalendarListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class CorporateCalendarListView(ListView):
    model = Calendar
    template_name = "news/units/_calendar_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CorporateCalendarListView, self).get_queryset(*args, **kwargs).order_by("-created_on").filter(visibility="public", active=True)
        return qs


class ConsultantCalendarListView(ListView):
    model = Calendar
    template_name = "news/units/_calendar_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ConsultantCalendarListView, self).get_queryset(*args, **kwargs).order_by("-created_on").filter(visibility="private", active=True)
        return qs