from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import *
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from .models import News, NewsType, Post, PostView, Event, EventTags, Category, Tags, Calendar


@admin.register(Category)
class CategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(NewsType)
class NewsTypeAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    # group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug','img', 'updated')
    search_fields = ['title']
    list_filter = ['active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(News)
class NewsAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'content',
                'news_type',
                'menu_link',
                'theme',
                # 'category',
                # 'menu',
                'designation',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    filter_horizontal = ('social',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
        'seo_title': ('title',)
    }
    list_display = (
        # 'image_img',
        'title',
        'language_selectors',
        'section',
        'news_type',
        'designation',
        'publish_start',
        'publish_end',
        'status',
        'visibility',
        'progress_status',
        'active',
        'updated_by',
        'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',  'status', 'featured']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_us_only, make_ca_only, make_us_ca_only, make_global, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
    radio_fields = {'seo_robot': admin.VERTICAL}


@admin.register(Post)
class PostAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()


    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'author',
                'title',
                'url',
                'content',
                'menu_link',
                'category',
                'theme',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    filter_horizontal = ('social',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status', 'status', 'visibility', 'created_by', 'updated_by', 'updated')
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_us_only, make_ca_only, make_us_ca_only, make_global, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Event)
class EventAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                # 'author',
                'section',
                'title',
                'url',
                'content',
                'start_date_time',
                'end_date_time',
                'calendar',
                'menu_link',
                'theme',
                'tags',
                'img',
                'designation',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    filter_horizontal = ('social', 'tags',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
        'seo_title': ('title',),
        'seo_keywords': ('slug',)
    }
    list_display = (
        'title',
        'start_date_time',
        'end_date_time',
        'publish_start',
        'publish_end',
        'section',
        'progress_status',
        'status',
        'visibility',
        'active',
        'updated_by',
        'updated')
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'start_date_time', 'end_date_time', 'publish_start',  'publish_end',  'status', 'featured']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_us_only, make_ca_only, make_us_ca_only, make_global, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Tags)
class TagAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'visibility', 'progress_status',)
    search_fields = ['title', 'slug']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(EventTags)
class EventTagsAdmin(ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    # group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status',)
    search_fields = ['title', 'slug']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Calendar)
class CalendarAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    # group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()
    filter_horizontal = ('social',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'section', 'progress_status', 'updated')
    search_fields = ['title', 'slug']
    list_filter = ['section', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]




