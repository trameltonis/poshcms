from modeltranslation.translator import translator, register, TranslationOptions
from .models import News, NewsType, Post, Event, EventTags, Tags, Category, Calendar


@register(News)
class NewsTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]

@register(NewsType)
class NewsTypeTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'img',
        ]


@register(Post)
class PostTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]


@register(Event)
class EventTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        'seo_title',
        # 'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]


@register(Calendar)
class CalendarTranslationOptions(TranslationOptions):
    fields = [
        'title',
        ]

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]

@register(Tags)
class TagTranslationOptions(TranslationOptions):
    fields = [
        'title',
        ]


@register(EventTags)
class EventTagsTranslationOptions(TranslationOptions):
    fields = [
        'title',
        ]



