from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from datetime import datetime
from filer.fields.image import FilerImageField
from django.conf import settings
from django.core.urlresolvers import reverse
from core.models import *
from theme.models import *
from menu_manager.models import *
from helpers.utils import *


class Tags(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=100,
        verbose_name=_('Name')
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_tag_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_tag_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("tag_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')


class Category(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the category."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("news_category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Calendar(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Name"),
        help_text=_("Calendar name"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="calendar_menu_link"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        related_name="calendar_theme"
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Calendar Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="calendar_social"
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="calendar_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="calendar_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("calendar_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Calendar')
        verbose_name_plural = _('Calendars')


class NewsType(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Name"),
        help_text=_("Menu Category name"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Keyword'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="newstype"
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Menu Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="newstype_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="newstype_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("menu_category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('News Type')
        verbose_name_plural = _('News Types')


class News(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("News title"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the page."),
        default=None
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="news_menu_link"
    )
    # category = models.ForeignKey(
    #     Category,
    #     blank=False,
    #     null=False,
    #     default=None,
    #     related_name="news_category"
    # )
    news_type = models.ForeignKey(
        NewsType,
        blank=False,
        null=False,
        default=None,
        verbose_name=_("New Type"),
        help_text=_("Select type of news"),
        related_name="news_type"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        related_name="news_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        default=None,
        related_name="news_img"
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    designation = models.CharField(
        max_length=60,
        null=True,
        blank=True,
        choices=DESIGNATION,
        verbose_name=_("Designation"),
        help_text=_("Set where you want content to be available.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="news_social"
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
        related_name="news_seo_og_img"
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("news_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('News Content')
        verbose_name_plural = _('News')


class PostManager(models.Manager):
    def all(self, *args, **kwargs):
        return super(PostManager, self).filter(draft=False).filter(publish__lte=timezone.now())


class Post(models.Model):
    #ct = get_content_type
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        default=None
    )
    title = models.CharField(
        max_length=255,
        default=None,
        blank=False,
        null=False,
        verbose_name=_("Post Title"),
        help_text=_("Title of Post")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    content = models.TextField(
        null=True,
        blank=True,
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="post_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="post_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        related_name="post_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="post_img",
        verbose_name=_("Image")
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="post_social"
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
        related_name="post_seo_og_img"
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="post_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="post_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    # objects = PostManager()

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')


class PostView(models.Model):
    #ct = get_content_type
    post = models.ForeignKey(
        "Post",
        related_name='postviews'
    )
    ip = models.CharField(
        max_length=40
    )
    session = models.CharField(
        max_length=40
    )
    created = models.DateTimeField(
        default=datetime.now,
    )

    def __str__(self):
        return str(self.post)

    class Meta:
        verbose_name = _('Post View')
        verbose_name_plural = _('Post Views')


# class EventTags(Tag):
#     def __str__(self):
#         return self.title
#
#     def get_absolute_url(self):
#         return reverse("event_tag_detail", kwargs={'slug': self.slug})
#
#     class Meta:
#         verbose_name = _('Event Tag')
#         verbose_name_plural = _('Event Tags')


class EventTags(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=100,
        verbose_name=_('Name')
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="evtag_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="evtag_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("event_tag_detail", kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Event Tag')
        verbose_name_plural = _('Event Tags')

# class EventManager(models.Manager):
#     def all(self, *args, **kwargs):
#         return super(EventManager, self).filter(draft=False).filter(publish__lte=timezone.now())


class Event(models.Model):
    #ct = get_content_type
    title = models.CharField(
        max_length=255,
        default=None,
        blank=False,
        null=False,
        verbose_name=_("Event Title"),
        help_text=_("Title of Event")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    content = models.TextField(
        null=True,
        blank=True,
    )

    start_date_time = models.DateTimeField(
        verbose_name=_("Event Start Date/Time"),
        auto_now_add=False,
        auto_now=False,
        blank=True,
        null=True,
    )
    end_date_time = models.DateTimeField(
        verbose_name=_("Event End Date/Time"),
        auto_now_add=False,
        auto_now=False,
        blank=True,
        null=True,
    )
    calendar = models.ForeignKey(
        Calendar,
        blank=False,
        null=False,
        default=None,
        verbose_name=_("Calendar"),
        help_text=_("Select calendar"),
        related_name="calendar"
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="event_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="event_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        related_name="event_theme"
    )
    tags = models.ManyToManyField(
        'EventTags',
        related_name='event',
        verbose_name=_('Event Tag'),
        blank=True
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="event_img",
        verbose_name=_("Image")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    designation = models.CharField(
        max_length=60,
        null=True,
        blank=True,
        choices=DESIGNATION,
        verbose_name=_("Designation"),
        help_text=_("Set where you want content to be available.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="event_social"
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
        related_name="event_seo_og_img"
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="event_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="event_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    # objects = EventManager()

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return reverse("event_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Event')
        verbose_name_plural = _('Events')