from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from rest_framework.urlpatterns import format_suffix_patterns

from .views import NewsCreateView, NewsUpdateView, NewsListView, NewsDetailView, EventTagCreateView, EventTagUpdateView, EventTagListView, EventTagDetailView, EventCreateView, EventUpdateView, EventListView, EventDetailView, CalendarCreateView,CalendarUpdateView, CalendarDetailView, CalendarListView, PostCreateView, PostListView, PostUpdateView, PostDetailView, CompanyNewsListView, CompanyNewsArchiveListView, ConsultantNewsListView, ConsultantNewsArchiveListView, CorporateEventListView, ConsultantEventListView, ConsultantCalendarListView, CorporateCalendarListView, GeneralNewsListView, UrgentNewsListView, NorwexMovementNewsListView, ProductNoticeNewsListView, EventsNewsListView, AllNewsListView


urlpatterns = [
    url(r'^news-admin/$', NewsListView.as_view(), name='admin_news_list'),
    url(r'^news/$', AllNewsListView.as_view(), name='all_news_list'),
    url(r'^news/company/$', CompanyNewsListView.as_view(), name='company_news_list'),
    url(r'^news/general/$', GeneralNewsListView.as_view(), name='general_news_list'),
    url(r'^news/product-notice/$', ProductNoticeNewsListView.as_view(), name='product_notice_news_list'),
    url(r'^news/urgent/$', UrgentNewsListView.as_view(), name='urgent_news_list'),
    url(r'^news/norwex-movement/$', NorwexMovementNewsListView.as_view(), name='norwex_movement_news_list'),
    url(r'^news/events/$', EventsNewsListView.as_view(), name='events_news_list'),
    url(r'^consultant/news/$', ConsultantNewsListView.as_view(), name='consultant_news_list'),
    url(r'^news/archive/$', CompanyNewsArchiveListView.as_view(), name='news_archive_list'),
    url(r'^consultant/news/archive/$', ConsultantNewsArchiveListView.as_view(), name='consultant_news_archive_list'),
    url(r'^news/create/', NewsCreateView.as_view(), name='news_create'),
    url(r'^news/edit/(?P<slug>[-a-zA-Z0-9_]+)/', NewsUpdateView.as_view(), name='news_update'),
    # url(r'^news/delete/(?P<pk>\d+)/', NewsDeleteView.as_view(), name='news_delete'),
    url(r'^news/(?P<slug>[-a-zA-Z0-9_]+)/$', NewsDetailView.as_view(), name='news_detail'),

    url(r'^events/tags/$', EventTagListView.as_view(), name='event_tag_list'),
    url(r'^events/tags/create/', EventTagCreateView.as_view(), name='event_tag_create'),
    url(r'^events/tags/edit/(?P<slug>[-a-zA-Z0-9_]+)/', EventTagUpdateView.as_view(), name='event_tag_update'),
    # url(r'^event/tags/delete/(?P<pk>\d+)/', EventTagDeleteView.as_view(), name='event_tag_delete'),
    url(r'^events/tags/(?P<slug>[-a-zA-Z0-9_]+)/$', EventTagDetailView.as_view(), name='event_tag_detail'),

    url(r'^events/$', CorporateEventListView.as_view(), name='corporate_event_list'),
    url(r'^consultant/events/$', ConsultantEventListView.as_view(), name='consultant_event_list'),
    url(r'^events/admin$', EventListView.as_view(), name='event_list'),
    url(r'^events/create/', EventCreateView.as_view(), name='event_create'),
    url(r'^events/edit/(?P<slug>[-a-zA-Z0-9_]+)/', EventUpdateView.as_view(), name='event_update'),
    # url(r'^event/delete/(?P<pk>\d+)/', EventDeleteView.as_view(), name='event_delete'),
    url(r'^events/(?P<slug>[-a-zA-Z0-9_]+)/$', EventDetailView.as_view(), name='event_detail'),

    url(r'^posts/$', PostListView.as_view(), name='post_list'),
    url(r'^posts/create/', PostCreateView.as_view(), name='post_create'),
    url(r'^posts/edit/(?P<slug>[-a-zA-Z0-9_]+)/', PostUpdateView.as_view(), name='post_update'),
    # url(r'^posts/delete/(?P<pk>\d+)/', PostDeleteView.as_view(), name='post_delete'),
    url(r'^posts/(?P<slug>[-a-zA-Z0-9_]+)/$', PostDetailView.as_view(), name='post_detail'),

    url(r'^calendar/admin$', CalendarListView.as_view(), name='calendar_list'),
    url(r'^calendar/$', CorporateCalendarListView.as_view(), name='corporate_calendar_list'),
    url(r'^consultant/calendar/$', ConsultantCalendarListView.as_view(), name='consultant_calendar_list'),
    url(r'^calendar/create/', CalendarCreateView.as_view(), name='calendar_create'),
    url(r'^calendar/edit/(?P<slug>[-a-zA-Z0-9_]+)/', CalendarUpdateView.as_view(), name='calendar_update'),
    # url(r'^calendar/delete/(?P<pk>\d+)/', CalendarDeleteView.as_view(), name='calendar_delete'),
    url(r'^calendar/(?P<slug>[-a-zA-Z0-9_]+)/$', CalendarDetailView.as_view(), name='calendar_detail'),

]

urlpatterns = format_suffix_patterns(urlpatterns)

