from django.contrib.auth.models import User, Group
from rest_framework import serializers
from core.models import SocialMedia, Locale, Format, AccountGroup
from cmf.models import Page, Category, Tags, Resource, Email, Dashboard
from menu_manager.models import Menu, MenuCategory
from news.models import Post, Tags, PostView, News, NewsType, Event, EventTags, Calendar
from cmf.serializers import CategoryCreateSerializer, CategorySerializer, TagSerializer, TagCreateSerializer
from menu_manager.serializers import MenuSerializer
from media_manager.models import File, Image, Gallery, MediaTag, MediaType


class NewsTypeSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = NewsType
        fields = '__all__'
        depth = 3


class NewsTypeCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = NewsType
        fields = '__all__'
        depth = 3


class NewsSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    news_type = NewsTypeSerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)

    class Meta:
        model = News
        fields = '__all__'
        # exclude = ['created_by', 'updated_by']
        depth = 3


class NewsCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    news_type = NewsTypeSerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)

    class Meta:
        model = News
        fields = '__all__'
        depth = 3


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        tags = TagSerializer(many=True)
        fields = '__all__'
        depth = 3


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        tags = TagSerializer(many=True)
        fields = '__all__'
        depth = 3


class EventTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTags
        fields = '__all__'
        depth = 3


class EventTagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTags
        fields = '__all__'
        depth = 3


class EventSerializer(serializers.ModelSerializer):
    # calendar = CalendarSerializer(many=False, read_only=True)
    class Meta:
        model = Event
        tags = EventTagSerializer(many=True)
        fields = '__all__'
        depth = 3


class EventCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        tags = EventTagSerializer(many=True)
        fields = '__all__'
        depth = 3


class CalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        fields = '__all__'
        depth = 3


class CalendarCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        fields = '__all__'
        depth = 3




