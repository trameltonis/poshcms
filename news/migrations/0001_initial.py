# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-23 19:04
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('menu_manager', '0001_initial'),
        ('filer', '0005_auto_20160624_0202'),
        ('core', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Calendar name', max_length=255, verbose_name='Name')),
                ('title_en_us', models.CharField(help_text='Calendar name', max_length=255, null=True, verbose_name='Name')),
                ('title_es_us', models.CharField(help_text='Calendar name', max_length=255, null=True, verbose_name='Name')),
                ('title_fr_ca', models.CharField(help_text='Calendar name', max_length=255, null=True, verbose_name='Name')),
                ('title_en_ca', models.CharField(help_text='Calendar name', max_length=255, null=True, verbose_name='Name')),
                ('slug', models.SlugField(help_text='Automatically generated from name field if left empty.', unique=True, verbose_name='Slug')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('active', models.BooleanField(default=True, help_text='Calendar Status.', verbose_name='Enabled')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
            ],
            options={
                'verbose_name_plural': 'Calendars',
                'verbose_name': 'Calendar',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Title', max_length=255, unique=True, verbose_name='Title')),
                ('title_en_us', models.CharField(help_text='Title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('title_es_us', models.CharField(help_text='Title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('title_fr_ca', models.CharField(help_text='Title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('title_en_ca', models.CharField(help_text='Title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('slug', models.SlugField(help_text='Automatically generated from category title if left empty.', unique=True, verbose_name='Slug')),
                ('description', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_en_us', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_es_us', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_fr_ca', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_en_ca', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published'), ('a', 'Archive'), ('w', 'Withdrawn')], default='p', max_length=1)),
                ('publish_start', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Publish Start Date')),
                ('publish_end', models.DateTimeField(blank=True, null=True, verbose_name='Publish End Date')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('active', models.BooleanField(default=True, help_text='Category Status.', verbose_name='Active')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
            ],
            options={
                'verbose_name_plural': 'Categories',
                'verbose_name': 'Category',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default=None, help_text='Title of Event', max_length=255, verbose_name='Event Title')),
                ('title_en_us', models.CharField(default=None, help_text='Title of Event', max_length=255, null=True, verbose_name='Event Title')),
                ('title_es_us', models.CharField(default=None, help_text='Title of Event', max_length=255, null=True, verbose_name='Event Title')),
                ('title_fr_ca', models.CharField(default=None, help_text='Title of Event', max_length=255, null=True, verbose_name='Event Title')),
                ('title_en_ca', models.CharField(default=None, help_text='Title of Event', max_length=255, null=True, verbose_name='Event Title')),
                ('slug', models.SlugField(unique=True)),
                ('content', models.TextField(blank=True, null=True)),
                ('content_en_us', models.TextField(blank=True, null=True)),
                ('content_es_us', models.TextField(blank=True, null=True)),
                ('content_fr_ca', models.TextField(blank=True, null=True)),
                ('content_en_ca', models.TextField(blank=True, null=True)),
                ('start_date_time', models.DateTimeField(blank=True, null=True, verbose_name='Event Start Date/Time')),
                ('end_date_time', models.DateTimeField(blank=True, null=True, verbose_name='Event End Date/Time')),
                ('sort_order', models.PositiveIntegerField(blank=True, default=0, help_text='Content Sort Order.', null=True, verbose_name='Sort Order')),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published'), ('a', 'Archive'), ('w', 'Withdrawn')], default='p', max_length=1)),
                ('publish_start', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Publish Start Date')),
                ('publish_end', models.DateTimeField(blank=True, null=True, verbose_name='Publish End Date')),
                ('active', models.BooleanField(default=True, help_text='Publication Status.', verbose_name='Enabled')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('designation', models.CharField(blank=True, choices=[('us', 'US Only'), ('ca', 'Canada Only'), ('us-ca', 'US and Canada Only'), ('global', 'Global')], help_text='Set where you want content to be available.', max_length=60, null=True, verbose_name='Designation')),
                ('featured', models.BooleanField(default=False, help_text='Enables page to be featured.', verbose_name='Featured')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('seo_title', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_en_us', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_es_us', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_fr_ca', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_en_ca', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_keywords', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_en_us', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_es_us', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_fr_ca', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_en_ca', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_description', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_en_us', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_es_us', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_fr_ca', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_en_ca', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_generator', models.CharField(blank=True, help_text='SEO Generator', max_length=255, null=True, verbose_name='Generator')),
                ('seo_copyright', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_en_us', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_es_us', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_fr_ca', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_en_ca', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_canonical_url', models.CharField(blank=True, help_text='SEO Canonical Url', max_length=255, null=True, verbose_name='Canonical Url')),
                ('seo_short_url', models.URLField(blank=True, help_text='SEO Short link url', max_length=255, null=True, verbose_name='Shortlink Url')),
                ('seo_publisher_url', models.CharField(blank=True, help_text='SEO Publisher Url', max_length=255, null=True, verbose_name='Publisher Url')),
                ('seo_robot', models.CharField(blank=True, choices=[('no-indexing', 'Prevent search engines from indexing this page'), ('no-follow', 'Prevent search engines from following links on this page')], help_text='Set how search engines related with content.', max_length=255, null=True, verbose_name='Search Engines & Robots')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
            ],
            options={
                'verbose_name_plural': 'Events',
                'verbose_name': 'Event',
            },
        ),
        migrations.CreateModel(
            name='EventTags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Name')),
                ('title_en_us', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('title_es_us', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('title_fr_ca', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('title_en_ca', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('slug', models.SlugField(default=None, help_text='Automatically generated from name field if left empty.', unique=True, verbose_name='Slug')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
            ],
            options={
                'verbose_name_plural': 'Event Tags',
                'verbose_name': 'Event Tag',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='News title', max_length=255, unique=True, verbose_name='Title')),
                ('title_en_us', models.CharField(help_text='News title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('title_es_us', models.CharField(help_text='News title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('title_fr_ca', models.CharField(help_text='News title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('title_en_ca', models.CharField(help_text='News title', max_length=255, null=True, unique=True, verbose_name='Title')),
                ('slug', models.SlugField(help_text='Automatically generated from title if left empty.', unique=True, verbose_name='Slug')),
                ('content', models.TextField(blank=True, default=None, help_text='More information about the page.', null=True, verbose_name='Description')),
                ('content_en_us', models.TextField(blank=True, default=None, help_text='More information about the page.', null=True, verbose_name='Description')),
                ('content_es_us', models.TextField(blank=True, default=None, help_text='More information about the page.', null=True, verbose_name='Description')),
                ('content_fr_ca', models.TextField(blank=True, default=None, help_text='More information about the page.', null=True, verbose_name='Description')),
                ('content_en_ca', models.TextField(blank=True, default=None, help_text='More information about the page.', null=True, verbose_name='Description')),
                ('sort_order', models.IntegerField(blank=True, default=0, help_text='Content Sort Order.', null=True, verbose_name='Sort Order')),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published'), ('a', 'Archive'), ('w', 'Withdrawn')], default='p', max_length=1)),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('designation', models.CharField(blank=True, choices=[('us', 'US Only'), ('ca', 'Canada Only'), ('us-ca', 'US and Canada Only'), ('global', 'Global')], help_text='Set where you want content to be available.', max_length=60, null=True, verbose_name='Designation')),
                ('publish_start', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Publish Start Date')),
                ('publish_end', models.DateTimeField(blank=True, null=True, verbose_name='Publish End Date')),
                ('active', models.BooleanField(default=True, help_text='Publication Status.', verbose_name='Enabled')),
                ('featured', models.BooleanField(default=False, help_text='Enables page to be featured.', verbose_name='Featured')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('seo_title', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_en_us', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_es_us', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_fr_ca', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_en_ca', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_keywords', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_en_us', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_es_us', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_fr_ca', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_en_ca', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_description', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_en_us', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_es_us', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_fr_ca', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_en_ca', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_generator', models.CharField(blank=True, help_text='SEO Generator', max_length=255, null=True, verbose_name='Generator')),
                ('seo_copyright', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_en_us', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_es_us', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_fr_ca', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_en_ca', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_canonical_url', models.CharField(blank=True, help_text='SEO Canonical Url', max_length=255, null=True, verbose_name='Canonical Url')),
                ('seo_short_url', models.URLField(blank=True, help_text='SEO Short link url', max_length=255, null=True, verbose_name='Shortlink Url')),
                ('seo_publisher_url', models.CharField(blank=True, help_text='SEO Publisher Url', max_length=255, null=True, verbose_name='Publisher Url')),
                ('seo_robot', models.CharField(blank=True, choices=[('no-indexing', 'Prevent search engines from indexing this page'), ('no-follow', 'Prevent search engines from following links on this page')], help_text='Set how search engines related with content.', max_length=255, null=True, verbose_name='Search Engines & Robots')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
            ],
            options={
                'verbose_name_plural': 'News',
                'verbose_name': 'News Content',
            },
        ),
        migrations.CreateModel(
            name='NewsType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Menu Category name', max_length=255, verbose_name='Name')),
                ('title_en_us', models.CharField(help_text='Menu Category name', max_length=255, null=True, verbose_name='Name')),
                ('title_es_us', models.CharField(help_text='Menu Category name', max_length=255, null=True, verbose_name='Name')),
                ('title_fr_ca', models.CharField(help_text='Menu Category name', max_length=255, null=True, verbose_name='Name')),
                ('title_en_ca', models.CharField(help_text='Menu Category name', max_length=255, null=True, verbose_name='Name')),
                ('slug', models.SlugField(help_text='Automatically generated from name field if left empty.', unique=True, verbose_name='Keyword')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('active', models.BooleanField(default=True, help_text='Menu Category Status.', verbose_name='Enabled')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
            ],
            options={
                'verbose_name_plural': 'News Types',
                'verbose_name': 'News Type',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default=None, help_text='Title of Post', max_length=255, verbose_name='Post Title')),
                ('title_en_us', models.CharField(default=None, help_text='Title of Post', max_length=255, null=True, verbose_name='Post Title')),
                ('title_es_us', models.CharField(default=None, help_text='Title of Post', max_length=255, null=True, verbose_name='Post Title')),
                ('title_fr_ca', models.CharField(default=None, help_text='Title of Post', max_length=255, null=True, verbose_name='Post Title')),
                ('title_en_ca', models.CharField(default=None, help_text='Title of Post', max_length=255, null=True, verbose_name='Post Title')),
                ('slug', models.SlugField(unique=True)),
                ('content', models.TextField(blank=True, null=True)),
                ('content_en_us', models.TextField(blank=True, null=True)),
                ('content_es_us', models.TextField(blank=True, null=True)),
                ('content_fr_ca', models.TextField(blank=True, null=True)),
                ('content_en_ca', models.TextField(blank=True, null=True)),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published'), ('a', 'Archive'), ('w', 'Withdrawn')], default='p', max_length=1)),
                ('publish_start', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Publish Start Date')),
                ('publish_end', models.DateTimeField(blank=True, null=True, verbose_name='Publish End Date')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('active', models.BooleanField(default=True, help_text='Publication Status.', verbose_name='Enabled')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('sort_order', models.PositiveIntegerField(blank=True, default=0, help_text='Content Sort Order.', null=True, verbose_name='Sort Order')),
                ('featured', models.BooleanField(default=False, help_text='Enables page to be featured.', verbose_name='Featured')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('seo_title', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_en_us', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_es_us', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_fr_ca', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_title_en_ca', models.CharField(blank=True, help_text='SEO title', max_length=255, null=True, verbose_name='SEO Title')),
                ('seo_keywords', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_en_us', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_es_us', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_fr_ca', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_keywords_en_ca', models.TextField(blank=True, default=None, help_text='SEO keywords for page.', null=True, verbose_name='SEO Keywords')),
                ('seo_description', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_en_us', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_es_us', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_fr_ca', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_description_en_ca', models.TextField(blank=True, default=None, help_text='SEO description for page.', null=True, verbose_name='SEO Description')),
                ('seo_generator', models.CharField(blank=True, help_text='SEO Generator', max_length=255, null=True, verbose_name='Generator')),
                ('seo_copyright', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_en_us', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_es_us', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_fr_ca', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_copyright_en_ca', models.CharField(blank=True, help_text='SEO Copyright', max_length=255, null=True, verbose_name='Copyright')),
                ('seo_canonical_url', models.CharField(blank=True, help_text='SEO Canonical Url', max_length=255, null=True, verbose_name='Canonical Url')),
                ('seo_short_url', models.URLField(blank=True, help_text='SEO Short link url', max_length=255, null=True, verbose_name='Shortlink Url')),
                ('seo_publisher_url', models.CharField(blank=True, help_text='SEO Publisher Url', max_length=255, null=True, verbose_name='Publisher Url')),
                ('seo_robot', models.CharField(blank=True, choices=[('no-indexing', 'Prevent search engines from indexing this page'), ('no-follow', 'Prevent search engines from following links on this page')], help_text='Set how search engines related with content.', max_length=255, null=True, verbose_name='Search Engines & Robots')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
                ('author', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='post_category', to='news.Category')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_created_by', to=settings.AUTH_USER_MODEL)),
                ('img', filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_img', to='filer.Image', verbose_name='Image')),
                ('img_en_ca', filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_img', to='filer.Image', verbose_name='Image')),
                ('img_en_us', filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_img', to='filer.Image', verbose_name='Image')),
                ('img_es_us', filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_img', to='filer.Image', verbose_name='Image')),
                ('img_fr_ca', filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_img', to='filer.Image', verbose_name='Image')),
                ('menu_link', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_menu_link', to='menu_manager.MenuLink', verbose_name='Menu Link')),
                ('seo_og_img', filer.fields.image.FilerImageField(blank=True, help_text='Image displayed when content is shared on social media', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_seo_og_img', to='filer.Image', verbose_name='SEO Open Graph Image')),
                ('seo_og_img_en_ca', filer.fields.image.FilerImageField(blank=True, help_text='Image displayed when content is shared on social media', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_seo_og_img', to='filer.Image', verbose_name='SEO Open Graph Image')),
                ('seo_og_img_en_us', filer.fields.image.FilerImageField(blank=True, help_text='Image displayed when content is shared on social media', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_seo_og_img', to='filer.Image', verbose_name='SEO Open Graph Image')),
                ('seo_og_img_es_us', filer.fields.image.FilerImageField(blank=True, help_text='Image displayed when content is shared on social media', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_seo_og_img', to='filer.Image', verbose_name='SEO Open Graph Image')),
                ('seo_og_img_fr_ca', filer.fields.image.FilerImageField(blank=True, help_text='Image displayed when content is shared on social media', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='post_seo_og_img', to='filer.Image', verbose_name='SEO Open Graph Image')),
                ('social', models.ManyToManyField(blank=True, related_name='post_social', to='core.SocialMedia', verbose_name='Share')),
            ],
            options={
                'verbose_name_plural': 'Posts',
                'verbose_name': 'Post',
            },
        ),
        migrations.CreateModel(
            name='PostView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip', models.CharField(max_length=40)),
                ('session', models.CharField(max_length=40)),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='postviews', to='news.Post')),
            ],
            options={
                'verbose_name_plural': 'Post Views',
                'verbose_name': 'Post View',
            },
        ),
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Name')),
                ('title_en_us', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('title_es_us', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('title_fr_ca', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('title_en_ca', models.CharField(max_length=100, null=True, verbose_name='Name')),
                ('slug', models.SlugField(default=None, help_text='Automatically generated from name field if left empty.', unique=True, verbose_name='Slug')),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news_tag_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='news_tag_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Tags',
                'verbose_name': 'Tag',
            },
        ),
    ]
