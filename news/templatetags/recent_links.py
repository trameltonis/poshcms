from django.template import Library, Node
from django.utils.translation import ugettext_lazy as _
from news.models import News

register = Library()


class LatestNewsNode(Node):
    def render(self, context):
        context['recent_links'] = News.objects.all()[:5]
        return _('No links found')


def get_latest_links(parser, token):
    return LatestNewsNode()
get_latest_links = register.tag(get_latest_links)