from rest_framework import generics
from django.views import generic as cbv
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from ..models import *
from .serializers import *


class NewsFilter(filters.FilterSet):
    class Meta:
        model = News
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'menu_link',
            # 'category',
            'news_type',
            'designation',
            'featured',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


class PostFilter(filters.FilterSet):
    class Meta:
        model = Post
        fields = [
            'slug',
            'status',
            'active',
            'featured',
            'visibility',
            'menu_link',
            'category',
            'progress_status',
            'publish_start',
            'publish_end',
            'id'
        ]


class EventTagsFilter(filters.FilterSet):
    class Meta:
        model = EventTags
        fields = [
            'slug',
            'visibility',
            'progress_status',
            'id'
        ]


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all().order_by('-created_on')
    serializer_class = PostSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tags.objects.all().order_by('-created_on')
    serializer_class = TagSerializer


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by('-created_on')
    serializer_class = NewsSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


# class AllPublishedNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-created_on")
#     serializer_class = NewsSerializer
#
#
# class ConsultantNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(visibility="private", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class GeneralNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="general-news", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class CompanyNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="company-news", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class ProductNoticeNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="product-notice", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class UrgentNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="urgent-news", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class NorwexMovementNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="norwex-movement", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class EventsNewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="event-news", status='p', active=True)
#     serializer_class = NewsSerializer
#
#
# class NewsArchiveViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all().order_by("-publish_start").filter(news_type__slug="corporate", status='a', active=True)
#     serializer_class = NewsSerializer
#
#
# class NewsTypeViewSet(viewsets.ModelViewSet):
#     queryset = NewsType.objects.all().order_by('-created_on')
#     serializer_class = NewsTypeSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all().order_by('-created_on')
    serializer_class = EventSerializer


class EventTagViewSet(viewsets.ModelViewSet):
    queryset = EventTags.objects.all().order_by('-created_on')
    serializer_class = EventTagSerializer


class CalendarViewSet(viewsets.ModelViewSet):
    queryset = Calendar.objects.all().order_by('-created_on')
    serializer_class = CalendarSerializer




















