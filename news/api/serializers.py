from ..models import *
from rest_framework import serializers
from cmf.api.serializers import *
from menu_manager.api.serializers import *



class NewsTypeSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = NewsType
        fields = '__all__'
        depth = 3


class NewsTypeCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = NewsType
        fields = '__all__'
        depth = 3


class NewsSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    news_type = NewsTypeSerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)

    class Meta:
        model = News
        fields = '__all__'
        # exclude = ['created_by', 'updated_by']
        depth = 3


class NewsCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    news_type = NewsTypeSerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)

    class Meta:
        model = News
        fields = '__all__'
        depth = 3


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        tags = TagSerializer(many=True)
        fields = '__all__'
        depth = 3


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        tags = TagSerializer(many=True)
        fields = '__all__'
        depth = 3


class EventTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTags
        fields = '__all__'
        depth = 3


class EventTagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTags
        fields = '__all__'
        depth = 3


class EventSerializer(serializers.ModelSerializer):
    # calendar = CalendarSerializer(many=False, read_only=True)
    class Meta:
        model = Event
        tags = EventTagSerializer(many=True)
        fields = '__all__'
        depth = 3


class EventCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        tags = EventTagSerializer(many=True)
        fields = '__all__'
        depth = 3


class CalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        fields = '__all__'
        depth = 3


class CalendarCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        fields = '__all__'
        depth = 3