from django.conf.urls import *
from django.contrib.auth.decorators import login_required

from .views import home


urlpatterns = patterns('dashboard.views',
                       # home page
                       url(r'^$', login_required(home), name='dashboard_home'),
                       )