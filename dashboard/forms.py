from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML
from django_messages.forms import ComposeForm


class MessageReplyForm(ComposeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'initial' in kwargs:
            recipient = kwargs['initial'].get('recipient')[0].get_full_name()
        else:
            recipient = ''

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('recipient', style="display: none;"),
            HTML('<div style="margin: -15px 0 15px;">'+recipient+'</div>'),
            'subject',
            'body',
        )