from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _
from rest_framework import viewsets
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from menu_manager.models import MenuLink, Menu, MenuCategory
from cmf.models import *
from featured.models import *


@login_required()
def home(request):
    pages = Page.objects.all()
    allpages = Page.objects.all()
    resources = Resource.objects.all()
    emails = Email.objects.all()
    dashboard = Dashboard.objects.all()
    incentives = Incentives.objects.all()
    specials = Specials.objects.all()
    testimonials = Testimonial.objects.all()
    timelines = Timeline.objects.all()
    quotes = Quote.objects.all()

    return render(request, 'dashboard/dashboard.html', {
        'pages': pages,
        'allpages': allpages,
        'resources': resources,
        'emails': emails,
        'dashboard': dashboard,
        'incentives': incentives,
        'specials': specials,
        'testimonials': testimonials,
        'timelines': timelines,
        'quotes': quotes,
    })