#!/bin/bash

d=$(date +%y-%m-%d)

echo ""
echo "Updating the Ubuntu server"
sudo apt-get update -y
sudo apt-get install -y software-properties-common rpm locate default-jre vim unzip wget curl nano lynx python-software-properties build-essential git git-core ghostscript libgs-dev

echo ""
echo "Installing Apache2"
sudo apt-get install apache2 -y
sudo service apache2 restart


echo ""
echo "Installing Python 3 Pip and dependencies"
sudo apt-get install python3-pip -y
apt-get install pip3
sudo apt-get install libapache2-mod-wsgi-py3 -y
sudo apt-get install libjpeg-dev -y
sudo apt-get install zlib1g-dev -y
sudo service apache2 restart -y


echo ""
echo "Installing Python 3 Virtual Environment"
sudo pip3 install virtualenv -y

echo ""
echo "Changing into web root"
cd /var/www/

echo ""
echo "Cloning CMS project repo"
sudo git clone https://norwexdev@bitbucket.org/norwexdev/poshcms.git
cd poshcms
sudo git fetch --all
sudo git checkout cms-stage
cd /var/www


echo ""
echo "Creating virtual environment for CMS project. Name of virtualenvironment will be 'nwxvenv' "
sudo virtualenv nwxvenv
sudo service apache2 restart -y

echo ""
echo "Activating virtual environment for CMS project. Python 3.4 will run inside the nwxvenv once it is activated"
source /var/www/nwxvenv/bin/activate


echo ""
echo "Installing CMS project apps"
cd /var/www/poshcms/
echo "This installs all dependencies required to run the CMS."
pip3 install -r requirements.txt
pip3 install mod_wsgi
pip3 install tldextract
pip3 install django-pipeline
pip3 install -r requirements.txt

echo "Finall attempt to install missing libraries for MySQL Connector."
sudo apt-get install python3-dev -y
pip3 install mysqlclient


echo ""
echo "Enabling CMS site conf"
cd /etc/apache2/sites-available/
sudo cp /var/www/poshcms/deploy/conf/poshcms.conf /etc/apache2/sites-available/
sudo a2ensite poshcms.conf
sudo service apache2 restart -y
sudo a2dissite 000-default.conf
sudo service apache2 restart -y


echo ""
echo "Enable apache prefork module"
sudo a2dismod mpm_event
sudo a2enmod mpm_prefork

echo ""
echo "Building Apache symlinks"
sudo a2enmod rewrite
sudo a2enmod wsgi

echo "Restarting Apache"
sudo service apache2 restart

echo "Setting Apache to start Automatically on boot"
sudo update-rc.d apache2 enable

cd /var/www/poshcms/
sudo cp /var/www/poshcms/deploy/migration/0005_auto_20160624_0202.py /var/www/nwxvenv/lib/python35/site-packages/filer/migrations/
python manage.py runserver 0.0.0.0:8000

