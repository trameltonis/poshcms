# from django.contrib import admin
from django.db import models
from datetime import datetime, timedelta, date
from django.contrib.contenttypes.models import ContentType
from ckeditor.widgets import CKEditorWidget
from django.http import HttpResponse
from django.core import serializers
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.translation import get_language
from django.utils.text import slugify


def get_content_type(self):
        content_type = ContentType.objects.get_for_model(self)
        return content_type

def max_redirect(value):
    current_locale = get_language()
    # if current_locale == 'en_US':
    #     base_locale = 'en_US'
    # elif current_locale == 'es_US':
    #     base_locale = 'es_US'
    # elif current_locale == 'en_CA':
    #     base_locale = 'en_CA'
    # elif current_locale == 'fr_CA':
    #     base_locale = 'fr_CA'
    # else:
    #     base_locale = 'en_US'
    return "r'^" + current_locale + "/" + value + "/$'"

def max_localizr(value):
    current_locale = get_language()
    if current_locale == 'en_US':
        localizr = 'en-us'
    elif current_locale == 'es_US':
        localizr = 'es-us'
    elif current_locale == 'en_CA':
        localizr = 'en-ca'
    elif current_locale == 'fr_CA':
        localizr = 'fr-ca'
    else:
        localizr = 'en-us'
    return "url='/" + slugify(localizr) + "/" + value + "'"

us_states =(
    ('AL', 'Alabama'),
    ('AK', 'Alaska'),
    ('AZ', 'Arizona'),
    ('AR', 'Arkansas'),
    ('CA', 'California'),
    ('CO', 'Colorado'),
    ('CT', 'Connecticut'),
    ('DE', 'Delaware'),
    ('DC', 'District of Columbia'),
    ('FL', 'Florida'),
    ('GA', 'Georgia'),
    ('HI', 'Hawaii'),
    ('ID', 'Idaho'),
    ('IL', 'Illinois'),
    ('IN', 'Indiana'),
    ('IA', 'Iowa'),
    ('KS', 'Kansas'),
    ('KY', 'Kentucky'),
    ('LA', 'Louisiana'),
    ('ME', 'Maine'),
    ('MD', 'Maryland'),
    ('MA', 'Massachusetts'),
    ('MI', 'Michigan'),
    ('MN', 'Minnesota'),
    ('MS', 'Mississippi'),
    ('MO', 'Missouri'),
    ('MT', 'Montana'),
    ('NE', 'Nebraska'),
    ('NV', 'Nevada'),
    ('NH', 'New Hampshire'),
    ('NJ', 'New Jersey'),
    ('NM', 'New Mexico'),
    ('NY', 'New York'),
    ('NC', 'North Carolina'),
    ('ND', 'North Dakota'),
    ('OH', 'Ohio'),
    ('OK', 'Oklahoma'),
    ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'),
    ('RI', 'Rhode Island'),
    ('SC', 'South Carolina'),
    ('SD', 'South Dakota'),
    ('TN', 'Tennessee'),
    ('TX', 'Texas'),
    ('UT', 'Utah'),
    ('VT', 'Vermont'),
    ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'),
    ('WI', 'Wisconsin'),
    ('WY', 'Wyoming'),
    ('OT', 'Non US State')
)

canadian_provinces = (
    ('AL', 'Alberta'),
    ('BC', 'British Columbia'),
    ('MB', 'Manitoba'),
    ('NB', 'New Brunswick'),
    ('NL', 'Newfoundland'),
    ('NT', 'Northwest Territories'),
    ('NS', 'Nova Scotia'),
    ('NU', 'Nunavut'),
    ('ON', 'Ontario'),
    ('PE', 'Prince Edward Island'),
    ('QC', 'Quebec'),
    ('SK', 'Saskatchewan'),
    ('YT', 'Yukon'),
)

country = (
    ('US', _('United States')),
    ('CA', _('Canada')),
    ('DE', _('Germany')),
    ('AU', _('Australia')),
    ('UK', _('United Kingdom')),
    ('NZ', _('New Zealand')),
)

mobile_country = (
    ('United States +1', 'United States +1'),
    ('Canada +1', 'Canada +1'),
)

address_type = (
    ('Residential', 'Residential'),
    ('Business', 'Business'),
)

gender = (
    ('M', 'Male'),
    ('F', 'Female'),
)

prefix = (
    ('Mr.', 'Mr'),
    ('Mrs.', 'Mrs'),
    ('Ms.', 'Ms'),
    ('Dr.', 'Dr'),
    ('Prof.', 'Prof'),
)

language = (
    ('English', _('English')),
    ('French', _('French')),
    ('Spanish', _('Spanish')),
    ('Dutch', _('Dutch')),
    ('German', _('German')),
)

route_type = (
    ('uri', _('URI')),
    ('route', _('Route')),
    # ('content', _('Content')),
)

menu_level = (
    ('1', _('Level One')),
    ('2', _('Level Two')),
    ('3', _('Level Three')),
    ('4', _('Level Four')),
    ('5', _('Level Five')),
)

target = (
    ('none', _('None (i.e. same window)')),
    ('_blank', _('New window (_blank)')),
    ('_top', _('Top window (_top)')),
    ('_self', _('Same window (_self)')),
    ('_parent', _('Parent window (_parent)')),
)

social_media = (
    ('facebook', _('Facebook')),
    ('twitter', _('Twitter')),
    ('pinterest', _('Pinterest')),
    ('googleplus', _('Google Plus')),
    ('linkedin', _('LinkedIn')),
    ('instagram', _('Instagram')),
    ('youtube', _('YouTube')),
)

status = (
    ('A', _('Active')),
    ('IN', _('Inactive')),
)

months = (
    ('jan', _('January')),
    ('feb', _('February')),
    ('mar', _('March')),
    ('apr', _('April')),
    ('may', _('May')),
    ('jun', _('June')),
    ('jul', _('July')),
    ('aug', _('August')),
    ('sep', _('September')),
    ('oct', _('October')),
    ('nov', _('November')),
    ('dec', _('December')),
)

visibility = (
    ('public', _('Public')),
    ('private', _('Private')),
    # ('corporate', _('Corporate')),
)

GRID_LAYOUT = (
    ('one', _('One Column/Full Page')),
    ('two', _('Two Columns')),
    ('three', _('Three Columns')),
    ('four', _('Four Columns')),
    ('six', _('Six Columns')),
)

MEDIA_LAYOUT = (
    ('grid-1', _('Single Grid')),
    ('grid-2', _('Two Grids(2x2)')),
    ('grid-3', _('Three Grids (3x3)')),
    ('grid-4', _('Four Grids (4x4)')),
    ('grid-6', _('Six Grids')),
)

DESIGNATION = (
    ('us', _('US Only')),
    ('ca', _('Canada Only')),
    ('us-ca', _('US and Canada Only')),
    ('global', _('Global')),
)

template_framework = (
    ('foundation', _('Zurb Foundation')),
    # ('bootstrap', _('Twitter Bootstrap')),
    # ('custom', _('Custom')),
)

category = (
    ('nco', _('NCO')),
    ('neo', _('NEO')),
    ('pws', _('PWS')),
    ('corporate', _('Corporate')),
)


SECTION = (
    ('nco', _('NCO')),
    ('neo', _('NEO')),
    ('pws', _('PWS')),
    ('corporate', _('Corporate')),
)



placement = (
    ('top', _('Top')),
    ('bottom', _('Bottom')),
    ('left', _('Left')),
    ('right', _('Right')),
    ('center', _('Center')),
)

POSITION = (
    ('absolute', _('Absolute')),
    ('relative', _('Relative')),
    ('fixed', _('Fixed')),
)


FLOAT = (
    ('right', _('Right')),
    ('left', _('Left')),
)


TEMPLATE_REGIONS = (
    ('message-top', _('Message Top')),
    ('topbar', _('Top Bar')),
    ('navigation', _('Navigation Bar')),
    ('sidebar_left', _('Left')),
    ('sidebar_right', _('Right')),
    ('content_center', _('Center')),
    ('footer_top', _('Footer Top')),
    ('footer_main', _('Footer Main')),
    ('footer_bottom', _('Footer Bottom')),
)


LAYOUT = (
    ('cta-right', _('Banner with CTA Right')),
    ('cta-left', _('Banner with CTA Left')),
    ('cta-center', _('Banner with CTA Center')),
    ('cta-center-subtext-below', _('Banner with CTA Center with Subtext Below')),
    ('hero-text-center', _('Hero Text Center')),
    ('hero-text-center-button', _('Hero Text Center Button')),
    ('hero-text-center-button-subtext-below', _('Hero Text Center Button with Subtext Below')),
    ('grid-3-cta-caption-top', _('Grid 3 CTA with Caption Top')),
    ('grid-3-cta-image-subtext-inside', _('Grid 3 CTA Image with Subtext Inside')),
    ('grid-4-cta-caption-top', _('Grid 4 CTA and Icons with Caption Top')),
    ('grid-2-image-left-cta-right', _('Grid 2 Image Left, CTA Right')),
    ('grid-2-cta-quote-left-image-right', _('Grid 2 CTA Quote Left, Image Right')),
    ('grid-2-cta-left-image-right', _('Grid 2 CTA Left, Image Right')),
    ('grid-2-text-left-text-right', _('Grid 2 Text Left, Text Right')),
    ('meet-leadership', _('Meet Our Leadership')),
    ('carousel', _('Carousel')),
    ('carousel-quote', _('Quote Carousel')),
    ('video-player-single', _('Video Player Single')),
    ('video-player-collection', _('Video Player Collection')),
    ('hero-content', _('Hero Text and Content Only')),
    ('starter-kit', _('Starter Kit plus Carousel')),
    ('hero-top-grid3-cta', _('Hero Text Top, Grid 3 CTA Down')),
    ('grid2-hero-content-img', _('Grid 2 Hero, Content and Image')),
    ('custom-layout', _('Custom Layout')),
)

STATUS_CHOICES = (
    ('d', _('Draft')),
    ('p', _('Published')),
    ('a', _('Archive')),
    ('w', _('Withdrawn')),
)

BATCH = (
    ('batch1', _('Batch 1 (1st to 15th)')),
    ('batch2', _('Batch 2 (15th to month end)')),
    ('full-month', _('Full month (1st to month end)')),
)

SPECIALS_TYPE = (
    ('host', _('Host Special')),
    ('consultant', _('Consultant Special')),
    ('customer', _('Customer Special')),
)

INCENTIVES_TYPE = (
    ('host', _('Host Incentive')),
    ('consultant', _('Consultant Incentive')),
    ('customer', _('Customer Incentive')),
)

MENU_PLACEMENT = (
    ('topbar_menu', _('Top Bar')),
    ('navigation_menu', _('Navigation Bar')),
    ('sidebar_left_menu', _('Sidebar Left')),
    ('sidebar_right_menu', _('Sidebar Right')),
    ('footer_menu', _('Footer')),
)

VIDEO_PROVIDER = (
    ('youtube', _('YouTube')),
    ('vimeo', _('Vimeo')),
)

shape = (
    ('circle', _('Circle')),
    ('rectangle', _('Rectangle')),
    ('square', _('Square')),
    ('oval', _('Oval')),
)


seo_robot = (
    ('no-indexing', _('Prevent search engines from indexing this page')),
    ('no-follow', _('Prevent search engines from following links on this page')),
)

media_types = (
    ('image', _('Image')),
    ('file', (
            ('pdf', _('PDF')),
            ('doc', _('Doc')),
            ('spreadsheet', _('Spreadsheet')),
        )
    ),
    ('Audio', (
            ('mp3', _('MP3'))
        )
    ),
    ('Video', (
            ('youtube', _('YouTube')),
            ('vimeo', _('Vimeo')),
            ('dailymotion', _('Daily Motion')),
        )
    ),
    ('unknown', _('Unknown')),
)

NOTSTARTED = '1'
ONHOLD = '2'
INPROGRESS = '3'
INREVIEW = '4'
COMPLETED = '5'
progress_status = (
    (NOTSTARTED, _('Not Started')),
    (ONHOLD, _('On Hold')),
    (INPROGRESS, _('In Progress')),
    (INREVIEW, _('In Review')),
    (COMPLETED, _('Completed')),
)


NEWS_TYPE = (
    ('urgent', _('Urgent')),
    ('company_news', _('Company News')),
    ('events', _('Events')),
    ('product_notice', _('Product Notice')),
    ('norwex_movement', _('Norwex Movement')),
    ('general_news', _('General News')),
)


# UTILITY FUNCTIONS

def time_seconds(self, obj):
    return obj.timefield.strftime("%d %b %Y %H:%M:%S")
time_seconds.admin_order_field = 'timefield'
time_seconds.short_description = 'Precise Time'

def year_only(self, obj):
    return obj.timefield.strftime("%Y")


def export_as_json(modeladmin, request, queryset):
        response = HttpResponse(content_type="application/json")
        serializers.serialize("json", queryset, stream=response)
        return response


def make_published(modeladmin, request, queryset):
    queryset.update(status='p', active=True)
    make_published.short_description = _("Mark selected as published")


def make_draft(modeladmin, request, queryset):
    queryset.update(status='d', active=False)
make_draft.short_description = _("Mark selected as draft")


def make_archive(modeladmin, request, queryset):
    queryset.update(status='a', active=True)
make_archive.short_description = _("Move selected to archive")


def make_withdrawn(modeladmin, request, queryset):
    queryset.update(status='w', active=False)
make_withdrawn.short_description = _("Mark selected as withdrawn")


def make_public(modeladmin, request, queryset):
    queryset.update(visibility='public')
    make_public.short_description = _("Mark selected as public")


def make_private(modeladmin, request, queryset):
    queryset.update(visibility='private')
    make_private.short_description = _("Mark selected as private")


def make_corporate(modeladmin, request, queryset):
    queryset.update(visibility='corproate')
    make_corporate.short_description = _("Mark selected as corporate")


def make_us_only(modeladmin, request, queryset):
    queryset.update(designation='us')
make_us_only.short_description = _("Mark selected as US only")


def make_ca_only(modeladmin, request, queryset):
    queryset.update(designation='ca')
    make_ca_only.short__description = _("Mark selected as Canada only")


def make_us_ca_only(modeladmin, request, queryset):
    queryset.update(designation='us-ca')
    make_us_ca_only.short_description = _("Mark selected as US and Canada only")


def make_global(modeladmin, request, queryset):
    queryset.update(designation='global')
    make_global.short_description = _("Mark selected as Global")


def formfield_editor(modeladmin, request, queryset):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }

def get_default_publish_start_date():
  return datetime.now() + timedelta(days=1)

def get_default_publish_end_date():
  return datetime.now() + timedelta(days=15)

def get_first_day_of_month(d):
    return date(d.year, d.month, 1)


def make_corp(modeladmin, request, queryset):
    queryset.update(section='corporate')
    make_published.short_description = _("Mark as Corporate")


def make_pws(modeladmin, request, queryset):
    queryset.update(section='pws')
    make_published.short_description = _("Mark as PWS")


def make_nco(modeladmin, request, queryset):
    queryset.update(section='nco')
    make_published.short_description = _("Mark as NCO")


def make_neo(modeladmin, request, queryset):
    queryset.update(section='neo')
    make_published.short_description = _("Mark as NEO")


