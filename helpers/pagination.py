from django.core.paginator import Paginator, EmptyPage, InvalidPage


def get_page(request, query_set, objects_per_page=20, param_name='page', links_length=10):
    """
    Returns django.core.paginator.Page object for the given QuerySet and current page number
    """
    paginator = Paginator(query_set, objects_per_page)
    page = request.GET.get(param_name) or 1
    try:
        page = paginator.page(page)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    except InvalidPage:
        page = paginator.page(1)

    get_params = request.GET.copy()
    if 'page' in get_params:
        del get_params['page']
    page.query = '&' + get_params.urlencode() if get_params else ''

    page.visible_links = get_visible_links(page, links_length)

    return page


def get_visible_links(page, max):
    """ returns list of visible page nums. zero means range dots (...) """
    current = page.number
    total = page.paginator.num_pages
    if total <= max:
        return page.paginator.page_range
    else:
        if current <= max/2:
            return range(1, max) + [0]
        elif current > (total - max/2):
            return [0] + range(total-max+2, total+1)
        else:
            return [0] + range(current - max/2 + 1, current + max/2 - 1) + [0]
