class Sorter(object):

    def __init__(self, request, query_set, sort_rules={'created_asc': '-created', 'created_desc': 'created'},
                 default='created', merge_rules=True):
        self.request = request
        self.query_set = query_set
        self.sort_rules = sort_rules
        self.default = default
        self.merge_rules = merge_rules
        super(Sorter, self).__init__()

    def get_sorted_model(self):
        """
        Applies order_by() function for the given QuerySet and returns QuerySet back
        """
        default_rules = {'created': '-created'}

        if isinstance(self.sort_rules, dict):
            if self.merge_rules:
                self.sort_rules.update(default_rules)
        else:
            self.sort_rules = default_rules

        order = self.get_order()
        return self.query_set.order_by(order)

    def get_order(self):
        if self.default not in self.sort_rules:
            init_order = list(self.sort_rules.keys())[0]
        else:
            init_order = self.default

        order_param = self.request.GET.get('order')
        if order_param in self.sort_rules:
            order = self.sort_rules[order_param]
        else:
            order = self.sort_rules[init_order]

        return order