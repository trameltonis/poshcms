# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-25 05:38
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('faq', '0002_auto_20160723_2338'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='FAQ Product', max_length=255, verbose_name='Product')),
                ('title_en_us', models.CharField(help_text='FAQ Product', max_length=255, null=True, verbose_name='Product')),
                ('title_es_us', models.CharField(help_text='FAQ Product', max_length=255, null=True, verbose_name='Product')),
                ('title_fr_ca', models.CharField(help_text='FAQ Product', max_length=255, null=True, verbose_name='Product')),
                ('title_en_ca', models.CharField(help_text='FAQ Product', max_length=255, null=True, verbose_name='Product')),
                ('slug', models.SlugField(help_text='Automatically generated from category title if left empty.', unique=True, verbose_name='Slug')),
                ('description', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_en_us', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_es_us', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_fr_ca', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_en_ca', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('sort_order', models.PositiveIntegerField(blank=True, default=0, help_text='Product Order.', null=True, verbose_name='Sort Order')),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published'), ('a', 'Archive'), ('w', 'Withdrawn')], default='p', max_length=1)),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('publish_start', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Publish Start Date')),
                ('publish_end', models.DateTimeField(blank=True, null=True, verbose_name='Publish End Date')),
                ('active', models.BooleanField(default=True, help_text='Product Status.', verbose_name='Active')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Product',
                'ordering': ('sort_order',),
                'verbose_name_plural': 'Products',
            },
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='FAQ Type', max_length=255, verbose_name='FAQ Type')),
                ('title_en_us', models.CharField(help_text='FAQ Type', max_length=255, null=True, verbose_name='FAQ Type')),
                ('title_es_us', models.CharField(help_text='FAQ Type', max_length=255, null=True, verbose_name='FAQ Type')),
                ('title_fr_ca', models.CharField(help_text='FAQ Type', max_length=255, null=True, verbose_name='FAQ Type')),
                ('title_en_ca', models.CharField(help_text='FAQ Type', max_length=255, null=True, verbose_name='FAQ Type')),
                ('slug', models.SlugField(help_text='Automatically generated from category title if left empty.', unique=True, verbose_name='Slug')),
                ('description', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_en_us', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_es_us', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_fr_ca', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('description_en_ca', models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description')),
                ('sort_order', models.PositiveIntegerField(blank=True, default=0, help_text='Type Sort Order.', null=True, verbose_name='Sort Order')),
                ('status', models.CharField(choices=[('d', 'Draft'), ('p', 'Published'), ('a', 'Archive'), ('w', 'Withdrawn')], default='p', max_length=1)),
                ('visibility', models.CharField(blank=True, choices=[('public', 'Public'), ('private', 'Private')], default='public', help_text='Set visibility of content to authenticated and anonymous users.', max_length=255, null=True, verbose_name='Visibility')),
                ('publish_start', models.DateTimeField(blank=True, default=datetime.datetime.now, null=True, verbose_name='Publish Start Date')),
                ('publish_end', models.DateTimeField(blank=True, null=True, verbose_name='Publish End Date')),
                ('active', models.BooleanField(default=True, help_text='FAQ Type Status.', verbose_name='Active')),
                ('progress_status', models.CharField(choices=[('1', 'Not Started'), ('2', 'On Hold'), ('3', 'In Progress'), ('4', 'In Review'), ('5', 'Completed')], default='NOTSTARTED', help_text='Select progress status level', max_length=120, verbose_name='Translation Status')),
                ('lock_locale', models.BooleanField(default=False, help_text='Use to lock locale.', verbose_name='Locked Locale')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last Updated')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='faq_type_created_by', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='faq_type_updated_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'FAQ Type',
                'ordering': ('sort_order',),
                'verbose_name_plural': 'FAQ Types',
            },
        ),
        migrations.AddField(
            model_name='category',
            name='description_en_ca',
            field=models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description'),
        ),
        migrations.AddField(
            model_name='category',
            name='description_en_us',
            field=models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description'),
        ),
        migrations.AddField(
            model_name='category',
            name='description_es_us',
            field=models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description'),
        ),
        migrations.AddField(
            model_name='category',
            name='description_fr_ca',
            field=models.TextField(blank=True, default=None, help_text='More information about the category.', null=True, verbose_name='Description'),
        ),
        migrations.AddField(
            model_name='category',
            name='title_en_ca',
            field=models.CharField(help_text='Question Category', max_length=255, null=True, verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='category',
            name='title_en_us',
            field=models.CharField(help_text='Question Category', max_length=255, null=True, verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='category',
            name='title_es_us',
            field=models.CharField(help_text='Question Category', max_length=255, null=True, verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='category',
            name='title_fr_ca',
            field=models.CharField(help_text='Question Category', max_length=255, null=True, verbose_name='Category'),
        ),
    ]
