# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-30 16:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0006_auto_20160731_1205'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='product',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='question',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
        migrations.AddField(
            model_name='type',
            name='url',
            field=models.SlugField(default=None, help_text='Automatically generated from title if left empty.', verbose_name='Url'),
        ),
    ]
