from modeltranslation.translator import translator, register, TranslationOptions
from faq.models import Question, Category, Type, Product


@register(Question)
class QuestionTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'answer',
        ]


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(Type)
class TypeTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(Product)
class ProductTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]