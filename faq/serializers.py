from django.contrib.auth.models import User, Group
from rest_framework import serializers
from faq.models import Category, Question, Type, Product


class FaqCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 3


class FaqCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 3


class FaqQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class FaqQuestionCreateSerializer(serializers.ModelSerializer):
    # category = FaqCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class FaqTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = '__all__'
        depth = 3


class FaqTypeCreateSerializer(serializers.ModelSerializer):
    # category = FaqCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Type
        fields = '__all__'
        depth = 3


class FaqProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
        depth = 3


class FaqProductCreateSerializer(serializers.ModelSerializer):
    # category = FaqCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        depth = 3