from django.db.models import Q
from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.views.generic import View
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from .forms import CategoryCreateForm, CategoryEditForm, TypeCreateForm, TypeEditForm, ProductCreateForm, ProductEditForm, QuestionCreateForm, QuestionEditForm
from .models import Question, Category, Type, Product
from .serializers import FaqCategorySerializer, FaqCategoryCreateSerializer, FaqQuestionSerializer, FaqQuestionCreateSerializer, FaqTypeSerializer, FaqTypeCreateSerializer, FaqProductSerializer, FaqProductCreateSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('-created_on')
    serializer_class = FaqCategorySerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by('-created_on')
    serializer_class = FaqQuestionSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class TypeViewSet(viewsets.ModelViewSet):
    queryset = Type.objects.all().order_by('-created_on')
    serializer_class = FaqTypeSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by('-created_on')
    serializer_class = FaqProductSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class CategoryFilter(filters.FilterSet):
    class Meta:
        model = Category
        fields = ['slug', 'id', 'status', 'visibility']


# Category Api endpoint
class CategoryList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = FaqCategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CategoryFilter

    # authentication_classes = [SessionAuthentication, BaseAuthentication, JSONWebTokenAuthentication]


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = FaqCategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CategoryCreate(generics.CreateAPIView):
    serializer_class = FaqCategorySerializer


@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        category = Category.objects.get(slug=slug)
    except Category.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = FaqCategorySerializer(category)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = FaqCategorySerializer(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# Question Api endpoint

class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = FaqQuestionSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class QuestionCreate(generics.CreateAPIView):
    serializer_class = FaqQuestionCreateSerializer


# Category Api endpoint
# class CategoryList(generics.ListCreateAPIView):
#     queryset = Category.objects.all()
#     serializer_class = FaqCategorySerializer
#     authentication_classes = [JSONWebTokenAuthentication]
#     permission_classes = [IsAuthenticated]
#
#
# class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Category.objects.all()
#     serializer_class = FaqCategorySerializer
#     authentication_classes = [JSONWebTokenAuthentication]
#     permission_classes = [IsAuthenticated]
#
#
# class CategoryCreate(generics.CreateAPIView):
#     serializer_class = FaqCategorySerializer


# Type Api endpoint
class TypeList(generics.ListCreateAPIView):
    queryset = Type.objects.all()
    serializer_class = FaqTypeSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Type.objects.all()
    serializer_class = FaqTypeSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TypeCreate(generics.CreateAPIView):
    serializer_class = FaqTypeCreateSerializer


# Product Api endpoint
class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = FaqProductSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = FaqProductSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class ProductCreate(generics.CreateAPIView):
    serializer_class = FaqProductCreateSerializer


class TypeCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = TypeCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TypeCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TypeCreateView, self).dispatch(*args, **kwargs)


class TypeUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = TypeEditForm
    model = Type

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TypeUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(TypeUpdateView, self).form_valid(form)


class TypeDetailView(DetailView):
    model = Type
    template_name = "faq/_type_detail.html"


class TypeListView(ListView):
    model = Type
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(TypeListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class ProductCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = ProductCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(ProductCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProductCreateView, self).dispatch(*args, **kwargs)


class ProductUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = ProductEditForm
    model = Product

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(ProductUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProductUpdateView, self).dispatch(*args, **kwargs)


class ProductDetailView(DetailView):
    model = Product
    template_name = "faq/_product_detail.html"


class ProductListView(ListView):
    model = Product
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ProductListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class CategoryCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = CategoryCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CategoryCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryCreateView, self).dispatch(*args, **kwargs)


class CategoryUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = CategoryEditForm
    model = Category

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(CategoryUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryUpdateView, self).dispatch(*args, **kwargs)


class CategoryDetailView(DetailView):
    model = Category
    template_name = "faq/_category_detail.html"


class CategoryListView(ListView):
    model = Category
    template_name = "themes/list2.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CategoryListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs



class QuestionCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = QuestionCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(QuestionCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuestionCreateView, self).dispatch(*args, **kwargs)


class QuestionUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = QuestionEditForm
    model = Question

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(QuestionUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuestionUpdateView, self).dispatch(*args, **kwargs)


class QuestionDetailView(DetailView):
    model = Question
    template_name = "faq/_question_detail.html"


class QuestionListView(ListView):
    model = Question
    template_name = "faq/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(QuestionListView, self).get_queryset(*args, **kwargs).order_by("-created_on").filter(status='p', active=True)
        return qs


class ProductFaqListView(ListView):
    model = Question
    page_title = _("Product FAQs"),
    template_name = "faq/product_faq_list.html"
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):
        qs = super(ProductFaqListView, self).get_queryset(*args, **kwargs).order_by("-created_on").filter(category__slug="product-faq", status='p', active=True)
        return qs


class ProductFaqSearchListView(ListView):
    model = Question
    page_title = _("Product FAQs"),
    template_name = "faq/product_faq_list.html"
    paginate_by = 250

    def get_queryset(self, *args, **kwargs):
        # qs = super(ProductFaqSearchListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        qs = Question.objects.all()
        query = self.request.GET.get("q")
        # query.group_by = ['category']
        if query:
            qs = self.model.objects.filter(
                Q(title__icontains=query) |
                Q(slug__icontains=query)|
                Q(product__title__icontains=query) |
                Q(product__slug__icontains=query) |
                Q(product__description__icontains=query) |
                Q(answer__icontains=query) |
                Q(type__title__icontains=query) |
                Q(type__slug__icontains=query) |
                Q(type__description__icontains=query)
            )
            try:
                qs2 = self.model.objects.filter(
                    Q(category__title__icontains=query) |
                    Q(category__slug__icontains=query) |
                    Q(category__description__icontains=query)
                )
                qs = (qs | qs2).distinct()
            except:
                pass
            return qs
        return qs









