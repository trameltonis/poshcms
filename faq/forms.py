from django import forms
from django.utils.translation import ugettext_lazy as _
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, Row, Field, Hidden, ButtonHolder
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import Question, Category, Type, Product


class CategoryCreateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Category
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'status',
            'visibility',
            'progress_status',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class CategoryEditForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Category
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'progress_status',
            'status',
            'visibility',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class TypeCreateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Type
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'status',
            'visibility',
            'progress_status',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class TypeEditForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Type
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'progress_status',
            'status',
            'visibility',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class ProductCreateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Product
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'status',
            'visibility',
            'progress_status',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class ProductEditForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Product
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'description',
            'progress_status',
            'status',
            'visibility',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class QuestionCreateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Question
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'answer',
            'type',
            'category',
            'product',
            'sort_order',
            'status',
            'visibility',
            'progress_status',
            'slug',
            'publish_start',
            'publish_end',
            'active',
            'featured',
            'lock_locale',
            'created_by',
            'updated_by'
            'seo_title',
            'seo_keywords',
            'seo_og_img',
            'seo_generator',
            'seo_copyright',
            'seo_canonical_url',
            'seo_short_url',
            'seo_publisher_url',
            'seo_robot',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class QuestionEditForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Question
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'answer',
            'type',
            'category',
            'product',
            'sort_order',
            'status',
            'visibility',
            'progress_status',
            'slug',
            'publish_start',
            'publish_end',
            'active',
            'featured',
            'lock_locale',
            'created_by',
            'updated_by'
            'seo_title',
            'seo_keywords',
            'seo_og_img',
            'seo_generator',
            'seo_copyright',
            'seo_canonical_url',
            'seo_short_url',
            'seo_publisher_url',
            'seo_robot',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )