from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns = [
    url(r'^faqs/$', QuestionListView.as_view(), name='faq_question_list'),
    url(r'^faqs/product/$', ProductFaqSearchListView.as_view(), name='product_faq_list'),
    url(r'^faqs/create/', QuestionCreateView.as_view(), name='faq_question_create'),
    url(r'^faqs/edit/(?P<slug>[\w-]+)/$', QuestionUpdateView.as_view(), name='faq_question_update'),
    # url(r'^faqs/delete/(?P<pk>\d+)/', QuestionDeleteView.as_view(), name='faq _question_delete'),
    url(r'^faqs/(?P<slug>[\w-]+)/$', QuestionDetailView.as_view(), name='faq_question_detail'),
]
urlpatterns = format_suffix_patterns(urlpatterns)