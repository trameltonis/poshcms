from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import *
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django.contrib.contenttypes.admin import GenericTabularInline
from modeltranslation.admin import TranslationAdmin
from .models import Question, Category, Type, Product


@admin.register(Type)
class TypeAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'section', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['section', 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Product)
class ProductAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'section', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'section', 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Category)
class CategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'section', 'status', 'visibility', 'progress_status', 'sort_order',  'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = ['section', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Question)
class QuestionAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'answer',
                'type',
                'category',
                'product',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'url',
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    filter_horizontal =  ["product"]
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'seo_title': ('title',),
        'seo_keywords': ('slug',),
        'url': ('title',),
    }
    list_display = ('title', 'language_selectors', 'section', 'publish_start',  'status', 'progress_status',  'active', 'updated_by', 'sort_order', 'updated')
    search_fields = ['title', 'slug', 'answer']
    list_filter = ['section', 'publish_start',  'publish_end',  'status', 'featured']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))



