from rest_framework import generics
from django.db.models import Q
from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.views.generic import View
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _

from .serializers import *
from ..models import *


class FaqCategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('-created_on')
    serializer_class = FaqCategorySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class FaqQuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by('-created_on')
    serializer_class = FaqQuestionSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class FaqTypeViewSet(viewsets.ModelViewSet):
    queryset = Type.objects.all().order_by('-created_on')
    serializer_class = FaqTypeSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class FaqProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by('-created_on')
    serializer_class = FaqProductSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class CategoryFilter(filters.FilterSet):
    class Meta:
        model = Category
        fields = ['slug', 'id', 'status', 'visibility']