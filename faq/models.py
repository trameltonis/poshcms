from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from datetime import datetime, timedelta
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from core.models import *
from helpers.utils import *


class Type(models.Model):
    title = models.CharField(
            max_length=255,
            null=False,
            blank=False,
            verbose_name=_("FAQ Type"),
            help_text=_("FAQ Type"),
            unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_('Slug'),
            help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
            null=True,
            blank=True,
            verbose_name=_("Description"),
            help_text=_("More information about the category."),
            default=None
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Type Sort Order."),
            default=0
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            default=datetime.now,
            blank=True,
            null=True,
            verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            blank=True,
            null=True,
            verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Active"),
            help_text=_("FAQ Type Status.")
    )
    progress_status = models.CharField(
            max_length=120,
            null=False,
            blank=False,
            choices=progress_status,
            verbose_name=_("Translation Status"),
            default="NOTSTARTED",
            help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
            default=False,
            verbose_name=_("Locked Locale"),
            help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="faq_type_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="faq_type_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('faq_type_detail', kwargs={'slug': self.slug })

    class Meta:
        verbose_name = _('FAQ Type')
        verbose_name_plural = _('FAQ Types')


class Product(models.Model):
    title = models.CharField(
            max_length=255,
            null=False,
            blank=False,
            verbose_name=_("Product"),
            help_text=_("FAQ Product"),
            unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_('Slug'),
            help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
            null=True,
            blank=True,
            verbose_name=_("Description"),
            help_text=_("More information about the category."),
            default=None
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Product Order."),
            default=0
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            default=datetime.now,
            blank=True,
            null=True,
            verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            blank=True,
            null=True,
            verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Active"),
            help_text=_("Product Status.")
    )
    progress_status = models.CharField(
            max_length=120,
            null=False,
            blank=False,
            choices=progress_status,
            verbose_name=_("Translation Status"),
            default="NOTSTARTED",
            help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
            default=False,
            verbose_name=_("Locked Locale"),
            help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="product_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="product_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('faq_products_detail', kwargs={'slug': self.slug })

    class Meta:
        ordering = ('sort_order',)
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class Category(models.Model):
    title = models.CharField(
            max_length=255,
            null=False,
            blank=False,
            verbose_name=_("Category"),
            help_text=_("Question Category"),
            unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_('Slug'),
            help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
            null=True,
            blank=True,
            verbose_name=_("Description"),
            help_text=_("More information about the category."),
            default=None
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Question Sort Order."),
            default=0
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            default=datetime.now,
            blank=True,
            null=True,
            verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            blank=True,
            null=True,
            verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Active"),
            help_text=_("Category Status.")
    )
    progress_status = models.CharField(
            max_length=120,
            null=False,
            blank=False,
            choices=progress_status,
            verbose_name=_("Translation Status"),
            default="NOTSTARTED",
            help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
            default=False,
            verbose_name=_("Locked Locale"),
            help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="qctg_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="qctg_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('faq_category_detail', kwargs={'slug': self.slug })

    class Meta:
        ordering = ('sort_order',)
        # unique_together = ('slug', 'title')
        verbose_name = _('FAQ Category')
        verbose_name_plural = _('FAQ Categories')


class Question(models.Model):
    title = models.CharField(
            max_length=255,
            null=False,
            blank=False,
            verbose_name=_("Question"),
            help_text=_("Question title"),
            unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_('Slug'),
            help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    answer = models.TextField(
            null=True,
            blank=True,
            verbose_name=_("Answer"),
            help_text=_("Answer to the question."),
            default=None
    )
    type = models.ForeignKey(
            Type,
            blank=False,
            null=True,
            default=None,
            verbose_name=_("FAQ Type"),
            help_text=_('Example: Product FAQ, Host a Party FAQ, etc'),
            related_name="%(class)s_type"
    )
    category = models.ForeignKey(
            Category,
            blank=False,
            null=True,
            default=None,
            verbose_name=_("Category"),
            help_text=_('Example: Kitchen, Microfiber, Floor Systems , etc'),
            related_name="question_category"
    )
    product = models.ManyToManyField(
            Product,
            blank=True,
            # null=True,
            default=None,
            verbose_name=_("Related Product"),
            help_text=_('Example: Mops, Carpet Stain Buster, Cleaning Paste, etc'),
            related_name="%(class)s_product"
    )
    sort_order = models.PositiveIntegerField(
            null=True,
            blank=True,
            verbose_name=_("Sort Order"),
            help_text=_("Sort Order for FAQ."),
            default=0
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            default=datetime.now,
            blank=True,
            null=True,
            verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
            auto_now=False,
            auto_now_add=False,
            blank=True,
            null=True,
            verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
            default=False,
            verbose_name=_("Featured"),
            help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
            max_length=120,
            null=False,
            blank=False,
            choices=progress_status,
            verbose_name=_("Translation Status"),
            default="NOTSTARTED",
            help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
            default=False,
            verbose_name=_("Locked Locale"),
            help_text=_("Use to lock locale.")
    )
    seo_title = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            verbose_name=_("SEO Title"),
            help_text=_("SEO title"),
            unique=False
    )
    seo_keywords = models.TextField(
            null=True,
            blank=True,
            verbose_name=_("SEO Keywords"),
            help_text=_("SEO keywords for page."),
            default=None
    )
    seo_description = models.TextField(
            null=True,
            blank=True,
            verbose_name=_("SEO Description"),
            help_text=_("SEO description for page."),
            default=None
    )
    seo_og_img = models.ImageField(
            blank=True,
            null=True,
            verbose_name=_("SEO Open Graph Image"),
            help_text=_('Image displayed when content is shared on social media'),
            upload_to="seo/open-graph"
    )
    seo_generator = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            verbose_name=_("Generator"),
            help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            verbose_name=_("Copyright"),
            help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            verbose_name=_("Canonical Url"),
            help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
            max_length=255,
            null=True,
            blank=True,
            verbose_name=_("Shortlink Url"),
            help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            verbose_name=_("Publisher Url"),
            help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            choices=seo_robot,
            verbose_name=_("Search Engines & Robots"),
            help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="question_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="question_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('faq_question_detail', kwargs={'slug': self.slug })

    class Meta:
        verbose_name = _('FAQ')
        verbose_name_plural = _('FAQs')