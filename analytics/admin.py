from django.contrib import admin

from .models import PageView, EmailView, SpecialsView, IncentivesView, ResourceView


@admin.register(PageView)
class PageView(admin.ModelAdmin):
    pass


@admin.register(EmailView)
class EmailView(admin.ModelAdmin):
    pass


@admin.register(SpecialsView)
class SpecialsView(admin.ModelAdmin):
    pass


@admin.register(IncentivesView)
class IncentivesView(admin.ModelAdmin):
    pass


@admin.register(ResourceView)
class ResourceView(admin.ModelAdmin):
    pass









