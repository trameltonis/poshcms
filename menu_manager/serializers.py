from rest_framework import serializers
from menu_manager.models import Menu, MenuCategory, MenuLink
from media_manager.models import *
from core.serializers import *
# from cmf.serializers import SpecialsSerializer, IncentivesSerializer


class MenuCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuCategory
        fields = '__all__'
        depth = 1


class MenuCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuCategory
        fields = '__all__'
        depth = 1


class MenuSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Menu
        fields = '__all__'
        depth = 1


class MenuCreateSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Menu
        fields = '__all__'
        depth = 1


class MenuLinkSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=True, read_only=True)

    class Meta:
        model = MenuLink
        fields = '__all__'
        depth = 3


class MenuLinkCreateSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=True, read_only=True)
    # menu = MenuSerializer(source='menu', many=True, read_only=True)

    class Meta:
        model = MenuLink
        fields = '__all__'
        depth = 3