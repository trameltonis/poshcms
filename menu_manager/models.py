from django.db import models
from django.db.models import Q
import base64
import uuid
from django.utils.translation import ugettext_lazy as _
from filebrowser.fields import FileBrowseField
from django.db import models
from import_export import resources, fields
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
from django.utils import timezone
from django.utils.text import slugify
from datetime import datetime, timedelta
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
# from core.models import Locale, SocialMedia
# from cmf.models import Page, Resource
# from news.models import News, Post, Event
from helpers.utils import route_type, MENU_PLACEMENT, progress_status, target, STATUS_CHOICES, visibility, get_content_type, menu_level


class MenuCategory(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Name"),
        help_text=_("Menu Category name"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Keyword'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Menu Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="menu_category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="menu_category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("menu_category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Menu Category')
        verbose_name_plural = _('Menu Categories')


class Menu(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Name"),
        help_text=_("Menu name"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Keyword'),
        help_text=_('Automatically generated from name field if left empty')
    )
    category = models.ForeignKey(
        MenuCategory,
        blank=False,
        null=False,
        related_name="menu_category",
        verbose_name=_("Category"),
        help_text=_("Use to specify section where menu will be displayed"),
        default=None
    )
    menu_location = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=MENU_PLACEMENT,
        verbose_name=_("Menu Location"),
        help_text=_("Select menu location on the site layout")
    )
    menu_link = models.ManyToManyField(
        "MenuLink",
        blank=True,
        verbose_name=_("Menu Links"),
        related_name="menu_link",
        # limit_choices_to=Q(active=True, menu__slug=('footer-menu'))
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the page"),
        default=None
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="menu_image"
    )
    code = models.UUIDField(
        max_length=255,
        default=uuid.uuid4,
        blank=False,
        null=False,
        verbose_name=_("Menu Code"),
        help_text=_("Menu Code"),
    )
    sort_order = models.PositiveIntegerField(
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order"),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("A menu entry is only displayed if the label is not empty")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    is_parent = models.BooleanField(
        default=False,
        verbose_name=_("Display as Parent"),
        help_text=_("Is this a parent menu.")
    )
    has_children = models.BooleanField(
        default=True,
        verbose_name=_("Display Children"),
        help_text=_("Children are only displayed if the menu entry itself is displayed.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="menu_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="menu_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("menu_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title', 'category')
        verbose_name = _('Menu')
        verbose_name_plural = _('Menus')


class MenuLink(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Menu link title"),
        help_text=_("The text to be used for this link in the menu."),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Keyword'),
        help_text=_('Automatically generated from name field if left empty')
    )
    route_type = models.CharField(
        max_length=255,
        choices=route_type,
        verbose_name=_("Route"),
        help_text=_("Route Type"),
        null=True,
        blank=True,
        default='route',
    )
    route = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Route"),
        help_text=_("Enter route name. Eg 'about_list' "),
        unique=False
    )
    params = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Route Params"),
        help_text=_("If you are linking to detail page, enter extra route params such as slug value of the page "),
        unique=False
    )
    url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name="url",
        help_text="Url. Accepts formats like '/about', 'http://mydomain.com', ",
        unique=False
    )
    menu = models.ManyToManyField(
        Menu,
        blank=True,
        # null=True,
        related_name="linked_menu",
        verbose_name=_("Menu"),
        help_text=_("The maximum depth for a link and all its children is fixed at 4. Some menu links may not be available as parents if selecting them would exceed this limit."),
        default=None,
        limit_choices_to=Q(status='p', active=True)
    )
    level = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        default=None,
        choices=menu_level,
        verbose_name=_("Menu Level"),
        help_text=_("Select menu level on hierarchy on the site layout")
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the page"),
        default=None
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="menu_link_image"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Menu links that are not enabled will not be listed in any menu.")
    )
    show_expanded = models.BooleanField(
        default=True,
        verbose_name=_("Show as expanded"),
        help_text=_("If selected and this menu link has children, the menu will always appear expanded.")
    )
    is_parent = models.BooleanField(
        default=False,
        verbose_name=_("Display as Parent"),
        help_text=_("Is this a parent menu.")
    )
    parent = models.ForeignKey(
        "MenuLink",
        blank=True,
        null=True,
        related_name="parent_menu",
        verbose_name=_("Parent"),
        help_text=_("The maximum depth for a link and all its children is fixed at 4. Some menu links may not be available as parents if selecting them would exceed this limit."),
        default=None,
        limit_choices_to= Q(status = 'p', active = True, is_parent = True)
    )
    has_children = models.BooleanField(
        default=True,
        verbose_name=_("Display Children"),
        help_text=_("Children are only displayed if the parent menu link entry is displayed.")
    )
    sort_order = models.PositiveIntegerField(
        verbose_name=_("Weight"),
        help_text=_("Optional. In the menu, the heavier links will sink and the lighter links will be positioned nearer the top."),
        blank=True,
        null=True,
        default=0
    )
    alt_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Alt Title"),
        help_text=_("The description displayed when hovering over the link."),
        unique=False
    )
    link_id = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("ID"),
        help_text=_("Specifies a unique ID for the link."),
        unique=False
    )
    link_classes = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Classes"),
        help_text=_("Enter additional classes to be added to the link."),
        unique=False
    )
    link_style = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Style"),
        help_text=_("Enter additional styles to be applied to the link."),
        unique=False
    )
    target = models.CharField(
        max_length=255,
        choices=target,
        verbose_name=_("Target"),
        help_text=_("Specifies where to open the link. Using this attribute breaks XHTML validation."),
        default=None,
        null=True,
        blank=True,
    )
    access_key = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        verbose_name=_("Access Key"),
        help_text=_("Specifies a keyboard shortcut to access this link."),
        unique=False
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="ml_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="ml_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_path(self):
        return self.route

    def get_absolute_url(self):
        return reverse("menu_link_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Menu Link')
        verbose_name_plural = _('Menu Links')