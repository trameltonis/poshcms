from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import View
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework import generics, permissions, mixins
from django.utils.translation import ugettext_lazy as _
from rest_framework import viewsets
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.utils.decorators import method_decorator
from .models import MenuCategory, Menu, MenuLink
from .serializers import MenuSerializer, MenuCreateSerializer, MenuCategorySerializer, MenuCategoryCreateSerializer, MenuLinkSerializer

from .forms import MenuCategoryCreateForm, MenuCategoryEditForm, MenuCreateForm, MenuEditForm, MenuLinkCreateForm, MenuLinkEditForm


class MenuCategoryCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = MenuCategoryCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(MenuCategoryCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MenuCategoryCreateView, self).dispatch(*args, **kwargs)


class MenuCategoryUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = MenuCategoryEditForm
    model = MenuCategory

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(MenuCategoryUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MenuCategoryUpdateView, self).dispatch(*args, **kwargs)


class MenuCategoryDetailView(DetailView):
    model = MenuCategory
    template_name = "menu_manager/_menu_category_detail.html"


class MenuCategoryListView(ListView):
    model = MenuCategory
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(MenuCategoryListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class MenuCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = MenuCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(MenuCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MenuCreateView, self).dispatch(*args, **kwargs)


class MenuUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = MenuEditForm
    model = Menu

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(MenuUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MenuUpdateView, self).dispatch(*args, **kwargs)


class MenuDetailView(DetailView):
    model = Menu
    template_name = "menu_manager/_menu_detail.html"


class MenuListView(ListView):
    model = Menu
    # template_name = "themes/list.html"
    template_name = "menu/_footer_menu.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(MenuListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class MenuLinkCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = MenuLinkCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(MenuLinkCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MenuLinkCreateView, self).dispatch(*args, **kwargs)


class MenuLinkUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = MenuLinkEditForm
    model = MenuLink

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(MenuLinkUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MenuLinkUpdateView, self).dispatch(*args, **kwargs)


class MenuLinkDetailView(DetailView):
    model = MenuLink
    template_name = "menu_manager/_menu_link_detail.html"


class MenuLinkListView(ListView):
    model = MenuLink
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(MenuLinkListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs

