from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from .views import MenuCategoryCreateView, MenuCategoryUpdateView, MenuCategoryDetailView, MenuCategoryListView, MenuCreateView, MenuUpdateView, MenuDetailView, MenuListView, MenuLinkEditForm, MenuLinkCreateForm, MenuLinkCreateView, MenuLinkDetailView, MenuLinkListView, MenuLinkUpdateView


urlpatterns = [

    url(r'^menus/$', MenuListView.as_view(), name='menu_list'),
    url(r'^menus/create/', MenuCreateView.as_view(), name='menu_create'),
    url(r'^menus/edit/(?P<slug>[\w-]+)/$', MenuUpdateView.as_view(), name='menu_update'),
    # url(r'^menus/delete/(?P<pk>\d+)/', MenuDeleteView.as_view(), name='menu_delete'),
    url(r'^menus/(?P<slug>[-a-zA-Z0-9_]+)/$', MenuDetailView.as_view(), name='menu_detail'),

    url(r'^menus-category/$', MenuCategoryListView.as_view(), name='menu_category_list'),
    url(r'^menus-category/create/', MenuCategoryCreateView.as_view(), name='menu_category_create'),
    url(r'^menus-category/edit/(?P<slug>[-a-zA-Z0-9_]+)/', MenuCategoryUpdateView.as_view(), name='menu_category_update'),
    # url(r'^menus/category/delete/(?P<pk>\d+)/', MenuCategoryDeleteView.as_view(), name='menu_category_delete'),
    url(r'^menus-category/(?P<slug>[-a-zA-Z0-9_]+)/$', MenuCategoryDetailView.as_view(), name='menu_category_detail'),

    url(r'^menu-links/$', MenuLinkListView.as_view(), name='menu_link_list'),
    url(r'^menu-links/create/', MenuLinkCreateView.as_view(), name='menu_link_create'),
    url(r'^menu-links/edit/(?P<slug>[\w-]+)/$', MenuLinkUpdateView.as_view(), name='menu_link_update'),
    # url(r'^menu-links/delete/(?P<pk>\d+)/', MenuLinkDeleteView.as_view(), name='menu_link_delete'),
    url(r'^menu-links/(?P<slug>[-a-zA-Z0-9_]+)/$', MenuLinkDetailView.as_view(), name='menu_link_detail'),

]

urlpatterns = format_suffix_patterns(urlpatterns)

