from modeltranslation.translator import translator, register, TranslationOptions
from .models import Menu, MenuCategory, MenuLink


@register(Menu)
class MenuTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        'img',
    ]
    # required_languages = ('en-us', 'es-us', 'en-ca', 'fr-ca', )
    # fallback_languages = {
    #     'default': ('en-us', 'en-ca',),
    #     'fr-ca': ('en-ca',),
    #     'es-us': ('en-us', 'en-ca',)
    # }


@register(MenuCategory)
class MenuCategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
    ]
    # required_languages = ('en-us', 'es-us', 'en-ca', 'fr-ca', )
    # fallback_languages = {
    #     'default': ('en-us', 'en-ca',),
    #     'fr-ca': ('en-ca',),
    #     'es-us': ('en-us', 'en-ca',)
    # }


@register(MenuLink)
class MenuLinkTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        'alt_title',
        'img',
    ]
    # required_languages = ('en-us', 'es-us', 'en-ca', 'fr-ca', )
    # fallback_languages = {
    #     'default': ('en-us', 'en-ca',),
    #     'fr-ca': ('en-ca',),
    #     'es-us': ('en-us', 'en-ca',)
    # }
