# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-31 10:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu_manager', '0004_auto_20160724_1149'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='menulink',
            name='menu',
        ),
        migrations.AddField(
            model_name='menulink',
            name='menu',
            field=models.ManyToManyField(blank=True, default=None, help_text='The maximum depth for a link and all its children is fixed at 4. Some menu links may not be available as parents if selecting them would exceed this limit.', related_name='linked_menu', to='menu_manager.Menu', verbose_name='Menu'),
        ),
    ]
