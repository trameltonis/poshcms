from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Field, Hidden
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import MenuCategory, Menu, MenuLink


class MenuCategoryCreateForm(forms.ModelForm):
    class Meta:
        model = MenuCategory
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'status',
            'visibility',
            'progress_status',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class MenuCategoryEditForm(forms.ModelForm):
    class Meta:
        model = MenuCategory
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'status',
            'visibility',
            'progress_status',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class MenuCreateForm(forms.ModelForm):
    class Meta:
        model = Menu
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'category',
            'title',
            'slug',
            # 'label',
            # 'route_type',
            # 'route',
            # 'url',
            'description',
            'img',
            'status',
            'visibility',
            'progress_status',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'is_parent',
            'has_children',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class MenuEditForm(forms.ModelForm):
    class Meta:
        model = Menu
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'category',
            'title',
            'slug',
            # 'label',
            # 'route_type',
            # 'route',
            # 'url',
            'description',
            'img',
            'status',
            'visibility',
            'progress_status',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'is_parent',
            'has_children',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class MenuLinkCreateForm(forms.ModelForm):
    class Meta:
        model = MenuLink
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            # 'path',
            'route_type',
            'route',
            'params',
            'url',
            'menu',
            'level',
            'is_parent',
            'parent',
            'has_children',
            'description',
            'img',
            'sort_order',
            'status',
            'visibility',
            'progress_status',
            'active',
            'show_expanded',
            'alt_title',
            'link_id',
            'link_classes',
            'link_style',
            'target',
            'slug',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class MenuLinkEditForm(forms.ModelForm):
    class Meta:
        model = MenuLink
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            # 'path',
            'route_type',
            'route',
            'params',
            'url',
            'menu',
            'level',
            'is_parent',
            'parent',
            'has_children',
            'description',
            'img',
            'sort_order',
            'status',
            'visibility',
            'progress_status',
            'active',
            'show_expanded',
            'alt_title',
            'link_id',
            'link_classes',
            'link_style',
            'target',
            'slug',
            'publish_start',
            'publish_end',
            'active',
            'lock_locale',
            'created_by',
            'updated_by',

            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )
