from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import export_as_json, make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from .models import Menu, MenuCategory, MenuLink


@admin.register(MenuCategory)
class MenuCategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'language_selectors', 'status', 'visibility', 'updated')
    search_fields = ['title']
    list_filter = ['active']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Menu)
class MenuAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'title',
                'category',
                'menu_location',
                'menu_link',
                'sort_order',
                'description',
                'img',
                'status',
                'visibility',
                'progress_status',

            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'is_parent',
                'has_children',
                'created_by',
                'updated_by'
            ),
        }),
    )
    filter_horizontal = ('menu_link',)
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug',  'language_selectors', 'status', 'category', 'menu_location', 'is_parent', 'has_children', 'progress_status', 'updated')
    search_fields = ['title', 'label']
    list_filter = ['is_parent', 'has_children', 'active']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(MenuLink)
class MenuLinkAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'title',
                # 'path',
                'route_type',
                'route',
                'params',
                'url',
                'menu',
                'level',
                'is_parent',
                'parent',
                'has_children',
                'description',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',

            )
        }),
        (_('Menu Link Attributes'), {
            'classes': ('open',),
            'fields': (
                'active',
                'show_expanded',
                'alt_title',
                'link_id',
                'link_classes',
                'link_style',
                'target',
            ),
        }),
        (_('Advanced options'), {
            'classes': ('open',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    filter_horizontal = ('menu',)
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug',  'language_selectors', 'level', 'status', 'visibility', 'parent', 'is_parent', 'has_children', 'progress_status', 'updated')
    search_fields = ['title', 'description', 'menu' ]
    list_filter = ['menu', 'is_parent', 'has_children', 'active', 'level']
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
