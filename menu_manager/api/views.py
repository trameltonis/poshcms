from rest_framework import generics
from django.views import generic as cbv
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from ..models import *
from .serializers import *


class MenuFilter(filters.FilterSet):
    class Meta:
        model = Menu
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'category',
            'menu_location',
            'is_parent',
            'has_children',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]

class MenuLinkFilter(filters.FilterSet):
    class Meta:
        model = MenuLink
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'route_type',
            'menu',
            'is_parent',
            'parent',
            'has_children',
            'show_expanded',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


class MenuCategoryFilter(filters.FilterSet):
    class Meta:
        model = MenuCategory
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'lock_locale',
            'progress_status',
            'id'
        ]


# API ViewSets

class MenuViewSet(viewsets.ModelViewSet):
    queryset = Menu.objects.all().order_by('sort_order')
    serializer_class = MenuSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MenuLinkViewSet(viewsets.ModelViewSet):
    queryset = MenuLink.objects.all().order_by('-sort_order')
    serializer_class = MenuLinkSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MenuCategoryViewSet(viewsets.ModelViewSet):
    queryset = MenuCategory.objects.all().order_by('-created_on')
    serializer_class = MenuCategorySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']




