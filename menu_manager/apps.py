from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MenuManagerConfig(AppConfig):
    name = 'menu_manager'
    verbose_name = _('Menu Manager')
