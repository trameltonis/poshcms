from django.contrib.auth.models import User, Group
from rest_framework import serializers
from core.models import SocialMedia, Locale, Format, AccountGroup
from .models import TeamPage, Category, People, Department, Region
from menu_manager.models import Menu, MenuCategory
from news.models import Post, Tags, PostView, News, Event, EventTags
from media_manager.models import File, Image, Gallery, MediaTag, MediaType
from core.serializers import LocaleSerializer, FormatSerializer, SocialMediaSerializer
from theme.models import Layout, Template
from theme.serializers import LayoutSerializer, LayoutCreateSerializer, TemplateSerializer, TemplateCreateSerializer
from menu_manager.serializers import MenuCategorySerializer, MenuCategoryCreateSerializer, MenuCreateSerializer, MenuSerializer


# HR Serialisers
class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class TeamPageSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = TeamPage
        fields = '__all__'
        depth = 3


class TeamPageCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = TeamPage
        fields = '__all__'
        depth = 3


class PeopleSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = People
        fields = '__all__'
        depth = 3


class PeopleCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = People
        fields = '__all__'
        depth = 3


class DepartmentSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Department
        fields = '__all__'
        depth = 3


class DepartmentCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Department
        fields = '__all__'
        depth = 3


class RegionSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 3


class RegionCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 3


