from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from .views import TeamPageCreateView, TeamPageUpdateView, TeamPageListView, TeamPageDetailView, CategoryCreateView, CategoryUpdateView, CategoryListView, CategoryDetailView, LeadershipTeamListView, PeopleListView, PeopleCreateView, PeopleUpdateView, PeopleDetailView, DepartmentListView, DepartmentCreateView, DepartmentUpdateView, DepartmentDetailView, RegionListView, RegionCreateView, RegionUpdateView, RegionDetailView


urlpatterns = [
    url(r'^categories/$', CategoryListView.as_view(), name='team_category_list'),
    url(r'^categories/create/', CategoryCreateView.as_view(), name='team_category_create'),
    url(r'^categories/edit/(?P<slug>[\w-]+)/$', CategoryUpdateView.as_view(), name='team_category_update'),
    url(r'^categories/(?P<slug>[\w-]+)/$', CategoryDetailView.as_view(), name='team_category_detail'),

    url(r'^people/$', PeopleListView.as_view(), name='people_list'),
    url(r'^people/create/', PeopleCreateView.as_view(), name='people_create'),
    url(r'^people/edit/(?P<slug>[\w-]+)/$', PeopleUpdateView.as_view(), name='people_update'),
    url(r'^people/(?P<slug>[\w-]+)/$', PeopleDetailView.as_view(), name='people_detail'),

    url(r'^team-pages/$', TeamPageListView.as_view(), name='team_page_list'),
    url(r'^leadership-team/$', LeadershipTeamListView.as_view(), name='leadership_team_list'),
    url(r'^team-page/create/', TeamPageCreateView.as_view(), name='team_page_create'),
    url(r'^team-pages/edit/(?P<slug>[\w-]+)/$', TeamPageUpdateView.as_view(), name='team_page_update'),
    url(r'^team/(?P<slug>[\w-]+)/$', TeamPageDetailView.as_view(), name='team_page_detail'),

    url(r'^department/$', DepartmentListView.as_view(), name='department_list'),
    url(r'^department/create/', DepartmentCreateView.as_view(), name='department_create'),
    url(r'^department/edit/(?P<slug>[\w-]+)/$', DepartmentUpdateView.as_view(), name='department_update'),
    url(r'^department/(?P<slug>[\w-]+)/$', DepartmentDetailView.as_view(), name='department_detail'),

    url(r'^region/$', RegionListView.as_view(), name='region_list'),
    url(r'^region/create/', RegionCreateView.as_view(), name='region_create'),
    url(r'^region/edit/(?P<slug>[\w-]+)/$', RegionUpdateView.as_view(), name='region_update'),
    url(r'^region/(?P<slug>[\w-]+)/$', RegionDetailView.as_view(), name='region_detail'),
]
urlpatterns = format_suffix_patterns(urlpatterns)

