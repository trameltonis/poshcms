from rest_framework import serializers
from ..models import *
from cmf.api.serializers import *
from menu_manager.api.serializers import *
from theme.api.serializers import *


class HrCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class HrCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class TeamPageSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # menu = MenuSerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = TeamPage
        fields = '__all__'
        depth = 3


class TeamPageCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = TeamPage
        fields = '__all__'
        depth = 3


class PeopleSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = People
        fields = '__all__'
        depth = 3


class PeopleCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = People
        fields = '__all__'
        depth = 3


class DepartmentSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Department
        fields = '__all__'
        depth = 3


class DepartmentCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Department
        fields = '__all__'
        depth = 3


class RegionSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 3


class RegionCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 3