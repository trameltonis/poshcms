from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _

from rest_framework import generics
from ..models import *
from .serializers import *


class TeamPageViewSet(viewsets.ModelViewSet):
    queryset = TeamPage.objects.all().order_by('-sort_order', '-created_on')
    serializer_class = TeamPageSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class PeopleViewSet(viewsets.ModelViewSet):
    queryset = People.objects.all().order_by('-sort_order', '-created_on')
    serializer_class = PeopleSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all().order_by('-sort_order', '-created_on')
    serializer_class = DepartmentSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all().order_by('-sort_order', '-created_on')
    serializer_class = RegionSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class TeamPageFilter(filters.FilterSet):
    class Meta:
        model = TeamPage
        fields = ['slug', 'id', 'status', 'visibility']


class PeopleFilter(filters.FilterSet):
    class Meta:
        model = People
        fields = ['slug', 'id', 'status', 'visibility']


class DepartmentFilter(filters.FilterSet):
    class Meta:
        model = Department
        fields = ['slug', 'id', 'status', 'visibility']


class RegionFilter(filters.FilterSet):
    class Meta:
        model = Region
        fields = ['slug', 'id', 'status', 'visibility']


@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        page = TeamPage.objects.get(slug=slug)
    except TeamPage.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TeamPageSerializer(page)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TeamPageSerializer(page, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        page.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)





