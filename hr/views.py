from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from .forms import TeamPageCreateForm, TeamPageEditForm, CategoryCreateForm, CategoryEditForm, PeopleEditForm, PeopleCreateForm, DepartmentEditForm, DepartmentCreateForm, RegionEditForm, RegionCreateForm
from .models import TeamPage, Category, People, Department, Region
from menu_manager.models import MenuCategory, Menu
from .serializers import CategoryCreateSerializer, CategorySerializer, TeamPageCreateSerializer, TeamPageSerializer, PeopleSerializer, PeopleCreateSerializer, DepartmentSerializer, DepartmentCreateSerializer, RegionSerializer, RegionCreateSerializer


class TeamPageViewSet(viewsets.ModelViewSet):
    queryset = TeamPage.objects.all().order_by('-created_on')
    serializer_class = TeamPageSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class PeopleViewSet(viewsets.ModelViewSet):
    queryset = People.objects.all().order_by('-created_on')
    serializer_class = PeopleSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all().order_by('-created_on')
    serializer_class = DepartmentSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all().order_by('-created_on')
    serializer_class = RegionSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class TeamPageFilter(filters.FilterSet):
    class Meta:
        model = TeamPage
        fields = ['slug', 'id', 'status', 'visibility']


class PeopleFilter(filters.FilterSet):
    class Meta:
        model = People
        fields = ['slug', 'id', 'status', 'visibility']


class DepartmentFilter(filters.FilterSet):
    class Meta:
        model = Department
        fields = ['slug', 'id', 'status', 'visibility']


class RegionFilter(filters.FilterSet):
    class Meta:
        model = Region
        fields = ['slug', 'id', 'status', 'visibility']


# Team Page Api endpoint
class TeamPageList(generics.ListAPIView):
    queryset = TeamPage.objects.all()
    serializer_class = TeamPageSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TeamPageFilter


class TeamPageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TeamPage.objects.all()
    serializer_class = TeamPageSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TeamPageCreate(generics.CreateAPIView):
    serializer_class = TeamPageCreateSerializer


@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        page = TeamPage.objects.get(slug=slug)
    except TeamPage.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TeamPageSerializer(page)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TeamPageSerializer(page, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        page.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# Category Api endpoint
class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CategoryCreate(generics.CreateAPIView):
    serializer_class = CategoryCreateSerializer


class TeamPageCreateView(CreateView):
    template_name = "hr/create_form.html"
    form_class = TeamPageCreateForm
    page_title = _("Team Page Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TeamPageCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TeamPageCreateView, self).dispatch(*args, **kwargs)


class TeamPageUpdateView(UpdateView):
    template_name = "hr/edit.html"
    form_class = TeamPageEditForm
    model = TeamPage
    page_title = _("Team Page Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(TeamPageUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TeamPageUpdateView, self).dispatch(*args, **kwargs)


class TeamPageDetailView(DetailView):
    model = TeamPage
    template_name = "hr/_team_page_detail.html"


class TeamPageListView(ListView):
    model = TeamPage
    page_title = _("Team Page List"),
    template_name = "hr/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(TeamPageListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class LeadershipTeamListView(ListView):
    model = TeamPage
    page_title = _("Team Page List"),
    template_name = "hr/leadership_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(LeadershipTeamListView, self).get_queryset(*args, **kwargs).order_by("sort_order").filter(category__slug="leadership", status='p', active=True)
        return qs


class CategoryCreateView(CreateView):
    template_name = "hr/create_form.html"
    form_class = CategoryCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CategoryCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryCreateView, self).dispatch(*args, **kwargs)


class CategoryUpdateView(UpdateView):
    template_name = "hr/edit.html"
    form_class = CategoryEditForm
    model = Category

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(CategoryUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryUpdateView, self).dispatch(*args, **kwargs)


class CategoryDetailView(DetailView):
    model = Category
    template_name = "hr/_category_detail.html"


class CategoryListView(ListView):
    model = Category
    template_name = "hr/list2.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CategoryListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class PeopleCreateView(CreateView):
    template_name = "hr/create_form.html"
    form_class = PeopleCreateForm
    page_title = _("People Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(PeopleCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PeopleCreateView, self).dispatch(*args, **kwargs)


class PeopleUpdateView(UpdateView):
    template_name = "hr/edit.html"
    form_class = PeopleEditForm
    model = People
    page_title = _("People Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(PeopleUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PeopleUpdateView, self).dispatch(*args, **kwargs)


class PeopleDetailView(DetailView):
    model = People
    template_name = "hr/_people_detail.html"


class PeopleListView(ListView):
    model = People
    page_title = _("People List"),
    template_name = "hr/_people_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(PeopleListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class DepartmentCreateView(CreateView):
    template_name = "hr/create_form.html"
    form_class = DepartmentCreateForm
    page_title = _("Department Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(DepartmentCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DepartmentCreateView, self).dispatch(*args, **kwargs)


class DepartmentUpdateView(UpdateView):
    template_name = "hr/edit.html"
    form_class = DepartmentEditForm
    model = Department
    page_title = _("Department Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(DepartmentUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DepartmentUpdateView, self).dispatch(*args, **kwargs)


class DepartmentDetailView(DetailView):
    model = Department
    template_name = "hr/_department_detail.html"


class DepartmentListView(ListView):
    model = Department
    page_title = _("Department List"),
    template_name = "hr/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(DepartmentListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class RegionCreateView(CreateView):
    template_name = "hr/create_form.html"
    form_class = RegionCreateForm
    page_title = _("Region Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(RegionCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RegionCreateView, self).dispatch(*args, **kwargs)


class RegionUpdateView(UpdateView):
    template_name = "hr/edit.html"
    form_class = RegionEditForm
    model = Region
    page_title = _("Region Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(RegionUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(RegionUpdateView, self).dispatch(*args, **kwargs)


class RegionDetailView(DetailView):
    model = Region
    template_name = "hr/_region_detail.html"


class RegionListView(ListView):
    model = Region
    page_title = _("Region List"),
    template_name = "hr/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(RegionListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


















