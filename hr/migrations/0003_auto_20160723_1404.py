# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-23 19:04
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('hr', '0002_auto_20160723_1404'),
        ('theme', '0001_initial'),
        ('filer', '0005_auto_20160624_0202'),
        ('core', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='teampage',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='team_page_theme', to='theme.BaseTheme', verbose_name='Theme'),
        ),
        migrations.AddField(
            model_name='teampage',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='team_page_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='region',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='region_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='region',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='region_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='position',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='position_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='position',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='position_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='people',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='people_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='people',
            name='department',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='people_dept', to='hr.Department'),
        ),
        migrations.AddField(
            model_name='people',
            name='img',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='profile_image', to='filer.Image'),
        ),
        migrations.AddField(
            model_name='people',
            name='position',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='people_position', to='hr.Position', verbose_name='Position'),
        ),
        migrations.AddField(
            model_name='people',
            name='region',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='people_region', to='hr.Region'),
        ),
        migrations.AddField(
            model_name='people',
            name='social',
            field=models.ManyToManyField(blank=True, related_name='people_social', to='core.SocialMedia', verbose_name='Share'),
        ),
        migrations.AddField(
            model_name='people',
            name='team_page',
            field=models.ForeignKey(blank=True, default=None, help_text='Select a content related to this', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='team_page_gallery', to='hr.TeamPage', verbose_name='Team Page'),
        ),
        migrations.AddField(
            model_name='people',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='people_theme', to='theme.BaseTheme', verbose_name='Theme'),
        ),
        migrations.AddField(
            model_name='people',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='people_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='department',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dept_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='department',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dept_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='category',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hr_category_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='category',
            name='updated_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hr_category_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='teampage',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='region',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='position',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='department',
            unique_together=set([('slug', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='category',
            unique_together=set([('slug', 'title')]),
        ),
    ]
