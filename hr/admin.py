from django.db import models
from import_export import resources, fields
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django import forms
from helpers.utils import *
import nested_admin
from filer.models import Image, File
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationStackedInline, TranslationAdmin, TranslationBaseModelAdmin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget, DecimalWidget, IntegerWidget, CharWidget, BooleanWidget, DateTimeWidget, TimeWidget, DateWidget
from .models import Department, Position, People, Region, TeamPage, Category


@admin.register(Category)
class CategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',),
    }
    list_display = ('title', 'slug', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class PeopleInline(nested_admin.NestedStackedInline, TranslationStackedInline):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    model = People
    extra = 1


@admin.register(People)
class PeopleAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'first_name',
                'last_name',
                'middle_name',
                'phone',
                'email',
                'facebook',
                'twitter',
                'linkedin',
                'position',
                'region',
                'department',
                'quote',
                'layout',
                'theme',
                'img',
                'content',
                'sort_order',
                'social',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'url',
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    filter_horizontal = ('quote', 'social',)
    prepopulated_fields = {
        'slug': ('section', 'first_name', 'last_name'),
        'url': ('first_name', 'last_name'),
    }
    list_display = (
        'first_name', 'last_name', 'language_selectors', 'position', 'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['first_name', 'last_name', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',   'status']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    list_editable = ['sort_order']
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Department)
class DepartmentAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'description',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )

    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = (
        'title', 'language_selectors', 'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',   'status']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Position)
class PositionAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'description',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = (
        'title', 'language_selectors',  'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',   'status']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))



@admin.register(Region)
class RegionAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'description',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
    }
    list_display = (
        'title', 'language_selectors',  'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',   'status']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(TeamPage)
class TeamPageAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'content',
                'category',
                'hb_title',
                'hb_slogan',
                'theme',
                'menu_link',
                'img',
                'people',
                'timelines',
                'quotes',
                'snippets',
                'sort_order',
                'social',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    filter_horizontal = (
        'people',
        'social',
        'timelines',
        'quotes',
        'snippets',
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',),
        'seo_title': ('title',),
        'seo_keywords': ('slug',)
    }
    list_display = (
        'title', 'language_selectors',  'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    # inlines = [PeopleInline]
    list_filter = ['section', 'publish_start',  'publish_end',   'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))

