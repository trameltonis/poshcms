from django.conf import settings
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
import base64
import uuid
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from core.models import *
from menu_manager.models import *
from theme.models import *
from featured.models import *
from cmf.models import *
from helpers.utils import *


class Category(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the category."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="hr_category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="hr_category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Department(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Department Name"),
        help_text=_("Name of functional department"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the department."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Department Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="dept_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="dept_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("dept_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Department')
        verbose_name_plural = _('Departments')


class Position(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the position."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Position Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="position_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="position_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("position_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Position')
        verbose_name_plural = _('Positions')


class People(models.Model):
    first_name = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        verbose_name=_("First Name"),
        help_text=_("First Name"),
        unique=False
    )
    last_name = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        verbose_name=_("Last Name"),
        help_text=_("Last Name"),
        unique=False
    )
    middle_name = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        verbose_name=_("Middle Name"),
        help_text=_("Middle Name"),
        unique=False
    )
    phone = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        verbose_name=_("Phone"),
        help_text=_("Phone number"),
        unique=False
    )
    email = models.EmailField(
        max_length=120,
        null=True,
        blank=True,
        verbose_name=_("Email"),
        help_text=_("Email address"),
    )
    facebook = models.URLField(
        max_length=120,
        null=True,
        blank=True,
        verbose_name=_("Facebook"),
        help_text=_("Facebook url"),
    )
    twitter = models.URLField(
        max_length=120,
        null=True,
        blank=True,
        verbose_name=_("Twitter"),
        help_text=_("Twitter url"),
    )
    linkedin = models.URLField(
        max_length=120,
        null=True,
        blank=True,
        verbose_name=_("LinkedIn"),
        help_text=_("LinkedIn url"),
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Bio"),
        help_text=_("More information."),
        default=None
    )
    position = models.ForeignKey(
        Position,
        blank=True,
        null=True,
        verbose_name=_("Position"),
        related_name="people_position"
    )
    department = models.ForeignKey(
        Department,
        blank=True,
        null=True,
        default=None,
        related_name="people_dept"
    )
    region = models.ForeignKey(
        "Region",
        blank=True,
        null=True,
        default=None,
        related_name="people_region"
    )
    quote = models.ManyToManyField(
        Quote,
        blank=True,
        verbose_name=_("Quotes"),
        help_text=_("Select quotes"),
        default=None,
        related_name="people_quotes",
        limit_choices_to=Q(status='p', active=True)
    )
    layout = models.ForeignKey(
        CustomLayout,
        max_length=255,
        blank=False,
        null=False,
        help_text=_("Add layout for this profile"),
        default=None,
        related_name="people_layout"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="people_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="profile_image"
    )
    team_page = models.ForeignKey(
        "TeamPage",
        blank=True,
        null=True,
        verbose_name=_("Team Page"),
        help_text=_("Select a content related to this"),
        default=None,
        related_name="team_page_gallery"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables person to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="people_social"
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="people_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="people_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.first_name + " " + self.last_name

    def get_absolute_url(self):
        return reverse("people_detail", kwargs={'slug': self.slug})

    def full_name(self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name = _('Person')
        verbose_name_plural = _('People')
        ordering = ['created_on']


class Region(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Region Name"),
        help_text=_("Region name, eg. Norwex USA"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the region."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Region Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="region_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="region_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("region_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Region')
        verbose_name_plural = _('Regions')


class TeamPage(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Team Page title"),
        unique=False
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("More information about the page."),
        default=None
    )
    hb_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("Header Block title"),
        help_text=_("Team page header block title")
    )
    hb_slogan = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Header block Slogan"),
        help_text=_("Header block slogan."),
        default=None

    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="team_page_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="team_page_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="team_page_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="team_page_image"
    )
    people = models.ManyToManyField(
        People,
        blank=True,
        # null=True,
        default=None,
        related_name="team_page_people"
    )
    timelines = models.ManyToManyField(
        Timeline,
        blank=True,
        verbose_name=_("Timelines"),
        help_text=_("Select timelines"),
        default=None,
        related_name="teampage_timelines"
    )
    quotes = models.ManyToManyField(
        Quote,
        blank=True,
        verbose_name=_("Quotes"),
        help_text=_("Select quotes"),
        default=None,
        related_name="teampage_quotes"
    )
    snippets = models.ManyToManyField(
        Snippet,
        blank=True,
        verbose_name=_("Snippets"),
        help_text=_("Select snippet"),
        default=None,
        related_name="teampage_snippets"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="team_page_social"
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="team_page_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="team_page_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("team_page_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Team Page')
        verbose_name_plural = _('Team Pages')


# class PeopleInline(People):
#     team_page = models.ForeignKey(
#         TeamPage,
#         blank=False,
#         verbose_name=_("Team Page"),
#         help_text=_("Select a content related to this"),
#         default=None,
#         related_name="team_page_gallery"
#     )
#     sort_order = models.PositiveIntegerField(
#         null=True,
#         blank=True,
#         verbose_name=_("Sort Order"),
#         help_text=_("Content Sort Order."),
#         default=0
#     )
#
#     def __str__(self):
#         return str(self.id)
#
#     class Meta:
#         verbose_name = _('People Inline')
#         verbose_name_plural = _('People Inline')
#







