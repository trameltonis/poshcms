from django import forms
from django.utils.translation import ugettext_lazy as _
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, Row, Field, Hidden, ButtonHolder
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import TeamPage, Category, People, Department, Region
from menu_manager.models import Menu, MenuCategory
from menu_manager.forms import MenuCategoryCreateForm, MenuCategoryEditForm, MenuCreateForm, MenuEditForm


class CategoryCreateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Category
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class CategoryEditForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Category
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'progress_status',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class TeamPageCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = TeamPage
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'content',
        'hb_title',
        'hb_slogan',
        'theme',
        'menu_link',
        'category',
        'img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'social'
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class TeamPageEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = TeamPage
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'content',
        'hb_title',
        'hb_slogan',
        'theme',
        'menu_link',
        'category',
        'img',
        'sort_order',
        'status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class PeopleCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = People
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'first_name',
        'last_name',
        'middle_name',
        'phone',
        'email',
        'facebook',
        'twitter',
        'linkedin',
        'position',
        'region',
        'department',
        'quote',
        'layout',
        'theme',
        'img',
        'content',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'slug',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class PeopleEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = People
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'first_name',
        'last_name',
        'middle_name',
        'phone',
        'email',
        'facebook',
        'twitter',
        'linkedin',
        'position',
        'region',
        'department',
        'quote',
        'layout',
        'theme',
        'img',
        'content',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'slug',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class DepartmentCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Department
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class DepartmentEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Department
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class RegionCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Region
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class RegionEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Region
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )





