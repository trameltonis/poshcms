from modeltranslation.translator import translator, register, TranslationOptions
from .models import Department, Position, People, Region, TeamPage, Category


@register(People)
class PeopleTranslationOptions(TranslationOptions):
    fields = [
        'content',
        ]


@register(Position)
class PositionTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(Department)
class DepartmentTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]

@register(Region)
class RegionTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]

@register(TeamPage)
class TeamPageTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'hb_title',
        'hb_slogan',
        'seo_title',
        # 'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]
