from modeltranslation.translator import translator, register, TranslationOptions
from .models import Layout, Template, BaseTheme, Header, Footer

@register(Layout)
class LayoutTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'description',
        ]


@register(Template)
class TemplateTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'description',
        ]


@register(BaseTheme)
class BaseThemeTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'description',
        ]


@register(Header)
class HeaderTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'description',
        ]


@register(Footer)
class FooterTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'description',
        ]
