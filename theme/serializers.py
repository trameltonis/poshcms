from django.contrib.auth.models import User, Group
from rest_framework import serializers
from core.models import SocialMedia, Locale, Format, AccountGroup
from cmf.models import Page, Category, Tags, Resource, Email, Dashboard
from menu_manager.models import Menu, MenuCategory
from news.models import Post, Tags, PostView, News, Event, EventTags
from media_manager.models import File, Image, Gallery, MediaTag, MediaType
from .models import Layout, Template, BaseTheme


class LayoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'
        depth = 3


class LayoutCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'
        depth = 3


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = '__all__'
        depth = 3


class TemplateCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = '__all__'
        depth = 3

class BaseThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseTheme
        fields = '__all__'
        depth = 3
class BaseThemeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseTheme
        fields = '__all__'
        depth = 3