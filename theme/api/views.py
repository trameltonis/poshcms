from rest_framework import generics
from django.views import generic as cbv
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend

from ..models import *
from .serializers import *


class LayoutFilter(filters.FilterSet):
    class Meta:
        model = Layout
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class TemplateFilter(filters.FilterSet):
    class Meta:
        model = Template
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class BaseThemeFilter(filters.FilterSet):
    class Meta:
        model = BaseTheme
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]




# API ViewSets
class LayoutViewSet(viewsets.ModelViewSet):
    queryset = Layout.objects.all().order_by('-created_on')
    serializer_class = LayoutSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = LayoutFilter


class TemplateViewSet(viewsets.ModelViewSet):
    queryset = Template.objects.all().order_by('-created_on')
    serializer_class = TemplateSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TemplateFilter


class BaseThemeViewSet(viewsets.ModelViewSet):
    queryset = BaseTheme.objects.all().order_by('-created_on')
    serializer_class = BaseThemeSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = BaseThemeFilter



