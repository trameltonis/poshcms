from ..models import *
from rest_framework import serializers


class LayoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'
        depth = 3


class LayoutCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'
        depth = 3


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = '__all__'
        depth = 3


class TemplateCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = '__all__'
        depth = 3

class BaseThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseTheme
        fields = '__all__'
        depth = 3
class BaseThemeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseTheme
        fields = '__all__'
        depth = 3