from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
import base64
import uuid
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from core.models import *
from menu_manager.models import *
from helpers.utils import *


class Header(models.Model):
    #ct = get_content_type
    name = models.CharField(
            max_length=255,
            default=None,
            blank=False,
            null=False,
            verbose_name=_("Header Name"),
            help_text=_("Header name eg. Page header, Footer header, Sitewide header, etc")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_("Slug")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    logo = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_('Logo Image'),
        related_name="header_logo"
    )
    description = models.TextField(
            blank=True,
            null=True,
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    set_primary = models.BooleanField(
            verbose_name=_("Set as Primary header"),
            help_text=_("Set as sitewide header."),
            default=False
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Header Status.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="header_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="header_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Header')
        verbose_name_plural = _('Headers')


class Footer(models.Model):
    #ct = get_content_type
    name = models.CharField(
            max_length=255,
            default=None,
            blank=False,
            null=False,
            verbose_name=_("Footer Name"),
            help_text=_("Footer name eg. Page footer, News footer, etc")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_("Slug")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    logo = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_('Logo Image'),
        related_name="footer_logo"
    )
    menu = models.ForeignKey(
        Menu,
        blank=True,
        null=True,
        related_name="footer_menu",
        verbose_name=_("Menu"),
        help_text=_("The maximum depth for a link and all its children is fixed at 4. Some menu links may not be available as parents if selecting them would exceed this limit."),
        default=None
    )
    description = models.TextField(
            blank=True,
            null=True,
    )
    copyright = models.ForeignKey(
        SiteConfig,
        blank=True,
        null=True,
        related_name="footer_copyright",
        verbose_name=_("Copyright"),
        help_text=_("Footer copyright text"),
        default=None
    )
    footer_text = models.ForeignKey(
        SiteConfig,
        blank=True,
        null=True,
        related_name="content_footer_text",
        verbose_name=_("Footer Text"),
        help_text=_("Footer text"),
        default=None
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    set_primary = models.BooleanField(
            verbose_name=_("Set as Primary header"),
            help_text=_("Set as sitewide header."),
            default=False
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Footer Status.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="footer_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="footer_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Footer')
        verbose_name_plural = _('Footers')


class Template(models.Model):
    #ct = get_content_type
    base_framework = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            choices=template_framework,
            default="foundation",
            verbose_name=_("Base Framework"),
            help_text=_("Select the base framework you use.")
    )
    name = models.CharField(
            max_length=255,
            default=None,
            blank=False,
            null=False,
            verbose_name=_("Template Name"),
            help_text=_("Template name eg. Blue Dolphin, Canvas, etc")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_("Slug")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    code = models.UUIDField(
            max_length=255,
            default=uuid.uuid4,
            blank=False,
            null=False,
            verbose_name=_("Template Code"),
            help_text=_("Template Code"),
    )
    description = models.TextField(
            blank=True,
            null=True,
    )
    set_primary = models.BooleanField(
            verbose_name=_("Set as primary template"),
            help_text=_("Set up as primary template."),
            default=False
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Template Status.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="template_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="template_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Template')
        verbose_name_plural = _('Templates')


class Layout(models.Model):
    base_framework = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            choices=template_framework,
            default="foundation",
            verbose_name=_("Base Framework"),
            help_text=_("Select the base framework you use.")
    )
    name = models.CharField(
            max_length=255,
            default=None,
            blank=False,
            null=False,
            verbose_name=_("Layout Name"),
            help_text=_("Layout name eg. Video Gallery Layout, Document Listing Layout, etc")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_("Slug")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    code = models.UUIDField(
            max_length=255,
            default=uuid.uuid4,
            blank=False,
            null=False,
            verbose_name=_("Layout Code"),
            help_text=_("Layout Code"),
    )
    columns = models.CharField(
            max_length=2,
            default="3",
            blank=False,
            null=False,
            verbose_name=_("Grid Columns"),
            help_text=_("Number of columns in the grid. Eg. for 'medium-4 columns', just enter the number 4")
    )
    description = models.TextField(
            blank=True,
            null=True,
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Layout Status.")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="layout_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="layout_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Layout')
        verbose_name_plural = _('Layouts')

    @property
    def template(self):
        return 'theme/custom-layouts/{}.html'.format(self.slug)


class BaseTheme(models.Model):
    base_framework = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            choices=template_framework,
            default="foundation",
            verbose_name=_("Base Framework"),
            help_text=_("Select the base framework you use.")
    )
    name = models.CharField(
            max_length=255,
            default=None,
            blank=False,
            null=False,
            verbose_name=_("Theme Name"),
            help_text=_("Theme name eg. Video Gallery Layout, Document Listing Layout, etc")
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
            unique=True,
            verbose_name=_("Slug")
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    code = models.UUIDField(
            max_length=255,
            default=uuid.uuid4,
            blank=False,
            null=False,
            verbose_name=_("Theme Code"),
            help_text=_("Theme Code"),
    )
    columns = models.CharField(
            max_length=2,
            default="3",
            blank=False,
            null=False,
            verbose_name=_("Grid Columns"),
            help_text=_("Number of columns in the grid. Eg. for 'medium-4 columns', just enter the number 4")
    )
    header = models.ForeignKey(
        Header,
        blank=True,
        null=True,
        related_name="theme_header"
    )
    sidebar_left = models.CharField(
            max_length=255,
            default="small-12 medium-3 large-3 columns hide",
            blank=False,
            null=False,
            verbose_name=_("Sidebar Left Classes"),
            help_text=_("Sidebar left classes. Eg. 'small-12 medium-3 large-3 columns'. To hide this section, add a class of 'hide' after columns")
    )
    content_center = models.CharField(
            max_length=255,
            default="small-12 medium-9 large-9 columns",
            blank=False,
            null=False,
            verbose_name=_("Content Area Classes"),
            help_text=_("Main content center classes. Eg. 'small-12 medium-6 large-6 columns'. To hide this section, add a class of 'hide' after columns")
    )
    sidebar_right = models.CharField(
            max_length=255,
            default="small-12 medium-3 large-3 columns",
            blank=False,
            null=False,
            verbose_name=_("Sidebar Right Classes"),
            help_text=_("Sidebar right classes. Eg. 'small-12 medium-3 large-3 columns'. To hide this section, add a class of 'hide' after columns")
    )
    footer = models.ForeignKey(
        Footer,
        blank=True,
        null=True,
        related_name="theme_footer"
    )
    description = models.TextField(
            blank=True,
            null=True,
    )
    status = models.CharField(
            max_length=1,
            default="p",
            choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
            max_length=255,
            null=True,
            blank=True,
            default="public",
            choices=visibility,
            verbose_name=_("Visibility"),
            help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    set_primary = models.BooleanField(
            verbose_name=_("Set as primary theme"),
            help_text=_("Set up as primary theme."),
            default=False
    )
    active = models.BooleanField(
            default=True,
            verbose_name=_("Enabled"),
            help_text=_("Theme Status.")
    )
    responsive = models.BooleanField(
            default=True,
            verbose_name=_("Responsive"),
            help_text=_("Is the theme responsive?")
    )
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="theme_created_by"
    )
    updated_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            null=True,
            blank=True,
            related_name="theme_updated_by"
    )
    created_on = models.DateTimeField(
            auto_now_add=True,
            auto_now=False,
            verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
            auto_now_add=False,
            auto_now=True,
            verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Theme')
        verbose_name_plural = _('Themes')


    @property
    def template(self):
        return 'theme/custom-themes/{}.html'.format(self.slug)


