from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import *
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect, request
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from .models import Template, Layout, BaseTheme, Header, Footer


@admin.register(Template)
class TemplateAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'code', 'section', 'visibility', 'set_primary', 'active', 'status', 'updated')
    search_fields = ['name', 'slug', 'description', 'code']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('code', 'created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Layout)
class LayoutAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'code', 'section', 'visibility', 'active', 'status', 'updated')
    search_fields = ['name', 'slug', 'description', 'code']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('code', 'created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(BaseTheme)
class BaseThemeAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'code', 'section', 'visibility', 'set_primary', 'active', 'responsive', 'status', 'updated')
    search_fields = ['name', 'slug', 'description', 'code']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('code', 'created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Header)
class HeaderAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'visibility', 'set_primary', 'active', 'status', 'updated')
    search_fields = ['name', 'slug', 'description', 'code']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Footer)
class FooterAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'visibility', 'set_primary', 'active', 'status', 'updated')
    search_fields = ['name', 'slug', 'description', 'code']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


