import poshcms.services
import requests
from django.views.generic import TemplateView, GenericViewError
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _
from rest_framework import viewsets
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from menu_manager.models import MenuLink, Menu, MenuCategory
from cmf.models import *
from featured.models import *
from django.http import HttpResponse
from django.views.generic import View

#
# def get_theme(request):
#     theme = {{object.theme.slug}}+'.html'
#     return theme

def find_a_consultant(request):
    return HttpResponsePermanentRedirect("/find-a-consultant/")

def about(request):
    # return render(request, "landing/about.html")
    return render_to_response('landing/about.html', {}, context_instance=RequestContext(request))


def shop(request):
    return render(request, "landing/shop.html")


def host_party(request):
    return render(request, "landing/host.html")


def join(request):
    return render(request, "landing/join.html")


def find_consultant(request):
    return render(request, "landing/find_consultant.html")


class FooterMenuListView(ListView):
    model = Menu
    page_title = _("Footer Menu"),
    template_name = "menu/_footer_menu.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(FooterMenuListView, self).get_queryset(*args, **kwargs).order_by("sort_order")
        return qs

class FooterMenuView(TemplateView):
    model = Menu
    page_title = _("Footer Menu"),
    template_name = "menu/_footer_menu.html"

    def footer_menus(self):
        return Menu.objects.all()

    def get_context_data(self, **kwargs):
        context = super(FooterMenuView, self).get_context_data(**kwargs)
        context['footer_menus'] = self.footer_menus()
        return context

