$(function () {
    /**
     * Change language link
     */
    $('.lng-change').click(function (e) {
       e.preventDefault();
       // console.log("testing");
       var el = $(this),
           token = $('input[name="csrfmiddlewaretoken"]').eq(0).val();
       //$.post(el.parents('ul').data('url'), {"language": el.data('code'), "csrfmiddlewaretoken": token})
       //    .success(function() {window.location.reload()});
        url = window.location.pathname;
        path = url.split('/').slice(2, 4).join('/');
       window.location.href = '/' + el.data('code') + '/' + path;
       // console.log(({"language": el.data('code'), "csrfmiddlewaretoken": token}));
        // alert(path);
    });

    // $('.lng-change').click(function (e) {
    //     e.preventDefault();
    //     var el = $(this),
    //         token = $('input[name="csrfmiddlewaretoken"]').eq(0).val();
    //     $.post(el.parents('ul').data('url'), {"language": el.data('code'), "csrfmiddlewaretoken": token})
    //         .success(function() {window.location.reload()});
    // });

    $('#id_is_pws').click(function(){
    if (this.checked) {
        $('p').css('color', '#0099ff')
    }
});



    $('.list-filter').change(function(e) {
        window.location = $(this).find(':checked').data('url');
    });

    $('.delete-link').click(function(e) {
        e.preventDefault();
        var el = $(this);
        var itemName = 'this';
        if (el.data('name')) {
            itemName = el.data('name');
        }
        var action = el.data('action') || 'delete';
        bootbox.confirm('<h4>Are you sure you want to ' + action + ' ' + itemName + '?</h4>', function (result) {
            if (result) {
                $.get(el.attr('href'), 'json', function (data) {
                    if ('redirect' in data) {
                        location.href = data['redirect'];
                    }
                    else {
                        location.reload();
                    }
                });
            }
        });
    });
});