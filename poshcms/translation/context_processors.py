# -*- coding: utf-8 -*-
from modeltranslation import settings as mt_settings


def available_languages(request):
    return {
        'available_languages': mt_settings.AVAILABLE_LANGUAGES
    }
