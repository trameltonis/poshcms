# -*- coding: utf-8 -*-
import itertools

from django.contrib.admin.utils import flatten_fieldsets
from django.core.urlresolvers import reverse
from modeltranslation.admin import (
    TranslationAdmin,
    mt_settings,
)
from django.template.loader import render_to_string


class ExtendedTranslationAdmin(TranslationAdmin):
    request_lang = None
    change_form_template = 'translation/admin/change_form.html'

    def _update_prepopulated_fields(self):
        available_languages = [
            self.normalize_language(lang)
            for lang in mt_settings.AVAILABLE_LANGUAGES
        ]
        not_active_languages = [
            lang for lang in available_languages if lang != self.request_lang
        ]
        prepopulated_fields = {}
        for dest, sources in self.prepopulated_fields.items():
            updated_sources = ()
            for source in sources:
                skip_source = False
                for lang in not_active_languages:
                    if source.endswith(lang):
                        skip_source = True
                        break
                if not skip_source:
                    updated_sources = updated_sources + (source,)
            if updated_sources:
                prepopulated_fields[dest] = updated_sources
        self.prepopulated_fields = prepopulated_fields

    def language_selectors(self, obj):
        info = self.model._meta.app_label, self.model._meta.model_name
        url_name = 'admin:%s_%s_change' % info
        return render_to_string(
            'translation/admin/language_selectors.html',
            {
                'obj': obj,
                'obj_url': reverse(url_name, args=[obj.pk]),
                'languages': mt_settings.AVAILABLE_LANGUAGES
            }
        )

    def normalize_language(self, lang):
        return lang.lower().replace('-', '_')

    def get_exclude_fields(self, request, obj=None, **kwargs):
        available_languages = [
            self.normalize_language(lang)
            for lang in mt_settings.AVAILABLE_LANGUAGES
        ]

        excluded_languages = [
            lang
            for lang in available_languages
            if lang != self.request_lang
        ]

        return [
            '{}_{}'.format(field, lang)
            for field, lang in itertools.product(
                self.trans_opts.fields,
                excluded_languages
            )
        ]

    def get_fields(self, request, obj=None):
        exclude = self.get_exclude_fields(request, obj)
        return [
            field
            for field in super().get_fields(request, obj)
            if field not in exclude
        ]

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        exclude_fields = self.get_exclude_fields(request, obj)
        new_fieldsets = []
        for name, fieldset in fieldsets:
            new_fieldset = fieldset.copy()
            new_fieldset['fields'] = [
                field
                for field in fieldset['fields']
                if field not in exclude_fields

            ]
            new_fieldsets.append((name, new_fieldset))

        return new_fieldsets

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        self.request_lang = self.normalize_language(
            request.GET.get('hl', mt_settings.DEFAULT_LANGUAGE)
        )
        self._update_prepopulated_fields()
        return super().changeform_view(request, object_id, form_url, extra_context)

