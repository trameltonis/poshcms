import warnings

from django.conf import settings
from django.conf.urls import patterns, url
from django.core.urlresolvers import LocaleRegexURLResolver
from django.utils import six

from django.utils.deprecation import RemovedInDjango110Warning
from django.views.i18n import set_language
import requests
import tldextract
import urllib
from django.contrib.sites.shortcuts import get_current_site
from django.utils.translation import get_language
from django.contrib.sites.models import Site

def maxi18n_patterns(prefix, *args):
    """
    Adds the language code prefix to every URL pattern within this
    function. This may only be used in the root URLconf, not in an included
    URLconf.
    """
    if isinstance(prefix, six.string_types):
        warnings.warn(
            "Calling i18n_patterns() with the `prefix` argument and with tuples "
            "instead of django.conf.urls.url() instances is deprecated and "
            "will no longer work in Django 1.10. Use a list of "
            "django.conf.urls.url() instances instead.",
            RemovedInDjango110Warning, stacklevel=2
        )
        pattern_list = patterns(prefix, *args)
    else:
        pattern_list = [prefix] + list(args)
    if not settings.USE_I18N:
        return pattern_list
    return [LocaleRegexURLResolver(pattern_list)]


urlpatterns = [
    url(r'^setlang/$', set_language, name='set_language'),
]


def get_domain(self, request):
    request.domain = None
    host = request.META.get('HTTP_HOST', '')
    ext = tldextract.extract(host)
    request.domain = ext.domain
    return request.domain


def get_cartitems(_locale, processNumber):
    domain = Site.objects.get_current().domain
    base_url = "https://" + domain
    base_url_first_partial = '/api/v1/public/web-order-processes/'
    base_url_second_partial = '/order-summary'
    url = base_url + '/' + _locale + base_url_first_partial + processNumber + base_url_second_partial
    print(url)
    params = {'_locale': _locale, 'processNumber': processNumber}
    resp = requests.get(url, verify=False) # need false coz of invalid certificate
    cartitems = None
    try:
        cartitems = resp.json()
    except:
        "Retrieving cart information..."

    if cartitems != None and 'data' in cartitems:
        return cartitems['data']


def get_consultant_info(_locale, token):
    domain = Site.objects.get_current().domain
    base_url = "https://" + domain
    base_url_first_partial = '/api/v1/public/consultants/'
    url = base_url + '/' + _locale + base_url_first_partial + token
    print(url)
    params = {'_locale': _locale, 'token': token}
    resp = requests.get(url, verify=False)
    consultant_info = resp.json()
    consultant_info = None
    try:
        consultant_info = resp.json()
    except:
        "Retrieving consultant info..."

    if consultant_info != None and 'data' in consultant_info:
        return consultant_info['data']



#
# API Url:
# https://max.dev/{_locale}/api/v1/public/web-order-processes/{processNumber}/order-summary
# https://max.dev/{_locale}/api/v1/public/consultants/{token}