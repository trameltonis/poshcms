import os
import sys
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from media_manager.utils import *
import datetime

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
MAIN_APP = os.path.dirname(__file__)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ADMINS = (
    ('Frank Uche', 'fuche@norwex.com'),
)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "$!8lj=!jp-)ri2z2j5@d)gf^+-^9fdl=mn_=i05dca8of(y6&9"


ALLOWED_HOSTS = '*'

# ALLOWED_HOSTS = [
#     # 'poshcms.com',
#     '.poshcms.com',
#     # 'nwxtest.com',
#     '.nwxtest.com',
#     '.nwx.io',
#     '172.24.48.210',
#     '172.17.5.88',
#     '172.16.5.138',
#     '.max.dev',
#     '172.16.5.42',
#     # 'localhost',
#     '.localhost',
#     '127.0.0.1',
#     '209.163.242.115',
#     '172.24.96.165',
#     '172.24.48.25',
# ]


# Application definition
INSTALLED_APPS = [
    'dal',
    'dal_select2',
    'filebrowser',
    'modeltranslation',
    'nested_admin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    # Custom Apps
    'core',
    'cmf',
    'media_manager',
    'api',
    'polls',
    'faq',
    'theme',
    'widget_manager',
    'featured',
    'news',
    'menu_manager',
    'analytics',
    'hr',
    'dashboard',


    # Third Party Apps
    'easy_thumbnails',
    'filer',
    'ckeditor',
    'ckeditor_uploader',
    'ckeditor_filebrowser_filer',
    'mptt',
    'corsheaders',
    'rest_framework',
    'allauth',
    'allauth.account',
    'crispy_forms',
    'crispy_forms_foundation',
    'django_bootstrap_breadcrumbs',
    'django_select2',
    'sorl.thumbnail',
    'hijack',
    'compat',
    'reversion',
    'import_export',
    'debug_toolbar',
    'maintenance_mode',
    'meta',
    'watchman',
    'colorfield',
    'raven.contrib.django.raven_compat',
    'fluent_contents',
    'fluent_contents.plugins.text',
    'django_wysiwyg',
    'adminsortable2',
    'forms_builder.forms',
    'url_filter',
    # 'pipeline',
    # 'sitetree',
    # 'subdomains',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    # 'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # 'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'maintenance_mode.middleware.MaintenanceModeMiddleware',
    'poshcms.middleware.SubdomainMiddleware',
]

ROOT_URLCONF = 'poshcms.urls'

# DEFAULT_URL_SCHEME = 'https'
#
# SESSION_COOKIE_DOMAIN = '.nwxtest.com'
#
# SESSION_COOKIE_SECURE = False

# A dictionary of urlconf module paths, keyed by their subdomain.
# SUBDOMAIN_URLCONFS = {
#     None: 'poshcms.urls',  # no subdomain, e.g. ``example.com``
#     'www': 'poshcms.urls',
#     # 'api': 'myproject.urls.api',
# }

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'poshcms.translation.context_processors.available_languages',
                'core.context_processors.siteconfig_processor',
                'core.context_processors.socials_processor',
                'cmf.context_processors.snippet_processor',
                'menu_manager.context_processors.menus_processor',
                # 'allauth.account.context_processors.account',
                # 'allauth.socialaccount.context_processors.socialaccount',
            ],
        },
    },
]

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

AUTHENTICATION_BACKENDS = (
    "rules.permissions.ObjectPermissionBackend",
    "django.contrib.auth.backends.ModelBackend",
    # all auth specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

WSGI_APPLICATION = 'poshcms.wsgi.application'


# SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'poshcms_production',
        'USER': 'nwx3prod3xn',
        'PASSWORD': '5zCV2MEYCwGYHw6H',
        'HOST': '172.16.5.42',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'poshcms_local',
#         'USER': 'root',
#         'PASSWORD': '',
#         'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
#         'PORT': '3306',
#         # 'CONN_MAX_AGE': 600,
#     }
# }

NORWEX_CMS_VERSION = 1.18

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Django Debug Toolbar Config
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

INTERNAL_IPS = [
    '172.24.48.210',
    '172.16.5.138',
    '172.24.96.165',
    '172.16.5.16',
    'nwxtest.com',
    'nwx.io',
    'norwex.biz',
    '127.0.0.1',
    '209.163.242.115',
    '172.24.48.25',
]

gettext = lambda s: s
LANGUAGES = (
    ('en-us', _('US English')),
    ('es-us', _('US Spanish')),
    ('fr-ca', _('Canadian French')),
    ('en-ca', _('Canadian English')),
)

LANGUAGE_CODE = 'en-us'

# EXTRA_LANG_INFO = {
#     'en': {
#         'bidi': True, # right-to-left
#         'code': 'en-us',
#         'name': 'English US',
#         # 'name_local': u'\u0626\u06C7\u064A\u063A\u06C7\u0631 \u062A\u0649\u0644\u0649', #unicode codepoints here
#     },
#     'es-us': {
#         'bidi': True, # right-to-left
#         'code': 'es-us',
#         'name': 'Spanish US',
#         # 'name_local': u'\u0626\u06C7\u064A\u063A\u06C7\u0631 \u062A\u0649\u0644\u0649', #unicode codepoints here
#     },
#     'en-ca': {
#         'bidi': True, # right-to-left
#         'code': 'en-ca',
#         'name': 'English CA',
#         # 'name_local': u'\u0626\u06C7\u064A\u063A\u06C7\u0631 \u062A\u0649\u0644\u0649', #unicode codepoints here
#     },
#     'fr-ca': {
#         'bidi': True, # right-to-left
#         'code': 'fr-cas',
#         'name': 'French CA',
#         # 'name_local': u'\u0626\u06C7\u064A\u063A\u06C7\u0631 \u062A\u0649\u0644\u0649', #unicode codepoints here
#     },
# }

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

USE_THOUSAND_SEPARATOR = True


LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)

MODELTRANSLATION_LANGUAGES = ('en-us', 'es-us', 'fr-ca', 'en-ca')
MODELTRANSLATION_DEFAULT_LANGUAGE = 'en-us'
MODELTRANSLATION_FALLBACK_LANGUAGES = {
    'default': ('en-us', 'en-ca',),
    'fr-ca': ('en-ca',),
    'es-us': ('en-us', 'en-ca',)
}
# MODELTRANSLATION_FALLBACK_LANGUAGES = ('en-us', 'en-ca')
MODELTRANSLATION_PREPOPULATE_LANGUAGE = 'en-us'
# MODELTRANSLATION_TRANSLATION_FILES = (
#     'core.translation',
#     'cmf.translation',
# )
# MODELTRANSLATION_AUTO_POPULATE = True
MODELTRANSLATION_AUTO_POPULATE = 'all'
MODELTRANSLATION_ENABLE_FALLBACKS = True
MODELTRANSLATION_LOADDATA_RETAIN_LOCALE = True
MODELTRANSLATION_DEBUG = True
# MODELTRANSLATION_CUSTOM_FIELDS = ('MyField', 'MyOtherField',)


MEDIA_ROOT = os.path.join(BASE_DIR, 'cms-media')

MEDIA_URL = '/cms-media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "poshcms/static"),
)

MAX_UPLOAD_SIZE = "500242880"

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    # 'pipeline.finders.PipelineFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

STATIC_URL = '/static/'

# STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'
#
# PIPELINE_COMPILERS = (
#     'pipeline.compressors.yuglify.YuglifyCompressor'
# )
#
# PIPELINE_JS = {
#     'main': {
#         'source_filenames': (
#             'coffee/app.coffee',
#         ),
#         'output_filename': 'js/main.js',
#     },
#     'vendor': {
#         'source_filenames': (
#             'vendor/jquery.min.js',
#             'vendor/bootstrap.min.js',
#         ),
#         'output_filename': 'js/vendor.js',
#     }
#
# }
#
# PIPELINE_CSS = {
#     'main': {
#         'source_filenames': (
#             'stylus/main.styl',
#         ),
#         'output_filename': 'css/main.css',
#     }
# }


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
        # 'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ),
    'PAGE_SIZE': 25,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        # 'oauth2_provider.ext.rest_framework.OAuth2Authentication',
    ),
    'DEFAULT_FILTER_BACKENDS': [
        'url_filter.integrations.drf.DjangoFilterBackend',
    ]
}

JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
        'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
        'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
        'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
        'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
    # 'api.utils.jwt_response_payload_handler',
        'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_SECRET_KEY': settings.SECRET_KEY,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=605000),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
}

WATCHMAN_AUTH_DECORATOR = 'django.contrib.admin.views.decorators.staff_member_required'

WATCHMAN_EMAIL_RECIPIENTS = ['fuche@norwex.com', 'uche.okereke@gmail.com']

WATCHMAN_EMAIL_HEADERS = {}
MAN_ENABLE_PAID_CHECKS = True
WATCHMAN_ERROR_CODE = 500

# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": "redis://127.0.0.1:6379/1",
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#         }
#     },
#     'select2': {
#         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#         'LOCATION': '127.0.0.1:11211',
#     }
# }


# Set the cache backend to select2
# SELECT2_CACHE_BACKEND = 'select2'

SELECT2_JS = 'static/js/select2.min.js'
SELECT2_CSS = 'static/css/select2.css'
RAVEN_CONFIG = {
    'dsn': 'http://eed5a4b110864b0796586ce4f3cd5cf2:a66682f779b745c9a84e32c664b5a60c@erlogic.com/10',
}

# CKEditor Configuration
# CKEDITOR_JQUERY_URL = '/poshcms/static/js/vendor/jquery.min.js'
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_UPLOAD_PATH = "cms-media/"

CKEDITOR_ALLOW_NONIMAGE_FILES = True



CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'CMSConfig',
        'skin': 'moono',
        # 'skin': 'office2013',
        'toolbar_Basic': [
            ['Source', '-', 'Bold', 'Italic']
        ],
        'toolbar_CMSConfig': [
            {'name': 'document', 'items': ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
            {'name': 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll']},
            {'name': 'forms',
             'items': ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
                       'HiddenField']},
            '/',
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-',
                       'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl',
                       'Language']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            {'name': 'insert',
             'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
            '/',
            {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']},
            {'name': 'about', 'items': ['About']},
            '/',  # put this to force next toolbar on new line
            {'name': 'CMStools', 'items': [
                # put the name of your editor.ui.addButton here
                'Preview',
                'Maximize',

            ]},
        ]
    }
}


CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    '172.17.5.88',
    '172.24.48.25',
    '172.24.48.210',
    '172.16.5.138',
    '172.24.96.165',
    '172.16.5.16',
    'poshcms.com',
    'nwxtest.com',
    'norwex.biz',
    'max.dev',
    'nwx.io',
)
CORS_ORIGIN_REGEX_WHITELIST = (
    '^(http?://)?(\w+\.)?poshcms\.com$',
    '^(https?://)?(\w+\.)?poshcms\.com$',
    '^(http?://)?(\w+\.)?max\.dev$',
    '^(https?://)?(\w+\.)?max\.dev$',
    '^(http?://)?(\w+\.)?norwex\.biz$',
    '^(https?://)?(\w+\.)?norwex\.biz$',
    '^(http?://)?(\w+\.)?nwxtest\.com$',
    '^(https?://)?(\w+\.)?nwxtest\.com$',
    '^(https?://)?(\w+\.)?nwxtest\.com$',
    '^(http?://)?(\w+\.)?nwxtest\.com$',
    '^(https?://)?(\w+\.)?nwx\.io$',
    '^(http?://)?(\w+\.)?nwx\.io$',
)
# CORS_ORIGIN_REGEX_WHITELIST = ()
# CORS_URLS_REGEX = r'^/api/.*$'
# CORS_URLS_REGEX = '^.*$'
CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'PUT',
    'PATCH',
    'DELETE',
    'OPTIONS'
)
CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken'
)
CORS_EXPOSE_HEADERS = ()
CORS_PREFLIGHT_MAX_AGE = 86400
CORS_ALLOW_CREDENTIALS = False
CORS_REPLACE_HTTPS_REFERER = True


FLUENT_CONTENTS_CACHE_PLACEHOLDER_OUTPUT = False


LOGIN_URL = '/accounts/login'
LOGIN_REDIRECT_URL = "/"
ACCOUNT_AUTHENTICATION_METHOD = 'username'
ACCOUNT_CONFIRM_EMAIL_ON_GET = False
ACCOUNT_USER_DISPLAY = lambda u: u.first_name
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = LOGIN_URL
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = None
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 7
ACCOUNT_EMAIL_REQUIRED=True
ACCOUNT_EMAIL_VERIFICATION = 'optional'
ACCOUNT_EMAIL_SUBJECT_PREFIX = 'Site'
ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'http'
# ACCOUNT_FORMS = {}
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE = False
ACCOUNT_LOGOUT_REDIRECT_URL = '/'
ACCOUNT_SIGNUP_FORM_CLASS = 'poshcms.forms.SignupForm'
ACCOUNT_SIGNUP_PASSWORD_VERIFICATION = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USER_MODEL_USERNAME_FIELD = 'username'
# ACCOUNT_USER_DISPLAY = 'user.username'
ACCOUNT_USERNAME_MIN_LENGTH = 4

ACCOUNT_USERNAME_BLACKLIST = []
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_PASSWORD_INPUT_RENDER_VALUE = False
ACCOUNT_PASSWORD_MIN_LENGTH = 6
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
# ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = '/'
ACCOUNT_LOGIN_ON_PASSWORD_RESET = False
ACCOUNT_SESSION_REMEMBER = None
ACCOUNT_SESSION_COOKIE_AGE = 1814400
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = True
SOCIALACCOUNT_STORE_TOKENS =True

SITE_ID = 1


# Avatar settings
AVATAR_DEFAULT_SIZE = 80
AVATAR_AUTO_GENERATE_SIZES = (AVATAR_DEFAULT_SIZE, 150, 200, 300)
AVATAR_CLEANUP_DELETED = True
AVATAR_GRAVATAR_BACKUP = True


# Crispy forms
CRISPY_ALLOWED_TEMPLATE_PACKS = ('bootstrap', 'uni_form', 'bootstrap3', 'foundation-5')
# CRISPY_TEMPLATE_PACK = 'foundation-5'
CRISPY_TEMPLATE_PACK = 'bootstrap3'


# Email settings
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'Norwex <no-reply@norwex.biz>'


# Geoposition settings
GEOPOSITION_MAP_OPTIONS = {
    'minZoom': 3,
    'maxZoom': 18,
    'scrollwheel': True,
    'center': {'lat': 37.81520859889623, 'lng': -122.47833251953125}
}
GEOPOSITION_MAP_WIDGET_HEIGHT = 400
GEOPOSITION_MARKER_OPTIONS = {
    'cursor': 'move',
    'position': {'lat': 37.81520859889623, 'lng': -122.47833251953125}
}

# Internationalization
DATE_INPUT_FORMATS = [
    '%Y-%m-%d', '%m/%d/%Y', '%m/%d/%y',
    '%b %d %Y', '%b %d, %Y',
    '%d %b %Y', '%d %b, %Y',
    '%B %d %Y', '%B %d, %Y',
    '%d %B %Y', '%d %B, %Y',
]

# Impersonation Configuration
HIJACK_LOGIN_REDIRECT_URL = '/'
HIJACK_LOGOUT_REDIRECT_URL = '/'
HIJACK_USE_BOOTSTRAP = True
HIJACK_DISPLAY_WARNING = True
HIJACK_AUTHORIZE_STAFF = True
HIJACK_AUTHORIZE_STAFF_TO_HIJACK_STAFF = True
HIJACK_URL_ALLOWED_ATTRIBUTES = ('user_id', 'email', 'username')


# File Manager Configuration
FILER_THUMBNAIL_HIGH_RESOLUTION = True
FILER_THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
FILER_CANONICAL_URL = 'cms-media/'
FILER_ENABLE_LOGGING = True
FILER_ENABLE_PERMISSIONS = True
FILER_IS_PUBLIC_DEFAULT = True
FILER_DEBUG = False
FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': os.path.join(MEDIA_ROOT, 'media-manager'),
                'base_url': os.path.join(MEDIA_URL, 'media-manager'),
            },
            # 'UPLOAD_TO': 'filer.utils.generate_filename.randomized',
            'UPLOAD_TO': 'media_manager.utils.generate_filename.maxilize',
            'UPLOAD_TO_PREFIX': '',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': os.path.join(MEDIA_ROOT, 'media-manager-thumbnails'),
                'base_url': os.path.join(MEDIA_URL, 'media-manager-thumbnails'),
            },
        },
    },
    'private': {
        'main': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': os.path.join(MEDIA_ROOT, 'media-manager'),
                'base_url': os.path.join(MEDIA_URL, 'media-manager'),
            },
            'UPLOAD_TO': 'media_manager.utils.generate_filename.maxilize',
            # 'UPLOAD_TO': 'assets',
            'UPLOAD_TO_PREFIX': '',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': os.path.join(MEDIA_ROOT, 'media-manager-thumbnails'),
                'base_url': os.path.join(MEDIA_URL, 'media-manager-thumbnails'),
            },
        },
    },
}
DEFAULT_FILER_SERVERS = {
    'private': {
        'main': {
            'ENGINE': 'filer.server.backends.default.DefaultServer',
        },
        'thumbnails': {
            'ENGINE': 'filer.server.backends.default.DefaultServer',
        }
    }
}
FILER_PAGINATE_BY = 50
FILER_SUBJECT_LOCATION_IMAGE_DEBUG = False
FILER_ALLOW_REGULAR_USERS_TO_ADD_ROOT_FOLDERS = True
FILER_UPLOADER_CONNECTIONS = 3
FILER_DUMP_PAYLOAD = True
FILER_MAX_UPLOAD_SIZE = 9980485760
FILER_EXTENSIONS = {
    'Image': ['.jpg','.jpeg','.gif','.png','.tif','.tiff'],
    'Document': ['.pdf','.doc','.rtf','.txt','.xls','.csv'],
    'Video': ['.mov','.wmv','.mpeg','.mpg','.avi','.rm'],
    'Audio': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p']
}


# Media Manager File Browser Settings
FILEBROWSER_EXTENSIONS = {
    'Image': ['.jpg','.jpeg','.gif','.png','.tif','.tiff'],
    'Document': ['.pdf','.doc','.rtf','.txt','.xls','.csv'],
    'Video': ['.mov','.wmv','.mpeg','.mpg','.avi','.rm'],
    'Audio': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p']
}
FILEBROWSER_DIRECTORY = 'media-manager/'
FILEBROWSER_SELECT_FORMATS = {
    'file': ['Image','Document','Video','Audio'],
    'image': ['Image'],
    'document': ['Document'],
    'media': ['Video','Audio'],
}
FILEBROWSER_VERSIONS_BASEDIR = ''
FILEBROWSER_VERSIONS = {
    'admin_thumbnail': {'verbose_name': 'Admin Thumbnail', 'width': 60, 'height': 60, 'opts': 'crop'},
    'thumbnail': {'verbose_name': 'Thumbnail (1 col)', 'width': 60, 'height': 60, 'opts': 'crop'},
    'small': {'verbose_name': 'Small (2 col)', 'width': 140, 'height': '', 'opts': ''},
    'medium': {'verbose_name': 'Medium (4col )', 'width': 300, 'height': '', 'opts': ''},
    'big': {'verbose_name': 'Big (6 col)', 'width': 460, 'height': '', 'opts': ''},
    'large': {'verbose_name': 'Large (8 col)', 'width': 680, 'height': '', 'opts': ''},
}
FILEBROWSER_VERSION_QUALITY = 90
FILEBROWSER_ADMIN_VERSIONS = ['thumbnail', 'small', 'medium', 'big', 'large']
FILEBROWSER_ADMIN_THUMBNAIL = 'admin_thumbnail'
FILEBROWSER_PLACEHOLDER = ""
FILEBROWSER_SHOW_PLACEHOLDER = False
FILEBROWSER_STRICT_PIL = False
FILEBROWSER_IMAGE_MAXBLOCK = 1024*1024
FILEBROWSER_EXCLUDE = (r'_(%(exts)s)_.*_q\d{1,3}\.(%(exts)s)' % {'exts': ('|'.join(FILEBROWSER_EXTENSIONS))},)
FILEBROWSER_MAX_UPLOAD_SIZE = 9980485760
FILEBROWSER_NORMALIZE_FILENAME = False
FILEBROWSER_CONVERT_FILENAME = True
FILEBROWSER_LIST_PER_PAGE = 50
FILEBROWSER_DEFAULT_SORTING_BY = ('date', 'filename_lower', 'filesize')
# Options are: date, filesize, filename_lower, filetype_checked, mimetype. You can also combine attributes, e.g. ('mimetype', 'filename_lower') or ('filename_lower', 'filesize', 'date', 'mimetype')
FILEBROWSER_DEFAULT_SORTING_ORDER = "desc"
FILEBROWSER_FOLDER_REGEX = r'^[\w._\ /-]+$'
FILEBROWSER_SEARCH_TRAVERSE = True
FILEBROWSER_DEFAULT_PERMISSIONS = 0o755
FILEBROWSER_OVERWRITE_EXISTING = False


# Form Builder Configuration
FORMS_BUILDER_FIELD_MAX_LENGTH = 5000
FORMS_BUILDER_LABEL_MAX_LENGTH = 255
FORMS_BUILDER_EXTRA_FIELDS = ()
FORMS_BUILDER_UPLOAD_ROOT = "cms-media/forms"
FORMS_BUILDER_USE_HTML5 = True
FORMS_BUILDER_USE_SITES = "django.contrib.sites"
FORMS_BUILDER_EDITABLE_SLUGS = True
FORMS_BUILDER_CHOICES_QUOTE = '`'
FORMS_BUILDER_CHOICES_UNQUOTE = '`'
FORMS_BUILDER_CSV_DELIMITER = ','
FORMS_BUILDER_EMAIL_FAIL_SILENTLY = settings.DEBUG


MAINTENANCE_MODE = False
MAINTENANCE_MODE_IGNORE_STAFF = False
MAINTENANCE_MODE_IGNORE_SUPERUSER = False
MAINTENANCE_MODE_IGNORE_IP_ADDRESSES = ()
MAINTENANCE_MODE_IGNORE_URLS = ("/en-us/status/dashboard/", )
# MAINTENANCE_MODE_IGNORE_URLS = ("/en-us/status/dashboard/", "/en-us/manage", )
MAINTENANCE_MODE_REDIRECT_URL = None
MAINTENANCE_MODE_TEMPLATE = 'maintenance.html'
MAINTENANCE_MODE_TEMPLATE_CONTEXT = None


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        }
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler"
        }
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
    }
}
