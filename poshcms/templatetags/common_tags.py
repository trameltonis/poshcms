import re
from collections import Iterable
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname

    path = context['request'].path
    return 'active' if re.search(pattern, path) else ''


@register.simple_tag()
def display_if(condition, text, reverse=False):
    if reverse:
        return text if not condition else ''
    else:
        return text if condition else ''


@register.simple_tag()
def display_labels(value, cls):
    tags = value.split(',')
    output = ''
    if tags:
        for tag in tags:
            output += '<span class="label rounded label-%s profile-label">%s</span> ' % (cls, tag)
    return output


@register.simple_tag()
def display_list(separator, *args):
    if len(args) == 1:
        if isinstance(args[0], Iterable):
            args = list(args[0])
    elements = [str(el) for el in args if el]
    return separator.join(elements)


@register.simple_tag(takes_context=True)
def form_filter_option(context, text, attribute, value=None):
    request = context['request']
    selected = False
    get_params = request.GET.copy()
    if attribute in get_params and value is None:
        del get_params[attribute]
    else:
        if attribute in get_params and get_params[attribute] == str(value):
            selected = True
        get_params[attribute] = value

    return "<option value='' %s data-url='%s'>%s</option>" % (
        'selected' if selected else '',
        request.path + ('?' + get_params.urlencode() if get_params else ''),
        text
    )


@register.simple_tag
def get_verbose_name(object):
    return object._meta.verbose_name.title()


@register.simple_tag(takes_context=True)
def sort_column(context, title, order_name, *args, **kwargs):
    if 'default' in kwargs:
        default = kwargs['default']
    else:
        default = 'desc'

    sorter = context['sorter']
    request = context['request']

    order = sorter.get_order()
    if order == order_name:
        caret = '&uarr;'
        link_param = order_name + '_asc'
    elif order == '-' + order_name:
        caret = '&darr;'
        link_param = order_name + '_desc'
    else:
        caret = ''
        link_param = order_name + '_' + default

    link_href = '%s?order=%s' % (request.path, link_param)
    link = '<a href="%s" class="sort_link">%s %s</a>' % (link_href, title, caret)

    return link