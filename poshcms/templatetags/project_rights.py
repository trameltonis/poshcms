import re
from collections import Iterable
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()


@register.filter()
def is_owner(project, user):
    return project.owner.organization_user.user.id == user.id


@register.filter()
def is_editor(project, user):
    return project.is_admin(user)