import poshcms.views
from poshcms.services import maxi18n_patterns
from filebrowser.sites import site
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token
# from api.views import *
from polls.api.views import *
from faq.api.views import *
# from cmf.views import *
from core.api.views import *
from cmf.api.views import *
from menu_manager.api.views import *
from media_manager.api.views import *
# from menu_manager.views import *
from news.api.views import *
from widget_manager.api.views import *
from hr.api.views import *
import forms_builder.forms.urls
from featured.api.views import *
from theme.api.views import *


router = routers.DefaultRouter()
router.register(r'pages', PageViewSet)
router.register(r'resources', ResourceViewSet)
router.register(r'emails', EmailViewSet)
router.register(r'dashboards', DashboardViewSet)
router.register(r'specials', SpecialsViewSet)
router.register(r'incentives', IncentivesViewSet)
router.register(r'timelines', TimelineViewSet)
router.register(r'testimonials', TestimonialViewSet)
router.register(r'quotes', QuoteViewSet)
router.register(r'snippets', SnippetViewSet)
router.register(r'news', NewsViewSet)
router.register(r'posts', PostViewSet)
router.register(r'events', EventViewSet)
router.register(r'calendars', CalendarViewSet)
# router.register(r'all-published-news', AllPublishedNewsViewSet)
# router.register(r'consultant-news', ConsultantNewsViewSet)
# router.register(r'general-news', GeneralNewsViewSet)
# router.register(r'company-news', CompanyNewsViewSet)
# router.register(r'urgent-news', UrgentNewsViewSet)
# router.register(r'product-notice-news', ProductNoticeNewsViewSet)
# router.register(r'events-news', EventsNewsViewSet)
# router.register(r'norwex-movement-news', NorwexMovementNewsViewSet)
# router.register(r'news-archive', NewsArchiveViewSet)
# router.register(r'news-types', NewsTypeViewSet)
# router.register(r'event-tags', EventTagViewSet)
router.register(r'menus', MenuViewSet)
router.register(r'menu-links', MenuLinkViewSet)
router.register(r'menu-categories', MenuCategoryViewSet)
router.register(r'files', FileViewSet)
router.register(r'images', ImageViewSet)
router.register(r'videos', VideoViewSet)
router.register(r'video-gallery', VideoGalleryViewSet)
router.register(r'document-gallery', DocumentGalleryViewSet)
router.register(r'galleries', GalleryViewSet)
router.register(r'landing-pages', BannerViewSet)
router.register(r'collections', CollectionViewSet)
router.register(r'custom-layouts', CustomLayoutViewSet)
router.register(r'carousels', CarouselViewSet)
# router.register(r'carousel-image-inline', CarouselImageInlineViewSet)
router.register(r'ctas', CtaViewSet)
router.register(r'icons', IconViewSet)
router.register(r'themes', BaseThemeViewSet)
router.register(r'layouts', LayoutViewSet)
router.register(r'templates', TemplateViewSet)
router.register(r'string-widgets', StringWidgetViewSet)
router.register(r'simple-widgets', SimpleWidgetViewSet)
router.register(r'media-widgets', MediaWidgetViewSet)
router.register(r'gallery-widgets', GalleryWidgetViewSet)
router.register(r'faqs', FaqQuestionViewSet)
router.register(r'faq-categories', FaqCategoryViewSet)
# router.register(r'polls-questions', PollQuestionViewSet)
# router.register(r'polls-groups', PollGroupViewSet)
# router.register(r'polls-choices', PollChoiceViewSet)
router.register(r'media-types', MediaTypeViewSet)
router.register(r'media-tags', MediaTagViewSet)
router.register(r'team-page', TeamPageViewSet)
router.register(r'people', PeopleViewSet)
router.register(r'departments', DepartmentViewSet)
router.register(r'regions', RegionViewSet)
router.register(r'social', SocialMediaViewSet)
router.register(r'site-config', SiteConfigViewSet)
router.register(r'account-group', AccountGroupViewSet)
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^select2/', include('django_select2.urls')),


    ]
urlpatterns += i18n_patterns(
# url(r'^admin_tools/', include('admin_tools.urls')),
    # url(r'^$', poshcms.views.home),
    # url(r'^accounts/password/change/$', password_change_view, name='account_change_password_custom'),
    url(r'^find-a-consultant/$', "poshcms.views.find_a_consultant", name="find_a_consultant"),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^find-a-consultant/$', poshcms.views.find_consultant),
    # url(r'^advanced-admin/', include('grappelli.urls')),
    url(r'^media-manager/', include('filer.urls')),
    url(r'^dashboard/', include('dashboard.urls'),),
    url(r'^site-maintenance/', include('maintenance_mode.urls')),
    # url(r'^filer/', include('filer.urls')),
    url(r'^', include('filer.server.urls')),
    # url(r'^media-browser/', include('ckeditor_filebrowser_filer.urls')),
    # url(r'^filebrowser_filer/', include('ckeditor_filebrowser_filer.urls')),
    url(r'^nested_admin/', include('nested_admin.urls')),
    url(r'^manage/file-browser/', include(site.urls)),
    url(r'^status/', include('watchman.urls', namespace='system_status')),
    url(r'^manage/', include(admin.site.urls)),
    url(r'^cms-api/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^cms-api/', include('api.urls')),
    url(r'^cms-api/', include(router.urls)),


    url(r'^cms-api/auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^impersonate/', include('hijack.urls')),
    url(r'^', include('core.urls')),
    url(r'^', include('core.max_urls_redirect')),
    url(r'^', include('news.urls')),
    url(r'^polls/', include('polls.urls')),
    url(r'^', include('menu_manager.urls')),
    url(r'^', include('media_manager.urls')),
    url(r'^', include('featured.urls')),
    url(r'^', include('hr.urls')),
    url(r'^', include('faq.urls')),
    url(r'^', include('cmf.urls')),
    url(r'^forms/', include(forms_builder.forms.urls)),

    url(r'^cms-api/auth/token/', obtain_jwt_token),
    url(r'^cms-api/auth/verify/', verify_jwt_token),
    url(r'^cms-api/auth/refresh/', refresh_jwt_token),

)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT
    )
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
