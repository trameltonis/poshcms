from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.views.generic import RedirectView
from helpers.utils import max_redirect, max_localizr
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.translation import get_language

urlpatterns = patterns('',
    # (r'^en_US/about/$', RedirectView.as_view(url='/about/')),
    # (r'^en_US/about/our-purpose/$', RedirectView.as_view(url='/about/')),
    # (r'^en_US/about/norwex-movement/$', RedirectView.as_view(url='/charitable-partners/')),
    # (r'^en_US/about/our-heritage/$', RedirectView.as_view(url='/our-heritage/')),
    # (r'^en_US/about/leadership-team/$', RedirectView.as_view(url='/leadership-team/')),
    # (r'^en_US/about/norwex-around-the-world/$', RedirectView.as_view(url='/about/')),
    # (r'^en_US/about/global-impact/$', RedirectView.as_view(url='/charitable-partners/')),
    # (r'^en_US/products/norwex-safe-haven-house/$', RedirectView.as_view(url='/safe-haven-house/')),
    # (r'^en_US/products/chemicals-of-concern-norwex-quiz/$', RedirectView.as_view(url='/chemicals-of-concern-quiz/')),
    # (r'^en_US/products/specials-and-sales/$', RedirectView.as_view(url='/specials-and-sales/')),
    # (r'^en_US/products/norwex-microfiber/$', RedirectView.as_view(url='/microfiber/')),
    # (r'^en_US/products-microfiber-recycling-program/$', RedirectView.as_view(url='/microfiber/')),
    # (r'^en_US/the-norwex-guarantee$', RedirectView.as_view(url='/corporate/norwex-guarantee/')),
    # (r'^en_US/shipping/$', RedirectView.as_view(url='/corporate/shipping/')),
    # (r'^en_US/privacy-policy/$', RedirectView.as_view(url='/corporate/privacy-policy/')),
    # (r'^en_US/terms-and-conditions/$', RedirectView.as_view(url='/corporate/terms-and-conditions/')),
    # (r'^en_US/contact-norwex/$', RedirectView.as_view(url='/contact/contact-norwex/')),
    # (r'^en_US/products/product-frequently-asked-questions/$', RedirectView.as_view(url='/faqs/product/')),
    # (r'^en_US/products/ingredient-standards/$', RedirectView.as_view(url='/products/ingredient-standards/')),
    # (r'^en_US/host-party/$', RedirectView.as_view(url='/host-a-party/')),
    # (r'^en_US/host-party-how-it-works/$', RedirectView.as_view(url='/host-a-party/')),
    # (r'^en_US/host-party/rewards/$', RedirectView.as_view(url='/host-a-party/')),
    # (r'^en_US/host-party/how-to-book-your-party/$', RedirectView.as_view(url='/host-a-party/')),
    # (r'^en_US/host-party/frequently-asked-questions/$', RedirectView.as_view(url='/host-a-party/')),
    # (r'^en_US/join/$', RedirectView.as_view(url='/join/')),
    # (r'^en_US/join/the-norwex-opportunity/$', RedirectView.as_view(url='/join/')),
    # (r'^en_US/join/how-to-get-started/$', RedirectView.as_view(url='/join/')),
    # (r'^en_US/join/how-to-earn-income/$', RedirectView.as_view(url='/join/')),
    # (r'^en_US/products/$', RedirectView.as_view(url='https://norwex.biz/en_US/shop/all')),
    # (r'^en_US/products/for-your-home/$', RedirectView.as_view(url='https://norwex.biz/en_US/shop/Home_Essentials')),
    # (r'^en_US/products/for-yourself/$', RedirectView.as_view(url='https://norwex.biz/en_US/shop/Personal_Care')),
    # (max_redirect('about/our-purpose'), RedirectView.as_view(url='/about/')),
)
