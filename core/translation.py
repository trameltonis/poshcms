from modeltranslation.translator import translator, register, TranslationOptions
from core.models import Format, SocialMedia, AccountGroup, SiteConfig


@register(Format)
class FormatTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(SocialMedia)
class SocialMediaTranslationOptions(TranslationOptions):
    fields = [
        'account_name',
        'social_media_url',
        'description',
        ]


@register(AccountGroup)
class AccountGroupTranslationOptions(TranslationOptions):
    fields = [
        'description',
        ]



@register(SiteConfig)
class SiteConfigTranslationOptions(TranslationOptions):
    fields = [
        'sitename',
        'description',
        'description',
        'copyright',
        'attribution',
        'locked_locale_message',
        'footer_text',
        'maintenance_mode_message',
        ]


