from django.conf import settings
from django.db import models
from import_export import resources, fields
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
# from model_utils import Choices, FieldTracker
# from model_utils.managers import InheritanceManager, QueryManager, QueryManagerMixin, InheritanceQuerySet, InheritanceManagerMixin, InheritanceQuerySetMixin
# from model_utils.models import StatusModel, TimeFramedModel, TimeStampedModel
# from model_utils.fields import MonitorField, StatusField, SplitField, SplitText, SplitDescriptor
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
import base64
import uuid
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from helpers.utils import us_states, country, mobile_country, visibility, address_type, social_media, progress_status, template_framework, STATUS_CHOICES, get_content_type


route_type = (
    ('uri', 'URI'),
    ('route', 'route'),
    ('content', 'content'),
)


class Tag(models.Model):
    """
    Predefined Tag list for autocomplete
    """
    ct = get_content_type
    title = models.CharField(
        max_length=100,
        verbose_name=_('Name')
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Keyword"),
        help_text=_("Keyword or slug will be auto-generated from title field if left empty")
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about this tag.")
    )
    active = models.BooleanField(
        default=True
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        choices=progress_status,
        default=_("Translation Status"),
        help_text=_("Select progress status level")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="content_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="content_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )
    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class Category(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Keyword'),
        help_text=_('Automatically generated from category title if left empty.')
    )
    parent = models.ForeignKey(
        'Category',
        blank=True,
        null=True,
        default=None
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the category."),
        default=None
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="categories"
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        choices=progress_status,
        default=_("Translation Status"),
        help_text=_("Select progress status level")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class SocialMedia(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=120,
        choices=social_media,
        verbose_name=_("Channel Name"),
        help_text=_("Name of social media channel")
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Slug"),
        help_text=_("Automatically generated from Name field")
    )
    account_name = models.CharField(
        max_length=255,
        default=None,
        blank=True,
        null=True,
        verbose_name=_("Account Name"),
        help_text=_("Social Media Channel Account Name. eg: disney, dolphin, etc")
    )
    social_media_url = models.URLField(
        max_length=255,
        default=None,
        blank=True,
        null=True,
        verbose_name=_("Url"),
        help_text=_("Url of social media channel, eg. http://facebook.com"),
    )
    description = models.TextField(
        blank=True,
        null=True,
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Social Media Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="social_media_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="social_media_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return reverse("social_media_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Social Media Channel')
        verbose_name_plural = _('Social Media Channels')


class Locale(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        verbose_name=_("Locale Name"),
        help_text=_("Name of locale")
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Slug"),
        help_text=_("Automatically generated from title field")
    )
    code = models.CharField(
        max_length=5,
        default=None,
        blank=False,
        null=False,
        verbose_name=_("Locale Code"),
        help_text=_("Short Locale Code. eg. en_US, fr_CA, etc")
    )
    description = models.TextField(
        blank=True,
        null=True,
    )
    locked_locale_message = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Locked Locale Message"),
        help_text=_("Used to set up default message that will be displayed when a locale is locked.")
    )
    default_locale = models.BooleanField(
        verbose_name=_("Is Default Locale"),
        help_text=_("Set As Default Locale"),
        default=False
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="locale_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="locale_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.code)

    def get_absolute_url(self):
        return reverse("locale_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Locale')
        verbose_name_plural = _('Locales')


class Format(models.Model):
    ct = get_content_type
    title = models.CharField(
        max_length=255,
        verbose_name=_("Name"),
        help_text=_("Name of media format")
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Slug"),
        help_text=_("Automatically generated from title field")
    )
    width = models.IntegerField(
        default=None,
        blank=False,
        null=False,
        verbose_name=_("Width"),
        help_text=_("Width")
    )
    height = models.IntegerField(
        default=None,
        blank=True,
        null=True,
        verbose_name=_("Height"),
        help_text=_("Height")
    )
    description = models.TextField(
        blank=True,
        null=True,
    )
    responsive = models.BooleanField(
        default=True
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="format_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="format_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.title)

    def get_absolute_url(self):
        return reverse("format_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Format')
        verbose_name_plural = _('Formats')


class FormatResource(resources.ModelResource):

    class Meta:
        model = Format
        fields = ('id', 'title', 'slug',)


class AccountGroupManager(models.Manager):
    def get_query_set(self):
        return super(AccountGroupManager, self).get_query_set().filter(active=True).distinct()


class AccountGroup(Group):
    ct = get_content_type
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Keyword"),
        help_text=_("Automatically generated from name field")
    )
    description = models.TextField(
        blank=True,
        null=True,
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="ag_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="ag_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("account_group_detail", kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Account Group')
        verbose_name_plural = _('Account Groups')
        ordering = ['name']


class SiteConfig(models.Model):
    ct = get_content_type
    sitename = models.CharField(
        max_length=255,
        verbose_name=_("Site Name"),
        help_text=_("Name of website")
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_("Slug"),
        help_text=_("Automatically generated from title field")
    )
    base_framework = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        default=None,
        choices=template_framework,
        verbose_name=_("Base Framework"),
        help_text=_("Select the base framework you use.")
    )
    key = models.SlugField(
        blank=False,
        null=False,
        default=None,
        verbose_name=_("Framework Key"),
        help_text=_("Automatically generated from base framework field")
    )
    domain = models.URLField(
        verbose_name=_("Domain"),
        blank=False,
        null=False,
        default=None,
    )
    code = models.UUIDField(
        max_length=255,
        default=uuid.uuid4,
        blank=False,
        null=False,
        verbose_name=_("Domain Key"),
    )
    email = models.EmailField(
        verbose_name=_("Site Email"),
        null=True,
        blank=True,
        default=None,
    )
    support_email = models.EmailField(
        verbose_name=_("Support Email"),
        null=True,
        blank=True,
        default=None,
    )
    phone = models.CharField(
        verbose_name=_("Phone"),
        default=None,
        max_length=16,
        null=True,
        blank=True
    )
    fax = models.CharField(
        verbose_name=_("Fax"),
        default=None,
        max_length=16,
        null=True,
        blank=True
    )
    address = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Address"),
        help_text=_("Information about the website.")
    )
    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Information"),
        help_text=_("Information about the website.")
    )
    copyright = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        verbose_name=_("Copyright"),
        help_text=_("Copyright text.")
    )
    attribution = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Attribution"),
        help_text=_("Attribution text.")
    )
    footer_text = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Footer Text"),
        help_text=_("Footer text.")
    )
    maintenance_mode = models.BooleanField(
        default=False
    )
    maintenance_mode_message = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Maintenance Mode Message"),
        help_text=_("Enter message that will be displayed on site under maintenance mode.")
    )
    restore_point = models.DateTimeField(
        verbose_name=_("Service Restore Date/Time"),
        auto_now_add=False,
        auto_now=False,
        blank=True,
        null=True,
    )
    locked_locale_message = models.TextField(
        blank=True,
        null=True,
        verbose_name=_("Locked Locale Message"),
        help_text=_("Used to set up default message that will be displayed when a locale is locked.")
    )
    width = models.CharField(
        max_length=10,
        default='1195px',
        blank=False,
        null=False,
        verbose_name=_("Width"),
        help_text=_("Width")
    )
    height = models.CharField(
        max_length=10,
        default='auto',
        blank=True,
        null=True,
        verbose_name=_("Min Height"),
        help_text=_("Height")
    )
    responsive = models.BooleanField(
        default=True
    )
    allow_subdomains = models.BooleanField(
        default=False,
        verbose_name=_("Allow sub domains")
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    version = models.CharField(
        null=True,
        blank=True,
        max_length=120,
        verbose_name=_("CMS Version"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="siteconfig_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="siteconfig_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return str(self.sitename)

    class Meta:
        unique_together = ('slug', 'sitename')
        verbose_name = _('Site Configuration')
        verbose_name_plural = _('Site Configuration')

