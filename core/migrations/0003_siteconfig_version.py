# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-08 16:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20160723_1900'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfig',
            name='version',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='CMS Version'),
        ),
    ]
