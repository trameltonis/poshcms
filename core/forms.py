from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms_foundation.layout import Layout, Fieldset, SplitDateTimeField, Row, Column, ButtonHolder, Submit
from crispy_forms.helper import FormHelper
from modeltranslation.forms import TranslationModelForm
from crispy_forms.layout import Layout
from crispy_forms.bootstrap import TabHolder, Tab
from crispy_forms.layout import Submit, Layout, Field
from crispy_forms.bootstrap import (
    PrependedText, PrependedAppendedText, FormActions)

from .models import Locale, Format, SocialMedia, AccountGroup, SiteConfig


class LocaleCreateForm(forms.ModelForm):
    class Meta:
        model = Locale
        fields = [
            'title',
            'slug',
            'code',
            'description',
            'locked_locale_message',
            'default_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'code',
            'description',
            'locked_locale_message',
            'default_locale',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class LocaleEditForm(forms.ModelForm):
    class Meta:
        model = Locale
        fields = [
            'title',
            'slug',
            'code',
            'description',
            'locked_locale_message',
            'default_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'code',
            'description',
            'locked_locale_message',
            'default_locale',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class FormatCreateForm(forms.ModelForm):
    class Meta:
        model = Format
        fields = [
            'title',
            'slug',
            'width',
            'height',
            'description',
            'responsive',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'width',
            'height',
            'description',
            'responsive',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class FormatEditForm(forms.ModelForm):
    class Meta:
        model = Format
        fields = [
            'title',
            'slug',
            'width',
            'height',
            'description',
            'responsive',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'width',
            'height',
            'description',
            'responsive',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class SocialMediaCreateForm(forms.ModelForm):
    class Meta:
        model = SocialMedia
        fields = [
            'title',
            'slug',
            'account_name',
            'social_media_url',
            'description',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'account_name',
            'social_media_url',
            'description',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class SocialMediaEditForm(forms.ModelForm):
    class Meta:
        model = SocialMedia
        fields = [
            'title',
            'slug',
            'account_name',
            'social_media_url',
            'description',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'title',
            'slug',
            'account_name',
            'social_media_url',
            'description',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class AccountGroupCreateForm(forms.ModelForm):
    class Meta:
        model = AccountGroup
        fields = [
            'slug',
            'description',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'slug',
            'description',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class AccountGroupEditForm(forms.ModelForm):
    class Meta:
        model = AccountGroup
        fields = [
            'slug',
            'description',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'slug',
            'description',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class SiteConfigCreateForm(forms.ModelForm):
    class Meta:
        model = SiteConfig
        fields = '__all__'

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'sitename',
            'slug',
            'base_framework',
            'key',
            'domain',
            'code',
            'email',
            'support_email',
            'phone',
            'address',
            'description',
            'copyright',
            'locked_locale_message',
            'footer_text',
            'maintenance_mode',
            'maintenance_mode_message',
            'restore_point',
            'width',
            'height',
            'responsive',
            'allow_subdomains',
            'status',
            'version',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class SiteConfigEditForm(forms.ModelForm):
    class Meta:
        model = SiteConfig
        fields = '__all__'
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
            'sitename',
            'slug',
            'base_framework',
            'key',
            'domain',
            'code',
            'email',
            'support_email',
            'phone',
            'address',
            'description',
            'copyright',
            'locked_locale_message',
            'footer_text',
            'maintenance_mode',
            'maintenance_mode_message',
            'restore_point',
            'width',
            'height',
            'responsive',
            'allow_subdomains',
            'status',
            'version',
            'visibility',
            'active',
            'created_by',
            'updated_by',
            FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )
