from django.contrib.auth.models import User, Group
from rest_framework import serializers
from ..models import *



class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            'url',
            'username',
            'email',
            'groups'
        )


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = (
            'url',
            'name'
        )

class AccountGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = '__all__'
        depth = 3

class AccountGroupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = '__all__'
        depth = 3

class SocialMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
        depth = 3

class SocialMediaCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
        depth = 3


class SiteConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteConfig
        fields = '__all__'
        depth = 3

class SiteConfigCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = '__all__'
        depth = 3




