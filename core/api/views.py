from rest_framework import generics
from django.contrib.auth.models import User, Group
from rest_framework import generics
from django.views import generic as cbv
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from rest_framework import serializers
from filer.models import File, Image

from ..models import *
from .serializers import *


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class SiteConfigViewSet(viewsets.ModelViewSet):
    queryset = SiteConfig.objects.all().order_by('-created_on')
    serializer_class = SiteConfigSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class SocialMediaViewSet(viewsets.ModelViewSet):
    queryset = SocialMedia.objects.all().order_by('-created_on')
    serializer_class = SocialMediaSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class AccountGroupViewSet(viewsets.ModelViewSet):
    queryset = AccountGroup.objects.all().order_by('-created_on')
    serializer_class = AccountGroupSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']