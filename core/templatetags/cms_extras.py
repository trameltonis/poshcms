from django import template
import re
import markdown
from django.contrib.sites.models import Site
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse, NoReverseMatch
from collections import Iterable
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.translation import get_language
from core.models import *

import poshcms.services
import tldextract
from featured.models import *
from cmf.models import *
from menu_manager.models import *
from media_manager.models import *
from faq.models import *

register = template.Library()


@register.simple_tag()
def newest_banners():
    return Banner.objects.all().filter(status='p', active=True)

@register.simple_tag()
def newest_cta(slug):
    return Banner.objects.all().filter(cta__banner_cta__slug=slug)


@register.filter('routify')
def routify(value):
    return "{% url '" + value + "' %}"


@register.filter('maxify')
def maxify(value):
    # sitename = Site.objects.get_current().domain
    base_domain = Site.objects.get_current().domain
    current_locale = get_language()
    if current_locale == 'en-us':
        base_locale = 'en_US'
    elif current_locale == 'es-us':
        base_locale = 'es_US'
    elif current_locale == 'en-ca':
        base_locale = 'en_CA'
    elif current_locale == 'fr-ca':
        base_locale = 'fr_CA'
    else:
        base_locale = 'en_US'
    return 'https://' + base_domain + '/' + base_locale + "/" + value


@register.filter('pwsurlize')
def pwsurlize(value):
    cta = Cta.objects.all().filter(active=True, is_pws=True)
    if cta:
        value = 'pws_link'
    else:
        value = 'link'
    return value

@register.filter('urlizeme')
def urlizeme(value):
    current_locale = get_language()
    return '/' + current_locale + value


@register.filter('localizme')
def localizme(value):
    current_locale = get_language()
    if current_locale == 'en-us':
        localizr = 'en_US'
    elif current_locale == 'es-us':
        localizr = 'es_US'
    elif current_locale == 'en-ca':
        localizr = 'en_CA'
    elif current_locale == 'fr-ca':
        localizr = 'fr_CA'
    else:
        localizr = 'en_US'
    return '/' + localizr + "/" + value


@register.filter('time_estimate')
def time_estimate(word_count):
    minutes = round(word_count/20)
    return minutes


@register.filter('markdown_to_html')
def markdown_to_html(markdown_text):
    html_body = markdown.markdown(markdown_text)
    return mark_safe(html_body)


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname
    path = context['request'].path
    if re.search(pattern, path):
        return 'active'
    return ''


# Inclusion tags
@register.inclusion_tag('cms-extras/edit.html')
def get_admin_url(self):
    from django.core.urlresolvers import reverse
    info = (self._meta.app_label, self._meta.model_name)
    return reverse('admin:%s_%s_change' % info, args=(self.pk,))

@register.inclusion_tag('menu/_main_menu.html')
def mainmenu():
    mainmenu = Menu.objects.all().order_by('sort_order').filter(active=True, status='p', slug="main-menu")
    return {'mainmenu': mainmenu}


@register.inclusion_tag('menu/_footer_menu.html')
def footermenu():
    footermenu = Menu.objects.all().order_by('sort_order').filter(slug="footer-menu")
    return {'footermenu': footermenu}


@register.inclusion_tag('menu/_sidemenu_right.html')
def sidemenuright():
    sidemenuright = Menu.objects.all().order_by('sort_order').filter(active=True, status='p', slug="side-menu-right")
    return {'sidemenuright': sidemenuright}

@register.inclusion_tag('menu/_newsmenu.html')
def newsmenu():
    newsmenu = Menu.objects.all().order_by('sort_order').filter(active=True, status='p', slug="news-menu")
    return {'newsmenu': newsmenu}

@register.inclusion_tag('menu/_sidemenu_left.html')
def sidemenuleft():
    sidemenuleft = Menu.objects.all().order_by('sort_order').filter(active=True, status='p', slug="side-menu-left")
    return {'sidemenuleft': sidemenuleft}

@register.inclusion_tag('menu/_main_menu.html')
def load_custom_banner_layout():
    # layout = Menu.objects.all().order_by('sort_order').filter(active=True, status='p', slug="main-menu")
    banner = Banner.objects.all().order_by('sort_order').filter(active=True, status='p')
    custom_layout = CustomLayout.objects.all()
    layout = "featured/banner_layouts/"+"CustomLayout.objects.all()"+".html"
    return {'layout': layout}

@register.inclusion_tag('cms-extras/timelines.html')
def timelines(slug):
    timelines = Timeline.objects.all().order_by('sort_order').filter(active=True, status='p')
    return {'timelines': timelines}

@register.inclusion_tag('cms-extras/quotes.html')
def quotes(slug):
    quotes = Quote.objects.all().order_by('sort_order').filter(active=True, status='p')
    return {'quotes': quotes}

@register.inclusion_tag('cms-extras/home_quotes.html')
def home_quotes():
    home_quotes = Quote.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, category__slug="corporate-home")
    return {'home_quotes': home_quotes}

@register.inclusion_tag('cms-extras/home_carousel_quotes.html')
def home_carousel_quotes():
    home_carousel_quotes = Quote.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, category__slug="corporate-home")
    return {'home_carousel_quotes': home_carousel_quotes}

@register.inclusion_tag('cms-extras/leadership_quotes.html')
def leadership_quotes():
    leadership_quotes = Quote.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, category__slug="corporate-our-leadership")
    return {'leadership_quotes': leadership_quotes}

@register.inclusion_tag('cms-extras/host_testimonials.html')
def host_testimonials():
    host_testimonials = Testimonial.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, category__slug="corporate-host-party")
    return {'host_testimonials': host_testimonials}

@register.inclusion_tag('cms-extras/host_testimonials_join.html')
def host_testimonials_join():
    host_testimonials_join = Testimonial.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, category__slug="corporate-join")
    return {'host_testimonials_join': host_testimonials_join}

@register.inclusion_tag('cms-extras/testimonial_videos.html')
def testimonial_videos():
    testimonial_videos = Video.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, category__slug="promotional-videos")[:1]
    related_videos = Video.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, related_videos__category__slug__exact="promotional-videos")[:5]
    return {'testimonial_videos': testimonial_videos, 'related_videos': related_videos}


@register.inclusion_tag('cms-extras/single_video.html')
def single_video():
    single_video = Video.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, slug='new-generation-norwex-microfiber', category__slug="promotional-videos")[:1]
    return {'single_video': single_video}


@register.inclusion_tag('cms-extras/energy_video.html')
def energy_video():
    energy_video = Video.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, slug='turning-trash-electricity', category__slug="microfiber")[:1]
    return {'energy_video': energy_video}

@register.inclusion_tag('cms-extras/current_specials.html')
def current_specials():
    current_specials = Specials.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, current=True)[:1]
    return {'current_specials': current_specials}

@register.inclusion_tag('cms-extras/customer_specials.html')
def customer_specials():
    customer_specials = Specials.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, type="customer")
    return {'customer_specials': customer_specials}

@register.inclusion_tag('cms-extras/host_specials.html')
def host_specials():
    # publish_start = Specials.objects.publish_start()
    # publish_end = Specials.objects.publish_end()
    host_specials = Specials.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, type="host")
    # host_specials = Specials.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, type="host", date__range=[publish_start, publish_end])
    # host_specials = Specials.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, type="host", publish_start__gt='publish_start', publish_end__lt='publish_end')
    return {'host_specials': host_specials}

# @register.inclusion_tag('cms-extras/collection.html')
# def single_video():
#     single_video = Cta.objects.all().order_by('sort_order').filter(active=True, status='p', featured=True, slug='new-generation-norwex-microfiber', category__slug="promotional-videos")[:1]
#     return {'single_video': single_video}

@register.inclusion_tag('cms-extras/collection_carousel.html')
def collection_carousel():
    collection_carousel = Cta.objects.all().filter(active=True, status='p', collection__slug="starter-kit")
    return {'collection_carousel': collection_carousel}


@register.inclusion_tag('cms-extras/join_earn_carousel.html')
def join_earn_carousel():
    join_earn_carousel = Cta.objects.all().filter(active=True, status='p', collection__slug="how-much-can-you-earn")
    return {'join_earn_carousel': join_earn_carousel}


@register.inclusion_tag('cms-extras/cart.html', takes_context=True)
def show_cart(context):
    request = context['request'] ##  for future when we get lcoale and processNumber dynamicly

    order_data = [
        data[1]
        for data in request.COOKIES.items()
        if data[0].lower().startswith('pws_web_order_')
    ]
    cartitems_list = None
    if len(order_data) > 0:
        cartitems_list = poshcms.services.get_cartitems(
            context['LANGUAGE_CODE'],
            order_data[0]
        )

    return {
        'cartitems_list': cartitems_list,
        'context': context
    }

@register.inclusion_tag('cms-extras/consultant_info.html', takes_context=True)
def show_consultant_info(context):
    request = context['request']

    token = request.subdomain
    consultant_info_list = None
    consultant_info_list = poshcms.services.get_consultant_info(
            context['LANGUAGE_CODE'],
            token
        )

    return {
        'consultant_info_list': consultant_info_list,
        'context': context
    }

@register.filter
def maxmoney(value):
    if value:
        value = round(float(value), 2)
        return "$%s" % (intcomma(int(value)))
    else:
        return ''


# Home Landing

# @register.inclusion_tag('cms-extras/featured/home-hero.html')
# def home_hero():
#     collection_carousel = Cta.objects.all().filter(active=True, status='p', collection__slug="starter-kit")
#     return {'collection_carousel': collection_carousel}


@register.inclusion_tag('cms-extras/featured/shop_host_join_learn.html')
def shop_host_join_learn():
    collection_cta = Cta.objects.all().order_by('sort_order').filter(active=True, status='p', collection__slug="shop-host-join-learn")
    return {'collection_cta': collection_cta }


# @register.inclusion_tag('cms-extras/faq/category_list.html')
# def faq_category_list():
#     faq_category_list = Question.objects.all().filter()
#     return {'faq_category_list': faq_category_list }
#

@register.inclusion_tag('cms-extras/faq/category_list.html', takes_context=True)
def faq_category_list(context):
  request = context['request']
  faq_category_list = Question.objects.all().filter(category__slug=request.GET.get('category'))
  return {'faq_category_list': faq_category_list }