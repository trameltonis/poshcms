from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import LocaleListView, LocaleCreateView, LocaleUpdateView, LocaleDetailView, FormatListView, FormatCreateView, FormatUpdateView, FormatDetailView, SocialMediaCreateView, SocialMediaUpdateView, SocialMediaListView, SocialMediaDetailView, AccountGroupCreateView, AccountGroupUpdateView, AccountGroupListView, AccountGroupDetailView

urlpatterns = [
    url(r'^locale/$', LocaleListView.as_view(), name='locale_list'),
    url(r'^locale/create/$', LocaleCreateView.as_view(), name='locale_create'),
    url(r'^locale/edit/(?P<slug>[\w-]+)/$', LocaleUpdateView.as_view(), name='locale_update'),
    url(r'^locale/(?P<slug>[\w-]+)/$', LocaleDetailView.as_view(), name='locale_detail'),
    url(r'^format/$', FormatListView.as_view(), name='format_list'),
    url(r'^format/create/$', FormatCreateView.as_view(), name='format_create'),
    url(r'^format/edit/(?P<slug>[\w-]+)/$', FormatUpdateView.as_view(), name='format_update'),
    url(r'^format/(?P<slug>[\w-]+)/$', FormatDetailView.as_view(), name='format_detail'),
    url(r'^social/$', SocialMediaListView.as_view(), name='social_media_list'),
    url(r'^social/create/$', SocialMediaCreateView.as_view(), name='social_media_create'),
    url(r'^social/edit/(?P<slug>[\w-]+)/$', SocialMediaUpdateView.as_view(), name='social_media_update'),
    url(r'^social/(?P<slug>[\w-]+)/$', SocialMediaDetailView.as_view(), name='social_media_detail'),
    url(r'^account-group/$', AccountGroupListView.as_view(), name='account_group_list'),
    url(r'^account-group/create/$', AccountGroupCreateView.as_view(), name='account_group_create'),
    url(r'^account-group/edit/(?P<slug>[\w-]+)/$', AccountGroupUpdateView.as_view(), name='account_group_update'),
    url(r'^account-group/(?P<slug>[\w-]+)/$', AccountGroupDetailView.as_view(), name='account_group_detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)