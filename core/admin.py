from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import export_as_json, make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate
from django.utils.translation import ugettext_lazy as _
from django import forms
from ckeditor.widgets import CKEditorWidget
# import locale
from import_export.admin import ImportExportModelAdmin
from .models import SocialMedia, Locale, Format, AccountGroup, SiteConfig
from reversion.admin import VersionAdmin
from poshcms.translation.admin import ExtendedTranslationAdmin
from modeltranslation.admin import TranslationAdmin, TranslationGenericStackedInline
# from schedule.models import Calendar, CalendarRelation, Event


class AddressAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('address_1',)}
    list_display = ('title', 'address_1', 'address_2', 'city', 'state',  'country',  'zipcode',  'phone', 'active', 'updated')
    search_fields = ['title', 'address_1', 'description']
    list_filter = ['active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class DiscountTypeAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'code', 'rate', 'active', 'publish', 'updated')
    search_fields = ['title', 'slug', 'description']
    list_filter = ['active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class CurrencyAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'symbol', 'set_primary', 'active', 'publish', 'updated')
    search_fields = ['title', 'slug', 'description']
    list_filter = ['active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


# @admin.register(Locale)
class LocaleAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'code', 'default_locale', 'active', 'updated')
    search_fields = ['title', 'slug', 'description']
    list_filter = ['active']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(SocialMedia)
class SocialMediaAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('title',)}
    list_display = (
        'title',
        'slug',
        'social_media_url',
        'sort_order',
        'active',
        'updated'
    )
    search_fields = ['title', 'slug', 'description']
    list_filter = ['active']
    fields = [
        'title',
        'slug',
        'account_name',
        'social_media_url',
        'description',
        'visibility',
        'active',
        'sort_order',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('created_by', 'updated_by', )
    list_editable = ['sort_order', 'active']
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class FormatAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'width', 'height',
                    'active', 'updated')
    search_fields = ['title', 'slug', 'description']
    list_filter = ['active']
    fields = [
        'title',
        'slug',
        'width',
        'height',
        'description',
        'responsive',
        'visibility',
        'active',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


class AccountGroupAdminForm(forms.ModelForm):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = AccountGroup
        fields = [
            'name',
            'slug',
            'description',
            'visibility',
            'active',
            'created_by',
            'updated_by',
        ]
        readonly_fields = ('created_by', 'updated_by', )
        admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(AccountGroup)
class AccountGroupAdmin(TranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    # form = AccountGroupAdminForm
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'slug', 'active')
    search_fields = ['name', 'slug', 'description']
    list_filter = ['active']
    fields = [
        'name',
        'slug',
        'description',
        'visibility',
        'active',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(SiteConfig)
class SiteConfigAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    prepopulated_fields = {'slug': ('sitename',), 'key': ('slug','base_framework',)}
    list_display = (
        'sitename',
        'language_selectors',
        'domain',
        'phone',
        'fax',
        'email',
        'support_email',
        'version',
        'visibility',
        'active'
    )
    search_fields = ['sitename', 'slug', 'description']
    list_filter = ['active']
    list_editable = ['version',]
    fields = [
        'sitename',
        'slug',
        'base_framework',
        'key',
        'domain',
        'code',
        'email',
        'support_email',
        'phone',
        'fax',
        'address',
        'description',
        'copyright',
        'attribution',
        'locked_locale_message',
        'footer_text',
        'maintenance_mode',
        'maintenance_mode_message',
        'restore_point',
        'width',
        'height',
        'responsive',
        'allow_subdomains',
        'status',
        'version',
        'visibility',
        'active',
        'created_by',
        'updated_by',
    ]
    readonly_fields = ('code', 'created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


admin.site.site_title = 'Norwex CMS'
admin.site.site_header = 'Norwex CMS'
