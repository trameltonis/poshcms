from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext
from django.http import HttpResponse
from django.views.generic import View
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.utils.decorators import method_decorator

from .forms import LocaleCreateForm, LocaleEditForm, FormatCreateForm, FormatEditForm, SocialMediaCreateForm, SocialMediaEditForm, AccountGroupCreateForm,AccountGroupEditForm
from .models import Locale, Format, SocialMedia, AccountGroup


class LocaleCreateView(CreateView):
    template_name = "core/create_form.html"
    form_class = LocaleCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(LocaleCreateView, self).form_valid(form)


class LocaleUpdateView(UpdateView):
    template_name = "core/edit.html"
    form_class = LocaleEditForm
    model = Locale

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(LocaleUpdateView, self).form_valid(form)


class LocaleDetailView(DetailView):
    model = Locale
    template_name = "core/_locale_detail.html"


class LocaleListView(ListView):
    model = Locale
    template_name = "core/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(LocaleListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class FormatCreateView(CreateView):
    template_name = "core/create_form.html"
    form_class = FormatCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(FormatCreateView, self).form_valid(form)


class FormatUpdateView(UpdateView):
    template_name = "core/edit.html"
    form_class = FormatEditForm
    model = Format

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(FormatUpdateView, self).form_valid(form)


class FormatDetailView(DetailView):
    model = Format
    template_name = "core/_format_detail.html"


class FormatListView(ListView):
    model = Format
    template_name = "core/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(FormatListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class SocialMediaCreateView(CreateView):
    template_name = "core/create_form.html"
    form_class = SocialMediaCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(SocialMediaCreateView, self).form_valid(form)


class SocialMediaUpdateView(UpdateView):
    template_name = "core/edit.html"
    form_class = SocialMediaEditForm
    model = SocialMedia

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(SocialMediaUpdateView, self).form_valid(form)


class SocialMediaDetailView(DetailView):
    model = SocialMedia
    template_name = "core/detail.html"


class SocialMediaListView(ListView):
    model = SocialMedia
    template_name = "core/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(SocialMediaListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class AccountGroupCreateView(CreateView):
    template_name = "core/create_form.html"
    form_class = AccountGroupCreateForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(AccountGroupCreateView, self).form_valid(form)


class AccountGroupUpdateView(UpdateView):
    template_name = "core/edit.html"
    form_class = AccountGroupEditForm
    model = AccountGroup

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(AccountGroupUpdateView, self).form_valid(form)


class AccountGroupDetailView(DetailView):
    model = AccountGroup
    template_name = "core/detail.html"


class AccountGroupListView(ListView):
    model = AccountGroup
    template_name = "core/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(FormatListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs

