from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import SocialMedia, Locale, Format, AccountGroup


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            'url',
            'username',
            'email',
            'groups'
        )


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = (
            'url',
            'name'
        )
        

class SocialMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
        depth = 3

class SocialMediaCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
        depth = 3

class LocaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Locale
        fields = '__all__'
        depth = 3

class LocaleCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Locale
        fields = '__all__'
        depth = 3


class FormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Format
        fields = '__all__'
        depth = 3


class FormatCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Format
        fields = '__all__'
        depth = 3

class AccountGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        field = (
            'id',
            'slug',
            'description',
            'active',
        )

class AccountGroupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        field = (
            'id',
            'slug',
            'description',
            'active',
        )
