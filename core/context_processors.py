from core.models import SiteConfig, SocialMedia


def siteconfig_processor(request):
 siteconfig = SiteConfig.objects.all()
 return {'siteconfig': siteconfig }

def socials_processor(request):
 socials = SocialMedia.objects.all().filter(active=True)
 return {'socials': socials }

