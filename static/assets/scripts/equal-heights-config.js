$(document).ready(function() {
  var phoneBreakpoint = 680;
  var equalHeightObjects = $('[data-behavior="equal-height"]');
  equalHeightObjects.equalheight();

  $(window).resize(function() {
    if ($(window).width() >= phoneBreakpoint) {
      equalHeightObjects.equalheight();
    } else {
      equalHeightObjects.removeAttr('style');
    }
  });
});
