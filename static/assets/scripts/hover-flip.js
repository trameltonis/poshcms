(function($) {
  
  $(document).ready(function() {
    var flipContainer = $('[data-structure="flip-container"]');
    flipContainer.on('touchstart', function(event) {
      $(this).classList.toggle('hover');
    });
  });
  
})(jQuery);