(function($) {
  
  $(document).ready(function() {
    var faqQuestion = $('.faq-question');
    var categoryDropdown = $('select#categories');
    var searchText = $('#search-criteria');

    //Toggle hiding/showing the Answer per Question
    faqQuestion.on('click', function(event) {
      $(this).find('.faq-answer').toggle();
    });

    //Navigate to the category selected
    categoryDropdown.on('change', function(event) {
    	updateCategories(this.value);

    	/*if(this.value == "all") {
    		$('.faq-section').show();
    	} else {
    		$('.faq-section').hide();
    		$('.' + this.value).show();
    	}*/
    });

    //Text Search
    searchText.keyup(function() {
    	var sText = this.value;
    	var sCategory = $('select#categories').val();

    	//Expand all Answers, otherwise close
    	if(sText == null || sText == undefined || sText.length == 0) {
    		$('.faq-answer').hide();

    		//Reset the view if there is a value in the dropdown
    		updateCategories(sCategory);

    	} else {
    		$('.faq-answer').show();
    	}

		$('.faq-section').each(function() {

			//Check if category should be visible
			var isVisible = sCategory == 'all' || $(this).hasClass(sCategory);

			$(this).unmark();

			if(sText.length > 0) {
				var divText = $(this).text().toUpperCase();

				$(this).mark(sText);

				if(divText.indexOf(sText.toUpperCase()) != -1 && isVisible){
					$(this).show();
				} else {
					$(this).hide();
				}		
			}	
		});
    });

    var updateCategories = function(val) {
    	if(val == "all") {
			$('.faq-section').show();
		} else {
			$('.faq-section').hide();
			$('.' + val).show();
		}
    };

  });
  
})(jQuery);