(function($) {
  
  $.extend({
    getYoutubeThumbnail: function( url, size ){
      if (url === null) { return ""; }
      size = (size === null) ? "big" : size;
      var vid;
      var results;
      results = url.match("[\?&]v=([^&#]*)");
      vid = ( results === null ) ? url : results[1];
      if (vid !== "") {
        if (size == "small") {
          return "http://img.youtube.com/vi/" + vid + "/2.jpg";
        } else {
          return "http://img.youtube.com/vi/" + vid + "/0.jpg";
        }
      }
    }
  });
  
  $(document).ready(function() {
    var videoPlayer = $('[data-structure="video-player"]').children('iframe');
    var videoThumbnail = $('[data-behavior="play-video"]');
    
    videoThumbnail.each(function() {
      var link = $(this);
      var src = link.attr('src');
      if (src !== "") {
        link.attr('style', 'background-image: url("' + $.getYoutubeThumbnail(src) + '");');
      }
    });
    
    videoThumbnail.click(function(event) {
      event.preventDefault();
      newSource = 'http://www.youtube.com/embed/' + $(this).attr('src');
      videoPlayer.attr('src', newSource);
      
      if ($(window).width() <= 1024) {
        $('html, body').animate({
          scrollTop: $('[data-structure="video-section"]').offset().top
        }, 380);
      }
    });
  });
  
})(jQuery);