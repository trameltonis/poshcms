(function($) {
  $(document).ready(function() {
    
    function initDefaultCarousel() {
      var carousel = $('[data-behavior="default-carousel"]');
      var nextArrow = carousel.parent().children('[data-behavior="slick-next"]');
      var prevArrow = carousel.parent().children('[data-behavior="slick-prev"]');
      
      carousel.slick({
        dots: true,
        nextArrow: nextArrow,
        prevArrow: prevArrow,
      });
    };
    
    function initNoDotsCarousel() {
      var carousel = $('[data-behavior="no-dots-carousel"]');
      var nextArrow = carousel.parent().children('[data-behavior="slick-next"]');
      var prevArrow = carousel.parent().children('[data-behavior="slick-prev"]');
      
      carousel.slick({
        dots: false,
        nextArrow: nextArrow,
        prevArrow: prevArrow,
      });
    };
    
    function initMultiCarousel() {
      var carousel = $('[data-behavior="multi-carousel"]');
      var nextArrow = carousel.parent().children('[data-behavior="slick-next"]');
      var prevArrow = carousel.parent().children('[data-behavior="slick-prev"]');
      
      carousel.slick({
        dots: false,
        infinite: true,
        nextArrow: nextArrow,
        prevArrow: prevArrow,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 680,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    };
    
    initDefaultCarousel();
    initNoDotsCarousel();
    initMultiCarousel();
  });
})(jQuery);