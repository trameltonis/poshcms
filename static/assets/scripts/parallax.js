(function($) {
  // Parallax elements scroll at a speed equal to a percentage
  // of the window scroll speed. This speed is 20% by default,
  // (0.2) and can be overridden by passing a speed parameter
  // as a decimal value.
  $.fn.parallax = function(speed) {
    var scrolled = $(window).scrollTop();
    var speed = speed || 0.2;
    var topValue = $(this).position().top;
    $(this).css('top', -(scrolled * speed) + 'px');
  };

  $(document).ready(function() {
    var heroImage = $('[data-behavior="hero-parallax"]');
    var slowSpeed = $('[data-behavior="parallax-slow"]');
    var mediumSpeed = $('[data-behavior="parallax-medium"]');
    var fastSpeed = $('[data-behavior="parallax-fast"]');
    
    if (heroImage.length > 0) {
      heroImage.parallax();
    }
    
    if (slowSpeed.length > 0) {
      slowSpeed.parallax(-0.075);
    }
    
    if (mediumSpeed.length > 0) {
      mediumSpeed.parallax(-0.15);
    }
    
    if (fastSpeed.length > 0) {
      fastSpeed.parallax(-0.25);
    }

    $(window).scroll(function() {
      
      if (heroImage.length > 0) {
        heroImage.parallax();
      }
      
      if (slowSpeed.length > 0) {
        slowSpeed.parallax(-0.075);
      }
      
      if (mediumSpeed.length > 0) {
        mediumSpeed.parallax(-0.15);
      }
      
      if (fastSpeed.length > 0) {
        fastSpeed.parallax(-0.25);
      }
    });
  });
})(jQuery);
