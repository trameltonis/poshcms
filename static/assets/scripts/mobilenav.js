(function($) {

	$(document).ready(function() {

		var $mobileMenuButton = $('.mobile-menu-button');
		var $mainNav = $('.nav-bar nav');
		var $body = $('body');

		$mobileMenuButton.on('click', function(){
			$mainNav.toggleClass('visible');
			$body.toggleClass('overflow-hidden');
		});

		var $mobileLanguageTrigger = $('a[data-language="trigger"]');
		var $mobileNavDropdown = $('ul.mobile-language-nav');

		$mobileLanguageTrigger.on('click', function(){
			$mobileNavDropdown.toggleClass('visible');
		});
	});

})(jQuery);