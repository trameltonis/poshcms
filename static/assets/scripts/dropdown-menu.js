(function($) {
  $(document).ready( function(){
    var parent = $("header nav ul.menu").children(".menu-item-has-children"),
        subMenuClass = ".sub-menu",
        mobileBreak = 680;

    parent.hover( function() {
      var subMenu = $(this).children(subMenuClass);
      if( $(window).width() > mobileBreak ) {
        var element = $(this);
        clearTimeout(element.data("leaveHoverTimeout"));
        var hoverTimeout = setTimeout( function(){
          element.children(subMenu).fadeIn();
        }, 300);
        element.data('hoverTimeout', hoverTimeout);
      }
    }, function(){
      var subMenu = $(this).children(subMenuClass);
      if( $(window).width() > mobileBreak ) {
        var element = $(this);
        var subMenu = element.children(subMenu);
        clearTimeout(element.data("hoverTimeout"));
        var leaveHoverTimeout = setTimeout(function(){
          subMenu.fadeOut( function(){
            subMenu.removeAttr("style");
          });
        }, 400);
        element.data("leaveHoverTimeout", leaveHoverTimeout);
      }
    });


    //Top Level drop down navs 
    var menuSelect = $(".menu-selected");

    menuSelect.on("click", function(e) {
      e.preventDefault();

      $(this).toggleClass("selected-top-nav");
      $(this).next(".menu-list").toggle();
    });

    var menuItemSelect = $(".menu-list .menu-selection");

    //Swap items - disabling functionality for static
    /*menuItemSelect.on("click", function(e) {
      //Pull the html for this selected item
      var item = $(this).html();

      //Find the correct selected menu to replace and pull its html
      var menuSelectedItem = $(".selected-top-nav");

      var selectedItem = menuSelectedItem.html(); 

      //Swap
      $(this).html(selectedItem);
      menuSelectedItem.html(item);

      //Close the menu
      $(".menu-list").toggle();
      menuSelectedItem.removeClass("selected-top-nav");


    });*/

    //Just close the menu for when selecting an item
    menuItemSelect.on("click", function(e) {

      var menuSelectedItem = $(".selected-top-nav");

      var selectedItem = menuSelectedItem.html();

      //Close the menu
      $(".menu-list").toggle();
      menuSelectedItem.removeClass("selected-top-nav");

    });


  });
})(jQuery);
