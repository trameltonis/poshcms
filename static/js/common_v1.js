$(function () {
    /**
     * Change language link
     */
    $('.lng-change').click(function (e) {
        e.preventDefault();
        var el = $(this),
            token = $('input[name="csrfmiddlewaretoken"]').eq(0).val();
        $.post(el.parents('ul').data('url'), {"language": el.data('code'), "csrfmiddlewaretoken": token})
            .success(function() {window.location.reload()});
    });

    $('.list-filter').change(function(e) {
        window.location = $(this).find(':checked').data('url');
    });

    $('.delete-link').click(function(e) {
        e.preventDefault();
        var el = $(this);
        var itemName = 'this';
        if (el.data('name')) {
            itemName = el.data('name');
        }
        var action = el.data('action') || 'delete';
        bootbox.confirm('<h4>Are you sure you want to ' + action + ' ' + itemName + '?</h4>', function (result) {
            if (result) {
                $.get(el.attr('href'), 'json', function (data) {
                    if ('redirect' in data) {
                        location.href = data['redirect'];
                    }
                    else {
                        location.reload();
                    }
                });
            }
        });
    });
});