/**
 * Created by frank.uche on 6/9/2016.
 */

$interpolateProvider.startSymbol('[[').endSymbol(']]');

$httpProvider.defaults.xsrfCookieName = 'csrftoken';

$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

$resourceProvider.defaults.stripTrailingSlashes = false;
