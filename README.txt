Norwex CMS Project

Content Managment System for Norwex. The application provides api endpoints that can be consumed by third party applications such as AngularJS. This is intended to decouple the Content Management layer from the rest of Symfony2 in the Norwex Max project.


Requirements:
    python3.4
    pip package manager
    python3.4 virtualenv
    mysql (prod), sqlite3(dev)

Installation:
    1. clone repository (git clone *repository link*)
    2. go to the local application folder
    3. create and activate a virtualenv (venv) for python3.4
    4. install python packages using pip (pip install -r requirements.txt)
    5. cd into poshcms folder and run python manage.py makemigrations
    6. run python manage.py migrate (this will create a sqlite3 db.sqlite file to be used for development.)
    7. run python manage.py runserver 0.0.0.0:8000
    8. Go to your browser on http://localhost:8000 to view site.