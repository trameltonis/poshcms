# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-30 18:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('widget_manager', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='faqwidget',
            old_name='news',
            new_name='question',
        ),
    ]
