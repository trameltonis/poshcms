from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import SimpleWidget, StringWidget, MediaWidget, GalleryWidget


class SimpleWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimpleWidget
        fields = '__all__'
        depth = 3


class SimpleWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimpleWidget
        fields = '__all__'
        depth = 3


class StringWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = StringWidget
        fields = '__all__'
        depth = 3


class StringWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = StringWidget
        fields = '__all__'
        depth = 3


class MediaWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = MediaWidget
        fields = '__all__'
        depth = 3


class MediaWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = MediaWidget
        fields = '__all__'
        depth = 3


class GalleryWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = GalleryWidget
        fields = '__all__'
        depth = 3


class GalleryWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = GalleryWidget
        fields = '__all__'
        depth = 3
