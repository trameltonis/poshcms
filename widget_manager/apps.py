from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class WidgetManagerConfig(AppConfig):
    name = 'widget_manager'
    verbose_name = _('Content Widgets')
