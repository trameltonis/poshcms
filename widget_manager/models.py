from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from datetime import datetime, timedelta
from filebrowser.fields import FileBrowseField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from theme.models import *
from core.models import *
from news.models import *
from faq.models import *
from media_manager.models import *
from helpers.utils import *


class StringWidget(models.Model):
    ct = get_content_type
    name = models.CharField(
        max_length=100,
        verbose_name=_('Widget Name')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        null=True,
        blank=True,
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        null=True,
        blank=True,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the widget."),
        default=None
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Widget Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="strwgt_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="strwgt_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )
    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('String Widget')
        verbose_name_plural = _('String Widgets')


class SimpleWidget(models.Model):
    ct = get_content_type
    name = models.CharField(
        max_length=100,
        verbose_name=_('Widget Name')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        null=True,
        blank=True,
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        null=True,
        blank=True,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    title = models.CharField(
        max_length=100,
        verbose_name=_('Content Title')
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the widget."),
        default=None
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Widget Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="simw_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="simw_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Simple Widget')
        verbose_name_plural = _('Simple Widgets')


class MediaWidget(models.Model):
    ct = get_content_type
    name = models.CharField(
        max_length=100,
        verbose_name=_('Widget Name')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        null=True,
        blank=True,
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        null=True,
        blank=True,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="mwd_img",
        verbose_name=_("Image")
    )
    format = models.ForeignKey(
        Format,
        blank=False,
        verbose_name=_("Gallery Format"),
        help_text=_("Add image format to gallery"),
        default=None
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the widget."),
        default=None
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Widget Sort Order."),
        default=0
    )
    caption = models.BooleanField(
        default=True,
        verbose_name=_("Caption"),
        help_text = _("Use to add caption to content.")
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="mw_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="mw_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )
    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Media Widget')
        verbose_name_plural = _('Media Widgets')


class GalleryWidget(models.Model):
    ct = get_content_type
    name = models.CharField(
        max_length=100,
        verbose_name=_('Widget Name')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        null=True,
        blank=True,
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        null=True,
        blank=True,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    title = models.CharField(
        max_length=100,
        verbose_name=_('Content Title')
    )
    gallery = models.ForeignKey(
        Gallery,
        blank=False,
        verbose_name=_("Gallery"),
        help_text=_("Add a gallery to widget"),
        related_name="gw_gallery",
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Layout"),
        help_text=_("Select a layout for widget"),
        related_name="gallery_widget_layout",
        default=None
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the widget."),
        default=None
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Widget Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="gw_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="gw_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )
    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Gallery Widget')
        verbose_name_plural = _('Gallery Widgets')


class NewsWidget(models.Model):
    ct = get_content_type
    name = models.CharField(
        max_length=100,
        verbose_name=_('Widget Name')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        null=True,
        blank=True,
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        null=True,
        blank=True,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    title = models.CharField(
        max_length=100,
        verbose_name=_('Content Title')
    )
    news = models.ManyToManyField(
        News,
        blank=False,
        verbose_name=_("News"),
        help_text=_("Add news to widget"),
        related_name="news_widget",
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Layout"),
        help_text=_("Select a layout for widget"),
        related_name="news_widget_layout",
        default=None
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the widget."),
        default=None
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Widget Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_widget_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="news_widget_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )
    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('News Widget')
        verbose_name_plural = _('News Widgets')


class FaqWidget(models.Model):
    ct = get_content_type
    name = models.CharField(
        max_length=100,
        verbose_name=_('Widget Name')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        null=True,
        blank=True,
        choices=SECTION,
        verbose_name=_('Section'),
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        null=True,
        blank=True,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    title = models.CharField(
        max_length=100,
        verbose_name=_('Content Title')
    )
    question = models.ManyToManyField(
        Question,
        blank=False,
        verbose_name=_("Faq"),
        help_text=_("Add faq to widget"),
        related_name="faq_widget",
        default=None
    )
    layout = models.ForeignKey(
        Layout,
        blank=False,
        verbose_name=_("Layout"),
        help_text=_("Select a layout for widget"),
        related_name="faq_widget_layout",
        default=None
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the widget."),
        default=None
    )
    sort_order = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Widget Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="faq_widget_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="faq_widget_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )
    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('slug', 'name')
        verbose_name = _('Faq Widget')
        verbose_name_plural = _('Faq Widgets')


