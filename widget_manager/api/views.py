from rest_framework import generics

from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import View
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render
from django.utils.decorators import method_decorator
from ..models import *
from .serializers import *


class StringWidgetViewSet(viewsets.ModelViewSet):
    queryset = StringWidget.objects.all().order_by('-created_on')
    serializer_class = StringWidgetSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class SimpleWidgetViewSet(viewsets.ModelViewSet):
    queryset = SimpleWidget.objects.all().order_by('-created_on')
    serializer_class = SimpleWidgetSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MediaWidgetViewSet(viewsets.ModelViewSet):
    queryset = MediaWidget.objects.all().order_by('-created_on')
    serializer_class = MediaWidgetSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryWidgetViewSet(viewsets.ModelViewSet):
    queryset = GalleryWidget.objects.all().order_by('-created_on')
    serializer_class = GalleryWidgetSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']