from django.db import models
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from helpers.utils import export_as_json, make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields, widgets, results, instance_loaders
from modeltranslation.admin import TranslationStackedInline
from widget_manager.models import *


@admin.register(StringWidget)
class StringWidgetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'active', 'visibility', 'progress_status', 'status', 'updated')
    search_fields = ['name', 'slug', 'content']
    list_filter = ['section', 'active']
    exclude = ['url', 'section']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(SimpleWidget)
class SimpleWidgetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'active', 'visibility', 'progress_status', 'status', 'updated')
    search_fields = ['name', 'slug', 'content']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))
    # list_editable = ('status',)


@admin.register(MediaWidget)
class MediaWidgetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'active', 'visibility', 'progress_status', 'status', 'updated')
    search_fields = ['name', 'slug', 'content']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(GalleryWidget)
class GalleryWidgetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'active', 'progress_status', 'status', 'updated')
    search_fields = ['name', 'slug', 'content']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(NewsWidget)
class NewsWidgetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    filter_horizontal = ["news"]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'status', 'visibility', 'active', 'progress_status', 'updated')
    search_fields = ['name', 'slug', 'content']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(FaqWidget)
class FaqWidgetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    filter_horizontal = ["question"]
    prepopulated_fields = {
        'slug': ('section', 'name',),
        'url': ('name',),
    }
    list_display = ('name', 'slug', 'section', 'status', 'visibility', 'active', 'progress_status', 'updated')
    search_fields = ['name', 'slug', 'content']
    list_filter = ['section', 'active']
    list_per_page = 50
    readonly_fields = ('created_by', 'updated_by', )
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


# Import and Export Resource Definition
class SimpleWidgetResource(resources.ModelResource):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    delete = fields.Field(widget=widgets.BooleanWidget())

    class Meta:
        model = SimpleWidget
        exclude = ('id', 'created_on', )
        export_order = (
            'id',
            'name',
            'slug',
            'content',
            'sort_order',
            'status',
            'visibility',
            'publish_start',
            'publish_end',
            'active',
            'progress_status',
            'lock_locale',
            'created_by',
            'updated_by',
            'created_on',
            'updated',
        )
        skip_unchanged = False
        report_skipped = False
        import_id_fields = [u'slug']
