from modeltranslation.translator import translator, register, TranslationOptions
from .models import StringWidget, GalleryWidget, MediaWidget, SimpleWidget, NewsWidget, FaqWidget


@register(StringWidget)
class StringWidgetTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'content',
        ]


@register(GalleryWidget)
class GalleryWidgetTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'title',
        'content',
        ]


@register(MediaWidget)
class MediaWidgetTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'content',
        'img',
        ]


@register(SimpleWidget)
class SimpleWidgetTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'title',
        'content',
        ]


@register(NewsWidget)
class NewsWidgetTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'title',
        'content',
        ]


@register(FaqWidget)
class FaqWidgetTranslationOptions(TranslationOptions):
    fields = [
        'name',
        'title',
        'content',
        ]


