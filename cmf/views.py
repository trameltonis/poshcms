from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.views.generic import View
from django.contrib.contenttypes.models import ContentType
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from .forms import PageCreateForm, PageEditForm, ResourceCreateForm, ResourceEditForm, CategoryCreateForm, CategoryEditForm, EmailCreateForm, EmailEditForm, DashboardCreateForm, DashboardEditForm, TagCreateForm, TagEditForm, SpecialsCreateForm, SpecialsEditForm, IncentivesCreateForm, IncentivesEditForm, TestimonialCreateForm, TestimonialEditForm, QuoteCreateForm, QuoteEditForm, TimelineCreateForm, TimelineEditForm
from .models import Page, Category, Resource, Email, Dashboard, Tags, Specials, Incentives, Testimonial, Quote, Timeline
from menu_manager.models import MenuCategory, Menu
from .serializers import SpecialsSerializer, SpecialsCreateSerializer, IncentivesSerializer, IncentivesCreateSerializer, CategorySerializer, CategoryCreateSerializer, PageSerializer, PageCreateSerializer, ResourceSerializer, ResourceCreateSerializer, EmailSerializer, EmailCreateSerializer, DashboardSerializer, DashboardCreateSerializer,  TagSerializer, TagCreateSerializer, TestimonialSerializer, TestimonialCreateSerializer, QuoteSerializer, QuoteCreateSerializer, TimelineSerializer, TimelineCreateSerializer


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all().order_by('-created_on')
    serializer_class = PageSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class ResourceViewSet(viewsets.ModelViewSet):
    queryset = Resource.objects.all().order_by('-created_on')
    serializer_class = ResourceSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class EmailViewSet(viewsets.ModelViewSet):
    queryset = Email.objects.all().order_by('-created_on')
    serializer_class = EmailSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class DashboardViewSet(viewsets.ModelViewSet):
    queryset = Dashboard.objects.all().order_by('-created_on')
    serializer_class = DashboardSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class SpecialsViewSet(viewsets.ModelViewSet):
    queryset = Specials.objects.all().order_by('-created_on')
    serializer_class = SpecialsSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class IncentivesViewSet(viewsets.ModelViewSet):
    queryset = Incentives.objects.all().order_by('-created_on')
    serializer_class = IncentivesSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']

class TestimonialViewSet(viewsets.ModelViewSet):
    queryset = Testimonial.objects.all().order_by('-created_on')
    serializer_class = TestimonialSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class QuoteViewSet(viewsets.ModelViewSet):
    queryset = Quote.objects.all().order_by('-created_on')
    serializer_class = QuoteSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class TimelineViewSet(viewsets.ModelViewSet):
    queryset = Timeline.objects.all().order_by('-created_on')
    serializer_class = TimelineSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class PageFilter(filters.FilterSet):
    class Meta:
        model = Page
        fields = ['slug', 'id', 'status', 'visibility']


# Page Api endpoint
class PageList(generics.ListAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PageFilter

    # authentication_classes = [SessionAuthentication, BaseAuthentication, JSONWebTokenAuthentication]


class PageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class PageCreate(generics.CreateAPIView):
    serializer_class = PageCreateSerializer


@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        page = Page.objects.get(slug=slug)
    except Page.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PageSerializer(page)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PageSerializer(page, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        page.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# Resource Api endpoint
# class ResourceList(generics.ListCreateAPIView):
#     queryset = Resource.objects.all()
#     serializer_class = ResourceSerializer
#     authentication_classes = [JSONWebTokenAuthentication]
#     permission_classes = [IsAuthenticated]


class ResourceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class ResourceCreate(generics.CreateAPIView):
    serializer_class = ResourceCreateSerializer


# Category Api endpoint
class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CategoryCreate(generics.CreateAPIView):
    serializer_class = CategoryCreateSerializer


# Post Tag Api endpoint
class TagList(generics.ListCreateAPIView):
    queryset = Tags.objects.all()
    serializer_class = TagSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TagDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tags.objects.all()
    serializer_class = TagSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TagCreate(generics.CreateAPIView):
    serializer_class = TagCreateSerializer


# Email Api endpoint
class EmailList(generics.ListCreateAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class EmailDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class EmailCreate(generics.CreateAPIView):
    serializer_class = EmailCreateSerializer


# Dashboard Api endpoint
class DashboardList(generics.ListCreateAPIView):
    queryset = Dashboard.objects.all()
    serializer_class = DashboardSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class DashboardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Dashboard.objects.all()
    serializer_class = DashboardSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class DashboardCreate(generics.CreateAPIView):
    serializer_class = DashboardCreateSerializer


# Specials Api endpoint
class SpecialsList(generics.ListCreateAPIView):
    queryset = Specials.objects.all()
    serializer_class = SpecialsSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class SpecialsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Specials.objects.all()
    serializer_class = SpecialsSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class SpecialsCreate(generics.CreateAPIView):
    serializer_class = SpecialsCreateSerializer


# Testimonial Api endpoint
class TestimonialList(generics.ListCreateAPIView):
    queryset = Testimonial.objects.all()
    serializer_class = TestimonialSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TestimonialDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Testimonial.objects.all()
    serializer_class = TestimonialSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TestimonialCreate(generics.CreateAPIView):
    serializer_class = TestimonialCreateSerializer



# Quote Api endpoint
class QuoteList(generics.ListCreateAPIView):
    queryset = Quote.objects.all()
    serializer_class = QuoteSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class QuoteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Quote.objects.all()
    serializer_class = QuoteSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class QuoteCreate(generics.CreateAPIView):
    serializer_class = QuoteCreateSerializer


# Timeline Api endpoint
class TimelineList(generics.ListCreateAPIView):
    queryset = Timeline.objects.all()
    serializer_class = TimelineSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TimelineDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Timeline.objects.all()
    serializer_class = TimelineSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class TimelineCreate(generics.CreateAPIView):
    serializer_class = TimelineCreateSerializer


class PageCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = PageCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="page")
    page_title = _("Page Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(PageCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PageCreateView, self).dispatch(*args, **kwargs)


class PageUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = PageEditForm
    model = Page
    #ct = ContentType.objects.get(app_label="cmf", model="page")
    page_title = _("Page Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(PageUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PageUpdateView, self).dispatch(*args, **kwargs)


class PageDetailView(DetailView):
    model = Page
    #ct = ContentType.objects.get(app_label="cmf", model="page")
    template_name = "cmf/_page_detail.html"


class PageListView(ListView):
    model = Page
    #ct = ContentType.objects.get(app_label="cmf", model="page")
    page_title = _("Page List"),
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(PageListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs

    # def get_context_data(self, **kwargs):
    #     context = super(PageListView, self).get_context_data(**kwargs)
    #     all_list = super(PageListView, self).get_queryset(**kwargs).order_by("-publish_start").filter(status='p', active=True)
    #     paginator = Paginator(all_list, self.paginate_by)
    #
    #     page = self.request.GET.get('page')
    #
    #     try:
    #         all_list = paginator.page(page)
    #     except PageNotAnInteger:
    #         all_list = paginator.page(1)
    #     except EmptyPage:
    #         all_list = paginator.page(paginator.num_pages)
    #
    #     context['all_list'] = all_list


class ResourceCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = ResourceCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="resource")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(ResourceCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ResourceCreateView, self).dispatch(*args, **kwargs)


class ResourceUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = ResourceEditForm
    model = Resource
    #ct = ContentType.objects.get(app_label="cmf", model="resource")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(ResourceUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ResourceUpdateView, self).dispatch(*args, **kwargs)


class ResourceDetailView(DetailView):
    model = Resource
    #ct = ContentType.objects.get(app_label="cmf", model="resource")
    template_name = "cmf/_resource_detail.html"


class ResourceListView(ListView):
    model = Resource
    #ct = ContentType.objects.get(app_label="cmf", model="resource")
    template_name = "cmf/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(ResourceListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs

    # def get_context_data(self, **kwargs):
    #     context = super(ResourceListView, self).get_context_data(**kwargs)
    #     resource_list = super(ResourceListView, self).get_queryset(**kwargs).order_by("-publish_start").filter(status='p', active=True)
    #     paginator = Paginator(resource_list, self.paginate_by)
    #
    #     page = self.request.GET.get('page')
    #
    #     try:
    #         resource_list = paginator.page(page)
    #     except PageNotAnInteger:
    #         resource_list = paginator.page(1)
    #     except EmptyPage:
    #         resource_list = paginator.page(paginator.num_pages)
    #
    #     context['resource_list'] = resource_list


class EmailCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = EmailCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="email")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(EmailCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmailCreateView, self).dispatch(*args, **kwargs)


class EmailUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = EmailEditForm
    model = Email
    #ct = ContentType.objects.get(app_label="cmf", model="email")

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmailUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(EmailUpdateView, self).form_valid(form)


class EmailDetailView(DetailView):
    model = Email
    #ct = ContentType.objects.get(app_label="cmf", model="email")
    template_name = "cmf/_email_detail.html"


class EmailListView(ListView):
    model = Email
    #ct = ContentType.objects.get(app_label="cmf", model="email")
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(EmailListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class DashboardCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = DashboardCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="dashboard")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(DashboardCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DashboardCreateView, self).dispatch(*args, **kwargs)


class DashboardUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = DashboardEditForm
    model = Dashboard
    #ct = ContentType.objects.get(app_label="cmf", model="dashboard")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(DashboardUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DashboardUpdateView, self).dispatch(*args, **kwargs)


class DashboardDetailView(DetailView):
    model = Dashboard
    #ct = ContentType.objects.get(app_label="cmf", model="dashboard")
    template_name = "cmf/_dashboard_detail.html"


class DashboardListView(ListView):
    model = Dashboard
    #ct = ContentType.objects.get(app_label="cmf", model="dashboard")
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(DashboardListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class SpecialsCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = SpecialsCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="specials")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(SpecialsCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SpecialsCreateView, self).dispatch(*args, **kwargs)


class SpecialsUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = SpecialsEditForm
    model = Specials
    #ct = ContentType.objects.get(app_label="cmf", model="specials")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(SpecialsUpdateView, self).form_valid(form)


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SpecialsUpdateView, self).dispatch(*args, **kwargs)


class SpecialsDetailView(DetailView):
    model = Specials
    #ct = ContentType.objects.get(app_label="cmf", model="specials")
    template_name = "cmf/_specials_detail.html"


class SpecialsListView(ListView):
    model = Specials
    #ct = ContentType.objects.get(app_label="cmf", model="specials")
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(SpecialsListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class IncentivesCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = IncentivesCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="incentives")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(IncentivesCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IncentivesCreateView, self).dispatch(*args, **kwargs)


class IncentivesUpdateView(UpdateView):
    template_name = "themes/edit.html"
    #ct = ContentType.objects.get(app_label="cmf", model="incentives")
    form_class = IncentivesEditForm
    model = Incentives

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(IncentivesUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IncentivesUpdateView, self).dispatch(*args, **kwargs)


class IncentivesDetailView(DetailView):
    model = Incentives
    #ct = ContentType.objects.get(app_label="cmf", model="incentives")
    template_name = "cmf/_incentives_detail.html"


class IncentivesListView(ListView):
    model = Incentives
    #ct = ContentType.objects.get(app_label="cmf", model="incentives")
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(IncentivesListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class TagCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = TagCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="tags")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TagCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TagCreateView, self).dispatch(*args, **kwargs)


class TagUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = TagEditForm
    model = Tags
    #ct = ContentType.objects.get(app_label="cmf", model="tags")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(TagUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TagUpdateView, self).dispatch(*args, **kwargs)


class TagDetailView(DetailView):
    model = Tags
    template_name = "cmf/_tag_detail.html"
    #ct = ContentType.objects.get(app_label="cmf", model="tags")


class TagListView(ListView):
    model = Tags
    #ct = ContentType.objects.get(app_label="cmf", model="tags")
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(TagListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class CategoryCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = CategoryCreateForm
    #ct = ContentType.objects.get(app_label="cmf", model="category")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CategoryCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryCreateView, self).dispatch(*args, **kwargs)


class CategoryUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = CategoryEditForm
    model = Category
    #ct = ContentType.objects.get(app_label="cmf", model="category")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(CategoryUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CategoryUpdateView, self).dispatch(*args, **kwargs)


class CategoryDetailView(DetailView):
    model = Category
    #ct = ContentType.objects.get(app_label="cmf", model="category")
    template_name = "cmf/_category_detail.html"


class CategoryListView(ListView):
    model = Category
    #ct = ContentType.objects.get(app_label="cmf", model="category")
    template_name = "themes/list2.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(CategoryListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class TestimonialCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = TestimonialCreateForm
    page_title = _("Testimonial Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TestimonialCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TestimonialCreateView, self).dispatch(*args, **kwargs)


class TestimonialUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = TestimonialEditForm
    model = Testimonial
    page_title = _("Testimonial Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(TestimonialUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TestimonialUpdateView, self).dispatch(*args, **kwargs)


class TestimonialDetailView(DetailView):
    model = Testimonial
    template_name = "cmf/_testimonial_detail.html"


class TestimonialListView(ListView):
    model = Testimonial
    page_title = _("Testimonial List"),
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(TestimonialListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class QuoteCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = QuoteCreateForm
    page_title = _("Quote Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(QuoteCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuoteCreateView, self).dispatch(*args, **kwargs)


class QuoteUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = QuoteEditForm
    model = Quote
    page_title = _("Quote Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(QuoteUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(QuoteUpdateView, self).dispatch(*args, **kwargs)


class QuoteDetailView(DetailView):
    model = Quote
    template_name = "cmf/_quote_detail.html"


class QuoteListView(ListView):
    model = Quote
    page_title = _("Quote List"),
    template_name = "themes/list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(QuoteListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class TimelineCreateView(CreateView):
    template_name = "themes/create_form.html"
    form_class = TimelineCreateForm
    page_title = _("Timeline Create"),

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TimelineCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TimelineCreateView, self).dispatch(*args, **kwargs)


class TimelineUpdateView(UpdateView):
    template_name = "themes/edit.html"
    form_class = TimelineEditForm
    model = Timeline
    page_title = _("Timeline Edit"),

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(TimelineUpdateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TimelineUpdateView, self).dispatch(*args, **kwargs)


class TimelineDetailView(DetailView):
    model = Timeline
    template_name = "cmf/_timeline_detail.html"


class TimelineListView(ListView):
    model = Timeline
    page_title = _("Timeline List"),
    template_name = "cmf/timeline_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(TimelineListView, self).get_queryset(*args, **kwargs).order_by("-created_on")
        return qs


class OurHeritageListView(ListView):
    model = Timeline
    page_title = _("Our Heritage"),
    template_name = "cmf/our_heritage.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(OurHeritageListView, self).get_queryset(*args, **kwargs).order_by("date")
        return qs


# PWS Routes

# class PWSOurHeritageListView(ListView):
#     model = Timeline
#     page_title = _("Our Heritage"),
#     template_name = "cmf/our_heritage.html"
#
#     def get_queryset(self, *args, **kwargs):
#         qs = super(PWSOurHeritageListView, self).get_queryset(*args, **kwargs).order_by("date")
#         return qs






