from django.conf import settings
# from featured.models import *
from import_export import resources
from model_utils import Choices, FieldTracker
from model_utils.fields import MonitorField, StatusField
from model_utils.managers import InheritanceManager, QueryManager, QueryManagerMixin, InheritanceQuerySet, InheritanceManagerMixin, InheritanceQuerySetMixin
from model_utils.models import StatusModel, TimeFramedModel, TimeStampedModel
from model_utils.fields import MonitorField, StatusField, SplitField, SplitText, SplitDescriptor
from filebrowser.fields import FileBrowseField
from filer.fields.image import FilerImageField
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.text import slugify
from datetime import datetime, timedelta
from django.conf import settings
from colorfield.fields import ColorField
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from core.models import *
from theme.models import *
from menu_manager.models import *
# from media_manager.models import GalleryImageInline
from helpers.utils import *



class Category(models.Model):
    # content_type = get_content_type
    title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Title"),
        help_text=_("Title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from category title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the category."),
        default=None
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Category Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="category_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="category_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("category_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Page(models.Model):
    content_type = get_content_type
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Page title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("More information about the page."),
        default=None
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="page_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="page_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="page_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="page_img"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="page_social"
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="page_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="page_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('page_detail', kwargs={'slug': self.slug, 'category': self.category.slug })

    @property
    def content_type(self):
        content_type = ContentType.objects.get_for_model(self)
        return content_type


    # def get_absolute_url(self):
    #     category = Category.objects.slug()
    #     return reverse('page_detail', kwargs={'slug': self.slug, 'category': category })
        # return reverse("page_detail", kwargs={'slug': self.slug})

    # def get_absolute_url(self):
    #     return reverse("page_detail", kwargs={'slug': self.slug})


    # def get_absolute_url(self):
    #     # category = self.category.slug()
    #     return reverse("page_detail", kwargs={'slug': self.slug, 'category': category})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')


class Resource(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Resource title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Description"),
        help_text=_("More information about the page."),
        default=None
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="resource_menu_link"
    )
    category = models.ForeignKey(
        "Category",
        blank=False,
        null=False,
        default=None,
        related_name="resource_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        related_name="resource_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="resource_img",
        default=None
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="private",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables page to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="resources_social"
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        default=None,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="resource_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="resource_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('resource_detail', kwargs={'slug': self.slug, 'category': self.category.slug })

    # def get_absolute_url(self):
    #     category =Category.slug
    #     return reverse('resource_detail', kwargs={'slug': self.slug, 'category': category })

    # def get_absolute_url(self):
    #     return reverse("resource_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Resource')
        verbose_name_plural = _('Resources')


class Email(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Email title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    subject = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Subject"),
        help_text=_("Subject"),
    )
    salutation = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Salutation"),
        help_text=_("Salutation"),
    )
    body_html = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Body Html"),
        help_text=_("Body html content."),
        default=None
    )
    body_text = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Body Text Only"),
        help_text=_("Body text content"),
        default=None
    )
    footer = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Footer"),
        help_text=_("Footer content."),
        default=None
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="email_theme"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="email_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="email_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="email_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("email_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Email')
        verbose_name_plural = _('Emails')


class Dashboard(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Dashboard title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    month = models.CharField(
        max_length=120,
        blank=False,
        null=False,
        choices=months,
    )
    batch = models.CharField(
        max_length=100,
        default="batch1",
        choices=BATCH
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("Content"),
        default=None
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="dashboard_theme"
    )
    # carousel = models.ForeignKey(
    #     Carousel,
    #     blank=True,
    #     null=True,
    #     related_name="dashboard_carousel"
    # )
    specials = models.ForeignKey(
        "Specials",
        blank=True,
        null=True,
        related_name="specials_category"
    )
    incentives = models.ForeignKey(
        "Incentives",
        blank=True,
        null=True,
        related_name="incentives_category"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="dashboard_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="dashboard_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("dashboard_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Dashboard')
        verbose_name_plural = _('Dashboards')


class Specials(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Specials title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    type = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        default=None,
        choices=SPECIALS_TYPE,
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("More information about the specials."),
        default=None
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="specials_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="specials_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="specials_theme"
    )
    img = models.ManyToManyField(
        "SpecialsImageInline",
        blank=True,
        verbose_name=_('Images'),
        related_name="specials_image"
    )
    cta_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Cta Image',
        related_name="sp_cta_image",
        help_text=_("CTA Image to be displayed on Specials and Sales page")
    )
    xtra_cta_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Extra CTA Image',
        related_name="sp_xtra_cta_image",
        help_text=_("CTA Image to be displayed on Specials and Sales page")
    )
    download_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Download Image',
        related_name="sp_download_image",
        help_text=_("Image to be displayed on Specials and Sales download page")
    )
    download_img_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Specials Caption"),
        help_text=_("Label for download image. Example, 'Current Specials' "),
        unique=False
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    current = models.BooleanField(
        default=False,
        verbose_name=_("Current Specials"),
        help_text=_("Current month's special")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables specials to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="specials_social"
    )
    download = models.CharField(
        max_length=120,
        verbose_name=_('Download'),
        help_text=_("Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    download_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Download Link Label"),
        help_text=_("Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    link = models.CharField(
        max_length=120,
        verbose_name=_('Link'),
        help_text=_("Link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    link_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
        related_name="specials_og_img"
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="specials_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="specials_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("specials_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Specials')
        verbose_name_plural = _('Specials')


class Incentives(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Incentives title"),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    type = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        default=None,
        choices=INCENTIVES_TYPE,
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("More information about the incentives."),
        default=None
    )
    menu_link = models.ForeignKey(
        MenuLink,
        blank=True,
        null=True,
        verbose_name=_("Menu Link"),
        related_name="incentives_menu_link"
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="incentives_category"
    )
    theme = models.ForeignKey(
        BaseTheme,
        blank=True,
        null=True,
        verbose_name="Theme",
        related_name="incentives_theme"
    )
    img = models.ManyToManyField(
        "IncentivesImageInline",
        blank=True,
        related_name="incentives_image"
    )
    cta_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Cta Image',
        related_name="in_cta_image",
        help_text=_("CTA Image to be displayed on Specials and Sales page")
    )
    xtra_cta_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Extra CTA Image',
        related_name="in_xtra_cta_image",
        help_text=_("CTA Image to be displayed on Specials and Sales page")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    current = models.BooleanField(
        default=False,
        verbose_name=_("Current Incentives"),
        help_text=_("Current month's incentive")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables specials to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    social = models.ManyToManyField(
        SocialMedia,
        blank=True,
        verbose_name=_("Share"),
        related_name="incentives_social"
    )
    download = models.CharField(
        max_length=120,
        verbose_name=_('Download'),
        help_text=_("Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    download_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Download Link Label"),
        help_text=_("Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    link = models.CharField(
        max_length=120,
        verbose_name=_('Link'),
        help_text=_("Link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    link_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    seo_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("SEO Title"),
        help_text=_("SEO title"),
        unique=False
    )
    seo_keywords = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Keywords"),
        help_text=_("SEO keywords for page."),
        default=None
    )
    seo_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("SEO Description"),
        help_text=_("SEO description for page."),
        default=None
    )
    seo_og_img = FilerImageField(
        null=True,
        blank=True,
        verbose_name=_("SEO Open Graph Image"),
        help_text=_('Image displayed when content is shared on social media'),
        related_name="incentives_og_img"
    )
    seo_generator = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Generator"),
        help_text=_("SEO Generator"),
    )
    seo_copyright = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Copyright"),
        help_text=_("SEO Copyright"),
    )
    seo_canonical_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Canonical Url"),
        help_text=_("SEO Canonical Url"),
    )
    seo_short_url = models.URLField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Shortlink Url"),
        help_text=_("SEO Short link url"),
    )
    seo_publisher_url = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Publisher Url"),
        help_text=_("SEO Publisher Url"),
    )
    seo_robot = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=seo_robot,
        verbose_name=_("Search Engines & Robots"),
        help_text=_("Set how search engines related with content.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="incentives_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="incentives_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("incentives_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Incentives')
        verbose_name_plural = _('Incentives')


class Tags(models.Model):
    title = models.CharField(
        max_length=100,
        verbose_name=_('Name')
    )
    slug = models.SlugField(
        unique=True,
        default=None,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from name field if left empty.')
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="tag_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="tag_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("tag_detail", kwargs={'slug': self.slug})

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')


class IncentivesImageInline(models.Model):
    incentives = models.ForeignKey(
        Incentives,
        blank=False,
        verbose_name="Incentives",
        help_text="Select a content related to this image",
        default=None
    )
    caption = models.CharField(
        max_length=255,
        verbose_name='Caption',
        help_text="Image caption"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        related_name="incentives_inline_image",
        help_text=_("Image that will be displayed on Dashboard as thumbnail and on download link")
    )
    related_file = FilerFileField(
        null=True,
        blank=True,
        related_name="incentives_inline_related_file",
        help_text=_("PDF file that will be displayed on Dashboard for download"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )

    def __str__(self):
        return str(self.caption)

    class Meta:
        verbose_name = 'Incentives Image Inline'
        verbose_name_plural = 'Incentives Image Inline'


class SpecialsImageInline(models.Model):
    specials = models.ForeignKey(
        Specials,
        blank=False,
        verbose_name="Specials",
        help_text="Select a content related to this image",
        default=None
    )
    caption = models.CharField(
        max_length=255,
        verbose_name='Caption',
        help_text="Image caption"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Image',
        related_name="specials_inline_image",
        help_text=_("Image that will be displayed on Dashboard as thumbnail and on download link")
    )
    related_file = FilerFileField(
        null=True,
        blank=True,
        related_name="specials_inline_related_file",
        help_text=_("PDF file that will be displayed on Dashboard for download"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="public",
        choices=visibility,
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )

    def __str__(self):
        return str(self.caption)

    class Meta:
        verbose_name = 'Specials Image Inline'
        verbose_name_plural = 'Specials Image Inline'


class Testimonial(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Testimonial title"),
        help_text=_("Caption for the testimonial."),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Testimonial"),
        help_text=_("Content of the testimonial."),
        default=None
    )
    author = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        verbose_name=_("Author"),
        help_text=_("The author of the testimonial"),
        unique=False
    )
    location = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("Location"),
        help_text=_("The author's location"),
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="testimonial_category"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Featured Image',
        related_name="testimonial_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables testimonial to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="testimonial_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="testimonial_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('testimonial_detail', kwargs={'slug': self.slug })

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Testimonial')
        verbose_name_plural = _('Testimonials')


class Quote(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Quote title"),
        help_text=_("Caption for the quote."),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Quote"),
        help_text=_("Quote content."),
        default=None
    )
    author = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        verbose_name=_("Author"),
        help_text=_("The author of the quote"),
        unique=False
    )
    role = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("Role"),
        help_text=_("The author's relationship with Norwex"),
        unique=False
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="quote_category"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Featured Image',
        related_name="quote_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables quote to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="quote_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="quote_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('quote_detail', kwargs={'slug': self.slug })

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Quote')
        verbose_name_plural = _('Quotes')


class Timeline(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Timeline title"),
        help_text=_("Caption for the timeline content."),
        unique=False
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("More information about the timeline."),
        default=None
    )
    date = models.DateField(
        verbose_name=_("Date"),
        help_text=_("Date of event on timeline."),
        blank=False,
        null=False,
        default=None,
    )
    category = models.ForeignKey(
        Category,
        blank=True,
        null=True,
        default=None,
        related_name="timeline_category"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Featured Image',
        related_name="timeline_image"
    )
    ximg = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Extra Image',
        related_name="xtra_timeline_image"
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables timeline to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="timeline_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="timeline_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('timeline_detail', kwargs={'slug': self.slug })

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Timeline')
        verbose_name_plural = _('Timelines')


class Snippet(models.Model):
    title = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_("Title"),
        help_text=_("Snippet title."),
        unique=False
    )
    label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("Label"),
        help_text=_("Snippet label."),
        unique=False
    )
    url = models.SlugField(
        unique=False,
        default=None,
        verbose_name=_('Url'),
        help_text=_('Automatically generated from title if left empty.')
    )
    slug = models.SlugField(
        unique=True,
        verbose_name=_('Slug'),
        help_text=_('Automatically generated from title if left empty.')
    )
    section = models.CharField(
        max_length=100,
        default="corporate",
        choices=SECTION,
        verbose_name=_('Section'),
    )
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Content"),
        help_text=_("Snippet content."),
        default=None
    )
    content_type = models.ForeignKey(
        ContentType,
        max_length=120,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("Content Type"),
        help_text=_("Select related content type"),
        unique=False
    )
    category = models.ForeignKey(
        Category,
        blank=False,
        null=False,
        default=None,
        related_name="%(class)s_category"
    )
    img = FilerImageField(
        null=True,
        blank=True,
        verbose_name='Featured Image',
        related_name="%(class)s_image"
    )
    download = FilerFileField(
        null=True,
        blank=True,
        related_name="snippet_download",
        help_text=_("File for download"),
    )
    sort_order = models.PositiveIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Sort Order"),
        help_text=_("Content Sort Order."),
        default=0
    )
    status = models.CharField(
        max_length=1,
        default="p",
        choices=STATUS_CHOICES,
    )
    publish_start = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        default=datetime.now,
        blank=True,
        null=True,
        verbose_name=_("Publish Start Date"),
    )
    publish_end = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
        verbose_name=_("Publish End Date"),
    )
    visibility = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        choices=visibility,
        default="public",
        verbose_name=_("Visibility"),
        help_text=_("Set visibility of content to authenticated and anonymous users.")
    )
    active = models.BooleanField(
        default=True,
        verbose_name=_("Enabled"),
        help_text=_("Publication Status.")
    )
    featured = models.BooleanField(
        default=False,
        verbose_name=_("Featured"),
        help_text=_("Enables snippet to be featured.")
    )
    progress_status = models.CharField(
        max_length=120,
        null=False,
        blank=False,
        choices=progress_status,
        verbose_name=_("Translation Status"),
        default="NOTSTARTED",
        help_text=_("Select progress status level")
    )
    lock_locale = models.BooleanField(
        default=False,
        verbose_name=_("Locked Locale"),
        help_text=_("Use to lock locale.")
    )
    link = models.CharField(
        max_length=120,
        verbose_name=_('Link'),
        help_text=_("Link to CTA resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    link_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    link_color = ColorField(
        default='#71c059',
        verbose_name=_("Link Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    xlink = models.CharField(
        max_length=120,
        verbose_name=_('Extra Link'),
        help_text=_("Link to CTA resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'"),
        default=None,
        blank=True,
        null=True
    )
    xlink_label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Extra Link Label"),
        help_text=_("Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field."),
        unique=False
    )
    xlink_color = ColorField(
        default='#71c059',
        verbose_name=_("Extra Link Color"),
        help_text=_("Hexcode color."),
        blank=True,
        null=True,
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="%(class)s_created_by"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="%(class)s_updated_by"
    )
    created_on = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        verbose_name=_('Created On')
    )
    updated = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Last Updated')
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('snippet_detail', kwargs={'slug': self.slug })

    class Meta:
        unique_together = ('slug', 'title')
        verbose_name = _('Snippet')
        verbose_name_plural = _('Snippets')
