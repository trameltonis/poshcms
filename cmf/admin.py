from django.db import models
from import_export import resources, fields
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django import forms
from helpers.utils import *
import nested_admin
from filer.models import Image, File
from django.utils.translation import ugettext_lazy as _
from poshcms.translation.admin import ExtendedTranslationAdmin
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationStackedInline, TranslationAdmin, TranslationBaseModelAdmin
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget, DecimalWidget, IntegerWidget, CharWidget, BooleanWidget, DateTimeWidget, TimeWidget, DateWidget
from .models import Page, Category, Tags, Resource, Email, Dashboard, Specials, Incentives, SpecialsImageInline, IncentivesImageInline, Testimonial, Quote, Timeline, Snippet


@admin.register(Page)
class PageAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                # 'ct',
                'title',
                'url',
                'content',
                'category',
                'theme',
                'menu_link',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'seo_title': ('title',),
        'seo_keywords': ('slug',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors', 'section',  'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    filter_horizontal = ('social',)
    list_filter = ['section', 'publish_start',  'publish_end',   'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Resource)
class ResourceAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'content',
                'category',
                'theme',
                'menu_link',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'seo_title': ('title',),
        'url': ('title',)
    }
    list_display = ('title', 'slug', 'language_selectors', 'section', 'status', 'publish_start',  'publish_end', 'progress_status',  'active', 'updated_by', 'updated')
    search_fields = ['title', 'slug', 'content']
    filter_horizontal = ('social',)
    list_filter = ['section', 'publish_start',  'publish_end',  'status',  'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Category)
class CategoryAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {
        'slug': ('title',),
        'url': ('title',)
    }
    list_display = ('title', 'slug', 'progress_status', 'active', 'updated')
    search_fields = ['title', 'description']
    list_filter = [ 'active']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Email)
class EmailAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'subject',
                'salutation',
                'body_html',
                'body_text',
                'footer',
                'img',
                'sort_order',
                'status',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'url',
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',)
    }
    list_display = ('title', 'slug', 'language_selectors', 'section', 'status', 'publish_start',  'publish_end', 'progress_status',  'active', 'created_by', 'updated_by', 'updated')
    search_fields = ['title', 'slug', 'body_html', 'body_text', 'footer', 'salutation']
    list_filter = ['section', 'publish_start',  'publish_end',  'status']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(SpecialsImageInline)
class SpecialsImageInlineAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'caption',
                    'img',
                    'related_file',
                    'sort_order',
                )
            }
        ),
    )

    model = SpecialsImageInline
    extra = 0


@admin.register(IncentivesImageInline)
class IncentivesImageInlineAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'caption',
                    'img',
                    'related_file',
                    'sort_order',
                )
            }
        ),
    )

    # model = IncentivesImageInline
    # extra = 1


@admin.register(Dashboard)
class DashboardAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'month',
                'batch',
                'content',
                # 'carousel',
                'specials',
                'incentives',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {'slug': ('title', 'section', 'batch'), 'url': ('title',)}
    list_display = ('title','language_selectors', 'section', 'status', 'month', 'publish_start',  'publish_end', 'progress_status',  'active', 'created_by', 'updated_by', 'updated')
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',  'status', 'created_by']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by',)
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Tags)
class TagAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = False
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'visibility', 'progress_status',)
    search_fields = ['title', 'slug']
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Specials)
class SpecialsAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()


    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'type',
                'content',
                'menu_link',
                'category',
                'current',
                'theme',
                'img',
                'cta_img',
                'xtra_cta_img',
                'download_img',
                'download_img_label',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'download',
                'download_label',
                'link',
                'link_label',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    # inlines = [SpecialsImageAdminInline,]
    filter_horizontal = ('img', 'social',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'seo_title': ('title',),
        'seo_keywords': ('slug',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors', 'publish_start',  'section', 'status', 'progress_status',  'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',  'status', 'visibility', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Incentives)
class IncentivesAdmin(nested_admin.NestedModelAdmin, ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()


    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'type',
                'content',
                'menu_link',
                'category',
                'current',
                'theme',
                'img',
                'cta_img',
                'xtra_cta_img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'download',
                'download_label',
                'link',
                'link_label',
                'social',
                'created_by',
                'updated_by'
            ),
        }),
        (_('SEO options'), {
            'classes': ('collapse',),
            'fields': (
                'seo_title',
                'seo_keywords',
                'seo_og_img',
                'seo_generator',
                'seo_copyright',
                'seo_canonical_url',
                'seo_short_url',
                'seo_publisher_url',
                'seo_robot'
            ),
        }),
    )
    # inlines = [IncentivesImageAdminInline,]
    filter_horizontal = ('img','social',)
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'seo_title': ('title',),
        'seo_keywords': ('slug',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors', 'publish_start', 'section', 'status', 'progress_status',  'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',  'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))



@admin.register(Testimonial)
class TestimonialAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'content',
                'author',
                'location',
                'category',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors', 'author', 'category', 'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content', 'author', 'location', 'category']
    list_filter = ['section', 'author', 'publish_start',  'publish_end',   'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Quote)
class QuoteAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'content',
                'author',
                'role',
                'category',
                'img',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors',  'author', 'category', 'section', 'status', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',   'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))


@admin.register(Timeline)
class TimelineAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'url',
                'content',
                'date',
                'category',
                'img',
                'ximg',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors', 'section', 'status', 'date', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end', 'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))



@admin.register(Snippet)
class SnippetAdmin(ExtendedTranslationAdmin, ImportExportModelAdmin, VersionAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }
    view_on_site = True
    group_fieldsets = True
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()

    fieldsets = (
        (None, {
            'classes': ('open',),
            'fields': (
                'section',
                'title',
                'label',
                'url',
                'content',
                'content_type',
                'category',
                'img',
                'download',
                'sort_order',
                'status',
                'visibility',
                'progress_status',
            )
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': (
                'slug',
                'publish_start',
                'publish_end',
                'active',
                'featured',
                'lock_locale',
                'link',
                'link_label',
                'xlink',
                'xlink_label',
                'created_by',
                'updated_by'
            ),
        }),
    )
    prepopulated_fields = {
        'slug': ('section', 'title',),
        'url': ('title',)
    }
    list_display = (
        'title', 'language_selectors', 'section', 'status', 'section', 'slug', 'publish_start', 'publish_end', 'progress_status', 'active', 'updated_by','sort_order', 'updated'
    )
    search_fields = ['title', 'slug', 'content']
    list_filter = ['section', 'publish_start',  'publish_end',   'status', 'featured', 'category']
    actions = [make_published, make_draft, make_archive, make_withdrawn, make_public, make_private, make_corporate, make_corp, make_pws, make_nco, make_neo]
    readonly_fields = ('created_by', 'updated_by', )
    admin.site.add_action(export_as_json, _('Export_selected_as_json'))

