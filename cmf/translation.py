from modeltranslation.translator import translator, register, TranslationOptions
from cmf.models import Page, Resource, Email, Dashboard, Tags, Category, Specials, Incentives, SpecialsImageInline, IncentivesImageInline, Testimonial, Quote, Timeline, Snippet


@register(Page)
class PageTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        'seo_title',
        # 'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]
    # required_languages = ('en-us', 'es-us', 'fr-ca', 'en-ca')


@register(Resource)
class ResourceTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        'seo_title',
        # 'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]


@register(Email)
class EmailTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'subject',
        'salutation',
        'body_html',
        'body_text',
        'img',
        'footer',
        ]


@register(Dashboard)
class DashboardTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        ]


@register(Tags)
class TagTranslationOptions(TranslationOptions):
    fields = [
        'title',
        ]

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'description',
        ]


@register(Specials)
class SpecialsTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'cta_img',
        'xtra_cta_img',
        'download',
        'download_label',
        'download_img',
        'download_img_label',
        'link',
        'link_label',
        'seo_title',
        # 'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]


@register(Incentives)
class IncentivesTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'cta_img',
        'xtra_cta_img',
        'download',
        'download_label',
        'link',
        'link_label',
        'seo_title',
        # 'seo_keywords',
        'seo_description',
        'seo_og_img',
        'seo_copyright',
        ]



@register(SpecialsImageInline)
class SpecialsImageInlineTranslationOptions(TranslationOptions):
    fields = [
        'caption',
        'img',
        'related_file',
        ]


@register(IncentivesImageInline)
class IncentivesImageInlineTranslationOptions(TranslationOptions):
    fields = [
        'caption',
        'img',
        'related_file',
        ]


@register(Testimonial)
class TestimonialTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'location',
        'img',
        ]


@register(Quote)
class QuoteTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        ]


@register(Timeline)
class TimelineTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'content',
        'img',
        'ximg',
        ]


@register(Snippet)
class SnippetTranslationOptions(TranslationOptions):
    fields = [
        'title',
        'label',
        'content',
        'img',
        'download',
        'link',
        'link_label',
        'xlink',
        'xlink_label',
        ]




