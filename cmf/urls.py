from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from .views import *


urlpatterns = [
    # url(r'^consultant/resources/$', ResourceListView.as_view(), name='resource_list'),
    # url(r'^consultant/resources/create/', ResourceCreateView.as_view(), name='resource_create'),
    # url(r'^consultant/resources/edit/(?P<slug>[\w-]+)/$', ResourceUpdateView.as_view(), name='resource_update'),
    # url(r'^consultant/resources/delete/(?P<pk>\d+)/', ResourceDeleteView.as_view(), name='resource_delete'),
    # url(r'^consultant/resources/(?P<category>[\w-]+)/(?P<slug>[\w-]+)/$', ResourceDetailView.as_view(), name='resource_detail'),
    #
    # url(r'^consultant/specials/$', SpecialsListView.as_view(), name='specials_list'),
    # url(r'^consultant/specials/create/', SpecialsCreateView.as_view(), name='specials_create'),
    # url(r'^consultant/specials/edit/(?P<slug>[\w-]+)/$', SpecialsUpdateView.as_view(), name='specials_update'),
    # url(r'^consultant/specia ls/delete/(?P<pk>\d+)/', SpecialsDeleteView.as_view(), name='specials_delete'),
    # url(r'^consultant/specials/(?P<categories>[-a-zA-Z0-9_]+)/(?P<slug>[\w-]+)/$', SpecialsDetailView.as_view(), name='specials_detail'),
    # url(r'^consultant/specials/(?P<slug>[\w-]+)/$', SpecialsDetailView.as_view(), name='specials_detail'),
    #
    # url(r'^consultant/incentives/$', IncentivesListView.as_view(), name='incentives_list'),
    # url(r'^consultant/incentives/create/', IncentivesCreateView.as_view(), name='incentives_create'),
    # url(r'^consultant/incentives/edit/(?P<slug>[\w-]+)/$', IncentivesUpdateView.as_view(), name='incentives_update'),
    # url(r'^consultant/incentives/delete/(?P<pk>\d+)/', IncentivesDeleteView.as_view(), name='incentives_delete'),
    # url(r'^consultant/incentives/(?P<categories>[-a-zA-Z0-9_]+)/(?P<slug>[\w-]+)/$', IncentivesDetailView.as_view(), name='incentives_detail'),
    # url(r'^consultant/incentives/(?P<slug>[\w-]+)/$', IncentivesDetailView.as_view(), name='incentives_detail'),
    #
    # url(r'^consultant/emails/$', EmailListView.as_view(), name='email_list'),
    # url(r'^consultant/emails/create/', EmailCreateView.as_view(), name='email_create'),
    # url(r'^consultant/emails/edit/(?P<slug>[\w-]+)/$', EmailUpdateView.as_view(), name='email_update'),
    # url(r'^consultant/emails/delete/(?P<pk>\d+)/', EmailDeleteView.as_view(), name='email_delete'),
    # url(r'^consultant/emails/(?P<slug>[\w-]+)/$', EmailDetailView.as_view(), name='email_detail'),
    #
    # url(r'^consultant/dashboards/$', DashboardListView.as_view(), name='dashboard_list'),
    # url(r'^consultant/dashboards/create/', DashboardCreateView.as_view(), name='dashboard_create'),
    # url(r'^consultant/dashboards/edit/(?P<slug>[\w-]+)/$', DashboardUpdateView.as_view(), name='dashboard_update'),
    # # url(r'^consultant/dashboards/delete/(?P<pk>\d+)/', DashboardDeleteView.as_view(), name='dashboard_delete'),
    # url(r'^consultant/dashboards/(?P<slug>[\w-]+)/$', DashboardDetailView.as_view(), name='dashboard_detail'),

    url(r'^tags/$', TagListView.as_view(), name='tag_list'),
    url(r'^tags/create/', TagCreateView.as_view(), name='tag_create'),
    url(r'^tags/edit/(?P<slug>[\w-]+)/$', TagUpdateView.as_view(), name='tag_update'),
    # url(r'^tags/delete/(?P<pk>\d+)/', TagDeleteView.as_view(), name='tag_delete'),
    url(r'^tags/(?P<slug>[\w-]+)/$', TagDetailView.as_view(), name='tag_detail'),

    url(r'^testimonials/$', TestimonialListView.as_view(), name='testimonial_list'),
    url(r'^testimonials/create/', TestimonialCreateView.as_view(), name='testimonial_create'),
    url(r'^testimonials/edit/(?P<slug>[\w-]+)/$', TestimonialUpdateView.as_view(), name='testimonial_update'),
    # url(r'^testimonials/delete/(?P<pk>\d+)/', TestimonialDeleteView.as_view(), name='testimonial_delete'),
    url(r'^testimonials/(?P<slug>[\w-]+)/$', TestimonialDetailView.as_view(), name='testimonial_detail'),

    url(r'^quotes/$', QuoteListView.as_view(), name='quote_list'),
    url(r'^quotes/create/', QuoteCreateView.as_view(), name='quote_create'),
    url(r'^quotes/edit/(?P<slug>[\w-]+)/$', QuoteUpdateView.as_view(), name='quote_update'),
    # url(r'^quotes/delete/(?P<pk>\d+)/', QuoteDeleteView.as_view(), name='quote_delete'),
    url(r'^quotes/(?P<slug>[\w-]+)/$', QuoteDetailView.as_view(), name='quote_detail'),

    url(r'^timelines/$', TimelineListView.as_view(), name='timeline_list'),
    url(r'^our-heritage/$', OurHeritageListView.as_view(), name='our_heritage_list'),
    url(r'^timelines/create/', TimelineCreateView.as_view(), name='timeline_create'),
    url(r'^timelines/edit/(?P<slug>[\w-]+)/$', TimelineUpdateView.as_view(), name='timeline_update'),
    # url(r'^norwex/timelines/delete/(?P<pk>\d+)/', TimelineDeleteView.as_view(), name='timeline_delete'),
    url(r'^timelines/(?P<slug>[\w-]+)/$', TimelineDetailView.as_view(), name='timeline_detail'),

    url(r'^categories/$', CategoryListView.as_view(), name='category_list'),
    url(r'^categories/create/', CategoryCreateView.as_view(), name='category_create'),
    url(r'^categories/edit/(?P<slug>[\w-]+)/$', CategoryUpdateView.as_view(), name='category_update'),
    # url(r'^categories/delete/(?P<pk>\d+)/', CategoryDeleteView.as_view(), name='category_delete'),
    url(r'^categories/(?P<slug>[\w-]+)/$', CategoryDetailView.as_view(), name='category_detail'),

    url(r'^pages/$', PageListView.as_view(), name='page_list'),
    url(r'^pages/create/', PageCreateView.as_view(), name='page_create'),
    url(r'^pages/edit/(?P<slug>[\w-]+)/$', PageUpdateView.as_view(), name='page_update'),
    # url(r'^pages/delete/(?P<pk>\d+)/', PageDeleteView.as_view(), name='page_delete'),
    url(r'^(?P<category>[\w-]+)/(?P<slug>[\w-]+)/$', PageDetailView.as_view(), name='page_detail'),


    # PWS Routes
    # url(r'^customer/our-heritage/$', PWSOurHeritageListView.as_view(), name='pws_our_heritage_list'),

]

urlpatterns = format_suffix_patterns(urlpatterns)

