from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CmfConfig(AppConfig):
    name = 'cmf'
    verbose_name = _("Content Manager")
