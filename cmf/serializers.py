from django.contrib.auth.models import User, Group
from rest_framework import serializers
from core.models import SocialMedia, Locale, Format, AccountGroup
from cmf.models import Page, Category, Tags, Resource, Email, Dashboard, Specials, Incentives, Testimonial, Quote, Timeline
from menu_manager.models import MenuCategory, Menu
from menu_manager.models import Menu, MenuCategory
from news.models import Post, Tags, PostView, News, Event, EventTags
from media_manager.models import File, Image, Gallery, MediaTag, MediaType
from core.serializers import LocaleSerializer, FormatSerializer, SocialMediaSerializer
from theme.models import Layout, Template
from theme.serializers import LayoutSerializer, LayoutCreateSerializer, TemplateSerializer, TemplateCreateSerializer
from menu_manager.serializers import MenuCategorySerializer, MenuCategoryCreateSerializer, MenuCreateSerializer, MenuSerializer


# CMF Serialisers
class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
        depth = 3


class TagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
        depth = 3


class PageSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Page
        fields = '__all__'
        depth = 3


class PageCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Page
        fields = '__all__'
        depth = 3


class ResourceSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
        depth = 3


class ResourceCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
        depth = 3


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
        depth = 3


class EmailCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
        depth = 3


class DashboardSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # specials = SpecialsSerializer(many=False, read_only=True)
    # incentives = IncentivesSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Dashboard
        fields = '__all__'
        depth = 3


class DashboardCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # specials = SpecialsCreateSerializer(many=False, read_only=True)
    # incentives = IncentivesCreateSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Dashboard
        fields = '__all__'
        depth = 3


class SpecialsSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Specials
        fields = '__all__'
        depth = 3


class SpecialsCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # menu = MenuSerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Specials
        fields = '__all__'
        depth = 3


class IncentivesSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Incentives
        fields = '__all__'
        depth = 3


class IncentivesCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Incentives
        fields = '__all__'
        depth = 3


class TestimonialSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Testimonial
        fields = '__all__'
        depth = 3


class TestimonialCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Testimonial
        fields = '__all__'
        depth = 3


class QuoteSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Quote
        fields = '__all__'
        depth = 3


class QuoteCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Quote
        fields = '__all__'
        depth = 3


class TimelineSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Timeline
        fields = '__all__'
        depth = 3


class TimelineCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Timeline
        fields = '__all__'
        depth = 3



