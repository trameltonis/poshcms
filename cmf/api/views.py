from rest_framework import generics
from django.views import generic as cbv
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from ..models import *
from .serializers import *


# API Filters
class PageFilter(filters.FilterSet):
    class Meta:
        model = Page
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'category',
            'menu_link',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


class ResourceFilter(filters.FilterSet):
    class Meta:
        model = Resource
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'category',
            'menu_link',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


# class CategoryFilter(filters.FilterSet):
#     class Meta:
#         model = Category
#         fields = [
#             'slug',
#             'status',
#             'active',
#             'visibility',
#             'lock_locale',
#             'progress_status',
#             'publish_start',
#             'publish_end',
#             'id'
#         ]
#
#
# class TagFilter(filters.FilterSet):
#     class Meta:
#         model = Tag
#         fields = [
#             'slug',
#             'status',
#             'visibility',
#             'id'
#         ]


class EmailFilter(filters.FilterSet):
    class Meta:
        model = Email
        fields = [
            'slug',
            'active',
            'visibility',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


class DashboardFilter(filters.FilterSet):
    class Meta:
        model = Dashboard
        fields = [
            'slug',
            'status',
            'visibility',
            'month',
            'batch',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


class SpecialsFilter(filters.FilterSet):
    class Meta:
        model = Specials
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class IncentivesFilter(filters.FilterSet):
    class Meta:
        model = Incentives
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class TestimonialFilter(filters.FilterSet):
    class Meta:
        model = Testimonial
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class QuoteFilter(filters.FilterSet):
    class Meta:
        model = Quote
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class TimelineFilter(filters.FilterSet):
    class Meta:
        model = Timeline
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class SnippetFilter(filters.FilterSet):
    class Meta:
        model = Snippet
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]



# API ViewSets
class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.filter(active=True).order_by('-created_on')
    serializer_class = PageSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PageFilter


class ResourceViewSet(viewsets.ModelViewSet):
    queryset = Resource.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = ResourceSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class EmailViewSet(viewsets.ModelViewSet):
    queryset = Email.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = EmailSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class DashboardViewSet(viewsets.ModelViewSet):
    queryset = Dashboard.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = DashboardSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class SpecialsViewSet(viewsets.ModelViewSet):
    queryset = Specials.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = SpecialsSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class IncentivesViewSet(viewsets.ModelViewSet):
    queryset = Incentives.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = IncentivesSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']

class TestimonialViewSet(viewsets.ModelViewSet):
    queryset = Testimonial.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = TestimonialSerializer
    # filter_backends = [DjangoFilterBackend]
    # filter_class = TestimonialFilter
    filter_fields = ['slug', 'status', 'visibility']


class QuoteViewSet(viewsets.ModelViewSet):
    queryset = Quote.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = QuoteSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class TimelineViewSet(viewsets.ModelViewSet):
    queryset = Timeline.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = TimelineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']


class SnippetViewSet(viewsets.ModelViewSet):
    queryset = Snippet.objects.all().order_by('-created_on').filter(active__exact=True, status__exact='p')
    serializer_class = SnippetSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'status', 'visibility']



@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        page = Specials.objects.get(slug=slug)
    except Page.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SpecialsSerializer(page)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SpecialsSerializer(page, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        page.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)






















