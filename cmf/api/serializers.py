from rest_framework import serializers
from ..models import *
from menu_manager.api.serializers import *
from theme.api.serializers import *


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
        depth = 3


class TagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
        depth = 3


class PageSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Page
        exclude = ('created_by', 'updated_by', )
        # depth = 3


class PageCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Page
        exclude = ('created_by', 'updated_by', )
        # depth = 3


class ResourceSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
        depth = 3


class ResourceCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
        depth = 3


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
        depth = 3


class EmailCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
        depth = 3


class DashboardSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # specials = SpecialsSerializer(many=False, read_only=True)
    # incentives = IncentivesSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Dashboard
        fields = '__all__'
        depth = 3


class DashboardCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # specials = SpecialsCreateSerializer(many=False, read_only=True)
    # incentives = IncentivesCreateSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Dashboard
        fields = '__all__'
        depth = 3


class SpecialsSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Specials
        fields = '__all__'
        depth = 3


class SpecialsCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # menu = MenuSerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Specials
        fields = '__all__'
        depth = 3


class IncentivesSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Incentives
        fields = '__all__'
        depth = 3


class IncentivesCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=True, read_only=True)
    # menu = MenuSerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Incentives
        fields = '__all__'
        depth = 3


class TestimonialSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Testimonial
        fields = '__all__'
        depth = 3


class TestimonialCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Testimonial
        fields = '__all__'
        depth = 3


class QuoteSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Quote
        fields = '__all__'
        depth = 3


class QuoteCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Quote
        fields = '__all__'
        depth = 3


class TimelineSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Timeline
        fields = '__all__'
        depth = 3


class TimelineCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Timeline
        fields = '__all__'
        depth = 3


class SnippetSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Snippet
        fields = '__all__'
        depth = 3


class SnippetCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Snippet
        fields = '__all__'
        depth = 3