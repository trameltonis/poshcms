from django import forms
from django.utils.translation import ugettext_lazy as _
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, Row, Field, Hidden, ButtonHolder
from crispy_forms.bootstrap import InlineField, FormActions, StrictButton, PrependedText
from .models import Page, Category, Resource, Email, Dashboard, Tags, Specials, Incentives, Testimonial, Quote, Timeline, Snippet
from menu_manager.models import Menu, MenuCategory
from menu_manager.forms import MenuCategoryCreateForm, MenuCategoryEditForm, MenuCreateForm, MenuEditForm


class PageCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Page
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'social'
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class PageEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Page
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'sort_order',
        'status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class ResourceCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Resource
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'sort_order',
        'status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class ResourceEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Resource
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'sort_order',
        'status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class EmailCreateForm(forms.ModelForm):
    body_html = forms.CharField(widget=CKEditorWidget())
    footer = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Email
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'subject',
        'salutation',
        'body_html',
        'body_text',
        'footer',
        'img',
        'sort_order',
        'status',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class EmailEditForm(forms.ModelForm):
    body_html = forms.CharField(widget=CKEditorWidget())
    footer = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Email
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'subject',
        'salutation',
        'body_html',
        'body_text',
        'footer',
        'img',
        'sort_order',
        'status',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class DashboardCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Dashboard
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'category',
        'month',
        'batch',
        'content',
        'sort_order',
        'status',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class DashboardEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Dashboard
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'category',
        'month',
        'batch',
        'content',
        'sort_order',
        'status',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class SpecialsCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Specials
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'type',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'cta_img',
        'xtra_cta_img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'current',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'download',
        'download_label',
        'link',
        'link_label',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class SpecialsEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Specials
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'type',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'cta_img',
        'xtra_cta_img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'current',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'download',
        'download_label',
        'link',
        'link_label',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class IncentivesCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Incentives
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'type',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'cta_img',
        'xtra_cta_img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'current',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'download',
        'download_label',
        'link',
        'link_label',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class IncentivesEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Incentives
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'type',
        'content',
        'theme',
        'menu_link',
        'category',
        'img',
        'cta_img',
        'xtra_cta_img',
        'sort_order',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'current',
        'active',
        'featured',
        'progress_status',
        'lock_locale',
        'download',
        'download_label',
        'link',
        'link_label',
        'seo_title',
        'seo_keywords',
        'seo_og_img',
        'seo_generator',
        'seo_copyright',
        'seo_canonical_url',
        'seo_short_url',
        'seo_publisher_url',
        'seo_robot',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class CategoryCreateForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Category
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class CategoryEditForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Category
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'description',
        'progress_status',
        'status',
        'visibility',
        'publish_start',
        'publish_end',
        'active',
        'lock_locale',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class TagCreateForm(forms.ModelForm):
    class Meta:
        model = Tags
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'visibility',
        'progress_status',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class TagEditForm(forms.ModelForm):
    class Meta:
        model = Tags
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'visibility',
        'progress_status',
        'created_by',
        'updated_by',

        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class TestimonialCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Testimonial
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'author',
        'location',
        'category',
        'img',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class TestimonialEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Testimonial
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'author',
        'location',
        'category',
        'img',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class QuoteCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Quote
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'author',
        'role',
        'category',
        'img',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class QuoteEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Quote
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'author',
        'role',
        'category',
        'img',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class TimelineCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Timeline
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'date',
        'category',
        'img',
        'ximg',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class TimelineEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Timeline
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'slug',
        'section',
        'content',
        'date',
        'category',
        'img',
        'ximg',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )


class SnippetCreateForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Snippet
        exclude = ['created_on', 'updated_on']

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'label',
        'slug',
        'section',
        'content',
        'content_type',
        'category',
        'img',
        'download',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'link',
        'link_label',
        'xlink',
        'xlink_label',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Create'), css_class='button-new-blue'))
    )


class SnippetEditForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Snippet
        exclude = ['created_on', 'updated_on']
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.layout = Layout(
        'title',
        'label',
        'slug',
        'section',
        'content',
        'content_type',
        'category',
        'img',
        'download',
        'sort_order',
        'status',
        'visibility',
        'progress_status',
        'publish_start',
        'publish_end',
        'active',
        'featured',
        'lock_locale',
        'link',
        'link_label',
        'xlink',
        'xlink_label',
        'created_by',
        'updated_by',
        FormActions(Submit(_('Create'), _('Update'), css_class='button-new-blue'))
    )



