# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-16 10:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0005_auto_20160624_0202'),
        ('cmf', '0020_auto_20160806_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='specials',
            name='download_img',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Image to be displayed on Specials and Sales download page', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sp_download_image', to='filer.Image', verbose_name='Download Image'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_en_ca',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Image to be displayed on Specials and Sales download page', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sp_download_image', to='filer.Image', verbose_name='Download Image'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_en_us',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Image to be displayed on Specials and Sales download page', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sp_download_image', to='filer.Image', verbose_name='Download Image'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_es_us',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Image to be displayed on Specials and Sales download page', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sp_download_image', to='filer.Image', verbose_name='Download Image'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_fr_ca',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Image to be displayed on Specials and Sales download page', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sp_download_image', to='filer.Image', verbose_name='Download Image'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_label',
            field=models.CharField(blank=True, help_text="Label for download image. Example, 'Current Specials' ", max_length=255, null=True, verbose_name='Specials Caption'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_label_en_ca',
            field=models.CharField(blank=True, help_text="Label for download image. Example, 'Current Specials' ", max_length=255, null=True, verbose_name='Specials Caption'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_label_en_us',
            field=models.CharField(blank=True, help_text="Label for download image. Example, 'Current Specials' ", max_length=255, null=True, verbose_name='Specials Caption'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_label_es_us',
            field=models.CharField(blank=True, help_text="Label for download image. Example, 'Current Specials' ", max_length=255, null=True, verbose_name='Specials Caption'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_img_label_fr_ca',
            field=models.CharField(blank=True, help_text="Label for download image. Example, 'Current Specials' ", max_length=255, null=True, verbose_name='Specials Caption'),
        ),
    ]
