# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-02 13:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmf', '0015_auto_20160802_0845'),
    ]

    operations = [
        migrations.AddField(
            model_name='incentives',
            name='download_en_ca',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_en_us',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_es_us',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_fr_ca',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_label_en_ca',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_label_en_us',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_label_es_us',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='download_label_fr_ca',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_en_ca',
            field=models.CharField(blank=True, default=None, help_text="Link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Link'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_en_us',
            field=models.CharField(blank=True, default=None, help_text="Link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Link'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_es_us',
            field=models.CharField(blank=True, default=None, help_text="Link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Link'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_fr_ca',
            field=models.CharField(blank=True, default=None, help_text="Link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Link'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_label_en_ca',
            field=models.CharField(blank=True, help_text="Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_label_en_us',
            field=models.CharField(blank=True, help_text="Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_label_es_us',
            field=models.CharField(blank=True, help_text="Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Link Label'),
        ),
        migrations.AddField(
            model_name='incentives',
            name='link_label_fr_ca',
            field=models.CharField(blank=True, help_text="Use as Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Link Label'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_en_ca',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_en_us',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_es_us',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_fr_ca',
            field=models.CharField(blank=True, default=None, help_text="Download link to resource. This can be an internal CMS path such as 'page_create', 'pages/create' or an external URL such as 'http://mycms.com'", max_length=120, null=True, verbose_name='Download'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_label_en_ca',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_label_en_us',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_label_es_us',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
        migrations.AddField(
            model_name='specials',
            name='download_label_fr_ca',
            field=models.CharField(blank=True, help_text="Use as Download Link Text when link is displayed on frontend. For example, http://norwex.biz/shop may have a 'Shop Now' text defined in the link label field.", max_length=255, null=True, verbose_name='Download Link Label'),
        ),
    ]
