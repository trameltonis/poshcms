from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
from api.views import UserViewSet, GroupViewSet
from news.views import NewsCreateView, NewsUpdateView, NewsListView, NewsDetailView, EventTagCreateView, EventTagUpdateView, EventTagListView, EventTagDetailView, EventCreateView, EventUpdateView, EventListView, EventDetailView, CalendarCreateView, CalendarUpdateView, CalendarDetailView, CalendarListView
from rest_framework.routers import DefaultRouter

router = DefaultRouter(
    trailing_slash=False
)


urlpatterns = [
    url(r'^pages/$', views.PageList.as_view()),
    url(r'^pages/create/$', views.PageCreate.as_view()),
    url(r'^pages/(?P<slug>\w+)/$', views.PageDetail.as_view()),
    url(r'^resources/$', views.ResourceList.as_view()),
    url(r'^resources/create/$', views.ResourceCreate.as_view()),
    url(r'^resources/(?P<slug>[-\w]+)/$', views.ResourceDetail.as_view()),
    url(r'^emails/$', views.EmailList.as_view()),
    url(r'^emails/create/$', views.EmailCreate.as_view()),
    url(r'^emails/(?P<slug>[-\w]+)/$', views.EmailDetail.as_view()),
    url(r'^dashboards/$', views.DashboardList.as_view()),
    url(r'^dashboards/create/$', views.DashboardCreate.as_view()),
    url(r'^dashboards/(?P<slug>[-\w]+)/$', views.DashboardDetail.as_view()),
    url(r'^menus/$', views.MenuList.as_view()),
    url(r'^menus/create/$', views.MenuCreate.as_view()),
    url(r'^menus/(?P<slug>[-\w]+)/$', views.MenuDetail.as_view()),
    url(r'^menus/categories/$', views.MenuCategoryList.as_view()),
    url(r'^menus/categories/create/$', views.MenuCategoryCreate.as_view()),
    url(r'^menus/categories/(?P<slug>[-\w]+)/$', views.MenuCategoryDetail.as_view()),
    url(r'^posts/$', views.PostList.as_view()),
    url(r'^posts/create/$', views.PostCreate.as_view()),
    url(r'^posts/(?P<slug>[-\w]+)/$', views.PostDetail.as_view()),
    url(r'^posts/tags/$', views.TagList.as_view()),
    url(r'^posts/tags/create/$', views.TagCreate.as_view()),
    url(r'^posts/tags/(?P<slug>[-\w]+)/$', views.TagDetail.as_view()),
    url(r'^news/$', views.NewsList.as_view()),
    url(r'^news/create/$', views.NewsCreate.as_view()),
    url(r'^news/(?P<slug>[-\w]+)/$', views.NewsDetail.as_view()),
    url(r'^categories/$', views.CategoryList.as_view()),
    url(r'^categories/(?P<slug>[-\w]+)/$', views.CategoryDetail.as_view()),
    url(r'^categories/create/$', views.CategoryCreate.as_view()),
    url(r'^tags/$', views.TagList.as_view()),
    url(r'^tags/(?P<slug>[-\w]+)/$', views.TagDetail.as_view()),
    url(r'^media/files/$', views.FileList.as_view()),
    url(r'^media/files/create/$', views.FileCreate.as_view()),
    url(r'^media/files/(?P<slug>[-\w]+)/$', views.FileDetail.as_view()),
    url(r'^media/images/$', views.ImageList.as_view()),
    url(r'^media/images/create/$', views.ImageCreate.as_view()),
    url(r'^media/images/(?P<slug>[-\w]+)/$', views.ImageDetail.as_view()),
    url(r'^media/types/$', views.MediaTypeList.as_view()),
    url(r'^media/types/create/$', views.MediaTypeCreate.as_view()),
    url(r'^media/types/(?P<slug>[-\w]+)/$', views.MediaTypeDetail.as_view()),
    url(r'^media/galleries/$', views.GalleryList.as_view()),
    url(r'^media/galleries/create/$', views.GalleryCreate.as_view()),
    url(r'^media/galleries/(?P<slug>[-\w]+)/$', views.GalleryDetail.as_view()),
    url(r'^media/tags/$', views.MediaTagList.as_view()),
    url(r'^media/tags/create/$', views.MediaTagCreate.as_view()),
    url(r'^media/tags/(?P<slug>[-\w]+)/$', views.MediaTagDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)