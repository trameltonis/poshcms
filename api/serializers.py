from django.contrib.auth.models import User, Group
from rest_framework import serializers
from filer.models import File, Image
from media_manager.models import *
from theme.models import *
from core.models import *
from cmf.models import *
from menu_manager.models import *
from news.models import *
from widget_manager.models import *
from polls.models import *
from hr.models import *
from featured.models import *
from faq.models import *


from media_manager.serializers import *
from theme.serializers import *
from core.serializers import *
from cmf.serializers import *
from menu_manager.serializers import *
from news.serializers import *
from widget_manager.serializers import *
from polls.serializers import *
from hr.serializers import *
from featured.serializers import *
from faq.serializers import *
# Theme API Serializers
class LayoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'
        depth = 3


class LayoutCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'
        depth = 3


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = '__all__'
        depth = 3


class TemplateCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Template
        fields = '__all__'
        depth = 3

class BaseThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseTheme
        fields = '__all__'
        depth = 3
class BaseThemeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseTheme
        fields = '__all__'
        depth = 3


# Core API Serializers
class AccountGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = '__all__'
        depth = 3

class AccountGroupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = '__all__'
        depth = 3

class SocialMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
        depth = 3

class SocialMediaCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMedia
        fields = '__all__'
        depth = 3


class SiteConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteConfig
        fields = '__all__'
        depth = 3

class SiteConfigCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = '__all__'
        depth = 3


# Menu Manager API Serializers
class MenuCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuCategory
        fields = '__all__'
        depth = 1


class MenuCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuCategory
        fields = '__all__'
        depth = 1


class MenuSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Menu
        fields = '__all__'
        depth = 1


class MenuCreateSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Menu
        fields = '__all__'
        depth = 1


class MenuLinkSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=True, read_only=True)

    class Meta:
        model = MenuLink
        fields = '__all__'
        depth = 3


class MenuLinkCreateSerializer(serializers.ModelSerializer):
    category = MenuCategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=True, read_only=True)
    # menu = MenuSerializer(source='menu', many=True, read_only=True)

    class Meta:
        model = MenuLink
        fields = '__all__'
        depth = 3


# CMF API Serializers
class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
        depth = 3


class TagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
        depth = 3


class PageSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Page
        fields = '__all__'
        depth = 3


class PageCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Page
        fields = '__all__'
        depth = 3


class ResourceSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
        depth = 3


class ResourceCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Resource
        fields = '__all__'
        depth = 3


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
        depth = 3


class EmailCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
        depth = 3


class DashboardSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # specials = SpecialsSerializer(many=False, read_only=True)
    # incentives = IncentivesSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Dashboard
        fields = '__all__'
        depth = 3


class DashboardCreateSerializer(serializers.ModelSerializer):
    # category = CategorySerializer(many=False, read_only=True)
    # specials = SpecialsCreateSerializer(many=False, read_only=True)
    # incentives = IncentivesCreateSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Dashboard
        fields = '__all__'
        depth = 3


class SpecialsSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Specials
        fields = '__all__'
        depth = 3


class SpecialsCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Specials
        fields = '__all__'
        depth = 3


class IncentivesSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Incentives
        fields = '__all__'
        depth = 3


class IncentivesCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Incentives
        fields = '__all__'
        depth = 3


class TestimonialSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Testimonial
        fields = '__all__'
        depth = 3


class TestimonialCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Testimonial
        fields = '__all__'
        depth = 3


class QuoteSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Quote
        fields = '__all__'
        depth = 3


class QuoteCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Quote
        fields = '__all__'
        depth = 3


class TimelineSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Timeline
        fields = '__all__'
        depth = 3


class TimelineCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)

    class Meta:
        model = Timeline
        fields = '__all__'
        depth = 3


# FAQ API Serializers
class FaqCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 3


class FaqCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 3


class FaqQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class FaqQuestionCreateSerializer(serializers.ModelSerializer):
    category = FaqCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class FaqTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = '__all__'
        depth = 3


class FaqTypeCreateSerializer(serializers.ModelSerializer):
    category = FaqCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Type
        fields = '__all__'
        depth = 3


class FaqProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
        depth = 3


class FaqProductCreateSerializer(serializers.ModelSerializer):
    category = FaqCategorySerializer(many=False, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        depth = 3


# Featured API Serializers
class CtaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cta
        fields = '__all__'
        depth = 6


class CtaCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cta
        fields = '__all__'
        depth = 6


class IconSerializer(serializers.ModelSerializer):
    class Meta:
        model = Icon
        fields = '__all__'
        depth = 3


class IconCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Icon
        fields = '__all__'
        depth = 3


class BannerSerializer(serializers.ModelSerializer):
    cta = CtaSerializer(many=True, read_only=True)
    icon = IconSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=True, read_only=True)

    class Meta:
        model = Banner
        fields = '__all__'
        depth = 3


class BannerCreateSerializer(serializers.ModelSerializer):
    cta = CtaSerializer(many=False, read_only=True)
    icon = IconSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Banner
        fields = '__all__'
        depth = 3


class ImageInlineSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = ImageInline
        fields = '__all__'
        depth = 3


class ImageInlineCreateSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = ImageInline
        fields = '__all__'
        depth = 3


class CarouselSerializer(serializers.ModelSerializer):
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Carousel
        fields = '__all__'
        depth = 3


class CarouselCreateSerializer(serializers.ModelSerializer):
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Carousel
        fields = '__all__'
        depth = 3


class CarouselImageInlineSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = CarouselImageInline
        fields = '__all__'
        depth = 3


class CarouselImageInlineCreateSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(many=False, read_only=True)

    class Meta:
        model = CarouselImageInline
        fields = '__all__'
        depth = 3


#Media Manager API Serializer
class MediaTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTag
        fields = '__all__'
        depth = 3

class MediaTagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTag
        fields = '__all__'
        depth = 3

class MediaTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaType
        fields = '__all__'
        depth = 3

class MediaTypeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaType
        fields = '__all__'
        depth = 3

class ImageSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Image
        fields = '__all__'
        depth = 3

class ImageCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Image
        fields = '__all__'
        depth = 3

class FileSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = File
        fields = '__all__'
        depth = 3

class FileCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = File
        fields = '__all__'
        depth = 3

class GallerySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Gallery
        fields = '__all__'
        depth = 3

class GalleryCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Gallery
        fields = '__all__'
        depth = 3

class GalleryImageInlineSerializer(serializers.ModelSerializer):
    # tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryImageInline
        fields = '__all__'
        depth = 3

class GalleryImageInlineCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryImageInline
        fields = '__all__'
        depth = 3

class GalleryFileInlineSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryFileInline
        fields = '__all__'
        depth = 3

class GalleryFileInlineCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = GalleryFileInline
        fields = '__all__'
        depth = 3

class DocumentGallerySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentGallery
        fields = '__all__'
        depth = 3

class DocumentGalleryCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentGallery
        fields = '__all__'
        depth = 3

class VideoSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3

class VideoCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3

class VideoGallerySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3

class VideoGalleryCreateSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = '__all__'
        depth = 3


# HR API Serializers
class HRCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class HRCategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 9


class TeamPageSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = TeamPage
        fields = '__all__'
        depth = 3


class TeamPageCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = TeamPage
        fields = '__all__'
        depth = 3


class PeopleSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = People
        fields = '__all__'
        depth = 3


class PeopleCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = People
        fields = '__all__'
        depth = 3


class DepartmentSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Department
        fields = '__all__'
        depth = 3


class DepartmentCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Department
        fields = '__all__'
        depth = 3


class RegionSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 3


class RegionCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)
    layout = LayoutSerializer(many=False, read_only=True)

    class Meta:
        model = Region
        fields = '__all__'
        depth = 3



# News API Serializers
class NewsTypeSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = NewsType
        fields = '__all__'
        depth = 3


class NewsTypeCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = NewsType
        fields = '__all__'
        depth = 3


class NewsSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    news_type = NewsTypeSerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)

    class Meta:
        model = News
        fields = '__all__'
        # exclude = ['created_by', 'updated_by']
        depth = 3


class NewsCreateSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=True)
    news_type = NewsTypeSerializer(many=False, read_only=True)
    menu = MenuSerializer(many=False, read_only=True)

    class Meta:
        model = News
        fields = '__all__'
        depth = 3


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        tags = TagSerializer(many=True)
        fields = '__all__'
        depth = 3


class PostCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        tags = TagSerializer(many=True)
        fields = '__all__'
        depth = 3


class EventTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTags
        fields = '__all__'
        depth = 3


class EventTagCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTags
        fields = '__all__'
        depth = 3


class EventSerializer(serializers.ModelSerializer):
    # calendar = CalendarSerializer(many=False, read_only=True)
    class Meta:
        model = Event
        tags = EventTagSerializer(many=True)
        fields = '__all__'
        depth = 3


class EventCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        tags = EventTagSerializer(many=True)
        fields = '__all__'
        depth = 3


class CalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        fields = '__all__'
        depth = 3


class CalendarCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        fields = '__all__'
        depth = 3


# Polls API Serializers
class PollQuestionSerializer(serializers.ModelSerializer):
    # group = PollGroupSerializer(many=False, read_only=True)

    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class PollQuestionCreateSerializer(serializers.ModelSerializer):
    # group = PollGroupSerializer(many=False, read_only=True)

    class Meta:
        model = Question
        fields = '__all__'
        depth = 3


class PollChoiceSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Choice
        fields = '__all__'
        depth = 3


class PollChoiceCreateSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Choice
        fields = '__all__'
        depth = 3


class PollGroupSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Group
        fields = '__all__'
        depth = 3


class PollGroupCreateSerializer(serializers.ModelSerializer):
    question = PollQuestionSerializer(many=False, read_only=True)

    class Meta:
        model = Group
        fields = '__all__'
        depth = 3


# Widget Manager Serializers
class SimpleWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimpleWidget
        fields = '__all__'
        depth = 3


class SimpleWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimpleWidget
        fields = '__all__'
        depth = 3


class StringWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = StringWidget
        fields = '__all__'
        depth = 3


class StringWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = StringWidget
        fields = '__all__'
        depth = 3


class MediaWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = MediaWidget
        fields = '__all__'
        depth = 3


class MediaWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = MediaWidget
        fields = '__all__'
        depth = 3


class GalleryWidgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = GalleryWidget
        fields = '__all__'
        depth = 3


class GalleryWidgetCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = GalleryWidget
        fields = '__all__'
        depth = 3




# Media API Serializers
# class ImageSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Image
#         fields = '__all__'
#         depth = 3
#
#
# class ImageCreateSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Image
#         fields = '__all__'
#         depth = 3
#
#
# class FileSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = File
#         fields = '__all__'
#         depth = 3
#
#
# class FileCreateSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = File
#         fields = '__all__'
#         depth = 3










