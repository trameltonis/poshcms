from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import User, Group
from django.views import generic as cbv
from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponse
from datetime import datetime, timedelta
# from braces.views import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, BasePermission, SAFE_METHODS, IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics, permissions, mixins
from rest_framework import viewsets, filters, fields
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from url_filter.backends.django import DjangoFilterBackend
from core.models import *
from media_manager.models import *
from filer.models import File, Image
from cmf.models import *
from menu_manager.models import *
from news.models import *
from .serializers import *
# from .serializers import *
from news.serializers import *
from cmf.serializers import *
from menu_manager.serializers import *
from core.serializers import *
from media_manager.serializers import *
from featured.models import *
from featured.serializers import *
from theme.models import *
from theme.serializers import *
from faq.models import *
from faq.serializers import *


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all().order_by('-created_on')
    serializer_class = PageSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class ResourceViewSet(viewsets.ModelViewSet):
    queryset = Resource.objects.all().order_by('-created_on')
    serializer_class = ResourceSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class EmailViewSet(viewsets.ModelViewSet):
    queryset = Email.objects.all().order_by('-created_on')
    serializer_class = EmailSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class DashboardViewSet(viewsets.ModelViewSet):
    queryset = Dashboard.objects.all().order_by('-created_on')
    serializer_class = DashboardSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class SpecialsViewSet(viewsets.ModelViewSet):
    queryset = Specials.objects.all().order_by('-created_on')
    serializer_class = SpecialsSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class IncentivesViewSet(viewsets.ModelViewSet):
    queryset = Incentives.objects.all().order_by('-created_on')
    serializer_class = IncentivesSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class LocaleViewSet(viewsets.ModelViewSet):
    queryset = Locale.objects.all().order_by('-created_on')
    serializer_class = LocaleSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class FormatViewSet(viewsets.ModelViewSet):
    queryset = Format.objects.all().order_by('-created_on')
    serializer_class = FormatSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class SocialMediaViewSet(viewsets.ModelViewSet):
    queryset = SocialMedia.objects.all().order_by('-created_on')
    serializer_class = SocialMediaSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class AccountGroupViewSet(viewsets.ModelViewSet):
    queryset = AccountGroup.objects.all().order_by('-created_on')
    serializer_class = AccountGroupSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class FileViewSet(viewsets.ModelViewSet):
    queryset = File.objects.all().order_by('-uploaded_at')
    serializer_class = FileSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['id']


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all().order_by('-uploaded_at')
    serializer_class = ImageSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['id']


class MediaTypeViewSet(viewsets.ModelViewSet):
    queryset = MediaType.objects.all().order_by('-created_on')
    serializer_class = MediaTypeSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryViewSet(viewsets.ModelViewSet):
    queryset = Gallery.objects.all().order_by('-created_on')
    serializer_class = GallerySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MediaTagViewSet(viewsets.ModelViewSet):
    queryset = MediaTag.objects.all().order_by('-created_on')
    serializer_class = MediaTagSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all().order_by('-created_on')
    serializer_class = VideoSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class VideoGalleryViewSet(viewsets.ModelViewSet):
    queryset = VideoGallery.objects.all().order_by('-created_on')
    serializer_class = VideoGallerySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class DocumentGalleryViewSet(viewsets.ModelViewSet):
    queryset = DocumentGallery.objects.all().order_by('-created_on')
    serializer_class = DocumentGallerySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryImageInlineViewSet(viewsets.ModelViewSet):
    queryset = GalleryImageInline.objects.all().order_by('-created_on')
    serializer_class = GalleryImageInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class GalleryFileInlineViewSet(viewsets.ModelViewSet):
    queryset = GalleryFileInline.objects.all().order_by('-created_on')
    serializer_class = GalleryFileInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MenuViewSet(viewsets.ModelViewSet):
    queryset = Menu.objects.all().order_by('sort_order')
    serializer_class = MenuSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MenuLinkViewSet(viewsets.ModelViewSet):
    queryset = MenuLink.objects.all().order_by('sort_order')
    serializer_class = MenuLinkSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class MenuCategoryViewSet(viewsets.ModelViewSet):
    queryset = MenuCategory.objects.all().order_by('-created_on')
    serializer_class = MenuCategorySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']
class BannerViewSet(viewsets.ModelViewSet):
    queryset = Banner.objects.all().order_by('-created_on')
    serializer_class = BannerSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CtaViewSet(viewsets.ModelViewSet):
    queryset = Cta.objects.all().order_by('-created_on')
    serializer_class = CtaSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CarouselViewSet(viewsets.ModelViewSet):
    queryset = Carousel.objects.all().order_by('-created_on')
    serializer_class = CarouselSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class IconViewSet(viewsets.ModelViewSet):
    queryset = Icon.objects.all().order_by('-created_on')
    serializer_class = IconSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']

class ImageInlineViewSet(viewsets.ModelViewSet):
    queryset = ImageInline.objects.all().order_by('-created_on')
    serializer_class = ImageInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class CarouselImageInlineViewSet(viewsets.ModelViewSet):
    queryset = CarouselImageInline.objects.all().order_by('-created_on')
    serializer_class = CarouselImageInlineSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class LayoutViewSet(viewsets.ModelViewSet):
    queryset = Layout.objects.all().order_by('-created_on')
    serializer_class = LayoutSerializer
    filter_fields = ['slug', 'id']
class BaseThemeViewSet(viewsets.ModelViewSet):
    queryset = BaseTheme.objects.all().order_by('-created_on')
    serializer_class = BaseThemeSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class TemplateViewSet(viewsets.ModelViewSet):
    queryset = Template.objects.all().order_by('-created_on')
    serializer_class = TemplateSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class FaqCategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('-created_on')
    serializer_class = FaqCategorySerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


class FaqQuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by('-created_on')
    serializer_class = FaqQuestionSerializer
    # filter_backends = [DjangoFilterBackend]
    filter_fields = ['slug', 'id']


# Page Api endpoint
class PageFilter(filters.FilterSet):
    class Meta:
        model = Page
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'category',
            'menu_link',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


# Page Api endpoint
class PageList(generics.ListAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PageFilter

    # authentication_classes = [SessionAuthentication, BaseAuthentication, JSONWebTokenAuthentication]


class PageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class PageCreate(generics.CreateAPIView):
    serializer_class = PageCreateSerializer


@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        page = Page.objects.get(slug=slug)
    except Page.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PageSerializer(page)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PageSerializer(page, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        page.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



# Resource Api endpoint
class ResourceFilter(filters.FilterSet):
    class Meta:
        model = Resource
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'category',
            'menu_link',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]


class ResourceList(generics.ListCreateAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ResourceFilter


class ResourceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ResourceFilter


class ResourceCreate(generics.CreateAPIView):
    serializer_class = ResourceCreateSerializer

@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        resource = Resource.objects.get(slug=slug)
    except Resource.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ResourceSerializer(resource)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ResourceSerializer(resource, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        resource.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# News Api endpoint
class NewsFilter(filters.FilterSet):
    class Meta:
        model = News
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'menu_link',
            # 'category',
            'news_type',
            'designation',
            'featured',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]

class NewsList(generics.ListCreateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = NewsFilter


class NewsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = NewsFilter


class NewsCreate(generics.CreateAPIView):
    serializer_class = NewsCreateSerializer


@api_view(['GET', 'PUT', 'DELETE'])
def snippet_detail(request, slug):
    """
    Retrieve, update or delete a snippet instance.
    """
    try:
        news = News.objects.get(slug=slug)
    except News.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = NewsSerializer(news)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = NewsSerializer(news, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        news.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



# Menu Api endpoint
class MenuFilter(filters.FilterSet):
    class Meta:
        model = Menu
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'category',
            'menu_location',
            'is_parent',
            'has_children',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]

class MenuList(generics.ListCreateAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MenuFilter


class MenuDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MenuFilter



class MenuCreate(generics.CreateAPIView):
    serializer_class = MenuCreateSerializer


# Menu Link Api endpoint
class MenuLinkFilter(filters.FilterSet):
    class Meta:
        model = MenuLink
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'route_type',
            'menu',
            'is_parent',
            'parent',
            'has_children',
            'show_expanded',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]
class MenuLinkList(generics.ListCreateAPIView):
    queryset = MenuLink.objects.all()
    serializer_class = MenuLinkSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MenuLinkFilter


class MenuLinkDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MenuLink.objects.all()
    serializer_class = MenuLinkSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MenuLinkFilter


class MenuLinkCreate(generics.CreateAPIView):
    serializer_class = MenuLinkCreateSerializer
# MenuCategory Api endpoint
class MenuCategoryFilter(filters.FilterSet):
    class Meta:
        model = MenuCategory
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'lock_locale',
            'progress_status',
            'id'
        ]

class MenuCategoryList(generics.ListCreateAPIView):
    queryset = MenuCategory.objects.all()
    serializer_class = MenuCategorySerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MenuCategoryFilter


class MenuCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MenuCategory.objects.all()
    serializer_class = MenuCategorySerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MenuCategoryFilter


class MenuCategoryCreate(generics.CreateAPIView):
    serializer_class = MenuCategoryCreateSerializer


# Category Api endpoint
class CategoryFilter(filters.FilterSet):
    class Meta:
        model = Category
        fields = [
            'slug',
            'status',
            'active',
            'visibility',
            'lock_locale',
            'progress_status',
            'publish_start',
            'publish_end',
            'id'
        ]

class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CategoryFilter


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CategoryFilter


class CategoryCreate(generics.CreateAPIView):
    serializer_class = CategoryCreateSerializer


# Post Api endpoint
class PostFilter(filters.FilterSet):
    class Meta:
        model = Post
        fields = [
            'slug',
            'status',
            'active',
            'featured',
            'visibility',
            'menu_link',
            'category',
            'progress_status',
            'publish_start',
            'publish_end',
            'id'
        ]

class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PostFilter


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PostFilter


class PostCreate(generics.CreateAPIView):
    serializer_class = PostCreateSerializer


# Post Tag Api endpoint
class TagFilter(filters.FilterSet):
    class Meta:
        model = Post
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]

class TagList(generics.ListCreateAPIView):
    queryset = Tags.objects.all()
    serializer_class = TagSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TagFilter


class TagDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tags.objects.all()
    serializer_class = TagSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TagFilter


class TagCreate(generics.CreateAPIView):
    serializer_class = TagCreateSerializer


# Event Tag Api endpoint
class EventTagsFilter(filters.FilterSet):
    class Meta:
        model = EventTags
        fields = [
            'slug',
            'visibility',
            'progress_status',
            'id'
        ]

class EventTagList(generics.ListCreateAPIView):
    queryset = EventTags.objects.all()
    serializer_class = EventTagSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EventTagsFilter


class EventTagDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = EventTags.objects.all()
    serializer_class = EventTagSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EventTagsFilter


class EventTagCreate(generics.CreateAPIView):
    serializer_class = EventTagCreateSerializer


# Email Api endpoint
class EmailFilter(filters.FilterSet):
    class Meta:
        model = Email
        fields = [
            'slug',
            'active',
            'visibility',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]

class EmailList(generics.ListCreateAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EmailFilter


class EmailDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EmailFilter


class EmailCreate(generics.CreateAPIView):
    serializer_class = EmailCreateSerializer


# Dashboard Api endpoint
class DashboardFilter(filters.FilterSet):
    class Meta:
        model = Dashboard
        fields = [
            'slug',
            'status',
            'visibility',
            'month',
            'batch',
            'theme',
            'lock_locale',
            'publish_start',
            'publish_end',
            'progress_status',
            'id'
        ]

class DashboardList(generics.ListCreateAPIView):
    queryset = Dashboard.objects.all()
    serializer_class = DashboardSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DashboardFilter


class DashboardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Dashboard.objects.all()
    serializer_class = DashboardSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DashboardFilter


class DashboardCreate(generics.CreateAPIView):
    serializer_class = DashboardCreateSerializer
class SpecialsFilter(filters.FilterSet):
    class Meta:
        model = Specials
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]
class SpecialsList(generics.ListAPIView):
    queryset = Specials.objects.all()
    serializer_class = SpecialsSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SpecialsFilter
class SpecialsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Specials.objects.all()
    serializer_class = SpecialsSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
class SpecialsCreate(generics.CreateAPIView):
    serializer_class = SpecialsCreateSerializer


#Media Manager Media Type Api endpoint
class MediaTypeFilter(filters.FilterSet):
    class Meta:
        model = MediaType
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]

class MediaTypeList(generics.ListCreateAPIView):
    queryset = MediaType.objects.all()
    serializer_class = MediaTypeSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MediaTypeFilter


class MediaTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MediaType.objects.all()
    serializer_class = MediaTypeSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MediaTypeFilter


class MediaTypeCreate(generics.CreateAPIView):
    serializer_class = MediaTypeCreateSerializer


#Media Manager Media Tag Api endpoint
class MediaTagFilter(filters.FilterSet):
    class Meta:
        model = MediaTag
        fields = ['slug', 'id']

class MediaTagList(generics.ListCreateAPIView):
    queryset = MediaTag.objects.all()
    serializer_class = MediaTagSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MediaTagFilter


class MediaTagDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MediaTag.objects.all()
    serializer_class = MediaTagSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MediaTagFilter


class MediaTagCreate(generics.CreateAPIView):
    serializer_class = MediaTagCreateSerializer


#Media Manager File Api endpoint
class FileFilter(filters.FilterSet):
    class Meta:
        model = File
        fields = ['id']

class FileList(generics.ListCreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = FileFilter


class FileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = FileFilter


class FileCreate(generics.CreateAPIView):
    serializer_class = FileCreateSerializer


#Media Manager Image Api endpoint
class ImageFilter(filters.FilterSet):
    class Meta:
        model = Image
        fields = ['id']

class ImageList(generics.ListCreateAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImageFilter


class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImageFilter


class ImageCreate(generics.CreateAPIView):
    serializer_class = ImageCreateSerializer


#Media Manager Gallery Api endpoint
class GalleryFilter(filters.FilterSet):
    class Meta:
        model = Gallery
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class GalleryList(generics.ListCreateAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_class = GalleryFilter


class GalleryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = GalleryFilter


class GalleryCreate(generics.CreateAPIView):
    serializer_class = GalleryCreateSerializer



#Featured Content Api endpoint
class BannerFilter(filters.FilterSet):
    class Meta:
        model = Banner
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class BannerList(generics.ListCreateAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class BannerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = BannerFilter


class BannerCreate(generics.CreateAPIView):
    serializer_class = BannerCreateSerializer


class CarouselFilter(filters.FilterSet):
    class Meta:
        model = Carousel
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class CarouselList(generics.ListCreateAPIView):
    queryset = Carousel.objects.all()
    serializer_class = CarouselSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CarouselDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Carousel.objects.all()
    serializer_class = CarouselSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CarouselFilter


class CarouselCreate(generics.CreateAPIView):
    serializer_class = CarouselCreateSerializer


class CtaFilter(filters.FilterSet):
    class Meta:
        model = Cta
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]


class CtaList(generics.ListCreateAPIView):
    queryset = Cta.objects.all()
    serializer_class = CtaSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CtaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cta.objects.all()
    serializer_class = CtaSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CtaFilter


class CtaCreate(generics.CreateAPIView):
    serializer_class = CtaCreateSerializer


class IconFilter(filters.FilterSet):
    class Meta:
        model = Icon
        fields = [
            'slug',
            'status',
            'visibility',
            'id'
        ]

class IconList(generics.ListCreateAPIView):
    queryset = Icon.objects.all()
    serializer_class = IconSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class IconDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Icon.objects.all()
    serializer_class = IconSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = IconFilter


class IconCreate(generics.CreateAPIView):
    serializer_class = IconCreateSerializer


class ImageInlineFilter(filters.FilterSet):
    class Meta:
        model = ImageInline

class ImageInlineList(generics.ListCreateAPIView):
    queryset = ImageInline.objects.all()
    serializer_class = ImageInlineSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class ImageInlineDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ImageInline.objects.all()
    serializer_class = ImageInlineSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImageInlineFilter


class ImageInlineCreate(generics.CreateAPIView):
    serializer_class = ImageInlineCreateSerializer





